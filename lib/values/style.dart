import 'dart:ui';

class AppFontFamily {
  static String pingFangRegular = "Regular";
  static String pingFangMedium = "Medium";
  static String pingFangSemibold = "Semibold";
  static String emoji = "Emoji";
  static String emojiIos = "EmojiIos";
  static String square = "Square";
}

class AppFontSize {
  /// 自定文字大小
  static double fontSize10 = 10;
  static double fontSize11 = 11;
  static double fontSize12 = 12;
  static double fontSize13 = 13;
  static double fontSize14 = 14;
  static double fontSize15 = 15;
  static double fontSize16 = 16;
  static double fontSize17 = 17;
  static double fontSize18 = 18;
  static double fontSize19 = 19;
  static double fontSize20 = 20;
  static double fontSize22 = 22;
  static double fontSize23 = 23;
  static double fontSize28 = 28;
}

class AppFontWeight {
  static FontWeight normal = FontWeight.w400;
  static FontWeight medium = FontWeight.w500;
  static FontWeight semiBold = FontWeight.w600;
  static FontWeight bold = FontWeight.w700;
}

class AppFontHeight {
  /// 自定文字行距
  static double lingHeight = 1.5;
  static double lingHeight3 = 3;
  static double lingHeight2 = 2;
}
