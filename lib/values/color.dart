import 'package:flutter/material.dart';

///
/// 颜色的透明度
/// 例如：FF191919 对应50%透明度：7F191919
/// 00%=FF（不透明）
/// 5%=F2
/// 10%=E5
/// 15%=D8
/// 20%=CC
/// 25%=BF
/// 30%=B2
/// 35%=A5
/// 40%=99
/// 45%=8c
/// 50%=7F
/// 55%=72
/// 60%=66
/// 65%=59
/// 70%=4c
/// 75%=3F
/// 80%=33
/// 85%=21
/// 90%=19
/// 95%=0c
/// 100%=00（全透明）
///
///

class LibColor {
  static Color _starColor = const Color(0xFFEFC7FF);
  static Color _color = const Color(0xFF9F89FF);

  static Color colorStarMain = _starColor; //渐变起始色
  static Color colorMain = _color;

  setColorMain(Color color, {Color? starColor}) {
    _color = color;
    if (starColor != null) {
      _starColor = starColor;
    }
  }
}

/// 深色
const Color dark191919 = Color(0xFF191919);
const Color dark1A1A1A = Color(0xFF1A1A1A);
const Color dark262626 = Color(0xFF262626);
const Color dark333333 = Color(0xFF333333);
const Color dark3D3D3D = Color(0xFF3D3D3D);
const Color dark555555 = Color(0xFF555555);
const Color dark666666 = Color(0xFF666666);
const Color dark868686 = Color(0xFF868686);
const Color dark999999 = Color(0xFF999999);
const Color darkB5B5B5 = Color(0xFFB5B5B5);
const Color dark0D0D0D = Color(0xFF0D0D0D);
const Color dark000000 = Color(0xFF000000);

/// 浅色
const Color lightD2D2D2 = Color(0xFFD2D2D2);
const Color lightE1E1E1 = Color(0xFFE1E1E1);
const Color lightEEEEEE = Color(0xFFEEEEEE);
const Color lightF1F1F1 = Color(0xFFF1F1F1);
const Color lightF8F8F8 = Color(0xFFF8F8F8);
const Color lightF9F9F9 = Color(0xFFF9F9F9);
const Color lightFFDEE3 = Color(0xFFFFDEE3);
const Color lightFFFFFF = Color(0xFFFFFFFF);

/// 主题色
const Color themeF2F0FE = Color(0xFFF2F0FE);
const Color themeECE7FF = Color(0xFFECE7FF);
const Color themeC5B8FF = Color(0xFFC5B8FF);
const Color themeC59AF3 = Color(0xFFC59AF3);
const Color theme7b7383 = Color(0xFF7b7383);
const Color theme7C7EA7 = Color(0xFF7C7EA7);
const Color theme7C5E9C = Color(0xFF7C5E9C);
const Color theme714D7D = Color(0xFF714D7D);
const Color themeE4DDFF = Color(0xFFE4DDFF);
const Color themeD8CDFF = Color(0xFFD8CDFF);
const Color themeFFB1D4 = Color(0xFFFFB1D4);
const Color themeEFC7FF = Color(0xFFEFC7FF);
const Color theme9D87FD = Color(0xFF9D87FD);
const Color theme9F89FF = Color(0xFF9F89FF);
const Color theme624EB2 = Color(0xFF624EB2);
const Color themeBBA9FF = Color(0xFFBBA9FF);
const Color theme8C77FE = Color(0xFF8C77FE);
const Color themeAE5BF1 = Color(0xFFAE5BF1);
const Color themeD178FF = Color(0xFFD178FF);

const Color theme967CFF = Color(0xFF967CFF);
const Color themeAA95FF = Color(0xFFAA95FF);
const Color theme6E3AFF = Color(0xFF6E3AFF);

/// 红色
const Color redFFB5BC = Color(0xFFFFB5BC);
const Color redFFDAD9 = Color(0xFFFFDAD9);
const Color redFF9D9B = Color(0xFFFF9D9B);
const Color redFFC4BE = Color(0xFFFFC4BE);
const Color redB81A1A = Color(0xFFB81A1A);
const Color redE02020 = Color(0xFFE02020);
const Color redF27E7E = Color(0xFFF27E7E);
const Color redFF688E = Color(0xFFFF688E);
const Color redFB8BAA = Color(0xFFFB8BAA);
const Color redFD5A71 = Color(0xFFFD5A71);
const Color redCF4D68 = Color(0xFFCF4D68);
const Color redFF7C7C = Color(0xFFFF7C7C);
const Color redFA5151 = Color(0xFFFA5151);
const Color redFF5B59 = Color(0xFFFF5B59);
const Color redFF4747 = Color(0xFFFF4747);
const Color redFF615E = Color(0xFFFF615E);
const Color redFF6675 = Color(0xFFFF6675);
const Color redFF8282 = Color(0xFFFF8282);

/// 蓝色
const Color blue467FAA = Color(0xFF467FAA);
const Color blue63B7FF = Color(0xFF63B7FF);
const Color blue71D2FF = Color(0xFF71D2FF);
const Color blue81D9FC = Color(0xFF81D9FC);
const Color blue8AB9FF = Color(0xFF8AB9FF);
const Color blue89C7FF = Color(0xFF89C7FF);
const Color blue10AEFF = Color(0xFF10AEFF);
const Color blue2D9DFF = Color(0xFF2D9DFF);
const Color blue4EABFB = Color(0xFF4EABFB);
const Color blue3935C4 = Color(0xFF3935C4);

/// 绿色
const Color greenC5F1EF = Color(0xFFC5F1EF);
const Color greenDDF6D8 = Color(0xFFDDF6D8);
const Color green00D493 = Color(0xFF00D493);
const Color green21D493 = Color(0xFF21D493);
const Color green1C874B = Color(0xFF1C874B);
const Color green268650 = Color(0xFF268650);
const Color green26D077 = Color(0xFF26D077);
const Color green00CB48 = Color(0xFF00CB48);
const Color green0ED288 = Color(0xFF0ED288);
const Color green42BE40 = Color(0xFF42BE40);
const Color green5CAD2E = Color(0xFF5CAD2E);
const Color green53E248 = Color(0xFF53E248);

/// 黄色
const Color yellowFFBA08 = Color(0xFFFFBA08);
const Color yellowFFC059 = Color(0xFFFFC059);
const Color yellowF7B500 = Color(0xFFF7B500);
const Color yellowFFDB24 = Color(0xFFFFDB24);
const Color yellowFAA651 = Color(0xFFFAA651);
const Color yellowFF8D2D = Color(0xFFFF8D2D);
const Color yellowFFB823 = Color(0xFFFFB823);
const Color yellowFFA33C = Color(0xFFFFA33C);
const Color yellowFF9850 = Color(0xFFFF9850);
const Color yellowFF8D28 = Color(0xFFFF8D28);
const Color yellowF77C00 = Color(0xFFF77C00);

/// 带透明度颜色
const Color color21000000 = Color(0x21000000);
const Color color4C000000 = Color(0x4C000000);
const Color color4CFFA33C = Color(0x4CFFA33C);
const Color color4CFD5A71 = Color(0x4CFD5A71);

/// 蒙层渐变色
const List<Color> colorsBlack = [Color(0x00000000), Color(0x33000000)];
const List<Color> colorsRedBlue = [Color(0xFF9F89FF), Color(0xFFFFD1D7)];

/// 渐变色
const Gradient gradientSilver = LinearGradient(
  colors: [Color(0xFF999999), Color(0xFFD8D8D8)],
  begin: Alignment.topCenter,
  end: Alignment.bottomCenter,
);

const Gradient gradientSystem = LinearGradient(
  colors: [Color(0xFF4C87D9), Color(0xFFD84C6F)],
  begin: Alignment.topCenter,
  end: Alignment.bottomCenter,
);

/// 通知回复项颜色列表
List<Color> noticeReplyItemColors = [
  green0ED288,
  theme8C77FE,
  themeAE5BF1,
  blue10AEFF,
  yellowFFA33C
];
