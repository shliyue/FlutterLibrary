class EventBusKeys {
  // token过期
  static const String tokenTimeout = 'token_timeout';
  static const String uploadSuccess = 'upload_progress_status_success';
  static const String uploadProgress = 'upload_progress';
  /// 原事件名写在了service中了，但基础页面中需要监听，所以这里增加两个
  /// resume
  static const String pageResume = 'c_resume';
  /// pause
  static const String pagePause = 'c_pause';

  /// LyAudioPlayer
  static const String lyAudioPlayer = 'ly_audio_player';

  /// LyAudioPlayer
  static const String lyVideoPlayer = 'ly_video_player';
}
