import 'dart:ui';

import 'package:flutter/material.dart';
import 'package:flutter_screenutil/src/size_extension.dart';
import 'package:zyocore/values/color.dart';
import 'package:zyocore/values/index.dart';

const TextStyle textBaseStyle = TextStyle(decoration: TextDecoration.none);

final TextStyle text7 =
    textBaseStyle.size(7.sp).textColor(lightFFFFFF).lineHeight.familyRegular;
final TextStyle text8 =
    textBaseStyle.size(8.sp).textColor(lightFFFFFF).lineHeight.familyRegular;
final TextStyle text9 =
    textBaseStyle.size(9.sp).textColor(lightFFFFFF).lineHeight.familyRegular;
final TextStyle text10 =
    textBaseStyle.size(10.sp).textColor(lightFFFFFF).lineHeight.familyRegular;
final TextStyle text11 =
    textBaseStyle.size(11.sp).textColor(lightFFFFFF).lineHeight.familyRegular;
final TextStyle text12 =
    textBaseStyle.size(12.sp).textColor(lightFFFFFF).lineHeight.familyRegular;
final TextStyle text13 =
    textBaseStyle.size(13.sp).textColor(lightFFFFFF).lineHeight.familyRegular;
final TextStyle text14 =
    textBaseStyle.size(14.sp).textColor(lightFFFFFF).lineHeight.familyRegular;
final TextStyle text15 =
    textBaseStyle.size(15.sp).textColor(lightFFFFFF).lineHeight.familyRegular;
final TextStyle text16 =
    textBaseStyle.size(16.sp).textColor(lightFFFFFF).lineHeight.familyRegular;
final TextStyle text17 =
    textBaseStyle.size(17.sp).textColor(lightFFFFFF).lineHeight.familyRegular;
final TextStyle text18 =
    textBaseStyle.size(18.sp).textColor(lightFFFFFF).lineHeight.familyRegular;
final TextStyle text19 =
    textBaseStyle.size(19.sp).textColor(lightFFFFFF).lineHeight.familyRegular;
final TextStyle text20 =
    textBaseStyle.size(20.sp).textColor(lightFFFFFF).lineHeight.familyRegular;
final TextStyle text21 =
    textBaseStyle.size(21.sp).textColor(lightFFFFFF).lineHeight.familyRegular;
final TextStyle text24 =
    textBaseStyle.size(24.sp).textColor(lightFFFFFF).lineHeight.familyRegular;
final TextStyle text26 =
    textBaseStyle.size(26.sp).textColor(lightFFFFFF).lineHeight.familyRegular;
final TextStyle text30 =
    textBaseStyle.size(30.sp).textColor(lightFFFFFF).familyRegular;
final TextStyle text32 =
    textBaseStyle.size(32.sp).textColor(lightFFFFFF).familyRegular;

TextStyle cfont3black17w500 =
    TextStyle(color: dark333333, fontSize: 17.sp).familyMedium;
TextStyle cfont6black17w400 =
    TextStyle(color: dark666666, fontSize: 17.sp).familyRegular;
TextStyle cfont6black14w400 =
    TextStyle(color: dark666666, fontSize: 14.sp).familyRegular;
TextStyle font14c9F89FFw400 =
    TextStyle(color: theme9F89FF, fontSize: 14.sp).familyRegular;

/// 根据字号对应的行高
const lineHeightConfig = {
  '8': 12 / 8,
  '9': 15 / 9,
  '10': 14 / 10,
  '11': 16 / 11,
  '12': 18 / 12,
  '13': 20 / 13,
  '14': 20 / 14,
  '15': 19 / 15,
  '16': 24 / 16,
  '17': 24 / 17,
  '18': 26 / 18,
  '20': 30 / 20,
  '24': 36 / 24,
  '30': 0,
  '32': 48 / 32,
};

extension TextStyleExtensions on TextStyle {
  /// Weights
  /// todo 后续去除，暂时为见人招生旧版本保留
  TextStyle get thin => weight(FontWeight.w100);

  TextStyle get extraLight => weight(FontWeight.w200);

  TextStyle get regular => weight(FontWeight.normal);

  TextStyle get medium => weight(FontWeight.w500);

  TextStyle get semiBold => weight(FontWeight.w600);

  TextStyle get bold => weight(FontWeight.w700);

  TextStyle get extraBold => weight(FontWeight.w800);

  /// 通过fontFamily处理粗细
  TextStyle get familyRegular => fontFamily(AppFontFamily.pingFangRegular);

  TextStyle get familyMedium => fontFamily(AppFontFamily.pingFangMedium);

  TextStyle get familyBold => fontFamily(AppFontFamily.pingFangSemibold);

  TextStyle get familySquare => fontFamily(AppFontFamily.square);

  /// 常用字体颜色 color（待添加）
  TextStyle get white => textColor(lightFFFFFF);

  TextStyle get red => textColor(redE02020);

  TextStyle get gray => textColor(dark999999);

  TextStyle get grape => textColor(theme9F89FF);

  TextStyle get light => textColor(dark666666);

  TextStyle get dark => textColor(dark1A1A1A);

  /// line-height
  TextStyle get lineHeight {
    return copyWith(
        height: (lineHeightConfig['${fontSize!.toInt()}'] ?? 0) * 1.0);
  }

  TextStyle get one {
    return copyWith(height: 1.w);
  }

  /// font-family
  TextStyle get impact {
    return copyWith(fontFamily: 'impact');
  }

  /// Shortcut for italic
  TextStyle get italic => style(FontStyle.italic);

  /// Shortcut for underline
  TextStyle get underline => textDecoration(TextDecoration.underline);

  /// Shortcut for linethrough
  TextStyle get lineThrough => textDecoration(TextDecoration.lineThrough);

  /// Shortcut for overline
  TextStyle get overline => textDecoration(TextDecoration.overline);

  /// Shortcut for color
  TextStyle textColor(Color? v) => copyWith(color: v);

  /// Shortcut for backgroundColor
  TextStyle textBackgroundColor(Color v) => copyWith(backgroundColor: v);

  /// Shortcut for fontSize
  TextStyle size(double v) => copyWith(fontSize: v);

  /// Scales fontSize up or down
  TextStyle scale(double v) => copyWith(fontSize: fontSize! * v);

  /// Shortcut for fontWeight
  TextStyle weight(FontWeight v) => copyWith(fontWeight: v);

  /// Shortcut for fontFamily
  TextStyle fontFamily(String v) => copyWith(fontFamily: v);

  /// Shortcut for FontStyle
  TextStyle style(FontStyle v) => copyWith(fontStyle: v);

  /// Shortcut for letterSpacing
  TextStyle letterSpace(double v) => copyWith(letterSpacing: v);

  /// Shortcut for wordSpacing
  TextStyle wordSpace(double v) => copyWith(wordSpacing: v);

  /// Shortcut for textBaseline
  TextStyle baseline(TextBaseline v) => copyWith(textBaseline: v);

  /// Shortcut for height
  TextStyle textHeight(double v) => copyWith(height: v);

  /// Shortcut for locale
  TextStyle textLocale(Locale v) => copyWith(locale: v);

  /// Shortcut for foreground
  TextStyle textForeground(Paint v) => copyWith(foreground: v);

  /// Shortcut for background
  TextStyle textBackground(Paint v) => copyWith(background: v);

  /// Shortcut for shadows
  TextStyle textShadows(List<Shadow> v) => copyWith(shadows: v);

  /// Shortcut for fontFeatures
  TextStyle textFeatures(List<FontFeature> v) => copyWith(fontFeatures: v);

  /// Shortcut for decoration
  TextStyle textDecoration(TextDecoration v,
          {Color? color, TextDecorationStyle? style, double? thickness}) =>
      copyWith(
          decoration: v,
          decorationColor: color,
          decorationStyle: style,
          decorationThickness: thickness);
}

final TextStyle appBannerText = text24.familyMedium;
