library zyocore;

export 'core/index.dart';
export 'util/index.dart';
export "widget/index.dart";
export 'extension/index.dart';
export 'http/index.dart';
export 'entitys/index.dart';

// export 'values/index.dart';
// export 'stream/event_bus.dart';

export 'package:common_utils/common_utils.dart';
export 'package:get/get.dart' hide Response;
export 'package:flutter_screenutil/flutter_screenutil.dart';
export 'package:dotted_border/dotted_border.dart';
export 'package:pull_to_refresh/pull_to_refresh.dart' hide IndicatorBuilder;
export 'package:flutter_easyloading/flutter_easyloading.dart' hide EasyLoading,FlutterEasyLoading,EasyLoadingAnimation;
export 'package:flutter_swiper_null_safety/flutter_swiper_null_safety.dart';
export 'package:qr_flutter/qr_flutter.dart';
export 'package:cached_network_image/cached_network_image.dart';
export 'package:provider/provider.dart';
export 'package:video_player/video_player.dart';
export 'package:package_info_plus/package_info_plus.dart';
export 'package:zyocore/util/app_utils.dart';
export 'package:skeletons/skeletons.dart';
export 'package:webview_flutter/webview_flutter.dart';
export 'package:webview_flutter/src/webview.dart';
export 'package:webview_flutter/platform_interface.dart';
export 'package:image_editor/image_editor.dart';
export 'package:path_provider/path_provider.dart';
export 'package:extended_image/extended_image.dart' hide MultipartFile, DoubleExtension;
export 'package:qiniu_flutter_sdk/qiniu_flutter_sdk.dart' hide Config;
export 'package:expandable_text/expandable_text.dart' hide StringCallback;
export 'package:chewie/chewie.dart';
export 'package:photo_manager/photo_manager.dart' hide NotifyManager;
export 'package:photo_manager_image_provider/photo_manager_image_provider.dart';
// export 'package:auto_orientation/auto_orientation.dart';
export 'package:camera_platform_interface/camera_platform_interface.dart';
export 'package:flutter_inappwebview/flutter_inappwebview.dart' hide CookieManager, CompressFormat, WebView, Storage, LocalStorage;
export 'package:navigation_history_observer/navigation_history_observer.dart';
export 'package:image_gallery_saver/image_gallery_saver.dart';
export 'package:dio/dio.dart' hide FormData, MultipartFile;
export 'package:zyocore/util/size_adapter_util.dart';
export 'package:majascan/majascan.dart';
export 'package:ios_orientation/ios_orientation.dart';
export 'package:easy_emoji/easy_emoji.dart';
export 'package:expandable/expandable.dart';
export 'package:zyocore/widget/path/strokeOrderAnimationController.dart';
export 'package:zyocore/widget/path/strokeOrderAnimator.dart';
export 'package:flutter_keyboard_visibility/flutter_keyboard_visibility.dart';
export 'package:device_info_plus/device_info_plus.dart';
export 'package:lottie/lottie.dart';

/// dialog暴露
export 'package:zyocore/widget/dialog/index.dart';

export 'package:weixin_kit/weixin_kit.dart';
