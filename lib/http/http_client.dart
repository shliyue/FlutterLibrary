import 'dart:io';
import 'package:dio/dio.dart';

import '../core/base/api_result.dart';
import '../util/logger.dart';
import 'app_dio.dart';
import 'default_http_transformer.dart';
import 'http_config.dart';
import 'http_exception.dart';
import 'http_response.dart';
import 'http_transformer.dart';

class ZyoCoreHttpClient {
  late final AppDio _dio;

  ZyoCoreHttpClient({BaseOptions? options, HttpConfig? dioConfig})
      : _dio = AppDio(options: options, dioConfig: dioConfig);

  Future<HttpResponse> get(String uri,
      {Map<String, dynamic>? queryParameters,
      Options? options,
      CancelToken? cancelToken,
      ProgressCallback? onReceiveProgress,
      HttpTransformer? httpTransformer}) async {
    Logger.d('GET: header == ${_dio.options.headers}');
    Logger.d('GET: uri == ${_dio.options.baseUrl}$uri');
    Logger.d('GET: queryParameters == $queryParameters');
    try {
      var response = await _dio.get(
        uri,
        queryParameters: queryParameters,
        options: options,
        cancelToken: cancelToken,
        onReceiveProgress: onReceiveProgress,
      );
      Logger.d('GET: $uri==> response == ${response.toString()}');
      return handleResponse(response, httpTransformer: httpTransformer);
    } on Exception catch (e) {
      Logger.d('GET: $uri==> Exception == ${e.toString()}');
      return handleException(e);
    }
  }

  Future<HttpResponse> post(String uri,
      {data,
      Map<String, dynamic>? queryParameters,
      Options? options,
      CancelToken? cancelToken,
      ProgressCallback? onSendProgress,
      ProgressCallback? onReceiveProgress,
      HttpTransformer? httpTransformer}) async {
    Logger.d('POST: header == ${_dio.options.headers}');
    Logger.d('POST: uri == ${_dio.options.baseUrl}$uri');
    Logger.d('POST: data == $data');
    try {
      var response = await _dio.post(
        uri,
        data: data,
        queryParameters: queryParameters,
        options: options,
        cancelToken: cancelToken,
        onSendProgress: onSendProgress,
        onReceiveProgress: onReceiveProgress,
      );
      Logger.d('POST: $uri==> response == ${response.toString()}');
      return handleResponse(response, httpTransformer: httpTransformer);
    } on Exception catch (e) {
      Logger.d('POST: $uri==> Exception == ${e.toString()}');
      return handleException(e);
    }
  }

  Future<HttpResponse> patch(String uri,
      {data,
      Map<String, dynamic>? queryParameters,
      Options? options,
      CancelToken? cancelToken,
      ProgressCallback? onSendProgress,
      ProgressCallback? onReceiveProgress,
      HttpTransformer? httpTransformer}) async {
    try {
      var response = await _dio.patch(
        uri,
        data: data,
        queryParameters: queryParameters,
        options: options,
        cancelToken: cancelToken,
        onSendProgress: onSendProgress,
        onReceiveProgress: onReceiveProgress,
      );
      return handleResponse(response, httpTransformer: httpTransformer);
    } on Exception catch (e) {
      return handleException(e);
    }
  }

  Future<HttpResponse> delete(String uri,
      {data,
      Map<String, dynamic>? queryParameters,
      Options? options,
      CancelToken? cancelToken,
      HttpTransformer? httpTransformer}) async {
    try {
      var response = await _dio.delete(
        uri,
        data: data,
        queryParameters: queryParameters,
        options: options,
        cancelToken: cancelToken,
      );
      return handleResponse(response, httpTransformer: httpTransformer);
    } on Exception catch (e) {
      return handleException(e);
    }
  }

  Future<HttpResponse> put(String uri,
      {data,
      Map<String, dynamic>? queryParameters,
      Options? options,
      CancelToken? cancelToken,
      HttpTransformer? httpTransformer}) async {
    try {
      var response = await _dio.put(
        uri,
        data: data,
        queryParameters: queryParameters,
        options: options,
        cancelToken: cancelToken,
      );
      return handleResponse(response, httpTransformer: httpTransformer);
    } on Exception catch (e) {
      return handleException(e);
    }
  }

  Future<Response> download(String urlPath, savePath,
      {ProgressCallback? onReceiveProgress,
      Map<String, dynamic>? queryParameters,
      CancelToken? cancelToken,
      bool deleteOnError = true,
      String lengthHeader = Headers.contentLengthHeader,
      data,
      Options? options,
      HttpTransformer? httpTransformer}) async {
    try {
      var response = await _dio.download(
        urlPath,
        savePath,
        onReceiveProgress: onReceiveProgress,
        queryParameters: queryParameters,
        cancelToken: cancelToken,
        deleteOnError: deleteOnError,
        lengthHeader: lengthHeader,
        data: data,
        options: data,
      );
      return response;
    } catch (e) {
      rethrow;
    }
  }

  HttpResponse handleResponse(Response? response,
      {HttpTransformer? httpTransformer}) {
    httpTransformer ??= DefaultHttpTransformer.getInstance();
    // 返回值异常
    if (response == null) {
      throw HttpResponse.failureFromError();
    }
    // token失效
    if (_isTokenTimeout(response.statusCode)) {
      ///  token过期跳转
      toLoginPage();
      throw HttpResponse.failureFromError(
          UnauthorisedException(message: "没有权限", code: response.statusCode));
    }
    // 接口调用成功
    if (_isRequestSuccess(response.statusCode)) {
      return httpTransformer.parse(response);
    } else {
      // 接口调用失败
      throw HttpResponse.failure(
          errorMsg: response.statusMessage, errorCode: response.statusCode);
    }
  }

  HttpResponse handleException(Exception exception) {
    var parseException = _parseException(exception);
    return HttpResponse.failureFromError(parseException);
  }

  /// 请求成功
  bool _isRequestSuccess(int? statusCode) {
    return (statusCode != null && statusCode == 200);
  }

  /// token处理
  bool _isTokenTimeout(int? statusCode) {
    return (statusCode != null && statusCode == 401);
  }

  HttpException _parseException(Exception error) {
    if (error is DioException) {
      switch (error.type) {
        case DioExceptionType.connectionTimeout:
          return NetworkException(message: '网络异常，请稍后再试。');
        case DioExceptionType.receiveTimeout:
          return NetworkException(message: '网络异常，请稍后再试。');
        case DioExceptionType.sendTimeout:
          return NetworkException(message: '网络异常，请稍后再试。');
        case DioExceptionType.cancel:
          return CancelException(error.message);
        case DioExceptionType.badResponse:
          try {
            int? errCode = error.response?.statusCode;
            switch (errCode) {
              case 400:
                return BadRequestException(message: "请求语法错误", code: errCode);
              case 401:
                {
                  toLoginPage();
                  return UnauthorisedException(message: "没有权限", code: errCode);
                }
              case 403:
                return BadRequestException(message: "服务器拒绝执行", code: errCode);
              case 404:
                return BadRequestException(message: "无法连接服务器", code: errCode);
              case 405:
                return BadRequestException(message: "请求方法被禁止", code: errCode);
              case 500:
                return BadServiceException(message: "服务器内部错误", code: errCode);
              case 502:
                return BadServiceException(message: "无效的请求", code: errCode);
              case 503:
                return BadServiceException(message: "服务器挂了", code: errCode);
              case 505:
                return UnauthorisedException(
                    message: "不支持HTTP协议请求", code: errCode);
              default:
                return UnauthorisedException(
                    message: error.message, code: errCode);
              // return UnknownException(error.error.message);
            }
          } on Exception catch (_) {
            return UnknownException(error.message);
          }
        case DioExceptionType.unknown:
          if (error.error is SocketException) {
            // 没网络权限
            return NetworkException(
              message: '当前网络不可用',
            );
          } else if (error.error is HandshakeException) {
            // 没网
            return NetworkException(message: '当前网络不可用');
          }
          return NetworkException(message: '当前网络不可用');
        default:
          return UnknownException(error.message);
      }
    } else {
      var errors = error as ApiResultException;
      if (error.code == 401) {
        toLoginPage();
        return UnauthorisedException(message: "没有权限", code: error.code);
      }
      return BadServiceException(
        message: errors.message,
        code: errors.code,
      ); //UnknownException(error.toString());
    }
  }

  void toLoginPage() {
    // if (!RouteUtil.contains([RootAppPages.loginPage])) {
    //   Get.offAllNamed(RootAppPages.loginPage);
    // }
  }
}
