import 'package:dio/dio.dart';
import 'package:dio/io.dart';
import 'package:dio/src/adapters/io_adapter.dart';
import 'package:flutter/foundation.dart';

import 'http_config.dart';

class AppDio with DioMixin implements Dio {
  AppDio({BaseOptions? options, HttpConfig? dioConfig}) {
    Map<String, dynamic> headers = {};
    if (dioConfig?.headers != null) {
      headers.addAll(dioConfig!.headers!);
    }
    options ??= BaseOptions(
      baseUrl: dioConfig?.baseUrl ?? "",
      contentType: 'application/json',
      connectTimeout: Duration(milliseconds: dioConfig!.connectTimeout),
      sendTimeout: Duration(milliseconds: dioConfig.sendTimeout),
      receiveTimeout: Duration(milliseconds: dioConfig.receiveTimeout),
      headers: headers,
    );

    this.options = options;

    if (kDebugMode) {
      interceptors.add(LogInterceptor(responseBody: true, error: true, requestHeader: false, responseHeader: false, request: false, requestBody: true));
    }
    if (dioConfig?.interceptors?.isNotEmpty ?? false) {
      interceptors.addAll(interceptors);
    }
    httpClientAdapter = IOHttpClientAdapter();
    if (dioConfig?.proxy?.isNotEmpty ?? false) {
      setProxy(dioConfig!.proxy!);
    }
  }

  setProxy(String proxy) {
    (httpClientAdapter as IOHttpClientAdapter).createHttpClient = (client) {
      // config the http client
      client.findProxy = (uri) {
        // proxy all request to localhost:8888
        return "PROXY $proxy";
      };
      // you can also create a HttpClient to dio
      // return HttpClient();
    } as CreateHttpClient?;
  }
}
