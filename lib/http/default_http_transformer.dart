import 'package:dio/dio.dart';
import 'package:zyocore/core/base/api_result.dart';
import 'package:zyocore/util/index.dart';
import 'package:zyocore/values/event_bus_keys.dart';

import 'http_response.dart';
import 'http_transformer.dart';

class DefaultHttpTransformer extends HttpTransformer {
  /// 假设接口返回类型
  ///   {
  ///     "success": true,
  ///     "code": 200,
  ///     "content": {},
  ///     "message": "success"
  /// }
  @override
  HttpResponse parse(Response response) {
    if (response.data["code"] == 200 || response.data["code"] == null) {
      if (response.data["success"]) {
        return HttpResponse.success(response.data["content"]);
      } else {
        throw ApiResultException(response.data["code"] ?? 200, response.data["message"]);
      }
    } else {
      if(response.data["code"] == 401){
        lyBus.emit(eventName: EventBusKeys.tokenTimeout);
      }
      throw ApiResultException(response.data["code"], response.data["message"]);
      // return HttpResponse.failure(
      //     errorMsg: response.data["message"], errorCode: response.data["code"]);
    }
  }

  /// 单例对象
  static final DefaultHttpTransformer _instance =
  DefaultHttpTransformer._internal();

  /// 内部构造方法，可避免外部暴露构造函数，进行实例化
  DefaultHttpTransformer._internal();

  /// 工厂构造方法，这里使用命名构造函数方式进行声明
  factory DefaultHttpTransformer.getInstance() => _instance;
}

