extension IntFormat on int {
  static List<String> NUMBERS = ["一", "二", "三", "四", "五", "六", "七", "八", "九", "十",""];

  // 仅支持一百以内
  String toChinese() {
    if(this > 100){
      return toString();
    }
    StringBuffer stringBuffer = StringBuffer();
    if(this / 10 < 1){
      return NUMBERS[this];
    }
    int tenUnit = (this / 10).toInt();
    int remainder = this % 10;
    if(remainder == 9){
      tenUnit++;
      remainder = 10;
    }
    if(tenUnit == 1){
      stringBuffer..write("十")..write(NUMBERS[remainder]);
      return stringBuffer.toString();
    }
    stringBuffer..write(NUMBERS[tenUnit - 1])..write("十")..write(NUMBERS[remainder]);
    return stringBuffer.toString();
  }
}
