typedef LINQCondition = bool Function(dynamic item);

typedef LINQSelector = dynamic Function(dynamic item);

typedef LINQSum = num Function(dynamic item);

typedef LINQAccumulator = dynamic Function(dynamic item, dynamic aggregate);

extension ListLinq on List {
  //筛选
  List linqWhere(LINQCondition predicate) {
    List list = [];
    for (var item in this) {
      if (predicate(item)) {
        list.add(item);
      }
    }
    return list;
  }

  //查询
  List linqSelect(LINQSelector transform) {
    List list = [];
    for (var item in this) {
      var obj = transform(item);
      if (obj != null) {
        list.add(obj);
      }
    }
    return list;
  }

  //正序
  List linqSort({
    LINQSelector? key,
  }) {
    if (key == null) {
      sort((a, b) => (a as Comparable).compareTo(b as Comparable));
    } else {
      sort((a, b) => (key(a) as Comparable).compareTo(key(b) as Comparable));
    }
    return this;
  }

  //倒序
  List linqSortDescending({
    LINQSelector? key,
  }) {
    if (key == null) {
      sort((a, b) => (b as Comparable).compareTo(a as Comparable));
    } else {
      sort((a, b) => (key(b) as Comparable).compareTo(key(a) as Comparable));
    }
    return this;
  }

  //合并子项数组
  List linqSelectMerge(LINQSelector transform) {
    List list = [];
    for (var item in this) {
      for (var child in transform(item)) {
        list.add(child);
      }
    }
    return list;
  }

  //去重
  List linqDistinct({
    LINQSelector? key,
  }) {
    List list = [];
    if (key == null) {
      for (var item in this) {
        if (!list.contains(item)) {
          list.add(item);
        }
      }
    } else {
      List keySet = [];
      for (var item in this) {
        var keyForItem = key(item);
        if (keyForItem == null) {
          continue;
        }
        if (!keySet.contains(keyForItem)) {
          keySet.add(keyForItem);
          list.add(item);
        }
      }
    }

    return list;
  }

  //聚合
  dynamic linqAggregate(LINQAccumulator accumulator) {
    dynamic aggregate;
    for (var item in this) {
      if (aggregate == null) {
        aggregate = item;
      } else {
        aggregate = accumulator(item, aggregate);
      }
    }
    return aggregate;
  }

  //第一个
  dynamic linqFirstOrNil({
    LINQCondition? predicate,
  }) {
    if (predicate == null) {
      return length == 0 ? null : this[0];
    } else {
      for (var item in this) {
        if (predicate(item)) {
          return item;
        }
      }
      return null;
    }
  }

  //最后一个
  dynamic linqLastOrNull({
    LINQCondition? predicate,
  }) {
    if (predicate == null) {
      return length == 0 ? null : this[length - 1];
    } else {
      for (var i = length - 1; i >= 0; i--) {
        if (predicate(this[i])) {
          return this[i];
        }
      }
      return null;
    }
  }

  //跳过
  List linqSkip(int count) {
    if (count < length) {
      return skip(count).toList();
    } else {
      return [];
    }
  }

  //获取
  List linqTake(int count) {
    if (count <= length) {
      return take(count).toList();
    } else {
      return [];
    }
  }

  //存在
  bool linqAny(LINQCondition condition) {
    for (var item in this) {
      if (condition(item)) {
        return true;
      }
    }
    return false;
  }

  //全部
  bool linqAll(LINQCondition condition) {
    for (var item in this) {
      if (!condition(item)) {
        return false;
      }
    }
    return true;
  }

  //分组
  Map linqGroupBy(
      LINQSelector groupKeySelector, {
        LINQSelector? value,
      }) {
    Map map = {};
    for (var item in this) {
      var key = groupKeySelector(item);
      if (key == null) {
        continue;
      }
      List arrayForKey;
      if (!map.containsKey(key)) {
        arrayForKey = [];
        map[key] = arrayForKey;
      } else {
        arrayForKey = map[key];
      }
      arrayForKey.add(value == null ? item : value(item));
    }
    return map;
  }

  //计数
  int linqCount(LINQCondition condition) {
    return linqWhere(condition).length;
  }

  //连接
  List linqConcat(Iterable array) {
    addAll(array);
    return this;
  }

  //反转
  List linqReverse() {
    return reversed.toList();
  }

  //合计
  num linqSum(LINQSum sum) {
    num ttl = 0;
    for (var item in this) {
      ttl += sum(item);
    }
    return ttl;
  }
}

extension  jso  on Object {

}
