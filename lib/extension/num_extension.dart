import 'package:flutter/cupertino.dart';
import 'package:flutter_screenutil/flutter_screenutil.dart';

extension ScreenAdaptive on num {
  ///[ScreenUtil.setWidth]
  double get sa => ScreenUtil().orientation == Orientation.portrait ? w : h;

}