import 'package:flutter/material.dart';
import 'package:zyocore/zyocore.dart';

extension LYEnumString on String {
  String getEnumDesc() {
    if (isEmpty) return '';
    if (!contains('-')) return '';
    return split('-').last;
  }

  String getEnumCode() {
    if (isEmpty) return '';
    if (!contains('-')) return '';
    return split('-').first;
  }
}

extension LYFormatString on String {
  String mobileHidden() {
    if (isEmpty) return '';
    if (!isMobile()) {
      return this;
    }
    return substring(0, 3) + "******" + substring(9, 11);
  }

  bool isMobile() {
    if (length != 11) return false;
    if (startsWith('308') || startsWith('88')) return true;
    /// 内部测试，308开头的认为是合法账号
    RegExp mobile = RegExp(
        r'^((13[0-9])|(14[0-9])|(15[0-l9])|(16[0-9])|(17[0-9])|(18[0-9])|(19[0-9]))\d{8}$');
    return mobile.hasMatch(this);
  }

  ///调整适应emoji
  String nameFormat({int maxLength = 5}) {
    if (isEmpty) return '';
    if (runes.length <= maxLength) {
      return this;
    }
    return String.fromCharCodes(runes, 0, maxLength) + "...";
  }

  ///英文混排自动换行留白问题
  ///在字符串中添加零宽度字符
  String fullFormat() {
    if (isEmpty) {
      return this;
    }
    String breakWord = ' ';
    for (var element in runes) {
      breakWord += String.fromCharCode(element);
      breakWord += '\u200B';
    }
    return breakWord;
  }

  double toDouble() => double.parse(this);

  int toInt() => int.parse(this);

  /// 转成 color
  Color toColor() {
    var defaultColor = Colors.black;

    if (!contains("#")) {
      return defaultColor;
    }
    var hexColor = replaceAll("#", "");

    /// 如果是6位，前加0xff
    if (hexColor.length == 6) {
      hexColor = "0xff" + hexColor;
      var color = Color(int.parse(hexColor));
      return color;
    }

    /// 如果是8位，前加0x
    if (hexColor.length == 8) {
      var color = Color(int.parse("0x$hexColor"));
      return color;
    }
    return defaultColor;
  }
}

/// 泛型扩展
extension AllExt<T> on T {
  T apply(Function(T e) f) {
    f(this);
    return this;
  }

  R let<R>(R Function(T e) f) {
    return f(this);
  }
}
