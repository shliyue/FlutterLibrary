import 'package:zyocore/extension/list_extension.dart';

extension ObjectExtension on Object {
  String toEnumString() {
    return toString().split('.').last;
  }

  T? toEnum<T>(List<T> values) {
    // return values.linqFirstOrNil(
    //     predicate: (p) => p.toString().split('.').last == this);
    return values.linqFirstOrNil(
        predicate: (p){
          return p.toString().split('.').last == this;
        });
  }

}



