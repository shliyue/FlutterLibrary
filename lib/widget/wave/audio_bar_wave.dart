import 'dart:math';

import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';

class AudioBarWaveController extends ChangeNotifier {
  /// 指示器索引
  int get index => _index;
  int _index;

  set index(int v) {
    if (_index == v || v > _values.length - 1) {
      return;
    }
    _index = v;
    reloadData();
  }

  /// 值集合,所有值需要在 0-100
  List<double> get values => _values;
  final List<double> _values;

  AudioBarWaveController({
    int index = 0,
    List<double>? values,
  })  : _index = index,
        _values = values ?? [];

  void add(double v) {
    _values.add(v);
    reloadData();
  }

  void addAndStep(double v) {
    _values.add(v);
    _index++;
    reloadData();
  }

  void clear() {
    if (_values.isEmpty) {
      return;
    }
    _values.clear();
    reloadData();
  }

  void reloadData() {
    if (_index > _values.length - 1) {
      _index = _values.length - 1;
    }
    notifyListeners();
  }
}

class AudioBarWave extends StatefulWidget {
  final AudioBarWaveController controller;
  final BarStyle barStyle;
  final BaselineStyle baselineStyle;
  final IndicatorStyle indicatorStyle;

  const AudioBarWave({
    Key? key,
    required this.controller,
    this.barStyle = const BarStyle(),
    this.baselineStyle = const BaselineStyle(),
    this.indicatorStyle = const IndicatorStyle(),
  }) : super(key: key);

  @override
  State<StatefulWidget> createState() => _AudioBarWaveState();
}

class _AudioBarWaveState extends State<AudioBarWave> {
  @override
  void initState() {
    // TODO: implement initState
    super.initState();

    widget.controller.addListener(_change);
  }

  @override
  void dispose() {
    // TODO: implement dispose

    widget.controller.removeListener(_change);

    super.dispose();
  }

  @override
  Widget build(BuildContext context) {
    // TODO: implement build
    return CustomPaint(
      size: Size.infinite,
      painter: _BarWavePainter(widget),
    );
  }

  void _change() {
    if (mounted) setState(() {});
  }
}

class _BarWavePainter extends CustomPainter {
  late Paint _barPaint;
  late Paint _baselinePaint;
  late Paint _indicatorPaint;

  final AudioBarWave _widget;

  _BarWavePainter(this._widget) {
    _barPaint = Paint()
      ..strokeWidth = _widget.barStyle.width
      ..color = _widget.barStyle.color
      ..style = PaintingStyle.fill;

    _baselinePaint = Paint()
      ..strokeWidth = _widget.baselineStyle.height
      ..color = _widget.baselineStyle.color
      ..style = PaintingStyle.fill;

    _indicatorPaint = Paint()
      ..strokeWidth = _widget.indicatorStyle.width
      ..color = _widget.indicatorStyle.color
      ..style = PaintingStyle.fill;
  }

  double formatValue(double v) {
    if (v < 0) {
      v = 0;
    } else if (v > 100) {
      v = 100;
    }
    return v / 100;
  }

  void _paintIndicator(Canvas canvas, Size size) {
    var cx = size.width / 2;
    var height = _widget.barStyle.maxHeight ?? size.height;
    var top = (size.height - height) / 2;

    canvas.drawLine(Offset(cx, top), Offset(cx, top + height), _indicatorPaint);
  }

  void _paintBaseline(Canvas canvas, Size size) {
    var cx = size.width / 2;
    var cy = size.height / 2;

    var dw = _widget.baselineStyle.width;
    final space = dw + _widget.baselineStyle.space;

    //中间的线条
    canvas.drawLine(
        Offset(cx - dw / 2, cy), Offset(cx + dw / 2, cy), _baselinePaint);

    int i = 0;
    double rightStart = cx + dw / 2 + _widget.baselineStyle.space;
    double leftStart = cx - dw / 2 - _widget.baselineStyle.space;

    while (i < 20) {
      //右边的线条
      var rx = rightStart + i * space;
      canvas.drawLine(Offset(rx, cy), Offset(rx + dw, cy), _baselinePaint);

      //左边的线条
      var lx = leftStart - i * space;
      canvas.drawLine(Offset(lx, cy), Offset(lx - dw, cy), _baselinePaint);
      i++;
      if (rx - lx > size.width) {
        break;
      }
    }
  }

  void _paintBar(Canvas canvas, Size size) {
    var values = _widget.controller.values;
    if (values.isEmpty) {
      return;
    }

    var idIndex = min(_widget.controller.index, values.length - 1);

    var cx = size.width / 2;
    var minHeight = _widget.barStyle.minHeight ?? (size.height * 0.1);
    var maxHeight = _widget.barStyle.maxHeight ?? (size.height * 0.8);

    var space = _widget.barStyle.width + _widget.barStyle.space;

    double max = 0;
    for (var i = 0; i < values.length; i++) {
      //右侧
      var rightIndex = idIndex + i;
      if (rightIndex <= values.length - 1) {
        var v = formatValue(values[rightIndex]);
        var h = minHeight + (maxHeight - minHeight) * v;
        var top = (size.height - h) / 2;

        var x = cx + i * space;
        canvas.drawLine(Offset(x, top), Offset(x, top + h), _barPaint);

        max += space;
      }

      //左侧
      var leftIndex = idIndex - (i + 1);
      if (leftIndex >= 0) {
        var v = formatValue(values[leftIndex]);
        var h = minHeight + (maxHeight - minHeight) * v;
        var top = (size.height - h) / 2;

        var x = cx - (i + 1) * space;
        canvas.drawLine(Offset(x, top), Offset(x, top + h), _barPaint);

        max += space;
      }

      if (max > size.width) {
        break;
      }
    }
  }

  @override
  void paint(Canvas canvas, Size size) {
    //绘制基准线
    _paintBaseline(canvas, size);
    //绘制指针
    _paintIndicator(canvas, size);
    //绘制刻度
    _paintBar(canvas, size);
  }

  @override
  bool shouldRepaint(covariant CustomPainter oldDelegate) {
    return oldDelegate != this;
  }
}

class IndicatorStyle {
  final Color color;
  final double width;

  /// 默认容器高度
  final double? height;

  const IndicatorStyle({
    this.color = const Color.fromARGB(255, 207, 77, 104),
    this.width = 2,
    this.height,
  });
}

class BarStyle {
  final Color color;
  final double width;
  final double space;

  /// 默认容器高度 0.1 倍
  final double? minHeight;

  /// 默认容器高度 0.8 倍
  final double? maxHeight;

  const BarStyle({
    this.color = const Color.fromARGB(255, 159, 137, 255),
    this.width = 2,
    this.space = 5,
    this.minHeight,
    this.maxHeight,
  });
}

class BaselineStyle {
  final Color color;
  final double height;
  final double width;
  final double space;

  const BaselineStyle({
    this.color = const Color.fromARGB(255, 234, 234, 234),
    this.height = 2,
    this.width = 10,
    this.space = 4,
  });
}
