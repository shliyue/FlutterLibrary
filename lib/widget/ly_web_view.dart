import 'package:flutter/material.dart';
import 'package:flutter_inappwebview/flutter_inappwebview.dart';
import 'package:get/get.dart';
import 'package:zyocore/util/toast_util.dart';
class WebViewParmsModel {
  late String type;
  dynamic arg;

  WebViewParmsModel.fromJson(dynamic json) {
    type = json['type'];
    arg = json['arg'] ?? "";
  }
}

class LYWebView extends StatelessWidget {
  const LYWebView({
    Key? key,
    required this.url,
    this.crossPlatform,
    this.print,
    this.toast,
    this.close,
    this.launchUrl,
    this.saveImage,
    this.wxShareMiniProgram,
    this.onLoadError,
    this.onLoadStop,
    this.onLoadStart,
    this.onWebViewCreated,
    this.getFlutterFunc,
    this.isCache = true,
    this.showLoading = false,
  }) : super(key: key);

  final String url;

  final InAppWebViewOptions? crossPlatform;
  final Function(List<dynamic>)? print;
  final Function(List<dynamic>)? toast;
  final Function(List<dynamic>)? close;
  final Function(List<dynamic>)? launchUrl;
  final Function(List<dynamic>)? wxShareMiniProgram;
  final Function(List<dynamic>)? saveImage;
  final Function(WebViewParmsModel)? getFlutterFunc;
  final Function(int code, String message)? onLoadError;
  final Function(InAppWebViewController controller)? onLoadStop;
  final Function? onLoadStart;
  final Function(InAppWebViewController controller)? onWebViewCreated;
  final bool isCache;
  final bool showLoading;

  @override
  Widget build(BuildContext context) {
    InAppWebViewGroupOptions? options = InAppWebViewGroupOptions(
        crossPlatform: crossPlatform ??
            InAppWebViewOptions(
              mediaPlaybackRequiresUserGesture: false,
              supportZoom: false,
              cacheEnabled: isCache,
              clearCache: !isCache,
              // useShouldOverrideUrlLoading: true,
            ),
        android: AndroidInAppWebViewOptions(
          useHybridComposition: true,
        ),
        ios: IOSInAppWebViewOptions(
          allowsInlineMediaPlayback: true,
        ));
    return InAppWebView(
      initialUrlRequest: URLRequest(url: Uri.tryParse(url)),
      initialOptions: options,
      onWebViewCreated: (webViewController) {
        webViewController.addJavaScriptHandler(handlerName: "print", callback: _print);
        webViewController.addJavaScriptHandler(handlerName: "toast", callback: _toast);
        webViewController.addJavaScriptHandler(handlerName: "close", callback: _close);
        webViewController.addJavaScriptHandler(handlerName: "launchUrl", callback: _launchUrl);
        webViewController.addJavaScriptHandler(handlerName: "wxShareMiniProgram", callback: _wxShareMiniProgram);
        webViewController.addJavaScriptHandler(handlerName: "saveImage", callback: _saveImage);
        webViewController.addJavaScriptHandler(handlerName: "getFlutterFunc", callback: _getFlutterFunc);
        if (onWebViewCreated != null) {
          onWebViewCreated!(webViewController);
        }
      },
      androidOnPermissionRequest: (controller, origin, resources) async {
        return PermissionRequestResponse(resources: resources, action: PermissionRequestResponseAction.GRANT);
      },
      onLoadStart: (webViewController, url) {
        if (showLoading) {
          ToastUtil.loading('');
        }
        if (onLoadStart != null) {
          onLoadStart!();
        }
      },
      onLoadStop: (webViewController, url) async {
        if (showLoading) {
          ToastUtil.dismiss();
        }
        if (onLoadStop != null) {
          onLoadStop!(webViewController);
        }
      },
      onLoadError: (webViewController, url, code, message) {
        if (showLoading) {
          ToastUtil.dismiss();
        }
        if (onLoadError != null) {
          onLoadError!(code, message);
        } else {
          ToastUtil.showToast(message);
        }
      },
    );
  }

  _getFlutterFunc(List<dynamic> args) {
    if (getFlutterFunc != null) {
      var rs = WebViewParmsModel.fromJson(args[0]);
      getFlutterFunc!(rs);
    }
  }

  _print(List<dynamic> args) {
    if (print != null) {
      print!(args);
    }
  }

  _toast(List<dynamic> args) {
    if (toast != null) {
      toast!(args);
    }
  }

  _close(List<dynamic> args) {
    if (close != null) {
      close!(args);
    } else {
      Get.back();
    }
  }

  _launchUrl(List<dynamic> args) {
    if (launchUrl != null) {
      launchUrl!(args);
    }
  }

  _wxShareMiniProgram(List<dynamic> args) {
    if (wxShareMiniProgram != null) {
      wxShareMiniProgram!(args);
    }
  }

  _saveImage(List<dynamic> args) {
    if (saveImage != null) {
      saveImage!(args);
    }
  }
}
