import 'package:flutter/material.dart';
import 'package:flutter_screenutil/flutter_screenutil.dart';
import 'package:photo_manager_image_provider/photo_manager_image_provider.dart';
import 'package:zyocore/values/color.dart';
import 'package:zyocore/widget/camera_picker/constants/constants.dart';

import '../core/configs/config.dart';
import '../util/logger.dart';
import '../values/edge_insets.dart';

///图片上传组件-带拖拽排序
class LyUploadImageDraggableWidget extends StatefulWidget {
  List<AssetEntity> images;
  int maxLength;
  bool canEdit;
  VoidCallback? addCallBack;
  Function(int)? delCallBack;
  Function(int)? onTapItemHandle;
  Function()? onDragStarted;
  Function()? onDraggableCanceled;

  LyUploadImageDraggableWidget({
    Key? key,
    required this.images,
    this.maxLength = 6,
    this.addCallBack,
    this.onTapItemHandle,
    this.onDragStarted,
    this.onDraggableCanceled,
    this.delCallBack,
    this.canEdit = true,
  }) : super(key: key);

  @override
  _LyUploadImageDraggableWidgetState createState() =>
      _LyUploadImageDraggableWidgetState();
}

class _LyUploadImageDraggableWidgetState
    extends State<LyUploadImageDraggableWidget> {
  /// 正在移动图片id
  var movingId = '';

  @override
  Widget build(BuildContext context) {
    int itemCount = (widget.images.length >= widget.maxLength)
        ? widget.maxLength
        : widget.canEdit
            ? widget.images.length + 1
            : widget.images.length;

    var row = (itemCount ~/ 4) + (itemCount % 4 == 0 ? 0 : 1);

    var itemW = ((375 - 32 - 15) / 4);

    var imageW = itemW - 5;

    var height = row * itemW + (row - 1) * 5;

    return SizedBox(
      height: height.w,
      child: GridView.builder(
        padding: EdgeInsets.zero,
        physics: const NeverScrollableScrollPhysics(),
        itemCount: itemCount,
        //SliverGridDelegateWithFixedCrossAxisCount 构建一个横轴固定数量Widget
        gridDelegate: SliverGridDelegateWithFixedCrossAxisCount(
          //横轴元素个数
          crossAxisCount: 4,
          //纵轴间距
          mainAxisSpacing: 5.w,
          //横轴间距
          crossAxisSpacing: 5.w,
          childAspectRatio: 1,
        ),
        itemBuilder: (BuildContext context, int index) {
          //Widget Function(BuildContext context, int index)
          if (index >= widget.images.length) {
            return GestureDetector(
                onTap: widget.addCallBack,
                child: Container(
                  width: itemW,
                  height: itemW,
                  alignment: Alignment.bottomLeft,
                  clipBehavior: Clip.hardEdge,
                  decoration: BoxDecoration(
                    borderRadius: BorderRadius.circular(5.w),
                  ),
                  child: Container(
                      clipBehavior: Clip.hardEdge,
                      margin: bottom5,
                      decoration: BoxDecoration(
                          borderRadius: BorderRadius.circular(5.w),
                          color: lightF8F8F8),
                      width: imageW.w,
                      height: imageW.w,
                      alignment: Alignment.center,
                      child: Image(
                        width: 50.w,
                        height: 50.w,
                        image: AssetImage("assets/images/add_img.png",
                            package: Config.packageName),
                      )),
                ));
          }
          return buildGridItem(context, index, itemW, imageW);
        },
      ),
    );
  }

  Widget buildGridItem(
      BuildContext context, int index, double itemW, double imageW) {
    return Draggable(
      data: widget.images[index],
      onDragCompleted: () {
        /// 清空标记，进行重新绘制
        _onDraggableCanceledAction();
        Logger.d("on---onDragCompleted");
      },
      onDragEnd: (d) {
        Logger.d("on---onDragEnd--${d}");
      },
      onDragUpdate: (v) {
        Logger.d("on---onDragUpdate--${v}");
      },
      onDragStarted: () {
        /// 开始拖拽
        _onDragStartedAction(widget.images[index]);
        if (widget.onDragStarted != null) {
          widget.onDragStarted!();
        }
        Logger.d("on---onDragStarted--");
      },
      onDraggableCanceled: (v, f) {
        /// 清空标记，进行重新绘制
        _onDraggableCanceledAction();
        if (widget.onDraggableCanceled != null) {
          widget.onDraggableCanceled!();
        }
        Logger.d("on---onDraggableCanceled--${v}---${f}");
      },
      child: DragTarget(
        /// candidateData为onWillAccept回调为true时可接收的数据列表，
        /// rejectedData为onWillAccept回调为false时拒绝时的数据列表
        builder: (context, candidateData, rejectedData) {
          return _buildMoveTargetItem(
              asset: widget.images[index], imageW: imageW, itemW: itemW);
        },

        /// 当拖拽锚点进入DragTarget范围时回调，
        /// true时会将Data数据添加到candidateData列表中；
        /// false时会将Data数据添加到rejectedData列表中
        onWillAccept: (moveData) {
          var accept = moveData != null;
          if (accept) {
            _exchangeItemAction({
              'moveData': moveData,
              'toData': widget.images[index],
              'onAccept': false
            });
          }
          return accept;
        },

        /// 接收Data数据，只有onWillAccept返回true且拖拽结束时拖拽锚点位于DragTarget内才会回调
        onAccept: (moveData) {
          _exchangeItemAction({
            'moveData': moveData,
            'toData': widget.images[index],
            'onAccept': true
          });
        },
      ),
      feedback: SizedBox(
          width: itemW,
          height: itemW,
          child: buildImageWidget(index: index, itemW: itemW, imageW: imageW)),
    );
  }

  Widget buildImageWidget(
      {required int index, required double itemW, required double imageW}) {
    return Stack(children: [
      Container(
          width: itemW.w,
          height: itemW.w,
          padding: widget.delCallBack == null
              ? null
              : EdgeInsets.only(top: 5.w, right: 5.w),
          alignment: Alignment.bottomLeft,
          child: Container(
            clipBehavior: Clip.hardEdge,
            decoration: BoxDecoration(
              borderRadius: BorderRadius.circular(5.w),
            ),
            child: Image(
              width: imageW.w,
              height: imageW.w,
              fit: BoxFit.cover,
              image: AssetEntityImageProvider(widget.images[index],
                  isOriginal: false),
            ),
          )),
      Visibility(
        visible: widget.delCallBack != null,
        child: Positioned(
          top: 0,
          right: 0,
          child: GestureDetector(
            onTap: () => widget.delCallBack!(index),
            child: Container(
              clipBehavior: Clip.hardEdge,
              decoration:
                  BoxDecoration(borderRadius: BorderRadius.circular(10.r)),
              child: Image(
                image: const AssetImage(
                  "assets/images/icon_del.png",
                  package: "zyocore",
                ),
                width: 20.r,
                height: 20.r,
              ),
            ),
          ),
        ),
      ),
    ]);
  }

  Widget _buildMoveTargetItem(
      {required AssetEntity asset,
      required double itemW,
      required double imageW}) {
    if (asset.id == movingId) {
      return Container();
    }
    var assetIndex =
        widget.images.indexWhere((element) => element.id == asset.id);
    return SizedBox(
        width: itemW,
        height: itemW,
        child:
            buildImageWidget(index: assetIndex, itemW: itemW, imageW: imageW));
  }

  /// 数据交换
  void _exchangeItemAction(Map info) {
    setState(() {
      var toIndex = widget.images
          .indexWhere((element) => element.id == info['toData'].id);
      var moveData = info['moveData'];
      var onAccept = info['onAccept'];
      widget.images.remove(moveData);
      widget.images.insert(toIndex, moveData);
      if (onAccept) {
        movingId = '';
      }
    });
  }

  /// 开始拖拽 记录开始拖拽的数据
  _onDragStartedAction(AssetEntity assetEntity) {
    setState(() {
      movingId = assetEntity.id;
    });
  }

  /// 拖拽取消
  _onDraggableCanceledAction() {
    setState(() {
      movingId = '';
    });
  }
}
