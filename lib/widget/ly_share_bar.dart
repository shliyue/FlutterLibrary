import 'package:flutter/material.dart';
import 'package:flutter_screenutil/flutter_screenutil.dart';
import 'package:get/get.dart';
import 'package:zyocore/values/style.dart';
import 'package:zyocore/values/color.dart';
import 'package:zyocore/widget/index.dart';

class ShareItem {
  String event;
  String iconName;
  String title;
  String? package;

  ShareItem({required this.event, required this.title, required this.iconName, this.package});
}

class LYShareBar extends StatelessWidget {
  final bool canDel;

  // type 0-单图 1-帖子
  final int type;
  final Function(String)? callback;
  final bool canPoster;
  final List<ShareItem>? itemIcons;
  final Color? bgColor;
  final Color? textColor;

  LYShareBar({
    Key? key,
    this.canDel = true,
    this.type = 0,
    this.callback,
    this.canPoster = false,
    this.itemIcons,
    this.bgColor,
    this.textColor,
  }) : super(key: key);

  // final itemIcons = ['icon_share_SESSION','icon_share_TIMELINE','icon_share_dowload','icon_share_delete'];
  // final itemName = ['微信','朋友圈','下载','删除'];
  List<ShareItem> icons = [
    ShareItem(event: "session", iconName: "icon_share_SESSION.png", title: "微信"),
    ShareItem(event: "timeline", iconName: "icon_share_TIMELINE.png", title: "朋友圈"),
    ShareItem(event: "poster", iconName: "icon_share_poster.png", title: "分享海报"),
    ShareItem(event: "download", iconName: "icon_share_dowload.png", title: "下载"),
    ShareItem(event: "delete", iconName: "icon_share_delete.png", title: "删除"),
  ];

  @override
  Widget build(BuildContext context) {
    if (type == 1) {
      icons.removeWhere((element) => element.title == '下载');
    }
    if (canDel == false) {
      icons.removeWhere((element) => element.title == '删除');
    }
    if (!canPoster) {
      icons.removeWhere((element) => element.title == '分享海报');
    }
    if (itemIcons != null) {
      icons.addAll(itemIcons!);
    }

    return Container(
      alignment: Alignment.topLeft,
      decoration: BoxDecoration(
        color: bgColor ?? (MediaQuery.of(context).orientation == Orientation.portrait ? lightF8F8F8 : const Color(0xFF191919).withOpacity(0.8)),
        borderRadius: BorderRadius.only(
          topLeft: Radius.circular(18.r),
          topRight: Radius.circular(18.r),
        ),
      ),
      child: SafeArea(
        child: Column(
          crossAxisAlignment: CrossAxisAlignment.start,
          children: [
            Container(
              padding: const EdgeInsets.symmetric(vertical: 21.0, horizontal: 21.0),
              child: Wrap(
                runSpacing: 21.0,
                spacing: 21.0,
                children: icons.map((e) {
                  return _buildItem(e);
                }).toList(),
              ),
            ),
            if (MediaQuery.of(context).orientation == Orientation.portrait)
              GestureDetector(
                onTap: () => Get.back(),
                child: Container(
                  alignment: Alignment.center,
                  color: MediaQuery.of(context).orientation == Orientation.portrait ? Colors.white : const Color(0xFF191919).withOpacity(0.8),
                  padding: const EdgeInsets.only(bottom: 16, top: 16),
                  child: LYText(
                    text: '取消',
                    fontSize: 17,
                    color: MediaQuery.of(context).orientation == Orientation.portrait ? dark191919 : Colors.white,
                    fontFamily: AppFontFamily.pingFangRegular,
                  ),
                ),
              ),
          ],
        ),
      ),
    );
  }

  Widget _buildItem(ShareItem item) {
    double size = 48;
    return GestureDetector(
      onTap: () {
        Get.back();
        if (callback != null) {
          callback!(item.event);
        }
      },
      child: Column(
        mainAxisAlignment: MainAxisAlignment.center,
        children: [
          LYAssetsImage(
            name: item.iconName,
            width: size,
            height: size,
            package: item.package,
            radius: size,
          ),
          const SizedBox(height: 8),
          LYText(
            text: item.title,
            fontSize: 12,
            fontFamily: AppFontFamily.pingFangRegular,
            color: textColor ?? dark666666,
          ),
        ],
      ),
    );
  }
}
