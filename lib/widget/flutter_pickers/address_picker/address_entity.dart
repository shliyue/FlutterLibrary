import 'dart:convert';

AddressEntity addressEntityFromJson(String str) =>
    AddressEntity.fromJson(json.decode(str));

String addressEntityToJson(AddressEntity data) => json.encode(data.toJson());

class AddressEntity {
  AddressEntity({
    this.childs,
    this.code,
    this.desc,
    this.level,
    this.parent,
  });

  List<AddressEntity>? childs;
  String? code;
  String? desc;
  int? level;
  String? parent;

  AddressEntity copyWith({
    List<AddressEntity>? childs,
    String? code,
    String? desc,
    int? level,
    String? parent,
  }) =>
      AddressEntity(
        childs: childs ?? this.childs,
        code: code ?? this.code,
        desc: desc ?? this.desc,
        level: level ?? this.level,
        parent: parent ?? this.parent,
      );

  factory AddressEntity.fromJson(Map<String, dynamic> json) => AddressEntity(
        childs: List<AddressEntity>.from(json["childs"] != null
            ? json["childs"].map((x) => AddressEntity.fromJson(x))
            : []),
        code: json["code"],
        desc: json["desc"],
        level: json["level"],
        parent: json["parent"],
      );

  Map<String, dynamic> toJson() => {
        "childs": childs != null
            ? List<dynamic>.from(childs!.map((x) => x.toJson()))
            : [],
        "code": code,
        "desc": desc,
        "level": level,
        "parent": parent,
      };
}
