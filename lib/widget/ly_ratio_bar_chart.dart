import 'dart:math';

import 'package:flutter/material.dart';

import '../util/logger.dart';

class LYRatioBar<T extends LYRatioBarModel> extends StatefulWidget {
  const LYRatioBar({
    Key? key,
    required this.model,
  }) : super(key: key);
  final T model;

  @override
  State<LYRatioBar> createState() => _LYRatioBarState();
}

class _LYRatioBarState extends State<LYRatioBar> {
  @override
  Widget build(BuildContext context) {
    return CustomPaint(
      painter: LYBarPainter(model: widget.model),
    );
  }
}

class LYRatioBarModel {
  final List<LYRatioBarData> datas;
  List<LYRatioBarData>? allZeroPlaceHolder;
  bool needPlaceHodler;

  LYRatioBarModel({
    required this.datas,
    this.allZeroPlaceHolder = const [
      LYRatioBarData(
        number: 100,
        color: Color(0xFFF2F2F2),
      )
    ],
    this.needPlaceHodler = true,
  });
}

class LYRatioBarData {
  final num number;
  final Color color;

  const LYRatioBarData({
    required this.number,
    required this.color,
  });
}

class LYBarPainter extends CustomPainter {
  int gap = 1;
  LYRatioBarModel model;

  LYBarPainter({
    required this.model,
  });

  @override
  void paint(Canvas canvas, Size size) {
    var list = model.datas;
    if (list.isEmpty) {
      return;
    }

    if (list.length == 1) {
      final model = list.first;
      final paint = Paint()
        ..color = model.color
        ..strokeWidth = size.height
        ..strokeCap = StrokeCap.round;
      canvas.drawLine(Offset(0, size.height / 2), Offset(size.width, size.height / 2), paint);
      return;
    }
    double total = list.map((e) => e.number).reduce((value, element) => value + element).toDouble();
    if (total == 0) {
      if (model.needPlaceHodler && model.allZeroPlaceHolder != null) {
        list = model.allZeroPlaceHolder!;
      } else {
        return;
      }
    }

    double effectH = size.height;
    final noZeroCount = list.where((element) => element.number != 0).length;
    double effectWidth = size.width - (noZeroCount - 1) * gap;

    double lineCenter = size.height / 2;
    final circleRadius = effectH / 2;
    // Paint.enableDithering = true;
    Path leftSemiCirclePath = Path();
    final startCircleCenter = Offset(circleRadius, lineCenter);
    leftSemiCirclePath.addArc(Rect.fromCircle(center: startCircleCenter, radius: circleRadius), pi / 2, pi);
    Path createBeforeCircle(double end) {
      Path p = Path();
      double semiHeight = sqrt(2 * circleRadius * end - end * end);
      double angle = acos(semiHeight / circleRadius);
      p.moveTo(end, circleRadius + semiHeight);
      p.arcToPoint(Offset(end, circleRadius - semiHeight), radius: Radius.circular(circleRadius), rotation: 2 * angle);
      p.close();
      return p;
    }

    final trailCircleStartX = size.width - circleRadius;
    Path rightSemiCirclePath = Path();
    final endCircleCenter = Offset(trailCircleStartX, lineCenter);
    rightSemiCirclePath.addArc(Rect.fromCircle(center: endCircleCenter, radius: circleRadius), -pi / 2, pi);
    Path createAfterCircle(double start) {
      if (start < trailCircleStartX) {
        Logger.d('超出预期');
        return Path();
      }
      if (start >= size.width) {
        return Path();
      }
      Path p = Path();
      final x = start - trailCircleStartX;
      if (x == 0) {
        Logger.d('error');
      }
      double semiHeight = sqrt(circleRadius * circleRadius - x * x);
      p.moveTo(start, circleRadius - semiHeight);
      double angle = acos(semiHeight / circleRadius);
      p.arcToPoint(Offset(start, circleRadius + semiHeight),
          radius: Radius.circular(circleRadius), rotation: 2 * angle);
      p.close();

      return p;
    }

    drawIn(double startX, double endX, Color color) {
      late Path drawPath;
      if (endX < circleRadius) {
        final bigPath = createBeforeCircle(endX);
        final smallPath = createBeforeCircle(startX);
        drawPath = Path.combine(PathOperation.difference, bigPath, smallPath);
        canvas.drawPath(
            drawPath,
            Paint()
              ..style = PaintingStyle.fill
              ..color = color
              ..strokeWidth = 1);
      } else if (endX <= trailCircleStartX) {
        if (startX < circleRadius) {
          final smallPath = createBeforeCircle(startX);
          drawPath = Path.combine(PathOperation.difference, leftSemiCirclePath, smallPath);
          final rectPath = Path()..addRect(Rect.fromLTRB(circleRadius, 0, endX, size.height));
          drawPath = Path.combine(PathOperation.union, drawPath, rectPath);
        } else {
          drawPath = Path()..addRect(Rect.fromLTRB(startX, 0, endX, size.height));
        }
      } else {
        if (startX < circleRadius) {
          // 1
          final smallPath1 = createBeforeCircle(startX);
          drawPath = Path.combine(PathOperation.difference, leftSemiCirclePath, smallPath1);
          // 2
          final rectPath = Path()..addRect(Rect.fromLTRB(circleRadius, 0, trailCircleStartX, size.height));
          drawPath = Path.combine(PathOperation.union, drawPath, rectPath);
          // 3
          final smallPath = createAfterCircle(endX);
          final diffPath = Path.combine(PathOperation.difference, rightSemiCirclePath, smallPath);
          drawPath = Path.combine(PathOperation.union, drawPath, diffPath);
        } else if (startX <= trailCircleStartX) {
          final rectPath = Path()..addRect(Rect.fromLTRB(startX, 0, trailCircleStartX, size.height));
          final smallPath = createAfterCircle(endX);
          drawPath = Path.combine(PathOperation.difference, rightSemiCirclePath, smallPath);
          drawPath = Path.combine(PathOperation.union, drawPath, rectPath);
        } else {
          Path left = createAfterCircle(startX);
          Path right = createAfterCircle(endX);
          drawPath = Path.combine(PathOperation.difference, left, right);
        }
      }
      canvas.drawPath(
          drawPath,
          Paint()
            ..style = PaintingStyle.fill
            ..color = color
            ..strokeWidth = 1);
    }

    double startX = 0.0;
    for (var i = 0; i < list.length; i++) {
      if (startX >= size.width) {
        // Logger.d('beyound');
        return;
      }
      final data = list[i];
      num theNum = data.number;
      double radio = theNum / total;
      if (radio == 0) continue;
      double ratioWidth = radio * effectWidth;

      final theStarX = startX;
      final theEndX = startX + ratioWidth;
      drawIn(theStarX, theEndX, data.color);
      startX = theEndX + (theNum == 0 ? 0 : gap);
    }
  }

  @override
  bool shouldRepaint(covariant LYBarPainter oldDelegate) {
    return oldDelegate.model != model;
  }
}
