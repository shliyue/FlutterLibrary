import 'package:flutter/material.dart';
import 'package:zyocore/values/style.dart';
import 'package:zyocore/zyocore.dart';

class FansNodataWidget extends StatelessWidget {

  const FansNodataWidget({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {

    List<String> s = [
      '外婆',
      '外公',
      '爷爷',
      '奶奶',
      '其他',
    ];
    return Center(
      child: Container(
        margin: EdgeInsets.only(top: 12.w),
        child: Column(
          children: [
            LYText(
              text: '邀请亲友',
              fontFamily: AppFontFamily.pingFangMedium,
              color: dark191919,
              fontSize: 18.sp,
              lineHeight: 1.5,
            ),
            LYText(
              text: '邀请家人，一起关注幼儿成长',
              fontFamily: AppFontFamily.pingFangMedium,
              color: dark666666,
              fontSize: 14.sp,
              lineHeight: 1.5,
            ),
            SizedBox(
              height: 10.w,
            ),
            Padding(
              padding: EdgeInsets.symmetric(horizontal: 22.w),
              child: Row(
                mainAxisAlignment: MainAxisAlignment.spaceBetween,
                children: [
                  ...s
                      .map(
                        (e) => Column(
                      children: [
                        Container(
                          child: DottedBorder(
                            strokeWidth: 0.5,
                            borderType: BorderType.RRect,
                            radius: Radius.circular(52.r),
                            color: const Color(0xffD178FF),
                            child: Container(
                              padding: EdgeInsets.all(16.w),
                              decoration: BoxDecoration(
                                borderRadius: BorderRadius.circular(52.r),
                                color: Color(0x21967CFF),
                              ),
                              child: Image.asset(
                                'assets/images/nodate_invite.png',
                                width: 18.w,
                                  package: "zyocore"
                              ),
                            ),
                          ),
                        ),
                        LYText(
                          text: e,
                          fontFamily: AppFontFamily.pingFangRegular,
                          color: dark666666,
                          fontSize: 13.sp,
                          lineHeight: 1.5,
                        ),
                      ],
                    ),
                  )
                      .toList()
                ],
              ),
            )
          ],
        ),
      ),
    );
  }
}
