import 'package:flutter/material.dart';

/// 三角形
class TriangleWidget extends StatelessWidget {
  final Color color;
  final double width;
  final double height;
  final bool isosceles;

  const TriangleWidget({Key? key, required this.color, required this.width, required this.height, this.isosceles = false}) : super(key: key);


  @override
  Widget build(BuildContext context) {
    return CustomPaint(
      painter: isosceles ? IsoscelesShapesPainter(color) : ShapesPainter(color),
      child: Container(
        width: width,
        height: height,
      ),
    );
  }
}
class IsoscelesShapesPainter extends CustomPainter {
  final Color color;

  IsoscelesShapesPainter(this.color);

  @override
  void paint(Canvas canvas, Size size) {
    final paint = Paint();
    paint.color = color;
    var path = Path();
    path.moveTo(size.width, size.height / 2);
    path.lineTo(0, size.height);
    path.lineTo(0, 0);
    path.close();
    canvas.drawPath(path, paint);
  }
  @override
  bool shouldRepaint(CustomPainter oldDelegate) => false;
}


class ShapesPainter extends CustomPainter {
  final Color color;

  ShapesPainter(this.color);

  @override
  void paint(Canvas canvas, Size size) {
    final paint = Paint();
    paint.color = color;
    var path = Path();
    path.moveTo(size.width, 0);
    path.lineTo(size.width, size.height);
    path.lineTo(size.width - size.height, 0);
    path.close();
    canvas.drawPath(path, paint);
  }
  @override
  bool shouldRepaint(CustomPainter oldDelegate) => false;
}
