import 'package:flutter/material.dart';

/// 圆形
class CircleWidget extends StatelessWidget {
  final Color? color;
  final double? size;
  final Border? border;
  final Gradient? gradient;
  final Widget? child;
  final bool circleChild;
  final EdgeInsets? padding;
  final EdgeInsets? margin;

  const CircleWidget({
    Key? key,
    required this.size,
    this.color = Colors.transparent,
    this.border,
    this.gradient,
    this.child,
    this.padding,
    this.margin,
    this.circleChild = false,
  }) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Container(
      margin: margin,
      child: Container(
        padding: padding,
        width: size,
        height: size,
        decoration: BoxDecoration(
          color: gradient == null ? this.color : null,
          border: border,
          borderRadius: BorderRadius.circular(size! / 2),
          gradient: gradient
        ),
        child: child == null ? Container() : Center(
          child: circleChild ? SizedBox(
            width: size! - (padding?.right??0),
            height: size! - (this.padding?.right??0),
            child: ClipOval(child: child),
          ) : child,
        ),
      ),
    );
  }
}
