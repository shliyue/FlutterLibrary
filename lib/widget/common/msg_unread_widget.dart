import 'package:flutter/material.dart';
import 'package:flutter_screenutil/flutter_screenutil.dart';
import 'package:zyocore/core/configs/config.dart';
import 'package:zyocore/values/edge_insets.dart';
import 'package:zyocore/values/text.dart';

import '../text/ly_text.dart';

/// 带数字的未读消息
class MsgUnreadWidget extends StatelessWidget {
  final int num;
  final Alignment numAlign;
  final TextStyle? style;
  final double? height;
  final double? paddingLeft;
  final double? paddingRight;
  final GestureTapCallback? onTap;

  const MsgUnreadWidget(
      {Key? key,
      required this.num,
      this.numAlign = Alignment.topLeft,
      this.style,
      this.paddingLeft,
      this.paddingRight,
      this.height,
      this.onTap})
      : super(key: key);

  @override
  Widget build(BuildContext context) {
    var unReadCount = num > 99 ? '99+' : '$num';
    return InkWell(
      onTap: onTap,
      child: Container(
        alignment: Alignment.center,
        height: height ?? 40.w,
        child: Stack(
          clipBehavior: Clip.none,
          children: [
            Container(
              margin: EdgeInsets.only(left: paddingLeft ?? 16.w, right: paddingRight ?? 16.w),
              height: height ?? 40.w,
              alignment: Alignment.center,
              child: Image.asset(
                'assets/images/icon_message.png',
                width: 20.w,
                height: 20.w,
                package: Config.packageName,
              ),
            ),
            Positioned(
              top: -5.w,
              left: numAlign == Alignment.topLeft
                  ? num > 99
                      ? 5.w
                      : 12.w
                  : null,
              right: numAlign == Alignment.topLeft
                  ? null
                  : num > 99
                      ? 0
                      : 8.w,
              child: num > 0
                  ? Container(
                      constraints: BoxConstraints(minWidth: 14.w, minHeight: 14.w),
                      padding: horizontal2,
                      decoration: BoxDecoration(borderRadius: BorderRadius.circular(7.w), color: const Color(0xffff1f1f)),
                      child: Center(
                        child: LYText.withStyle(
                          unReadCount,
                          style: style ?? text10,
                        ),
                      ),
                    )
                  : Container(),
            )
          ],
        ),
      ),
    );
  }
}
