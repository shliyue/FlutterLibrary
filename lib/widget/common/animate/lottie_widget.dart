import 'package:flutter/material.dart';
import 'package:lottie/lottie.dart';

import '../../../core/configs/config.dart';

class LottieWidget extends StatelessWidget {
  final double width;
  final double height;
  final String? file;
  final String? package;
  final List<BoxShadow>? boxShadow;
  final double radius;
  final Color? bgColor;
  final EdgeInsetsGeometry? margin;

  const LottieWidget({
    Key? key,
    this.width = 30,
    this.height = 20,
    this.file,
    this.package,
    this.boxShadow,
    this.radius = 0,
    this.bgColor,
    this.margin,
  }) : super(key: key);

  @override
  Widget build(BuildContext context) {
    if (file == null) return Container();
    return Container(
      margin: margin,
      decoration: BoxDecoration(
        color: bgColor,
        boxShadow: boxShadow,
        borderRadius: BorderRadius.all(Radius.circular(radius)),
      ),
      child: Lottie.asset(file!, width: width, height: height, package: package == "" ? null : (package ?? Config.packageName)),
    );
  }
}
