import 'package:flutter/material.dart';
import 'package:zyocore/values/circular_radius.dart';
import 'package:zyocore/values/edge_insets.dart';
import 'package:zyocore/values/text.dart';
import 'package:zyocore/widget/common/flex/row.dart';
import 'package:zyocore/widget/common/red_point_widget.dart';
import 'package:zyocore/zyocore.dart';

class BigButtonWidget extends StatefulWidget {
  final String text;
  final Widget? iconWidget;
  final IconData? iconData;
  final int? num;
  final double? height;
  final double? opacity;
  final Color? bgColor;
  final Color? borderColor;
  final TextStyle? btnStyle;
  final VoidCallback? onTap;
  final BorderRadius? radius;
  final bool enable;
  final EdgeInsetsGeometry? margin;
  final EdgeInsetsGeometry? padding;

  const BigButtonWidget({
    Key? key,
    required this.text,
    this.num,
    this.height,
    this.btnStyle,
    this.iconWidget,
    this.iconData,
    this.bgColor,
    this.borderColor,
    this.onTap,
    this.radius,
    this.enable = true,
    this.margin,
    this.padding,
    this.opacity,
  }) : super(key: key);

  @override
  _BigButtonWidgetState createState() => _BigButtonWidgetState();
}

class _BigButtonWidgetState extends State<BigButtonWidget> {
  @override
  Widget build(BuildContext context) {
    return GestureDetector(
      onTap: widget.enable ? widget.onTap : null,
      child: Opacity(
        opacity: widget.opacity ?? (widget.enable ? 1 : 0.3),
        child: Container(
          margin: widget.margin ?? EdgeInsets.zero,
          padding: widget.padding ?? EdgeInsets.only(top: (widget.num == null || widget.num == 0) ? 0 : 5.w),
          child: Stack(
            clipBehavior: Clip.none,
            alignment: Alignment.topRight,
            children: <Widget>[
              Container(
                height: widget.height ?? 48.w,
                decoration: BoxDecoration(
                  color: widget.bgColor,
                  border: widget.enable ? Border.all(width: 1.w, color: widget.borderColor ?? const Color(0xFFBBA9FF)) : null,
                  borderRadius: widget.radius,
                ),
                child: RCC(
                  children: <Widget>[
                    widget.iconWidget != null || widget.iconData != null
                        ? Container(
                            margin: right5,
                            child: widget.iconData != null
                                ? Icon(
                                    widget.iconData,
                                    size: 25.w,
                                    color: dark333333,
                                  )
                                : widget.iconWidget)
                        : Container(),
                    LYText.withStyle(
                      widget.text,
                      style: widget.btnStyle ?? text16,
                    ),
                  ],
                ),
              ),
              _redPoint
            ],
          ),
        ),
      ),
    );
  }

  Widget get _redPoint {
    if (widget.num == null || widget.num == 0) {
      return Container();
    } else {
      return Positioned(
        top: -5.w,
        right: -5.w,
        child: RedPointWidget("${widget.num}"),
      );
    }
  }
}
