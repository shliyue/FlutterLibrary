import 'package:flutter/material.dart';
import 'package:zyocore/values/color.dart';

import '../../text/ly_text.dart';

class GradientButtonWidget extends StatefulWidget {
  final String text;
  final bool enable;
  final Color textColor;
  final double fontSize;
  final Gradient gradient;
  final double radius;
  final double? width;
  final double? height;
  final EdgeInsetsGeometry? margin;
  final EdgeInsetsGeometry padding;
  final VoidCallback? onTap;

  const GradientButtonWidget({Key? key,
    this.enable = true,
    this.text = "",
    this.textColor = lightFFFFFF,
    this.fontSize = 14,
    this.gradient = gradientSilver,
    this.width,
    this.height,
    this.onTap,
    this.margin,
    this.padding = const EdgeInsets.fromLTRB(16, 7, 16, 7),
    this.radius = 5})
      : super(key: key);

  @override
  _GradientButtonWidgetState createState() => _GradientButtonWidgetState();
}

class _GradientButtonWidgetState extends State<GradientButtonWidget> {
  @override
  Widget build(BuildContext context) {
    return GestureDetector(
      onTap: widget.enable ? widget.onTap : null,
      child: Container(
        width: widget.width,
        height: widget.height,
        margin: widget.margin,
        padding: widget.padding,
        alignment: Alignment.center,
        clipBehavior: Clip.hardEdge,
        decoration: BoxDecoration(
          borderRadius: BorderRadius.all(Radius.circular(widget.radius)),
          gradient: widget.gradient,
        ),
        child: LYText.withStyle(
          widget.text,
          style: TextStyle(fontSize: widget.fontSize, color: widget.textColor),
        ),
      ),
    );
  }
}
