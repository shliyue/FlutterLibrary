import 'package:flutter/material.dart';
import 'package:flutter_screenutil/flutter_screenutil.dart';
import 'package:zyocore/values/color.dart';
import 'package:zyocore/values/edge_insets.dart';
import 'package:zyocore/values/text.dart';
import 'package:zyocore/widget/common/flex/row.dart';
import 'package:zyocore/widget/common/red_point_widget.dart';

import '../../text/ly_text.dart';

class SmallButtonWidget extends StatefulWidget {
  final String? text;
  final Color textColor;
  final Widget? textWidget;
  final Widget? icon;
  final int? num;
  final Color? bgColor;
  final Color? borderColor;
  final bool disable;
  final double? height;
  final double? width;
  final double fontSize;
  final EdgeInsetsGeometry? padding;
  final EdgeInsetsGeometry? margin;
  final VoidCallback? onTap;
  final double? radius;
  final String? fontFamily;
  final BorderRadiusGeometry? borderRadius;
  final BoxBorder? border;
  final TextStyle? textStyle;
  final Gradient? gradient;
  final List<BoxShadow>? boxShadow;
  final AlignmentGeometry? alignment;
  final Matrix4? matrix;

  const SmallButtonWidget({
    Key? key,
    this.text,
    this.height,
    this.width,
    this.fontSize = 14,
    this.num,
    this.icon,
    this.textWidget,
    this.disable = false,
    this.bgColor,
    this.padding,
    this.margin,
    this.textColor = lightFFFFFF,
    this.onTap,
    this.borderColor,
    this.radius,
    this.fontFamily,
    this.borderRadius,
    this.border,
    this.textStyle,
    this.gradient,
    this.boxShadow,
    this.alignment,
    this.matrix,
  }) : super(key: key);

  @override
  _SmallButtonWidgetState createState() => _SmallButtonWidgetState();
}

class _SmallButtonWidgetState extends State<SmallButtonWidget> {
  @override
  Widget build(BuildContext context) {
    return GestureDetector(
      onTap: widget.disable ? null : widget.onTap,
      child: Stack(
        children: <Widget>[
          Container(
            transform: widget.matrix,
            alignment: widget.alignment ?? Alignment.center,
            decoration: BoxDecoration(
              color: widget.disable ? dark666666 : widget.bgColor ?? theme9F89FF,
              border: widget.border,
              gradient: widget.gradient,
              boxShadow: widget.boxShadow,
              borderRadius: widget.borderRadius ?? BorderRadius.circular(widget.radius ?? ((widget.height ?? 0) / 2)),
            ),
            width: widget.width,
            height: widget.height,
            padding: widget.padding,
            margin: widget.margin,
            child: RSC(
              mainAxisAlignment: MainAxisAlignment.center,
              mainAxisSize: MainAxisSize.min,
              children: <Widget>[
                widget.icon != null ? Container(margin: right2, child: widget.icon) : Container(),
                widget.textWidget ??
                    LYText.withStyle(
                      widget.text,
                      style: widget.textStyle ?? text14.copyWith(fontSize: widget.fontSize, color: widget.textColor, fontFamily: widget.fontFamily),
                    ),
              ],
            ),
          ),
          if (widget.num != null && widget.num != 0)
            Positioned(
              top: -4.w,
              right: -4.w,
              child: RedPointWidget("${widget.num}"),
            )
        ],
      ),
    );
  }
}
