import 'package:flutter/material.dart';

class RadiusContainerWidget extends StatelessWidget {

  final double borderRadius;
  final Border? border;
  final Color? color;
  final Widget? child;
  final EdgeInsets? padding;
  final EdgeInsets? margin;

  const RadiusContainerWidget(this.borderRadius, {Key? key, this.child, this.border, this.color, this.padding, this.margin}) : super(key: key);
  @override
  Widget build(BuildContext context) {
    return border != null ? _buildBorderContainer : _buildContainer;
  }

  Widget get _buildContainer => Container(
    padding: padding ?? const EdgeInsets.all(0.0),
    margin: margin ?? const EdgeInsets.all(0.0),
    child: child ?? SizedBox(width: borderRadius, height: borderRadius,),
    decoration: BoxDecoration(
      color: color,
      borderRadius: BorderRadius.circular(borderRadius),
    ),
  );

  Widget get _buildBorderContainer => Container(
    padding: padding ?? const EdgeInsets.all(0.0),
    margin: margin ?? const EdgeInsets.all(0.0),
    child: child ?? SizedBox(width: borderRadius, height: borderRadius,),
    decoration: BoxDecoration(
      color: color,
      border: border,
      borderRadius: BorderRadius.circular(borderRadius),
    ),
  );
}