import 'package:flutter/material.dart';
import 'package:flutter/widgets.dart';

/// 带数字的红色圆形组件
class LyGradientMask extends StatelessWidget {
  List<Color>? colors;
  final double? width;
  final double? height;
  final double? radius;
  final VoidCallback? onTap;

  LyGradientMask(
      {Key? key, this.colors, this.width, this.height, this.radius, this.onTap})
      : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Container(
      width: width,
      height: height,
      decoration: BoxDecoration(
        borderRadius: BorderRadius.only(
            bottomLeft: Radius.circular(radius ?? 5),
            bottomRight: Radius.circular(radius ?? 5)),
        gradient: LinearGradient(
          begin: Alignment.topCenter,
          end: Alignment.bottomCenter,
          stops: [0.37,1],
          colors:
          colors ?? [const Color(0x00000000), const Color(0x7F000000)],
        ),
      ),
    );
  }
}
