import 'package:flutter/material.dart';

/// 带点击效果占位布局，常用于一些弹窗带空白部分
class ExpandedPopWidget extends StatelessWidget {
  const ExpandedPopWidget({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Expanded(
      child: GestureDetector(
        behavior: HitTestBehavior.opaque,
        onTap: () {
          Navigator.pop(context);
        },
        child: const SizedBox.expand(),
      ),
    );
  }
}
