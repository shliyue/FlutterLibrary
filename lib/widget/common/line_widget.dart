import 'package:flutter/material.dart';
import 'package:zyocore/values/color.dart';

class LineWidget extends StatelessWidget {
  final double height;
  final double? width;
  final Color? color;

  const LineWidget(
      {Key? key, this.height = 5, this.color = dark666666, this.width})
      : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Container(height: height, width: width, color: color);
  }
}
