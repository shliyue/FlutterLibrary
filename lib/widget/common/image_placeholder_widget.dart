import 'package:flutter/material.dart';
import 'package:zyocore/values/color.dart';

class ImagePlaceholderWidget extends StatelessWidget {
  final int type; // 0 video 1 image
  final int state; // 0 加载中 1 失败
  final double? width;
  final double? height;
  final Color? color;

  const ImagePlaceholderWidget(
      {Key? key, this.type = 0, this.state = 0, this.width, this.height,this.color = lightF8F8F8})
      : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Container(
      width: width,
      height: height,
      alignment: Alignment.center,
      color: color,
      child: Image.asset(
        'assets/images/${type == 0 ? 'video' : 'image'}_placeholder_$state.png',
        width: 36,
        fit: BoxFit.contain,
        package: "zyocore",
      ),
    );
  }
}
