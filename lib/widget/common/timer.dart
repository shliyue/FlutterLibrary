import 'dart:async';

Timer ZUCountDown({
  required int second,
  required Function(int) callback,
}) {
  Timer timer = Timer.periodic(const Duration(seconds: 1), (timer) {
    callback(--second);
    if (second <= 0) {
      timer.cancel();
    }
  });
  return timer;
}
