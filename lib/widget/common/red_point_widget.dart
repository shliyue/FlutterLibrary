import 'package:flutter/material.dart';
import 'package:zyocore/values/color.dart';
import 'package:zyocore/values/edge_insets.dart';
import 'package:zyocore/values/text.dart';
import 'package:zyocore/widget/common/shapes/circle_widget.dart';
import '../text/ly_text.dart';

/// 带数字的红色圆形组件
class RedPointWidget extends StatelessWidget {
  final String? num;
  final TextStyle? style;
  final double? size;
  const RedPointWidget(this.num, {Key? key, this.style, this.size}) : super(key: key);
  @override
  Widget build(BuildContext context) {
    double size = this.size ?? 14;
    return (num??"").isEmpty ? Container() : Container(
      padding: num!.length > 1 ? (size >= 20 ? horizontal6 :  horizontal4) : edge0,
      constraints: BoxConstraints(
        minHeight: size,
        maxHeight: size,
        minWidth: size,
      ),
      decoration: BoxDecoration(
        color: redE02020,
        borderRadius: BorderRadius.circular(size / 2)
      ),
      child: Center(child: LYText.withStyle(num, style: style ?? text10,)),
    );
  }
}

/// 不带数字的实心小红点
class TinyRedPointWidget extends StatelessWidget {
  final bool show;
  const TinyRedPointWidget({Key? key, this.show = false}) : super(key: key);
  @override
  Widget build(BuildContext context) {
    return show ? const CircleWidget(size: 6, color: redE02020,) : Container();
  }
}
