import 'package:flutter/cupertino.dart';
import 'package:zyocore/zyocore.dart';

/// 刷新控件
class LyRefresh extends StatelessWidget {
  final RefreshController controller;
  final Widget? child;
  final VoidCallback? onRefresh;
  final VoidCallback? onLoading;
  final Color? bgColor;
  //是否保留原有样式
  final bool retain;
  final ScrollController? scrollController;
  final bool moreMessageVisible;

  const LyRefresh({
    Key? key,
    required this.controller,
    this.onRefresh,
    this.onLoading,
    this.bgColor,
    this.child,
    this.retain = false,
    this.moreMessageVisible = true,
    this.scrollController
  }) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return SmartRefresher(
      controller: controller,
      enablePullDown: onRefresh != null,
      enablePullUp: onLoading != null,
      scrollController: scrollController,
      // header: const MaterialClassicHeader(color: Colors.grey,backgroundColor: Colors.white,),
      footer: CustomFooter(
        builder: (BuildContext context, LoadStatus? mode) {
          Widget foot;
          if (mode == LoadStatus.idle) {
            foot = const Text("");
          } else if (mode == LoadStatus.loading) {
            foot = const CupertinoActivityIndicator();
          } else if (mode == LoadStatus.failed) {
            foot = const Text("加载失败！点击重试！");
          } else if (mode == LoadStatus.canLoading) {
            foot = const Text("松手,加载更多");
          } else {
            foot =Text(moreMessageVisible?"没有更多数据了":"");
          }
          return SizedBox(
            height: 55.0,
            child: Center(child: foot),
          );
        },
      ),
      onLoading: onLoading,
      onRefresh: onRefresh,
      child: _autoScroller(child),
    );
  }

  Widget? _autoScroller(Widget? child) {
    if (child is SingleChildScrollView ||
        child is ListView ||
        child is CustomScrollView ||
        child is GridView ||
        retain
    ) {
      return child;
    }
    return SingleChildScrollView(
      child: child,
    );
  }
}
