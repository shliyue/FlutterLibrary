import 'package:flutter/material.dart';
import 'package:flutter_screenutil/flutter_screenutil.dart';
import 'package:zyocore/values/style.dart';
import 'package:zyocore/widget/text/ly_text.dart';

import '../values/color.dart';

class LYActionText extends StatelessWidget {
  final VoidCallback? onTap;
  final String title;
  final bool enable;
  final bool showBg;
  const LYActionText({Key? key,this.onTap,required this.title,this.enable = false,this.showBg = false}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Center(
      child: GestureDetector(
        onTap: enable ? onTap : null,
        child: Container(
          padding: EdgeInsets.symmetric(horizontal: 16.w, vertical: 4.w),
          margin: showBg ? EdgeInsets.only(right: 16.w) : null,
          decoration: showBg ? BoxDecoration(
              borderRadius: BorderRadius.circular(20.r),
              color: enable ? LibColor.colorMain : themeC5B8FF) : null,
          child: LYText(
            text: title,
            color: showBg ? Colors.white : enable ? LibColor.colorMain : themeC5B8FF,
            fontSize: 15.sp,
            fontFamily: AppFontFamily.pingFangMedium,
          ),
        ),
      ),
    );
  }
}