import 'package:flutter/material.dart';
import 'package:zyocore/widget/index.dart';

import '../core/configs/config.dart';

class LYAssetsImage extends StatelessWidget {
  final String name;
  final double? radius;
  final double? width;
  final double? height;
  final BoxFit fit;
  String? package;
  final Color? color;
  final GestureTapCallback? onTap;

  LYAssetsImage(
      {Key? key, required this.name, this.radius, this.width, this.height, this.fit = BoxFit.fill, this.package, this.color,
        this.onTap})
      : super(key: key);

  @override
  Widget build(BuildContext context) {
    String url = "assets/images/$name";
    if (package == null) {
      package = Config.packageName;
    } else if (package == "") {
      package = null;
    }
    return LYImageWidget(
      src: url,
      radius: radius,
      width: width,
      height: height,
      fit: fit,
      color: color,
      isShowDefault: false,
      package: package,
      onTap: onTap,
    );
  }
}
