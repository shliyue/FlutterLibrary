import 'package:flutter/cupertino.dart';
import 'package:flutter_screenutil/flutter_screenutil.dart';
import '../../core/configs/config.dart';

class LYRadioIcon extends StatelessWidget {
  final int status;
  final int colorType;
  final String? packageName;
  final double width;

  const LYRadioIcon({Key? key, this.status = 0, this.colorType = 1, this.packageName, this.width = 22}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    String? package;
    if (packageName == null) {
      package = Config.packageName;
    } else if (packageName == "") {
      package = null;
    } else {
      package = packageName;
    }
    return Image.asset(
      'assets/images/public/icon_radio_${status == 1 ? 1 : (colorType == 1 ? '0' : "f0")}.png',
      width: width.w,
      package: package,
    );
  }
}
