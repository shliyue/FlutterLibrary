import 'package:flutter/material.dart';
import 'package:flutter_screenutil/flutter_screenutil.dart';

import '../../core/configs/config.dart';

class ArrowRightIcon extends StatelessWidget {
  const ArrowRightIcon({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Image.asset('assets/images/icon_arrow_right.png',width: 8.w,height: 12.h,package: Config.packageName,);
  }
}
