import 'package:flutter/material.dart';
import 'package:zyocore/zyocore.dart';

class LyVideoFullScreenState {
  RxString resourceId = ''.obs;

  var hideCollect = false;

  /// 隐藏分享
  var hideShare = false;

  int position = 0;

  // var resourceType;
  /// 是否在前台
  var resume = true;

  /// 标题栏
  // RxString title = "".obs;
  RxString content = "".obs;

  /// 当前资源
  var resourcePath = ''.obs;
  var title = ''.obs;

  /// 循环次数
  var loopCount = 0;

  /// 视频是否全屏
  RxBool fullScreened = true.obs;
  RxBool isInitialized = false.obs;
  RxBool isEnded = false.obs;
  RxBool isPlaying = true.obs;

  /// 是否显示控制栏
  var showController = false;

  /// 控制栏位置
  RxDouble top = (-kToolbarHeight).obs;
  RxDouble bottom = (-kToolbarHeight).obs;

  /// 屏幕的实际方向
  var deviceOrientation = Orientation.portrait;

  ResourceVideoState() {
    ///Initialize variables
  }
}
