import 'dart:async';
import 'dart:io';

import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:zyocore/values/event_bus_keys.dart';
import 'package:zyocore/widget/video_bar/video_player_option.dart';
import 'package:zyocore/zyocore.dart';
import 'state.dart';

class LyVideoFullScreenLogic extends GetxController
    with GetSingleTickerProviderStateMixin {
  final LyVideoFullScreenState state = LyVideoFullScreenState();

  VideoPlayerController? videoController;
  late VideoPlayOptions? playOptions;
  late BuildContext viewContext;

  // late StreamSubscription ssLifeCircleEvent;

  AnimationController? controlBarAnimationController;
  late Animation<double> controlTopBarAnimation;
  late Animation<double> controlBottomBarAnimation;

  late IosOrientation iosOrientationPlugin;

  Timer? aniTimer;

  late StreamSubscription ssResume;
  late StreamSubscription ssPause;

  @override
  void onClose() {
    _resetVideoConfig();
    // videoController?.removeListener(listenVideoState);
    // videoController?.dispose();
    ssResume.cancel();
    ssPause.cancel();
    aniTimer?.cancel();
    super.onClose();
  }

  @override
  void onInit() {
    /// 开始进入的时间
    iosOrientationPlugin = IosOrientation();
    var args = Get.arguments;
    if (args != null) {
      state.title.value = args['title'];
      state.resourcePath.value = args['resourcePath'];
      state.position = args["position"] ?? 0;
      videoController = args["videoController"];
    }

    /// 配置视频页面状态栏及返回
    _initVideoConfig();
    toggleFullScreen();
    _initEvent();

    /// 初始化数据,有些从课程详情进入，有些从分类进入
    _initVideo();

    _initAnimationController();

    super.onInit();
  }

  _initAnimationController() {
    /// 控制拦动画
    controlBarAnimationController = AnimationController(
        lowerBound: -kToolbarHeight,
        upperBound: 0,
        duration: const Duration(milliseconds: 300),
        vsync: this);
    controlBarAnimationController!.addListener(() {
      Logger.d('top>>>addListener=${controlBarAnimationController!.value}');
      state.top.value = controlBarAnimationController!.value;
      state.bottom.value = controlBarAnimationController!.value;
    });
  }

  /// 菜单栏显示与隐藏
  toggleControls() {
    _clearHideControlBarTimer();
    if (!state.showController) {
      state.showController = true;
      _createHideControlBarTimer();
    } else {
      state.showController = false;
    }
    if (state.showController) {
      controlBarAnimationController?.forward();
    } else {
      controlBarAnimationController?.reverse();
    }
  }

  _createHideControlBarTimer() {
    _clearHideControlBarTimer();

    ///如果是播放状态3秒后自动隐藏
    aniTimer = Timer(const Duration(milliseconds: 5000), () {
      if (videoController != null && videoController!.value.isPlaying) {
        if (state.showController) {
          state.showController = false;
          controlBarAnimationController?.reverse();
        }
      }
    });
  }

  _clearHideControlBarTimer() {
    aniTimer?.cancel();
  }

  _initEvent() {
    // ssLifeCircleEvent =
    //     EventUtil.instance.on<AppLifeCircleEvent>().listen((event) {
    //   Logger.d(
    //       '==ResourceVideoLogic==========event.resumed===${event.resumed}');
    //   switchVideo(toPlay: event.resumed);
    // });
    ssResume =
        lyBus.on(eventName: EventBusKeys.pageResume).listen(onPageResume);
    ssPause = lyBus.on(eventName: EventBusKeys.pagePause).listen(onPagePause);
  }

  /// 监听屏幕旋转
  onScreenChange(Orientation orientation) {
    var portrait = orientation == Orientation.portrait;
    Logger.d(
        'ResourceVideoLogic=onScreenChange=state.resume=${state.resume}');
    state.deviceOrientation = orientation;
    if (state.resume) {
      Logger.d('onScreenChange=portrait=$portrait');
      state.fullScreened.value = !portrait ? true : false;
    }
  }

  _initVideoConfig() async {
    SystemChrome.setEnabledSystemUIMode(
        Platform.isAndroid ? SystemUiMode.manual : SystemUiMode.edgeToEdge,
        overlays: []);
    SystemChrome.setSystemUIOverlayStyle(SystemUiOverlayStyle.light);
    // if (Platform.isIOS) {
    //   SystemChrome.setPreferredOrientations([
    //     DeviceOrientation.portraitUp,
    //     DeviceOrientation.landscapeLeft,
    //     DeviceOrientation.landscapeRight,
    //   ]);
    // }
    SystemChrome.setPreferredOrientations([
      DeviceOrientation.portraitUp,
      DeviceOrientation.landscapeLeft,
      DeviceOrientation.landscapeRight,
    ]);
  }

  _resetVideoConfig() {
    ///竖屏
    SystemChrome.setEnabledSystemUIMode(
        Platform.isAndroid ? SystemUiMode.manual : SystemUiMode.edgeToEdge,
        overlays: [
          SystemUiOverlay.top,
          SystemUiOverlay.bottom,
        ]);
    SystemChrome.setSystemUIOverlayStyle(SystemUiOverlayStyle.dark);

    // if (Platform.isIOS) {
    //   SystemChrome.setPreferredOrientations([
    //     DeviceOrientation.portraitUp,
    //   ]);
    // }
    SystemChrome.setPreferredOrientations([
      DeviceOrientation.portraitUp,
    ]);
  }

  _initVideo() async {
    // if (videoController != null) {
    //   videoController!.removeListener(listenVideoState);
    //   videoController = null;
    // }
    VideoPlayOptions playOptions = VideoPlayOptions(
      seekSeconds: 10,
      aspectRatio: 16 / 9,
      loop: true,
      autoplay: true,
      allowScrubbing: false,
      startPosition: Duration(seconds: state.position),
    );
    this.playOptions = playOptions;
    if (videoController == null) {
      videoController = createVideoPlayerController()
        ..initialize().then(handleInit)
        ..setLooping(playOptions.loop ?? false)
        ..addListener(listenVideoState);
    } else {
      handleInit(null);
      videoController!.setLooping(playOptions.loop ?? false);
      videoController!.addListener(listenVideoState);
    }
  }

  /// 页面pop/push切换
  onPageResume(LyEvent data) {
    ///其他页面pop后回到课程详情
    state.resume = true;
    if (state.isPlaying.value) {
      videoController!.play();
    }

    /// 横屏后，如果手机当前竖屏，则直接竖屏（受当前手机的实际屏幕方向的影响）
    if (state.deviceOrientation == Orientation.portrait &&
        state.fullScreened.value &&
        Platform.isIOS) {
      setFullScreened();
    }
    // /// 横屏后，如果手机当前竖屏，先竖屏，再立即直接横屏（（受当前手机的实际屏幕方向的影响）
    // if (state.fullScreened.value && Platform.isIOS) {
    //   toggleFullScreen();
    // }
  }

  /// 页面pop/push切换
  onPagePause(LyEvent data) {
    ///其他页面pop后回到课程详情
    state.resume = false;

    ///课程详情push到其他页面
    if (state.isPlaying.value) {
      videoController!.pause();
    }
  }

  /// 监听视频
  listenVideoState() async {
    if (state.isInitialized.value && state.isPlaying.value) {
      bool isOnEnd = videoController!.value.position.inSeconds >=
          videoController!.value.duration.inSeconds;
      if (isOnEnd) {
        state.loopCount++;
      }
      if (state.loopCount > 0 &&
          videoController!.value.position.inSeconds < 1) {
        state.isPlaying.value = false;
        await videoController!.pause();
        state.loopCount = 0;
      }
    }
  }

  handleInit(_) {
    state.isInitialized.value = true;
    if (playOptions!.autoplay!) {
      if (playOptions!.startPosition.inSeconds != 0) {
        videoController!.seekTo(playOptions!.startPosition);
      }
      videoController!.play();
      state.isEnded.value = false;
    }
  }

  void toggleFullScreen() {
    // Logger.d('toggleFullScreen=fullScreened=${state.fullScreened.value}');
    if (state.fullScreened.value) {
      if (Platform.isIOS) {
        Future.delayed(const Duration(milliseconds: 10), () {
          iosOrientationPlugin.setOrientation(OrientationIOS.landscapeLeft);
        });
      } else {
        // OrientationPlugin.forceOrientation(DeviceOrientation.landscapeRight);
        SystemChrome.setPreferredOrientations([
          DeviceOrientation.landscapeRight,
        ]);
      }
    } else {
      if (Platform.isIOS) {
        Future.delayed(const Duration(milliseconds: 10), () {
          iosOrientationPlugin.setOrientation(OrientationIOS.portraitUp);
        });
      } else {
        SystemChrome.setPreferredOrientations([
          DeviceOrientation.portraitUp,
        ]);
        // OrientationPlugin.forceOrientation(DeviceOrientation.portraitUp);
      }
    }
  }

  /// 播放或者暂停
  switchPlay() {
    state.isPlaying.value = !state.isPlaying.value;
    if (state.isPlaying.value) {
      bool isOnEnd = videoController!.value.position.inSeconds >=
          videoController!.value.duration.inSeconds;
      if (isOnEnd) {
        videoController!.seekTo(Duration.zero);
      }
      videoController!.play();
    } else {
      videoController!.pause();
    }

    /// 控制状态栏
    _createHideControlBarTimer();
  }

  /// 点击全屏或取消
  setFullScreened() {
    Logger.d(
        'setFullScreened=before=state.fullScreened.value=${state.fullScreened.value}');
    state.fullScreened.value = state.fullScreened.value ? false : true;

    /// 须去掉，否则影响部分模块顶部标题栏展示
    // if (state.fullScreened.value) {
    //   SystemChrome.setEnabledSystemUIMode(SystemUiMode.immersiveSticky,
    //       overlays: []);
    // } else {
    //   SystemChrome.setEnabledSystemUIMode(SystemUiMode.edgeToEdge);
    // }
    toggleFullScreen();
  }

  String get processing {
    String position = "00:00";
    if (videoController!.value.isInitialized) {
      var oPosition = videoController!.value.position;

      if (oPosition.inHours == 0) {
        var strPosition = oPosition.toString().split('.')[0];
        position = "${strPosition.split(':')[1]}:${strPosition.split(':')[2]}";
      } else {
        position = oPosition.toString().split('.')[0];
      }
    }
    return position;
  }

  String get duration {
    String duration = "--:--";
    if (videoController!.value.isInitialized) {
      var oDuration = videoController!.value.duration;
      if (oDuration.inHours == 0) {
        var strDuration = oDuration.toString().split('.')[0];
        duration = "${strDuration.split(':')[1]}:${strDuration.split(':')[2]}";
      } else {
        duration = oDuration.toString().split('.')[0];
      }
    }
    return duration;
  }

  /// 创建video controller
  VideoPlayerController createVideoPlayerController() {
    bool isHttp = (state.resourcePath.value ?? '').indexOf("http") == 0;
    return isHttp
        ? VideoPlayerController.network(state.resourcePath.value)
        : VideoPlayerController.file(File(state.resourcePath.value));
  }

  handleBack() {
    if (state.fullScreened.value) {
      setFullScreened();
    } else {
      Get.back();
    }
  }

  Future<bool> handleWillPop() async {
    if (Navigator.canPop(viewContext)) {
      handleBack();
    }
    return false;
  }
}
