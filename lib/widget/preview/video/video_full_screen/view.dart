import 'dart:io';

import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:zyocore/core/configs/config.dart';
import 'package:zyocore/values/edge_insets.dart';
import 'package:zyocore/widget/common/flex/column.dart';
import 'package:zyocore/widget/common/flex/row.dart';
import 'package:zyocore/widget/preview/video/video_full_screen/logic.dart';
import 'package:zyocore/widget/text/ly_text.dart';
import 'package:zyocore/values/text.dart';
import 'package:zyocore/widget/video_bar/video_progress_bar.dart';
import 'package:zyocore/zyocore.dart';

/// 全屏播放-旋转屏幕
class LyVideoFullScreenPage extends GetView<LyVideoFullScreenLogic> {
  const LyVideoFullScreenPage({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    controller.viewContext = context;
    return WillPopScope(
      onWillPop: () async {
        return false;
      },
      child: OrientationBuilder(
        builder: (context, orientation) {
          controller.onScreenChange(orientation);
          return Obx(
            () {
              return Platform.isAndroid
                  ? Scaffold(
                      backgroundColor: Colors.black,
                      body: CSS(
                        children: [
                          _appBarWidget,
                          _contentWrapper,
                        ],
                      ),
                    )
                  : AnnotatedRegion(
                      value: SystemUiOverlayStyle.light,
                      child: Scaffold(
                        backgroundColor: Colors.black,
                        body: CSS(
                          children: [
                            _appBarWidget,
                            _contentWrapper,
                          ],
                        ),
                      ),
                    );
            },
          );
        },
      ),
    );
  }

  Widget get _appBarWidget {
    if (controller.state.fullScreened.value) {
      return Container();
    }
    return _appBar;
  }

  Widget get _appBar {
    return Container(
      color: dark1A1A1A.withOpacity(0.1),
      height: kToolbarHeight,
      margin: EdgeInsets.only(top: ScreenUtil().statusBarHeight),
      child: RBC(
        children: [
          _backWidget,
          Container(),
        ],
      ),
    );
  }

  Widget get _backWidget {
    return RSC(
      mainAxisSize: MainAxisSize.min,
      children: [
        LYButtonUtil.backButton(
            enumIconColor: EnumIconColor.white,
            backTap: () {
              controller.handleBack();
            }),
        if ((controller.state.title.value ?? '').isNotEmpty)
          Container(
            constraints: BoxConstraints(maxWidth: 170.w),
            child: LYText.withStyle(
              controller.state.title.value ?? '',
              style: text17,
              maxLines: 1,
              overflow: TextOverflow.ellipsis,
            ),
          )
      ],
    );
  }

  Widget get _contentWrapper {
    if (controller.state.fullScreened.value) {
      return _contentFullScreen;
    } else {
      return _content;
    }
  }

  Widget get _content {
    return Expanded(
      child: Container(
        color: Colors.black,
        alignment: Alignment.center,
        child: videoBarWrapper,
      ),
    );
  }

  Widget get videoBarWrapper {
    return Container(
      padding: !controller.state.isInitialized.value ? bottom1 : bottom20,
      child: CSS(
        mainAxisSize: MainAxisSize.min,
        children: [
          AspectRatio(
            aspectRatio: 16 / 9,

            /// build 所有video组件
            child: videoPlayer,
          ),
          _bottomBarWidget,
        ],
      ),
    );
  }

  Widget get _fullScreenAppBar {
    return Positioned(
      left: 0,
      right: 0,
      top: controller.state.top.value,
      child: Container(
        margin: horizontal10,
        child: _appBar,
      ),
    );
  }

  Widget get _contentFullScreen {
    return GestureDetector(
      onTap: () {
        controller.toggleControls();
      },
      child: Container(
        color: Colors.black,
        child: AspectRatio(
          aspectRatio: DeviceUtil.calculateAspectRatio(),

          /// build 所有video组件
          child: Stack(children: [
            /// 视频区域
            videoPlayer,
            _fullScreenAppBar,
            _bottomBarFullScreen,
          ]),
        ),
      ),
    );
  }

  Widget get videoPlayer {
    return Container(
        color: Colors.black,
        child: Center(
          child: controller.state.isInitialized.value && controller.videoController != null
              ? AspectRatio(
                  aspectRatio: controller.videoController!.value.aspectRatio,
                  child: VideoPlayer(controller.videoController!),
                )
              : _videoCover,
        ));
  }

  Widget get _videoCover {
    return AspectRatio(
        aspectRatio: DeviceUtil.calculateAspectRatio(),
        child: Image(
          image: AssetImage('assets/images/video_enlarge.png', package: Config.packageName),
          width: ScreenUtil().screenWidth,
          fit: BoxFit.fitWidth,
        ));
  }

  Widget get _bottomBarFullScreen {
    return Positioned(
      left: 0,
      right: 0,
      bottom: controller.state.bottom.value,
      child: Container(
        margin: horizontal10,
        alignment: Alignment.center,
        child: _bottomBar,
      ),
    );
  }

  Widget get _bottomBarWidget {
    return Container(
      margin: top12,
      child: _bottomBar,
    );
  }

  Widget get _bottomBar {
    if (controller.videoController == null) return Container();
    return Container(
      color: dark1A1A1A.withOpacity(0.1),
      height: kToolbarHeight,
      alignment: Alignment.centerLeft,
      child: ValueListenableBuilder(
        valueListenable: controller.videoController!,
        builder: (BuildContext context, value, Widget? child) {
          return RSC(
            children: [
              GestureDetector(
                onTap: () {
                  controller.switchPlay();
                },
                child: Container(
                  padding: EdgeInsets.only(
                    left: controller.state.fullScreened.value ? 20 : 10,
                    right: 10.0,
                  ),
                  child: Image(
                    image: (controller.state.isPlaying.value)
                        ? AssetImage('assets/images/audio_pause.png', package: Config.packageName)
                        : AssetImage('assets/images/audio_play.png', package: Config.packageName),
                    width: 24,
                    height: 24,
                  ),
                ),
              ),
              Expanded(
                child: SizedBox(
                  height: 24,
                  child: VideoProgressBar(
                    controller.videoController!,
                  ),
                ),
              ),
              const SizedBox(
                width: 10.0,
              ),

              /// 当前播放时间
              Padding(
                padding: left10,
                child: LYText.withStyle(
                  '${controller.processing}/${controller.duration}',
                  style: const TextStyle(fontSize: 11, color: lightFFFFFF),
                ),
              ),
              GestureDetector(
                onTap: () {
                  controller.setFullScreened();
                },
                child: Container(
                  color: Colors.transparent,
                  padding: EdgeInsets.only(left: 10, right: controller.state.fullScreened.value ? 26 : 16),
                  height: 44,
                  child: Image(
                    image: (controller.state.fullScreened.value)
                        ? AssetImage('assets/images/video_narrow.png', package: Config.packageName)
                        : AssetImage('assets/images/video_enlarge.png', package: Config.packageName),
                    width: 24,
                    height: 24,
                  ),
                ),
              ),
            ],
          );
        },
      ),
    );
  }
}
