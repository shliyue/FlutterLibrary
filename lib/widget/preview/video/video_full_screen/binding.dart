import 'package:get/get.dart';

import 'logic.dart';

class LyVideoFullScreenBinding extends Bindings {
  @override
  void dependencies() {
    Get.lazyPut(() => LyVideoFullScreenLogic());
  }
}
