import 'package:flutter/foundation.dart';
import 'package:flutter/material.dart';
import 'package:zyocore/widget/preview/video/controller/ly_video_progress_bar_controller.dart';

class LyVideoProgress extends StatefulWidget {
  LyVideoProgress(
      {Key? key,
        ProgressColors? colors,
        required this.controller,
        required this.barHeight,
        this.handleHeight = 6,
        this.onDragStart,
        this.onDragEnd,
        this.onDragUpdate,
        this.onTapDown})
      : colors = colors ?? ProgressColors(),
        super(key: key);

  final ProgressColors colors;
  final Function()? onDragStart;
  final Function(double progress)? onDragEnd;
  final Function()? onDragUpdate;
  final Function(double progress)? onTapDown;

  final double barHeight;
  final double handleHeight;

  final LyVideoProgressBarController controller;

  @override
  State<LyVideoProgress> createState() => _LyVideoProgressState();
}

class _LyVideoProgressState extends State<LyVideoProgress> {
  double progress = .0;

  @override
  void initState() {
    // TODO: implement initState
    super.initState();
    progress = widget.controller.progressBarValue;
    widget.controller.addListener(_updateProgressValue);
  }

  @override
  void dispose() {
    widget.controller.removeListener(_updateProgressValue);
    super.dispose();
  }

  _updateProgressValue() {
    setState(() {
      progress = widget.controller.progressBarValue;
    });
  }

  void _seekToRelativePosition(Offset globalPosition) {
    final box = context.findRenderObject()! as RenderBox;
    final Offset tapPos = box.globalToLocal(globalPosition);
    progress = tapPos.dx / box.size.width;
    if (progress < 0) progress = 0;
    if (progress > 1) progress = 1;

    setState(() {
      widget.controller.progressBarValue = progress;
    });
  }

  @override
  Widget build(BuildContext context) {

    return GestureDetector(
      onHorizontalDragStart: (details) {
        widget.onDragStart?.call();
      },
      onHorizontalDragUpdate: (details) {
        _seekToRelativePosition(details.globalPosition);
        widget.onDragUpdate?.call();
      },
      onHorizontalDragEnd: (details) {
        widget.onDragEnd?.call(progress);
      },
      onTapDown: (details) {
        _seekToRelativePosition(details.globalPosition);
        widget.onTapDown?.call(progress);
      },
      child: Center(
        child: Container(
          height: MediaQuery.of(context).size.height,
          width: MediaQuery.of(context).size.width,
          child: CustomPaint(
            painter: _VideoProgressBarPainter(
                barHeight: widget.barHeight,
                handleHeight: widget.handleHeight,
                colors: widget.colors,
                value: progress),
          ),
        ),
      ),
    );
  }
}

class _VideoProgressBarPainter extends CustomPainter {
  _VideoProgressBarPainter({
    required this.barHeight,
    required this.handleHeight,
    required this.colors,
    required this.value,
  });

  final double barHeight;
  final double handleHeight;
  final ProgressColors colors;
  final double value;

  @override
  bool shouldRepaint(CustomPainter painter) {
    return true;
  }

  @override
  void paint(Canvas canvas, Size size) {
    // TODO: implement paint
    final double baseOffset = size.height / 2 - barHeight / 2;
    final double radius = 4.0;

    canvas.drawRRect(
        RRect.fromRectAndRadius(
          Rect.fromPoints(
            Offset(.0, baseOffset),
            Offset(size.width, baseOffset + barHeight),
          ),
          const Radius.circular(4.0),
        ),
        colors.backgroundPaint);

    double playedPart =
    value > 1 ? size.width - radius : value * size.width - radius;
    if (playedPart < radius) {
      playedPart = radius;
    }

    canvas.drawRRect(
        RRect.fromRectAndRadius(
          Rect.fromPoints(
            Offset(.0, baseOffset),
            Offset(playedPart, baseOffset + barHeight),
          ),
          Radius.circular(radius),
        ),
        colors.playedPaint);

    canvas.drawCircle(
      Offset(playedPart, baseOffset + barHeight / 2),
      handleHeight,
      colors.playedPaint,
    );
  }
}

class ProgressColors {
  ProgressColors({
    Color playedColor = const Color(0xffAA95FF),
    Color bufferedColor = const Color.fromRGBO(30, 30, 200, 0.2),
    Color handleColor = const Color.fromRGBO(200, 200, 200, 1.0),
    Color backgroundColor = const Color.fromRGBO(200, 200, 200, 0.5),
  })  : playedPaint = Paint()..color = playedColor,
        bufferedPaint = Paint()..color = bufferedColor,
        handlePaint = Paint()..color = handleColor,
        backgroundPaint = Paint()..color = backgroundColor;
  final Paint playedPaint;
  final Paint bufferedPaint;
  final Paint handlePaint;
  final Paint backgroundPaint;
}
