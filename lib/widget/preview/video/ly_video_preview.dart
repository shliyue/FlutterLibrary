import 'dart:async';
import 'dart:io';

import 'package:flutter/material.dart';
import 'package:wakelock_plus/wakelock_plus.dart';
import 'package:zyocore/core/configs/config.dart';
import 'package:zyocore/values/event_bus_keys.dart';
import 'package:zyocore/widget/common/image_placeholder_widget.dart';
import 'package:zyocore/zyocore.dart';

import '../../../util/cache_manager_util.dart';

// 支持网络视频 + 本地视频AssetEntity
class LyVideoPreview extends StatefulWidget {
  /// 资源地址
  final dynamic src;

  /// 点击回调
  final VoidCallback? onTap;

  /// 是否自动播放
  final bool isAutoPlay;

  /// 是否全屏
  final bool isFullScreen;

  /// 视频标题
  final String title;

  /// 全屏点击回调
  final VoidCallback? fullScreenCallback;

  /// 播放状态变化回调
  final ValueChanged<bool>? playStatusChanged;

  /// 控制器
  final LyVideoPreviewController? controller;

  /// 播放进度回调
  final ValueChanged<int>? positionChanged;

  const LyVideoPreview({
    Key? key,
    required this.src,
    this.onTap,
    this.isAutoPlay = true,
    this.isFullScreen = false,
    this.title = '',
    this.fullScreenCallback,
    this.playStatusChanged,
    this.controller,
    this.positionChanged,
  }) : super(key: key);

  @override
  State<LyVideoPreview> createState() => _LyVideoPreviewState();
}

class _LyVideoPreviewState extends State<LyVideoPreview> with TickerProviderStateMixin {
  VideoPlayerController? videoPlayerController;

  ChewieController? chewieController;
  int _p = 0;
  int _d = 0;
  String _ps = '00:00';
  String _ds = '00:00';
  bool playing = true;
  late StreamSubscription lyBusPlayer;

  LyVideoProgressBarController videoProgressBarController = LyVideoProgressBarController();

  @override
  void initState() {
    super.initState();

    Logger.d('initState src = ${widget.src}');

    /// 防止进入熄屏状态
    WakelockPlus.enable();
    initVideoController();
    _ds = utils.date.secondToTime(_d);

    // 控制器
    if (widget.controller != null) {
      widget.controller?.playStatusChanged = (isPlaying) {
        if (isPlaying) {
          videoPlayerController?.play();
        } else {
          videoPlayerController?.pause();
        }
        playing = isPlaying;
      };
      widget.controller?.positionStatusChanged = (position) {
        videoPlayerController?.seekTo(Duration(milliseconds: position));
      };
      widget.controller?.positionCallback = () {
        return videoPlayerController?.value.position.inSeconds ?? 0;
      };
    }
    playing = widget.isAutoPlay;
    //监听音频播放
    lyBusPlayer = lyBus.on(eventName: EventBusKeys.lyVideoPlayer).listen(eventBusPlayer);
  }

  void eventBusPlayer(LyEvent event) {
    if (event.data == "stop") {
      videoPlayerController?.pause();
    } else if (event.data == "play") {
      videoPlayerController?.play();
    }
  }

  void initVideoController() async {
    if (widget.src is String) {
      initNetVideoController();
    } else if (widget.src is AssetEntity) {
      initAssetVideoController();
    }
  }

  void initNetVideoController() async {
    Logger.d('initNetVideoController...');
    var f = await CacheManagerUtil().getFileFromCache(widget.src);
    if (f == null) {
      if(!FileUtil.isM3U8Video(widget.src)){
        bool flag = await LyPermission.storage();
        if (flag) {
          Logger.d('++++++++++++++LyVideoPreview 视频缓存+++++++++++++++++++++++++++++++');
          CacheManagerUtil().downloadFile(widget.src).then((v) => Logger.d('++++++++++++++LyVideoPreview 视频缓存 成功+++++++++++${v.file.path}++++++++++++++++++++'));
        }
      }
      videoPlayerController = VideoPlayerController.networkUrl(Uri.parse(FileUtil.m3u8formatVideoSrc(widget.src)));
    } else {
      Logger.d('++++++++++++++LyVideoPreview 播放缓存++++++++++++${f.file.path}+++++++++++++++++++');
      videoPlayerController = VideoPlayerController.file(f.file);
    }
    videoPlayerController!.initialize().then((_) {
      videoPlayerController?.addListener(() {
        _updateState();
      });
      _initChewieController();
      if (mounted) setState(() {});
      _updateState();
    }).catchError((e) {
      ToastUtil.showToast('加载失败~');
    });
  }

  void initAssetVideoController() async {
    Logger.d('initAssetVideoController...');
    File? file;
    // if (Platform.isIOS) {
    //   String? assetUrl = await (widget.src as AssetEntity).getMediaUrl();
    //   if (assetUrl == null) {
    //     return;
    //   }
    //   file = File(assetUrl);
    // } else if (Platform.isAndroid) {
    //   file = await (widget.src as AssetEntity).file;
    // }
    file = await (widget.src as AssetEntity).originFile;
    if (file == null) {
      return;
    }
    videoPlayerController = VideoPlayerController.file(file)
      ..initialize().then((_) {
        videoPlayerController?.addListener(_videoPlayerListener);
        _initChewieController();
        if (mounted) {
          setState(() {});
        }
      }).catchError((e) {
        ToastUtil.showToast('加载失败~');
      });
  }

  void _videoPlayerListener() {
    _updateState();
  }

  void _updateState() {
    playing = videoPlayerController?.value.isPlaying ?? false;
    _d = videoPlayerController?.value.duration.inSeconds ?? 1;
    _p = videoPlayerController?.value.position.inSeconds ?? 1;
    _ds = utils.date.secondToTime(_d);
    videoProgressBarController.updateProgressValue(_p / _d);
    _ps = utils.date.secondToTime(_p);
    if (mounted) {
      setState(() {});
    }
    if (widget.positionChanged != null) {
      widget.positionChanged!(_p);
    }
  }

  onFullScreenTap() async {
    if (widget.fullScreenCallback != null) {
      widget.fullScreenCallback!();
    } else {
      Get.toNamed(Config.videoFullScreen, arguments: {
        'title': widget.title,
        'resourcePath': widget.src,
        'videoController': videoPlayerController,
        'position': videoPlayerController?.value.position.inSeconds ?? 0,
      });
    }
  }

  /// 初始化ChewieController
  void _initChewieController() {
    chewieController = ChewieController(
      videoPlayerController: videoPlayerController!,
      aspectRatio: videoPlayerController!.value.aspectRatio,
      showOptions: false,
      maxScale: 1,
      autoPlay: widget.isAutoPlay,
      allowFullScreen: widget.isFullScreen,
      showControls: false,
      materialProgressColors: ChewieProgressColors(
        bufferedColor: Colors.white,
        playedColor: theme9F89FF,
      ),
      cupertinoProgressColors: ChewieProgressColors(
        bufferedColor: Colors.white,
        playedColor: theme9F89FF,
      ),
    );
  }

  Widget _customControls() {
    return Positioned(
      bottom: 0.0.w,
      left: 0.0.w,
      right: 0.0.w,
      child: GestureDetector(
        onTap: () {},
        child: Container(
          height: MediaQuery.of(context).orientation == Orientation.portrait ? 44.w : 44.h,
          decoration: BoxDecoration(
            gradient: LinearGradient(
              colors: [const Color(0xFF000000).withOpacity(0), const Color(0xFF000000).withOpacity(0.5)],
              begin: Alignment.topCenter,
              end: Alignment.bottomCenter,
            ),
          ),
          alignment: Alignment.center,
          padding: EdgeInsets.symmetric(
            horizontal: MediaQuery.of(context).orientation == Orientation.portrait ? 8.w : 8.h,
          ),
          width: Get.width,
          child: Row(
            children: [
              GestureDetector(
                onTap: onTapPlay,
                child: Icon(
                  playing ? Icons.pause : Icons.play_arrow_rounded,
                  size: 20,
                  color: Colors.white,
                ),
              ),
              SizedBox(
                width: ScreenUtil().orientation == Orientation.portrait ? 8.w : 8.h,
              ),
              Expanded(
                child: LyVideoProgress(
                  controller: videoProgressBarController,
                  barHeight: 2,
                  onDragUpdate: () {
                    Logger.d('onDragUpdate....');
                    var progress = videoProgressBarController.progressBarValue;
                    int milliseconds = (progress * _d * 1000).toInt();
                    videoPlayerController?.seekTo(Duration(milliseconds: milliseconds));
                  },
                  onTapDown: (progress) {
                    Logger.d('onTapDown....$progress');
                    int milliseconds = (progress * _d * 1000).toInt();
                    videoPlayerController?.seekTo(Duration(milliseconds: milliseconds));
                  },
                ),
              ),
              SizedBox(
                width: ScreenUtil().orientation == Orientation.portrait ? 8.w : 8.h,
              ),
              LYText(
                text: '$_ps/$_ds',
                fontSize: 12,
                color: Colors.white,
              ),
              if (widget.isFullScreen)
                Container(
                  margin: EdgeInsets.only(left: 8.0.w),
                  child: GestureDetector(
                    onTap: onFullScreenTap,
                    child: Image.asset(
                      "assets/images/video_enlarge.png",
                      package: Config.packageName,
                      width: 20.0,
                    ),
                  ),
                ),
            ],
          ),
        ),
      ),
    );
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      backgroundColor: const Color.fromRGBO(0, 0, 0, 1),
      body: Stack(
        alignment: AlignmentDirectional.center,
        children: <Widget>[
          // 视频组件
          chewieController != null ? _buildChewie() : _buildPlaceholder()
        ],
      ),
    );
  }

  Widget _buildPlaceholder() {
    return Stack(
      alignment: AlignmentDirectional.center,
      children: [
        if (widget.src is String)
          Center(
            child: CachedNetworkImage(
              fit: BoxFit.contain,
              imageUrl: FileUtil.getQiuVideoImage(
                widget.src,
              ),
              placeholder: (context, url) {
                return const ImagePlaceholderWidget(
                  type: 0,
                  state: 0,
                );
              },
              errorWidget: (a, b, c) => const ImagePlaceholderWidget(
                type: 0,
                state: 1,
              ),
            ),
          ),
        if (widget.src is AssetEntity)
          Center(
            child: Image(
              width: Get.width,
              image: AssetEntityImageProvider(widget.src, isOriginal: false),
              fit: BoxFit.cover,
            ),
          ),
        const CircularProgressIndicator(
          color: theme9F89FF,
        ),
      ],
    );
  }

  Widget _buildChewie() {
    return Stack(
      alignment: AlignmentDirectional.center,
      children: [
        GestureDetector(
          onTap: onTapPlay,
          behavior: HitTestBehavior.translucent,
          child: Center(
            child: Chewie(
              controller: chewieController!,
            ),
          ),
        ),
        _customControls(),
        GestureDetector(
          onTap: onTapPlay,
          child: Visibility(
            visible: !playing,
            child: Image.asset(
              'assets/images/icon_video_play.png',
              package: Config.packageName,
              width: 60,
            ),
          ),
        ),
      ],
    );
  }

  void onTapPlay() async {
    if (widget.onTap != null) {
      widget.onTap!();
    }

    if (videoPlayerController?.value.isPlaying ?? false) {
      videoPlayerController?.pause();
    } else {
      if (Platform.isIOS) {
        /// 有的视频播放完毕position ！=duration
        if ((videoPlayerController?.value.position.inSeconds ?? 0) - (videoPlayerController?.value.duration.inSeconds ?? 0) <= 1) {
          videoPlayerController?.seekTo(Duration.zero);
          setState(() {});
        }
      }
      videoPlayerController?.play();
      // if (!widget.isAutoPlay) {
      //   var connectivityResult = await (Connectivity().checkConnectivity());
      //   if (connectivityResult == ConnectivityResult.mobile) {
      //     ToastUtil.showToast("当前正在使用流量播放视频",
      //         duration: const Duration(seconds: 1));
      //   }
      // }
    }
    setState(() {
      playing = videoPlayerController?.value.isPlaying ?? false;
    });
    if (widget.playStatusChanged != null) {
      widget.playStatusChanged!(playing);
    }
  }

  void disposeVideoResource() {
    videoPlayerController?.removeListener(_videoPlayerListener);
    videoPlayerController?.dispose();
    chewieController?.dispose();
    videoPlayerController = null;
  }

  @override
  void dispose() {
    /// 取消防止进入熄屏状态
    WakelockPlus.disable();
    disposeVideoResource();
    ToastUtil.dismiss();
    lyBusPlayer.cancel();
    super.dispose();
  }

  @override
  void didUpdateWidget(covariant LyVideoPreview oldWidget) {
    // TODO: implement didUpdateWidget
    super.didUpdateWidget(oldWidget);
    if (widget.controller != null) return;
    setState(() {
      disposeVideoResource();
      initVideoController();
    });
  }
}
