import 'package:flutter/material.dart';

class LyVideoProgressBarController extends ChangeNotifier {
  double progressBarValue = .0;

  updateProgressValue(double value) {
    progressBarValue = value;
    notifyListeners();
  }
}