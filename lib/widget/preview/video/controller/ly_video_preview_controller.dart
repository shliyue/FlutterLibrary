import 'package:flutter/material.dart';

typedef PositionCallBack = int Function();

/// 视频预览控制器
class LyVideoPreviewController {
  /// 播放状态变化回调
  ValueChanged<bool>? playStatusChanged;

  /// 进度变化回调
  ValueChanged<int>? positionStatusChanged;

  PositionCallBack? positionCallback;

  /// 更新播放状态
  void updatePlayStatus({required bool isPlaying}) {
    if (playStatusChanged != null) {
      playStatusChanged!(isPlaying);
    }
  }

  /// 更新进度（秒）
  void updatePosition({required int position}) {
    if (positionStatusChanged != null) {
      positionStatusChanged!(position);
    }
  }

  int getPosition() {
    if (positionCallback != null) {
      return positionCallback!();
    }
    return 0;
  }
}