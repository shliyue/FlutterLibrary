import 'package:flutter/material.dart';
import 'package:zyocore/zyocore.dart';
import 'package:zyocore/widget/assets_picker/entity/ly_asset_entity.dart';

class LyMediaPreview extends StatefulWidget {
  final List srcs;
  final int index;
  final bool showMore;

  /// 仅仅预览
  final bool justPreview;
  final Function(int)? onTapMoreHandle;
  final GestureTapCallback? onLongPress;
  final Widget? topBar;
  final ValueChanged<int>? onIndexChanged;
  final SwiperController? swiperController;

  const LyMediaPreview({
    Key? key,
    required this.srcs,
    this.topBar,
    this.onLongPress,
    this.index = 0,
    this.onIndexChanged,
    this.onTapMoreHandle,
    this.swiperController,
    this.showMore = false,
    this.justPreview = false,
  }) : super(key: key);

  @override
  State<LyMediaPreview> createState() => _VideoViewerState();
}

class _VideoViewerState extends State<LyMediaPreview> with TickerProviderStateMixin {
  var currentIndex = 0.obs;
  var animationValue = 0.0.obs;

  late BuildContext _context;
  late AnimationController _animationController;
  late Animation<double> _animation;
  late List _files;

  @override
  void initState() {
    // TODO: implement initState

    super.initState();
    _animationController = AnimationController(duration: const Duration(milliseconds: 250), vsync: this);
    _animation = Tween<double>(begin: 0, end: -88).animate(_animationController)
      ..addListener(() {
        // setState(() {});
        animationValue.value = _animation.value;
      });
  }

  Widget _topBar() {
    return Container(
      width: Get.width,
      color: const Color.fromRGBO(0, 0, 0, 0.3),
      child: Container(
        margin: EdgeInsets.only(top: 44.w),
        padding: EdgeInsets.only(bottom: 8.w),
        child: Row(
          mainAxisAlignment: MainAxisAlignment.spaceBetween,
          crossAxisAlignment: CrossAxisAlignment.center,
          children: [
            GestureDetector(
              onTap: () => Get.back(),
              child: Padding(
                padding: EdgeInsets.symmetric(horizontal: 16.w, vertical: 8.w),
                child: const Icon(
                  Icons.clear,
                  color: Colors.white,
                  size: 20,
                ),
              ),
            ),
            Obx(() {
              return LYText(
                text: '${currentIndex.value + 1}/${_files.length}',
                fontSize: 17.sp,
                color: Colors.white,
              );
            }),
            GestureDetector(
              onTap: () {
                if (widget.onTapMoreHandle != null) {
                  widget.onTapMoreHandle!(currentIndex.value);
                }
              },
              child: Visibility(
                visible: widget.showMore,
                child: Padding(
                  padding: EdgeInsets.symmetric(horizontal: 16.w, vertical: 8.w),
                  child: Icon(
                    Icons.more_horiz_outlined,
                    color: Colors.white,
                    size: 20.sp,
                  ),
                ),
              ),
            ),
          ],
        ),
      ),
    );
  }

  @override
  Widget build(BuildContext context) {
    _context = context;
    _files = widget.srcs;
    if (widget.index > _files.length - 1) {
      currentIndex.value = _files.length - 1;
    } else {
      currentIndex.value = widget.index;
    }
    return Container(
      child: Stack(
        children: <Widget>[
          Swiper(
            key: UniqueKey(),
            loop: false,
            controller: widget.swiperController,
            itemBuilder: (_, index) {
              var files = _files[index];
              if (files is AssetEntity) {
                //集合里面既有asset 又有网络图片  asset.netUrl里面存储的是网络地址
                dynamic src;
                if (files is LyAssetEntity && !TextUtil.isEmpty(files.netUrl)) {
                  src = (widget.srcs[index] as LyAssetEntity).netUrl;
                } else {
                  src = files;
                }
                if (files.type == AssetType.video) {
                  return LyVideoPreview(
                    src: src,
                    onTap: onTapHandle,
                  );
                }
                return LyPhotoPreview(
                  src: src,
                  onLongPress: widget.onLongPress,
                  justPreview: widget.justPreview,
                  onTap: onTapHandle,
                );
              } else if (files is FileEntity) {
                if ((FileUtil.isVideo(files.fileUrl ?? ''))) {
                  return LyVideoPreview(
                    src: files.fileUrl ?? '',
                    onTap: onTapHandle,
                  );
                }
                return LyPhotoPreview(
                  src: files.fileUrl ?? '',
                  onLongPress: widget.onLongPress,
                  justPreview: widget.justPreview,
                  onTap: onTapHandle,
                );
              }else if(files is String){
                if (FileUtil.isVideo(files)) {
                  return LyVideoPreview(
                    src: files,
                    onTap: onTapHandle,
                  );
                }else if(FileUtil.isAudio(files)){
                  return LyAudioPreview(src: files);
                }
              }

              return LyPhotoPreview(
                src: files,
                onLongPress: widget.onLongPress,
                justPreview: widget.justPreview,
                onTap: onTapHandle,
              );
            },
            index: currentIndex.value,
            onIndexChanged: (v) {
              currentIndex.value = v;
              // if (mounted) {
              //   setState(() {});
              // }
              if (widget.onIndexChanged != null) {
                widget.onIndexChanged!(v);
              }
            },
            itemCount: _files.length,
          ),
          Obx(() {
            return Positioned(
              top: animationValue.value,
              child: widget.topBar != null ? widget.topBar! : _topBar(),
            );
          })
        ],
      ),
    );
  }

  void onTapHandle() {
    if (_animation.value == 0) {
      showBar();
    } else {
      hideBar();
    }
  }

  void showBar() {
    _animationController.forward();
  }

  void hideBar() {
    _animationController.reverse();
  }

  @override
  void dispose() {
    // TODO: implement dispose
    _animationController.dispose();
    super.dispose();
  }
}
