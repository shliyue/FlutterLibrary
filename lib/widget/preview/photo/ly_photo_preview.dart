import 'package:flutter/material.dart';
import 'package:photo_view/photo_view.dart';
import 'package:zyocore/zyocore.dart';

class LyPhotoPreview extends StatefulWidget {
  final GestureLongPressCallback? onLongPress;
  final GestureTapCallback? onTap;
  final dynamic src;

  /// 仅仅预览
  final bool justPreview;

  const LyPhotoPreview({
    Key? key,
    required this.src,
    this.onLongPress,
    this.justPreview = false,
    this.onTap,
  }) : super(key: key);

  @override
  State<LyPhotoPreview> createState() => _LyPhotoPreviewState();
}

class _LyPhotoPreviewState extends State<LyPhotoPreview> {
  @override
  Widget build(BuildContext context) {
    return PhotoView.customChild(
      child: LYImageWidget(
        src: widget.src is AssetEntity ? "" : widget.src,
        onTap: widget.onTap,
        fit: BoxFit.contain,
        isOriginal: true,
        assetEntity: widget.src is AssetEntity ? widget.src : null,
        onLongPress: onNetImageLongPress,
      ),
      maxScale: 1.8,
      minScale: 0.8,
      enablePanAlways: false,
    );
  }

  onNetImageLongPress() {
    if (widget.src is AssetEntity) return;
    if (widget.onLongPress != null) {
      widget.onLongPress!();
    } else {
      if (widget.justPreview) return;
      LYSheetUtil.showTextSheet(context, text: '保存', callback: () async {
        try {
          await LyDownloadUtil.downloadNetImage(widget.src);
          ToastUtil.showToast('保存成功', toastPosition: EasyLoadingToastPosition.bottom);
        } catch (e) {
          ToastUtil.showToast('保存失败', toastPosition: EasyLoadingToastPosition.bottom);
        }
      });
    }
  }
}
