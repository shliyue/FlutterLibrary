import 'dart:async';
import 'dart:io';

import 'package:flutter/material.dart';
import 'package:just_audio/just_audio.dart';
import 'package:zyocore/values/index.dart';
import '../../../zyocore.dart';
import '../../common/flex/column.dart';
import '../../common/flex/row.dart';

class LyAudioPreview extends StatefulWidget {
  /// 资源地址
  dynamic src;

  LyAudioPreview({super.key, required this.src});

  @override
  State<LyAudioPreview> createState() => _LyAudioPreviewState();
}

class _LyAudioPreviewState extends State<LyAudioPreview> {
  var speedTime = "00:00".obs;
  var endTime = "".obs;
  var curDuration = 0.obs;
  var ttlDuration = 1.obs;
  var isPlaying = false.obs;
  var sliderChange = false;
  var sliderDuration = 0.obs;
  AudioPlayer? _audioPlayer;
  late StreamSubscription lyBusPlayer;

  @override
  void initState() {
    // TODO: implement initState
    super.initState();
    playerAudio();
  }

  void eventBusPlayer(LyEvent event) {
    if (event.data == "stop") {
      _audioPlayer!.pause();
    } else if (event.data == "play") {
      _audioPlayer!.play();
    }
  }

  void playerAudio() async {
    if (widget.src == null) {
      return;
    }
    //监听音频播放
    lyBusPlayer = lyBus.on(eventName: EventBusKeys.lyAudioPlayer).listen(eventBusPlayer);

    _audioPlayer = AudioPlayer();
    AudioSource audioSource;
    if (widget.src is String) {
      audioSource = AudioSource.uri(Uri.parse(widget.src));
    } else {
      AssetEntity assetEntity = (widget.src as AssetEntity);
      if (Platform.isIOS) {
        String? assetUrl = await assetEntity.getMediaUrl();
        if (assetUrl == null) {
          return;
        }
        audioSource = AudioSource.file(assetUrl);
      } else {
        File? file = await assetEntity.file;
        audioSource = AudioSource.file(file!.path);
      }
      ttlDuration.value = assetEntity.duration;
    }
    _audioPlayer!.setAudioSource(audioSource);
    _audioPlayer!.play();
    _audioPlayer!.positionStream.listen((position) {
      curDuration.value = position.inSeconds;
      speedTime.value = LyHelperUtil.getStrDuration(curDuration.value);
      if (!sliderChange) {
        sliderDuration.value = curDuration.value;
      }
    });
    _audioPlayer!.playerStateStream.listen((event) {
      isPlaying.value = event.playing;
      // print("playerStateStream++++++++++++:${event}");
      switch (event.processingState) {
        case ProcessingState.idle:
          break;
        case ProcessingState.loading:
          break;
        case ProcessingState.buffering:
          break;
        case ProcessingState.ready:
          if (_audioPlayer != null) {
            Duration? dr = _audioPlayer!.duration;
            if (dr != null) {
              ttlDuration.value = _audioPlayer!.duration!.inSeconds;
            }
            endTime.value = LyHelperUtil.getStrDuration(ttlDuration.value);
          }
          break;
        case ProcessingState.completed:
          _audioPlayer!.stop();
          onPlayerSeek(0);
          break;
      }
    });
  }

  void onPlayerSeek(int seconds) {
    curDuration.value = seconds;
    _audioPlayer!.seek(Duration(seconds: seconds));
  }

  void onPlayAudio() {
    if (isPlaying.value) {
      _audioPlayer!.pause();
    } else {
      _audioPlayer!.play();
    }
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      backgroundColor: lightFFFFFF,
      body: _contentWrapper,
    );
  }

  Widget get _contentWrapper {
    return Stack(
      alignment: Alignment.topCenter,
      children: [
        _contentWidget,
      ],
    );
  }

  Widget get _bgWidget {
    return LYAssetsImage(
      name: "bg_audio.png",
      width: 146.w,
      height: 118.w,
    );
  }

  Widget get _contentWidget {
    return Container(
        alignment: Alignment.center,
        child: CSC(
          mainAxisAlignment: MainAxisAlignment.center,
          crossAxisAlignment: CrossAxisAlignment.center,
          children: [
            _bgWidget,
            _playWidget,
            // _actionButton,
          ],
        ));
  }

  Widget get _playWidget {
    return Container(
      margin: EdgeInsets.only(top: 72.w, left: 16.w, right: 16.w),
      padding: EdgeInsets.all(6.w),
      height: 60.w,
      decoration: BoxDecoration(
        borderRadius: BorderRadius.all(Radius.circular(10.w)),
        color: const Color(0xFFF9F6FF),
      ),
      child: Container(
        padding: EdgeInsets.all(6.w),
        decoration: BoxDecoration(
          borderRadius: BorderRadius.all(Radius.circular(10.w)),
          boxShadow: const [
            // BoxShadow(color: Colors.white, offset: Offset(0, 0), blurRadius: 10),
            BoxShadow(color: Colors.white, offset: Offset(0, 0), blurRadius: 10)
          ],
          color: Colors.white,
        ),
        child: RSC(
          children: [
            Obx(() {
              return LYAssetsImage(
                name: isPlaying.value ? "btn_play.png" : "btn_pause.png",
                width: 36.w,
                height: 36.w,
                onTap: () {
                  onPlayAudio();
                },
              );
            }),
            _sliderWidget,
            _dateWidget,
          ],
        ),
      ),
    );
  }

  Widget get _sliderWidget {
    return Expanded(
      child: Container(
        child: Obx(() {
          return Slider(
            value: sliderDuration.value.toDouble(),
            min: 0,
            max: ttlDuration.value.toDouble(),

            /// 滑块以及滑块左侧的颜色
            activeColor: theme9F89FF,

            /// 滑块右侧的颜色
            inactiveColor: lightE1E1E1,
            label: 'value:${curDuration.value}',
            onChanged: (e) {
              sliderDuration.value = e.toInt();
            },
            onChangeStart: (value) {
              sliderChange = true;
            },
            onChangeEnd: (value) {
              sliderChange = false;
              onPlayerSeek(value.toInt());
            },
          );
        }),
      ),
    );
  }

  Widget get _dateWidget {
    return Row(
      children: [
        Obx(() {
          return LYText(text: speedTime.value, fontSize: 10.sp, color: theme9F89FF, fontFamily: AppFontFamily.pingFangSemibold);
        }),
        Obx(() {
          return LYText(
              text: "/${endTime.value}", fontSize: 10.sp, color: dark999999, fontFamily: AppFontFamily.pingFangSemibold);
        }),
      ],
    );
  }

  @override
  void dispose() {
    // TODO: implement dispose
    super.dispose();
    disposeAudioResource();
  }

  void disposeAudioResource() {
    lyBusPlayer.cancel();
    if (_audioPlayer != null) {
      _audioPlayer!.stop();
      _audioPlayer!.dispose();
      _audioPlayer = null;
    }
  }

  @override
  void didUpdateWidget(covariant LyAudioPreview oldWidget) {
    // TODO: implement didUpdateWidget
    super.didUpdateWidget(oldWidget);
    setState(() {
      disposeAudioResource();
      playerAudio();
    });
  }
}
