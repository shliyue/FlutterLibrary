import 'package:flutter/material.dart';
import 'package:zyocore/values/style.dart';
import 'package:zyocore/values/color.dart';
import 'package:flutter_screenutil/flutter_screenutil.dart';
import 'icons/arrow_right_icon.dart';
import 'text/ly_label_hjc.dart';
import 'text/ly_text.dart';

class LYFormSelector extends StatelessWidget {
  final String title;
  final double titleWidth;
  final String hintText;
  final bool must;
  final String value;
  final TextAlign align;
  final VoidCallback? onTap;
  final bool showBorder;
  final Color? textColor;

  const LYFormSelector({Key? key,
    required this.title,
    this.titleWidth = 92,
    this.hintText = '请选择',
    required this.value,
    required this.onTap,
    this.align = TextAlign.left,
    this.textColor,
    this.showBorder = true,
    this.must = false})
      : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Container(
      margin: EdgeInsets.only(left: 16.w),
      padding: EdgeInsets.only(top: 16.w, bottom: 16.w, right: 16.w),
      decoration: !showBorder ? null : const BoxDecoration(
        border: Border(
          bottom: BorderSide(
            width: 0.5,
            color: Color(0xFFE1E1E1),
          ),
        ),
      ),
      child: GestureDetector(
        onTap: onTap,
        child: Row(
          children: [
            SizedBox(
              width: titleWidth,
              child: LYLabelHjc(
                title: title,
                hjc: must,
              ),),
            SizedBox(
              width: 12.w,
            ),
            Expanded(
              child: LYText(
                textAlign: align,
                text: value.isEmpty ? hintText : value,
                fontFamily: AppFontFamily.pingFangRegular,
                color:
                value.isEmpty ? dark999999 : (textColor ?? dark1A1A1A),
                fontSize: 17.sp,
              ),
            ),
            SizedBox(
              width: 4.w,
            ),
            if(onTap != null)
              const ArrowRightIcon(),
          ],
        ),
      ),
    );
  }
}