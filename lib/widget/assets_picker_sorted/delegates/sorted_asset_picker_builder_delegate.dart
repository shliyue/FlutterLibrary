///
/// [Author] Alex (https://github.com/AlexV525)
/// [Date] 2020-10-29 21:50
///
import 'dart:async';
import 'dart:io';

import 'package:flutter/material.dart';
import 'package:flutter/scheduler.dart';
import 'package:flutter/services.dart';
import 'package:zyocore/values/edge_insets.dart';
import 'package:zyocore/values/style.dart';
import 'package:zyocore/values/text.dart';
import 'package:zyocore/widget/assets_picker/constants/colors.dart';
import 'package:zyocore/widget/assets_picker/constants/constants.dart';
import 'package:zyocore/widget/assets_picker/constants/enums.dart';
import 'package:zyocore/widget/assets_picker/constants/extensions.dart';
import 'package:zyocore/widget/assets_picker/delegates/asset_picker_builder_delegate.dart';
import 'package:zyocore/widget/assets_picker/delegates/assets_picker_text_delegate.dart';
import 'package:zyocore/widget/assets_picker/entity/group_entity.dart';
import 'package:zyocore/widget/assets_picker/entity/ly_asset_entity.dart';
import 'package:zyocore/widget/assets_picker/entity/path_wrapper.dart';
import 'package:zyocore/widget/assets_picker/ly_assets_picker.dart';
import 'package:zyocore/widget/assets_picker/widget/builder/asset_entity_grid_item_builder.dart';
import 'package:zyocore/widget/assets_picker/widget/builder/value_listenable_builder_2.dart';
import 'package:zyocore/widget/assets_picker/widget/fixed_appbar.dart';
import 'package:zyocore/widget/assets_picker/widget/platform_progress_indicator.dart';
import 'package:zyocore/widget/assets_picker/widget/scale_text.dart';
import 'package:zyocore/widget/assets_picker_sorted/provider/sorted_assets_picker_provider.dart';
import 'package:zyocore/widget/assets_picker_sorted/sorted_asset_picker_viewer.dart';
import 'package:zyocore/widget/assets_picker_sorted/sorted_assets_picker.dart';
import 'package:zyocore/widget/common/flex/row.dart';
import 'package:zyocore/widget/common/shapes/circle_widget.dart';

import '../../../core/configs/config.dart';
import '../../../zyocore.dart';

class SortedAssetPickerBuilderDelegate {
  SortedAssetPickerBuilderDelegate(
      {required this.provider,
      required this.initialPermission,
      this.gridCount = 3,
      Color? themeColor,
      AssetsPickerTextDelegate? textDelegate,
      this.pickerTheme,
      this.loadingIndicatorBuilder,
      this.keepScrollOffset = false,
      this.gridThumbnailSize = defaultAssetGridPreviewSize,
      this.previewThumbnailSize = defaultPathThumbnailSize,
      this.specialPickerType,
      this.packageName})
      : assert(
          pickerTheme == null || themeColor == null,
          'Theme and theme color cannot be set at the same time.',
        ),
        themeColor =
            pickerTheme?.colorScheme.secondary ?? themeColor ?? C.themeColor {
    Constants.textDelegate = textDelegate ?? AssetsPickerTextDelegate();
    // Add the listener if [keepScrollOffset] is true.
    if (keepScrollOffset) {
      gridScrollController.addListener(keepScrollOffsetListener);
    }

    /// 本次可以选择的数量
    maxToSelect = provider.maxAssets - provider.selectedAssets.length;
  }

  /// The current special picker type for the picker.
  /// 当前特殊选择类型
  ///
  /// Several types which are special:
  /// * [SpecialPickerType.wechatMoment] When user selected video, no more images
  /// can be selected.
  /// * [SpecialPickerType.noPreview] Disable preview of asset; Clicking on an
  /// asset selects it.
  ///
  /// 这里包含一些特殊选择类型：
  /// * [SpecialPickerType.wechatMoment] 微信朋友圈模式。当用户选择了视频，将不能选择图片。
  /// * [SpecialPickerType.noPreview] 禁用资源预览。多选时单击资产将直接选中，单选时选中并返回。
  final SpecialPickerType? specialPickerType;

  /// Preview thumbnail size in the viewer.
  /// 预览时图片的缩略图大小
  ///
  /// This only works on images and videos since other types does not have to
  /// request for the thumbnail data. The preview can speed up by reducing it.
  /// 该参数仅生效于图片和视频类型的资源，因为其他资源不需要请求缩略图数据。
  /// 预览图片的速度可以通过适当降低它的数值来提升。
  ///
  /// Default is `null`, which will request the origin data.
  /// 默认为空，即读取原图。
  final ThumbnailSize? previewThumbnailSize;

  // [ChangeNotifier] for asset picker.
  /// 资源选择器状态保持
  final LySortedAssetPickerProvider provider;

  /// The [PermissionState] when the picker is called.
  /// 当选择器被拉起时的权限状态
  final PermissionState initialPermission;

  /// Assets count for the picker.
  /// 资源网格数
  final int gridCount;

  /// Main color for the picker.
  /// 选择器的主题色
  final Color themeColor;

  /// Theme for the picker.
  /// 选择器的主题
  ///
  /// Usually the WeChat uses the dark version (dark background color)
  /// for the picker. However, some others want a light or a custom version.
  ///
  /// 通常情况下微信选择器使用的是暗色（暗色背景）的主题，
  /// 但某些情况下开发者需要亮色或自定义主题。
  final ThemeData? pickerTheme;

  /// Indicates the loading status for the builder.
  /// 指示目前加载的状态
  final IndicatorBuilder? loadingIndicatorBuilder;

  /// Whether the picker should save the scroll offset between pushes and pops.
  /// 选择器是否可以从同样的位置开始选择
  final bool keepScrollOffset;

  /// The [ScrollController] for the preview grid.
  final ScrollController gridScrollController = ScrollController();

  /// The [GlobalKey] for [assetsGridBuilder] to locate the [ScrollView.center].
  /// [assetsGridBuilder] 用于定位 [ScrollView.center] 的 [GlobalKey]
  final GlobalKey gridRevertKey = GlobalKey();

  // /// Whether the assets grid should revert.
  // /// 判断资源网格是否需要倒序排列
  // ///
  // /// [Null] means judging by [isAppleOS].
  // /// 使用 [Null] 即使用 [isAppleOS] 进行判断。
  // final bool? shouldRevertGrid;

  /// [ThemeData] for the picker.
  /// 选择器使用的主题---与普通的相册采用相同的样式主题
  ThemeData get theme => pickerTheme ?? LyAssetsPicker.themeData(themeColor);

  /// 本次可以选择的最大数量
  int maxToSelect = 0;

  /// 资源包名
  String? packageName;

  /// Return a system ui overlay style according to
  /// the brightness of the theme data.
  /// 根据主题返回状态栏的明暗样式
  SystemUiOverlayStyle get overlayStyle =>
      theme.appBarTheme.systemOverlayStyle ??
      (theme.effectiveBrightness.isDark
          ? SystemUiOverlayStyle.light
          : SystemUiOverlayStyle.dark);

  /// The color for interactive texts.
  /// 可交互的文字的颜色
  Color interactiveTextColor(BuildContext context) => Color.lerp(
        context.themeData.iconTheme.color?.withOpacity(.7) ?? Colors.white,
        Colors.blueAccent,
        0.4,
      )!;

  /// Whether the current platform is Apple OS.
  /// 当前平台是否苹果系列系统 (iOS & MacOS)
  bool get isAppleOS => Platform.isIOS;

  /// Whether the picker is under the single asset mode.
  /// 选择器是否为单选模式
  // bool get isSingleAssetMode => provider.maxAssets == 1;

  /// 分组标题栏的高度
  double get groupHeight => 44;

  /// Space between assets item widget.
  /// 资源部件之间的间隔
  double get itemSpacing => 2;

  /// Item's height in app bar.
  /// 顶栏内各个组件的统一高度
  double get appBarItemHeight => 32;

  /// Blur radius in Apple OS layout mode.
  /// 苹果系列系统布局方式下的模糊度
  double get appleOSBlurRadius => 10;

  /// Height for the bottom occupied section.
  /// 底部区域占用的高度
  double get bottomSectionHeight =>
      bottomActionBarHeight + permissionLimitedBarHeight;

  /// Height for bottom action bar.
  /// 底部操作栏的高度
  double get bottomActionBarHeight => kToolbarHeight / 1.1;

  double get itemHeight =>
      (utils.style.screenWidth - (gridCount + 1) * itemSpacing) / gridCount;

  /// Notifier for the current [PermissionState].
  /// 当前 [PermissionState] 的监听
  late final ValueNotifier<PermissionState> permission =
      ValueNotifier<PermissionState>(
    initialPermission,
  );
  final ValueNotifier<bool> permissionOverlayHidden =
      ValueNotifier<bool>(false);

  /// Whether the permission is limited currently.
  /// 当前的权限是否为受限
  bool get isPermissionLimited => permission.value == PermissionState.limited;

  bool get effectiveShouldRevertGrid => isAppleOS;

  /// Height for the permission limited bar.
  /// 权限受限栏的高度
  double get permissionLimitedBarHeight => isPermissionLimited ? 75 : 0;

  /// Thumbnail size in the grid.
  /// 预览时网络的缩略图大小
  ///
  /// This only works on images and videos since other types does not have to
  /// request for the thumbnail data. The preview can speed up by reducing it.
  /// 该参数仅生效于图片和视频类型的资源，因为其他资源不需要请求缩略图数据。
  /// 预览图片的速度可以通过适当降低它的数值来提升。
  ///
  /// This cannot be `null` or a large value since you shouldn't use the
  /// original data for the grid.
  /// 该值不能为空或者非常大，因为在网格中使用原数据不是一个好的决定。
  final ThumbnailSize gridThumbnailSize;

  /// 手指最后滑动的资源
  LyAssetEntity? _lastScrollAsset;

  /// 最后一次滑动位置
  DragUpdateDetails? _lastScrollDetails;

  /// 滑动Timer
  Timer? _scrollTimer;

  /// 滑动选择处理
  bool? _scrollAction;

  /// The listener to track the scroll position of the [gridScrollController]
  /// if [keepScrollOffset] is true.
  /// 当 [keepScrollOffset] 为 true 时，跟踪 [gridScrollController] 位置的监听。
  void keepScrollOffsetListener() {
    if (gridScrollController.hasClients) {
      Constants.scrollPosition = gridScrollController.position;
    }
  }

  // /// Keep a `initState` method to sync with [State].
  // /// 保留一个 `initState` 方法与 [State] 同步。
  @mustCallSuper
  void initSortedState(SortedAssetsPickerState state) {}

  /// todo 数据更新可能有风险
  Future<void> onAssetsChanged(MethodCall call, StateSetter setState) async {
    if (!isPermissionLimited) {
      return;
    }
    if (call.arguments is Map) {
      final Map<dynamic, dynamic> arguments =
          call.arguments as Map<dynamic, dynamic>;
      if (arguments['newCount'] == 0) {
        provider
          ..currentAssets = <LyAssetEntity>[]
          ..currentPath = null
          ..selectedAssets = <LyAssetEntity>[]
          ..hasAssetsToDisplay = false
          ..isAssetsEmpty = true
          ..totalAssetsCount = 0
          ..paths.clear();
        return;
      }
    }
    await provider.getPaths();
    provider.currentPath = provider.paths.first;
    final PathWrapper<AssetPathEntity>? currentWrapper = provider.currentPath;
    if (currentWrapper != null) {
      final AssetPathEntity newPath =
          await currentWrapper.path.obtainForNewProperties();
      final int assetCount = await newPath.assetCountAsync;
      final PathWrapper<AssetPathEntity> newPathWrapper =
          PathWrapper<AssetPathEntity>(
        path: newPath,
        assetCount: assetCount,
      );
      provider
        ..currentPath = newPathWrapper
        ..hasAssetsToDisplay = assetCount != 0
        ..isAssetsEmpty = assetCount == 0
        ..totalAssetsCount = assetCount
        ..getThumbnailFromPath(newPathWrapper);
      if (newPath.isAll) {
        await provider.getAssetsFromCurrentPath();
        final List<AssetEntity> entitiesShouldBeRemoved = <AssetEntity>[];
        for (final AssetEntity entity in provider.selectedAssets) {
          if (!provider.currentAssets.contains(entity)) {
            entitiesShouldBeRemoved.add(entity);
          }
        }
        entitiesShouldBeRemoved.forEach(provider.selectedAssets.remove);
      }
    }
  }

  Widget androidLayout(BuildContext context) {
    return FixedAppBarWrapper(
      appBar: appBar(context),
      body: Selector<LySortedAssetPickerProvider, bool>(
        selector: (_, LySortedAssetPickerProvider provider) =>
            provider.hasAssetsToDisplay,
        builder: (_, bool hasAssetsToDisplay, __) {
          final bool shouldDisplayAssets = hasAssetsToDisplay;
          return AnimatedSwitcher(
            duration: const Duration(milliseconds: 300),
            child: shouldDisplayAssets
                ? RepaintBoundary(
                    child: Column(
                      children: <Widget>[
                        Expanded(child: assetsAndroidListBuilder(context)),
                        bottomActionBar(context),
                      ],
                    ),
                  )
                : loadingIndicator(context),
          );
        },
      ),
    );
  }

  Widget bottomActionBar(BuildContext context) {
    Widget child = Container(
      height: 60 + context.bottomPadding,
      padding: const EdgeInsets.symmetric(horizontal: 16).copyWith(
        bottom: context.bottomPadding,
      ),
      color: theme.colorScheme.onPrimary,
      child: Row(
        children: <Widget>[
          previewButton(context),
        ],
      ),
    );
    if (isPermissionLimited) {
      child = Column(
        mainAxisSize: MainAxisSize.min,
        children: <Widget>[accessLimitedBottomTip(context), child],
      );
    }
    return child;
  }

  /// The tip widget displays when the access is limited.
  /// 当访问受限时在底部展示的提示
  Widget accessLimitedBottomTip(BuildContext context) {
    return GestureDetector(
      onTap: PhotoManager.openSetting,
      child: Container(
        padding: const EdgeInsets.symmetric(horizontal: 10),
        height: permissionLimitedBarHeight,
        color: theme.primaryColor.withOpacity(isAppleOS ? 0.90 : 1),
        child: Row(
          children: <Widget>[
            const SizedBox(width: 5),
            Icon(
              Icons.warning,
              color: Colors.orange[400]!.withOpacity(.8),
            ),
            const SizedBox(width: 15),
            Expanded(
              child: ScaleText(
                Constants.textDelegate.accessAllTip,
                style: context.themeData.textTheme.caption?.copyWith(
                  fontSize: 14,
                ),
              ),
            ),
            Icon(
              Icons.keyboard_arrow_right,
              color: context.themeData.iconTheme.color?.withOpacity(.5),
            ),
          ],
        ),
      ),
    );
  }

  Widget previewButton(BuildContext context) {
    return Selector<LySortedAssetPickerProvider, bool>(
      selector: (_, LySortedAssetPickerProvider p) => p.isSelectedNotEmpty,
      builder: (BuildContext c, bool isSelectedNotEmpty, Widget? child) {
        return GestureDetector(
          onTap: () async {
            if (!isSelectedNotEmpty) {
              return;
            }
            final List<LyAssetEntity> _selected = provider.selectedAssets;

            /// 预览
            final List<LyAssetEntity>? result =
                await SortedAssetPickerViewer.pushToViewer(
              context,
              currentIndex: 0,
              previewAssets: _selected,
              previewThumbnailSize: previewThumbnailSize,
              selectedAssets: _selected,
              selectorProvider: provider,
              themeData: theme,
              maxAssets: provider.maxAssets,
              packageName: packageName,
            );
            if (result != null) {
              Navigator.of(context).maybePop(result);
            }
          },
          child: Selector<LySortedAssetPickerProvider, String>(
            selector: (_, LySortedAssetPickerProvider p) =>
                p.selectedDescriptions,
            builder: (_, String selectedDescriptions, ___) => ScaleText(
              Constants.textDelegate.preview,
              style: TextStyle(
                color: selectedDescriptions.isEmpty
                    ? themeColor.withOpacity(0.6)
                    : themeColor,
                fontSize: 17,
                fontFamily: AppFontFamily.pingFangMedium,
              ),
              maxScaleFactor: 1.2,
            ),
          ),
        );
      },
    );
  }

  /// Back button.
  /// 返回按钮
  Widget backButton(BuildContext context) {
    return GestureDetector(
      onTap: Navigator.of(context).maybePop,
      child: Container(
        height: kToolbarHeight,
        width: kToolbarHeight,
        color: theme.colorScheme.onPrimary,
        child: Icon(
          Icons.close,
          size: 20,
          color: theme.colorScheme.onSurface,
        ),
      ),
    );
  }

  PreferredSizeWidget appBar(BuildContext context) {
    return FixedAppBar(
      backgroundColor: theme.appBarTheme.backgroundColor,
      centerTitle: true,
      title: Text(
        '手机相册',
        style: TextStyle(
          fontSize: 17,
          color: theme.colorScheme.onSurface,
          fontFamily: AppFontFamily.pingFangMedium,
        ),
      ),
      leading: backButton(context),
      actions: [confirmButton(context)],
      actionsPadding: const EdgeInsetsDirectional.only(end: 16),
      // blurRadius: isAppleOS ? appleOSBlurRadius : 0,
    );
  }

  /// It'll pop with [AssetPickerProvider.selectedAssets]
  /// when there are any assets were chosen.
  /// 当有资源已选时，点击按钮将把已选资源通过路由返回。
  Widget confirmButton(BuildContext context) {
    return Consumer<LySortedAssetPickerProvider>(
      builder: (_, LySortedAssetPickerProvider provider, __) {
        return GestureDetector(
          onTap: () {
            if (provider.isSelectedNotEmpty) {
              if (provider.selectedAssets.length > provider.maxAssets) {
                Navigator.of(context).maybePop(
                    provider.selectedAssets.sublist(0, provider.maxAssets));
              } else {
                Navigator.of(context).maybePop(provider.selectedAssets);
              }
            }
          },
          child: Container(
            constraints: BoxConstraints(
              minWidth: provider.isSelectedNotEmpty ? 70 : 20,
            ),
            child: ScaleText(
              provider.isSelectedNotEmpty
                  ? '${Constants.textDelegate.confirm}'
                      ' (${provider.selectedAssets.length})'
                  : Constants.textDelegate.confirm,
              style: TextStyle(
                color: provider.isSelectedNotEmpty
                    ? theme.indicatorColor
                    : theme.indicatorColor.withOpacity(0.6),
                fontSize: 17,
                fontFamily: AppFontFamily.pingFangMedium,
              ),
            ),
          ),
        );
      },
    );
  }

  /// iOS页面布局，待测试验证
  /// todo ios布局待测试调整,
  /// 已弃用，统一使用android布局
  Widget appleOSLayout(BuildContext context) {
    return Stack(
      children: <Widget>[
        Positioned.fill(
          child: Selector<LySortedAssetPickerProvider, bool>(
            selector: (_, LySortedAssetPickerProvider p) =>
                p.hasAssetsToDisplay,
            builder: (_, bool hasAssetsToDisplay, __) {
              final Widget _child;
              final bool shouldDisplayAssets = hasAssetsToDisplay;
              if (shouldDisplayAssets) {
                _child = RepaintBoundary(
                  child: Stack(
                    children: <Widget>[
                      Positioned.fill(
                        child: assetsAndroidListBuilder(context),
                      ),
                    ],
                  ),
                );
              } else {
                _child = loadingIndicator(context);
              }
              return AnimatedSwitcher(
                duration: const Duration(milliseconds: 300),
                child: _child,
              );
            },
          ),
        ),
        appBar(context),
      ],
    );
  }

  /// The effective direction for the assets grid.
  /// 网格实际的方向
  ///
  /// By default, the direction will be reversed if it's iOS/macOS.
  /// 默认情况下，在 iOS/macOS 上方向会反向。
  TextDirection effectiveGridDirection(BuildContext context) {
    final TextDirection _od = Directionality.of(context);
    if (effectiveShouldRevertGrid) {
      if (_od == TextDirection.ltr) {
        return TextDirection.rtl;
      }
      return TextDirection.ltr;
    }
    return _od;
  }

  /// 排序分组列表
  Widget assetsAndroidListBuilder(BuildContext context) {
    return Selector<LySortedAssetPickerProvider, PathWrapper<AssetPathEntity>?>(
      selector: (_, LySortedAssetPickerProvider p) => p.currentPath,
      builder: (_, PathWrapper<AssetPathEntity>? wrapper, __) {
        Widget _sliverList(BuildContext ctx, List<GroupEntity> groups) {
          return SliverList(
            delegate: SliverChildBuilderDelegate(
              (_, int index) => Builder(
                builder: (BuildContext c) {
                  return Directionality(
                    textDirection: Directionality.of(context),
                    child: assetListItemBuilder(c, index, groups),
                  );
                },
              ),
              childCount: groups.length,
            ),
          );
        }

        return LayoutBuilder(
          builder: (BuildContext c, BoxConstraints constraints) {
            return Directionality(
              textDirection: effectiveGridDirection(context),
              child: ColoredBox(
                color: theme.canvasColor,
                child: Selector<LySortedAssetPickerProvider, List<String>>(
                    selector: (_, LySortedAssetPickerProvider p) =>
                        p.titleSelected,
                    builder: (_, List<String> titleSelected, __) {
                      return Selector<LySortedAssetPickerProvider,
                              List<String>>(
                          selector: (_, LySortedAssetPickerProvider p) =>
                              p.notLocallyAvailables,
                          builder: (_, List<String> notLocallyAvailables, __) {
                            return Selector<LySortedAssetPickerProvider,
                                    List<String>>(
                                selector: (_, LySortedAssetPickerProvider p) =>
                                    p.currentUploaded,
                                builder: (_, List<String> uploadsAssets, __) {
                                  return Selector<LySortedAssetPickerProvider,
                                      List<GroupEntity>>(
                                    selector:
                                        (_, LySortedAssetPickerProvider p) =>
                                            p.groupList,
                                    builder: (_, List<GroupEntity> groups, __) {
                                      // final SliverGap _bottomGap = SliverGap.v(
                                      //   context.bottomPadding + bottomSectionHeight,
                                      // );
                                      return GestureDetector(
                                        onPanStart: (details) {
                                          _scrollAction = null;
                                        },
                                        onPanUpdate: (details) {
                                          _onPanUpdate(
                                            groups: groups,
                                            details: details,
                                            context: context,
                                          );
                                        },
                                        onPanEnd: (details) {
                                          _lastScrollAsset = null;
                                          _stopScrollTimer();
                                        },
                                        child: CustomScrollView(
                                          key: gridRevertKey,
                                          physics:
                                              const AlwaysScrollableScrollPhysics(),
                                          controller: gridScrollController,
                                          cacheExtent: utils.style.screenHeight,
                                          anchor: 0,
                                          center: null,
                                          slivers: <Widget>[
                                            _sliverList(_, groups),
                                          ],
                                        ),
                                      );
                                    },
                                  );
                                });
                          });
                    }),
              ),
            );
          },
        );
      },
    );
  }

  /// 滑动选择手势处理
  void _onPanUpdate({
    required List<GroupEntity> groups,
    required DragUpdateDetails details,
    required BuildContext context,
  }) {
    var fingerX = details.localPosition.dx;
    var fingerY = details.localPosition.dy + gridScrollController.offset;
    var scrollHeight = gridRevertKey.currentContext?.size?.height ?? 0.0;
    var fingerOffset = Offset(fingerX, fingerY);
    // Logger.d('---  finger $fingerOffset');
    var assetWidth = itemHeight;
    var assetHeight = itemHeight;
    var x = 0.0;
    var y = 0.0;
    int? groupIndex;
    int? childIndex;
    for (var i = 0; i < groups.length; i++) {
      var group = groups[i];
      x = itemSpacing;
      if (group.child.isEmpty) {
        y += groupHeight;
      }
      for (var j = 0; j < group.child.length; j++) {
        var rect = Rect.fromLTWH(x, y, assetWidth, assetHeight);
        // Logger.d(
        //     "-- rect i:$i j:$j ${rect.left} ${rect.top} ${rect.width} ${rect.height}");
        if (rect.contains(fingerOffset)) {
          groupIndex = i;
          childIndex = j;
          break;
        }
        if (j == group.child.length - 1) {
          // 换行
          x = 0;
          y += (assetHeight + itemSpacing);
        } else {
          x += (assetWidth + itemSpacing);
        }
      }
      if (groupIndex != null) {
        break;
      }
    }
    if (groupIndex != null && childIndex != null) {
      // Logger.d("当前位置：$groupIndex  $childIndex");
      var group = groups[groupIndex];
      var asset = group.child[childIndex];
      if (_lastScrollAsset == null || _lastScrollAsset!.id != asset.id) {
        _lastScrollAsset = asset;
        var selected = provider.selectedAssets.contains(asset);
        _scrollAction ??= !selected;
        if (_scrollAction!) {
          var invalid = provider.invalidFormats.contains(asset.id);
          var notLocallyAvailable =
              provider.notLocallyAvailables.contains(asset.id);
          final List<String>? allowTypes = provider.allowTypes;
          var isAllow = (asset.type == AssetType.image &&
              allowTypes != null &&
              allowTypes.contains(AssetUtil.assetSuffix(asset)));
          if (((asset.type == AssetType.image && !invalid) ||
                  isAllow ||
                  (asset.type == AssetType.video && asset.duration <= 300)) &&
              !notLocallyAvailable) {
            provider.selectAsset(asset);
          }
        } else {
          provider.unSelectAsset(asset);
        }
      }
    } else {
      _lastScrollAsset = null;
    }
    if (fingerY < gridScrollController.offset + itemHeight) {
      _startScrollTimer(isToTop: true, groups: groups, context: context);
    } else if (fingerY >
        gridScrollController.offset + scrollHeight - itemHeight) {
      _startScrollTimer(isToTop: false, groups: groups, context: context);
    } else {
      _stopScrollTimer();
    }
  }

  void _startScrollTimer({
    required bool isToTop,
    required List<GroupEntity> groups,
    required BuildContext context,
  }) {
    var duration = const Duration(milliseconds: 16);
    _scrollTimer ??= Timer.periodic(duration, (timer) async {
      var y = gridScrollController.offset;
      var max = gridScrollController.position.maxScrollExtent;
      if (isToTop) {
        y -= ScreenUtil().pixelRatio ?? 1;
        if (y < 0) {
          y = 0;
        }
      } else {
        y += ScreenUtil().pixelRatio ?? 1;
        if (y > max) {
          y = max;
        }
      }
      if (y != gridScrollController.offset) {
        gridScrollController.jumpTo(y);
        if (_lastScrollDetails != null) {
          _onPanUpdate(
            groups: groups,
            details: _lastScrollDetails!,
            context: context,
          );
        }
      } else {
        _stopScrollTimer();
      }
    });
  }

  void _stopScrollTimer() {
    _scrollTimer?.cancel();
    _scrollTimer = null;
  }

  /// 资源构建有几个条件：
  ///  * 根据资源类型返回对应类型的构建：
  ///    * [AssetType.audio] -> [audioItemBuilder] 音频类型
  ///    * [AssetType.image], [AssetType.video] -> [imageAndVideoItemBuilder]
  ///      图片和视频类型
  ///  * 在索引到达倒数第三列的时候加载更多资源。
  Widget assetListItemBuilder(
    BuildContext context,
    int index,
    List<GroupEntity> currentGroups,
  ) {
    final LySortedAssetPickerProvider p =
        context.read<LySortedAssetPickerProvider>();
    final PathWrapper<AssetPathEntity>? currentWrapper = p.currentPath;
    final AssetPathEntity? currentPathEntity = currentWrapper?.path;

    // Directly return the special item when it's empty.
    /// 路径没有时的布局，这里为空
    if (currentPathEntity == null) {
      return const SizedBox.shrink();
    }

    // Logger.d('>>>>>>>>>>>>>>>>>>.index========$index');
    // Logger.d(
    //     '>>>>>>>>>>>>>>>>>>.currentGroups.length========${currentGroups.length}');
    // Logger.d(
    //     '>>>>>>>>>>>>>>>>>>.(index == (_length - 2))========${(index == (_length - 2))}');
    // Logger.d(
    //     '>>>>>>>>>>>>>>>>>>.provider.currentAssets.length)========${(provider.currentAssets.length)}');
    // Logger.d(
    //     '>>>>>>>>>>>>>>>>>>.hasmore========${context.select<LySortedAssetPickerProvider, bool>(
    //   (LySortedAssetPickerProvider p) => p.hasMoreToLoad,
    // )}');
    final int _length = currentGroups.length;
    if (p.hasMoreToLoad) {
      if (index == (_length - 2)) {
        p.loadMoreAssets();
      }
    }

    final GroupEntity group = currentGroups.elementAt(index);
    Widget builder = assetGroupBuilder(context, group);
    return Container(
      color: theme.colorScheme.onPrimary,
      child: builder,
    );
  }

  /// 分组的item builder
  Widget assetGroupBuilder(BuildContext context, GroupEntity group) {
    Widget childGroup = Container();
    if (group.child.isEmpty) {
      childGroup = buildGroupTitle(group);
    } else {
      childGroup = listItemBuilder(context, group);
    }

    return RepaintBoundary(
      child: childGroup,
    );
  }

  /// 分组标题
  Widget buildGroupTitle(GroupEntity data) {
    return Selector<LySortedAssetPickerProvider, String>(
        selector: (_, LySortedAssetPickerProvider p) => p.selectedDescriptions,
        builder: (BuildContext context, _, __) {
          final List<String> titleSelected =
              context.select<LySortedAssetPickerProvider, List<String>>(
            (LySortedAssetPickerProvider p) => p.titleSelected,
          );
          return Container(
            alignment: Alignment.centerLeft,
            padding: left16,
            height: 44,
            child: RBC(
              children: [
                RSC(
                  mainAxisSize: MainAxisSize.min,
                  children: [
                    CircleWidget(
                      margin: right5,
                      size: 7.w,
                      color: themeColor,
                    ),
                    LYText.withStyle(
                      utils.date.formatDateStr(data.createDate,
                          format: DateFormats.y_mo_d),
                      style: text15.dark.familyMedium,
                    ),

                    /// 涉及筛选，922暂时不做
                    // Container(
                    //   margin: left1,
                    //   child: const Icon(
                    //     Icons.chevron_right,
                    //     size: 18,
                    //     color: dark999999,
                    //   ),
                    // )
                  ],
                ),
                GestureDetector(
                  onTap: () {
                    provider.onGroupTap(data);
                  },
                  child: Container(
                    padding: horizontal16 + vertical5,
                    child: LYText.withStyle(
                      titleSelected.contains(data.createDate) ? '取消全选' : '全选',
                      style: text15.textColor(themeColor),
                    ),
                  ),
                )
              ],
            ),
          );
        });
  }

  /// 已弃用
  /// 分组网格
  Widget buildGroupGrid(BuildContext context, GroupEntity data) {
    return GridView.builder(
      physics: const NeverScrollableScrollPhysics(),
      itemCount: data.child.length,
      shrinkWrap: true,
      padding: EdgeInsets.zero,
      gridDelegate: SliverGridDelegateWithFixedCrossAxisCount(
        crossAxisCount: gridCount,
        mainAxisExtent: null,
        childAspectRatio: 1,
        crossAxisSpacing: itemSpacing,
        mainAxisSpacing: itemSpacing,
      ),
      itemBuilder: (_, index) {
        LyAssetEntity asset = data.child[index];
        Widget builder = imageAndVideoItemBuilder(context, index, asset);
        return Stack(
          key: ValueKey<String>(asset.id),
          children: <Widget>[
            builder,

            /// 遮罩层
            selectedBackdrop(context, index, data, asset),

            /// 勾选窗
            selectIndicator(context, data, asset),

            /// 禁止选择遮挡层
            // itemBannedIndicator(context, asset),
          ],
        );
      },
    );
  }

  Widget listItemBuilder(BuildContext context, GroupEntity data) {
    List<Widget> itemList = [];
    var len = data.child.length;
    final List<String> notLocallyAvailables =
        context.select<LySortedAssetPickerProvider, List<String>>(
      (LySortedAssetPickerProvider p) => p.notLocallyAvailables,
    );
    for (var i = 0; i < len; i++) {
      LyAssetEntity asset = data.child[i];
      Widget builder = imageAndVideoItemBuilder(context, i, asset);
      var notLocalAvailble = notLocallyAvailables.contains(asset.id);
      var assetChild = Container(
        width: itemHeight,
        height: itemHeight,
        margin: EdgeInsets.only(right: itemSpacing),
        child: Stack(
          key: ValueKey<String>(asset.id),
          children: notLocalAvailble
              ? <Widget>[builder]
              : <Widget>[
                  builder,

                  /// 遮罩层
                  selectedBackdrop(context, i, data, asset),

                  /// 勾选窗
                  selectIndicator(context, data, asset),

                  /// 禁止选择遮挡层
                  itemBannedIndicator(context, asset),
                ],
        ),
      );
      itemList.add(assetChild);
    }
    return Container(
      padding: EdgeInsets.only(left: itemSpacing, bottom: itemSpacing),
      child: RSC(
        children: itemList,
      ),
    );
  }

  Widget imageAndVideoItemBuilder(
    BuildContext context,
    int index,
    LyAssetEntity asset,
  ) {
    final AssetEntityImageProvider imageProvider = AssetEntityImageProvider(
      asset,
      isOriginal: false,
      thumbnailSize: gridThumbnailSize,
    );
    SpecialImageType? type;
    if (imageProvider.imageFileType == ImageFileType.gif) {
      type = SpecialImageType.gif;
    } else if (imageProvider.imageFileType == ImageFileType.heic) {
      type = SpecialImageType.heic;
    }
    return Stack(
      children: <Widget>[
        Positioned.fill(
          child: RepaintBoundary(
            child: AssetEntityGridItemBuilder(
              image: imageProvider,
              assetPickerProvider: provider,
              failedItemBuilder: failedItemBuilder,
            ),
          ),
        ),
        // if (type == SpecialImageType.gif) // 如果为GIF则显示标识
        //   gifIndicator(context, asset),
        if (asset.type == AssetType.video) // 如果为视频则显示标识
          videoIndicator(context, asset),

        if (asset.type == AssetType.image) // 如果为图片，只展示云端标识
          imageIndicator(context, asset),
      ],
    );
  }

  /// Item widgets when the thumb data load failed.
  /// 资源缩略数据加载失败时使用的部件
  // Widget failedItemBuilder(BuildContext context) {
  //   return Center(
  //     child: ScaleText(
  //       Constants.textDelegate.loadFailed,
  //       textAlign: TextAlign.center,
  //       style: const TextStyle(fontSize: 18),
  //     ),
  //   );
  // }
  Widget failedItemBuilder(BuildContext context) {
    return Container(
      color: const Color(0xFFEAEAEA),
      alignment: Alignment.center,
      child: Image(
        image: AssetImage('assets/images/load_fail.png',
            package: Config.packageName),
        width: 48.w,
        height: 48.w,
      ),
    );
  }

  Widget loadingIndicator(BuildContext context) {
    return Center(
      child: Selector<LySortedAssetPickerProvider, bool>(
        selector: (_, LySortedAssetPickerProvider p) => p.isAssetsEmpty,
        builder: (_, bool isAssetsEmpty, __) {
          if (isAssetsEmpty) {
            return ScaleText(
              Constants.textDelegate.emptyList,
              maxScaleFactor: 1.5,
            );
          }
          return PlatformProgressIndicator(
            color: theme.iconTheme.color,
            size: MediaQuery.of(context).size.width / gridCount / 3,
            radius: Platform.isIOS ? 20 : 10,
            brightness: Brightness.light,
          );
        },
      ),
    );
  }

  Widget itemBannedIndicator(BuildContext context, LyAssetEntity asset) {
    return Consumer<LySortedAssetPickerProvider>(
      builder: (_, LySortedAssetPickerProvider p, __) {
        if (asset.type == AssetType.video) {
          if (asset.duration > (p.maxMinute * 60)) {
            return Container(
              color: Colors.white.withOpacity(.8),
              alignment: Alignment.center,
              child: LYText.withStyle(
                '视频超过${p.maxMinute}分钟\n不允许上传',
                style: text12.textColor(Colors.black),
                textAlign: TextAlign.center,
              ),
            );
          }
          return const SizedBox.shrink();
        } else if (asset.type == AssetType.image) {
          var isNotAllow = (p.allowTypes != null &&
              !p.allowTypes!.contains(AssetUtil.assetSuffix(asset)));
          if (p.invalidFormats.contains(asset.id) || isNotAllow) {
            return Container(
              color: Colors.white.withOpacity(.8),
              alignment: Alignment.center,
              child: LYText.withStyle(
                '不支持${AssetUtil.pureAssetSuffix(asset)}格式',
                style: text12.textColor(Colors.black),
              ),
            );
          }
          return const SizedBox.shrink();
        }
        return const SizedBox.shrink();
      },
    );
  }

  /// 勾选框
  Widget selectIndicator(
      BuildContext context, GroupEntity data, LyAssetEntity asset) {
    final Duration duration = const Duration(milliseconds: 300) * 0.75;
    return Selector<LySortedAssetPickerProvider, String>(
      selector: (_, LySortedAssetPickerProvider p) => p.selectedDescriptions,
      builder: (BuildContext context, String descriptions, __) {
        final LySortedAssetPickerProvider p =
            context.read<LySortedAssetPickerProvider>();
        // final bool selected = descriptions.contains(asset.toString());

        final List<String> invalidFormats = p.invalidFormats;
        final List<String>? allowTypes = p.allowTypes;
        var package =
            packageName == "" ? null : packageName ?? Config.packageName;
        final Widget innerSelector = AnimatedSwitcher(
          duration: duration,
          reverseDuration: duration,
          child: asset.notSelected
              ? Image(
                  image: AssetImage('assets/images/public/icon_radio_f0.png',
                      package: package),
                  width: 22.w,
                  height: 22.w,
                )
              : Image(
                  image: AssetImage('assets/images/public/icon_radio_1.png',
                      package: package),
                  width: 22.w,
                  height: 22.w,
                ),
        );
        final GestureDetector selectorWidget = GestureDetector(
          behavior: HitTestBehavior.opaque,
          onTap: () async {
            /// 点击勾选框
            final bool selected = descriptions.contains(asset.toString());
            provider.onAssetTap(data, asset, !selected);
          },
          child: Container(
            color: Colors.transparent,
            padding: const EdgeInsets.all(8),
            width: 36,
            height: 36,
            alignment: AlignmentDirectional.topEnd,
            child: innerSelector,
          ),
        );

        if (invalidFormats.contains(asset.id) ||
            (asset.type == AssetType.video && asset.duration > (p.maxMinute * 60)) ||
            (asset.type == AssetType.image &&
                allowTypes != null &&
                !allowTypes.contains(AssetUtil.assetSuffix(asset)))) {
          return const SizedBox.shrink();
        }
        return PositionedDirectional(
          top: 0,
          start: 0,
          child: selectorWidget,
        );
      },
    );
  }

  Widget selectedBackdrop(
      BuildContext context, int index, GroupEntity data, LyAssetEntity asset) {
    bool selectedAllAndNotSelected() =>
        !provider.selectedAssets.contains(asset) &&
        provider.selectedMaximumAssets;
    // bool selectedPhotosAndIsVideo() =>
    //     asset.type == AssetType.video && provider.selectedAssets.isNotEmpty;

    return Positioned.fill(
      child: GestureDetector(
        onTap: () async {
          // When we reached the maximum select count and the asset
          // is not selected, do nothing.
          // When the special type is WeChat Moment, pictures and videos cannot
          // be selected at the same time. Video select should be banned if any
          // pictures are selected.
          if (selectedAllAndNotSelected()) {
            // ToastUtil.showToast(
            //     '最多只能选择${(maxToSelect)}${(provider.requestType == RequestType.common ? '个图片和视频' : (provider.requestType == RequestType.video ? '个视频' : '张图'))}');
            ToastUtil.showToast('已超过可选数量，不可进行选择！');
            return;
          }
          provider.curGroupEntity = data;
          final List<LyAssetEntity> _current = provider.currentAssets;
          final List<LyAssetEntity>? _selected = provider.selectedAssets;
          final int _index = provider.currentAssets
              .indexWhere((element) => element.id == asset.id);
          // Logger.d("xx=isLocallyAvailable=${await asset.isLocallyAvailable()}");
          ///预览
          final List<LyAssetEntity>? result =
              await SortedAssetPickerViewer.pushToViewer(
            context,
            currentIndex: _index,
            previewAssets: _current,
            themeData: theme,
            previewThumbnailSize: previewThumbnailSize,
            selectedAssets: _selected,
            selectorProvider: provider,
            specialPickerType: specialPickerType,
            maxAssets: provider.maxAssets,
            packageName: packageName,
            // shouldReversePreview: isAppleOS,
          );
          if (result != null) {
            Navigator.of(context).maybePop(result);
          }
        },
        child: Consumer<LySortedAssetPickerProvider>(
          builder: (_, LySortedAssetPickerProvider p, __) {
            final int index = p.selectedAssets.indexOf(asset);
            final bool selected = index != -1;
            return AnimatedContainer(
              duration: const Duration(milliseconds: 300),
              color: selected
                  ? theme.colorScheme.primary.withOpacity(.45)
                  : Colors.transparent,
              child: const SizedBox.shrink(),
            );
          },
        ),
      ),
    );
  }

  /// Videos often contains various of color in the cover,
  /// so in order to keep the content visible in most cases,
  /// the color of the indicator has been set to [Colors.white].
  ///
  /// 视频封面通常包含各种颜色，为了保证内容在一般情况下可见，此处
  /// 将指示器的图标和文字设置为 [Colors.white]。
  Widget videoIndicator(BuildContext context, LyAssetEntity asset) {
    return PositionedDirectional(
      start: 0,
      end: 0,
      bottom: 0,
      child: Container(
        width: double.maxFinite,
        height: 26,
        padding: const EdgeInsets.symmetric(horizontal: 2, vertical: 2),
        decoration: BoxDecoration(
          gradient: LinearGradient(
            begin: AlignmentDirectional.bottomCenter,
            end: AlignmentDirectional.topCenter,
            colors: <Color>[theme.dividerColor, Colors.transparent],
          ),
        ),
        child: Row(
          crossAxisAlignment: CrossAxisAlignment.center,
          children: <Widget>[
            Image(
              image: AssetImage('assets/images/icon_play.png',
                  package: Config.packageName),
              width: 12.w,
              height: 12.w,
            ),
            Expanded(
              child: Padding(
                padding: const EdgeInsetsDirectional.only(start: 4),
                child: Text(
                  Constants.textDelegate.durationIndicatorBuilder(
                    Duration(seconds: asset.duration),
                  ),
                  style: TextStyle(
                      color: theme.colorScheme.onPrimary, fontSize: 11),
                  maxLines: 1,
                ),
              ),
            ),
            _videoIcloudIndicator(context, asset),
          ],
        ),
      ),
    );
  }

  /// image 云端标识
  /// 将指示器的图标和文字设置为 [Colors.white]。
  Widget _videoIcloudIndicator(BuildContext context, LyAssetEntity asset) {
    final List<String> currentUploaded =
        context.select<LySortedAssetPickerProvider, List<String>>(
      (LySortedAssetPickerProvider p) => p.currentUploaded,
    );
    if (!currentUploaded.contains(asset.id)) return Container();
    return Image(
      image: AssetImage('assets/images/cloud_upload.png',
          package: Config.packageName),
      width: 24.w,
      height: 24.w,
    );
  }

  /// image 云端标识
  /// 将指示器的图标和文字设置为 [Colors.white]。
  Widget imageIndicator(BuildContext context, LyAssetEntity asset) {
    final List<String> currentUploaded =
        context.select<LySortedAssetPickerProvider, List<String>>(
      (LySortedAssetPickerProvider p) => p.currentUploaded,
    );
    if (!currentUploaded.contains(asset.id)) return Container();
    return PositionedDirectional(
      start: 0,
      end: 0,
      bottom: 0,
      child: Container(
        width: double.maxFinite,
        height: 26,
        padding: const EdgeInsets.symmetric(horizontal: 2, vertical: 2),
        decoration: BoxDecoration(
          gradient: LinearGradient(
            begin: AlignmentDirectional.bottomCenter,
            end: AlignmentDirectional.topCenter,
            colors: <Color>[theme.dividerColor, Colors.transparent],
          ),
        ),
        child: Row(
          mainAxisAlignment: MainAxisAlignment.spaceBetween,
          crossAxisAlignment: CrossAxisAlignment.center,
          children: <Widget>[
            Container(),
            Image(
              image: AssetImage('assets/images/cloud_upload.png',
                  package: Config.packageName),
              width: 24.w,
              height: 24.w,
            ),
          ],
        ),
      ),
    );
  }

  /// Keep a dispose method to sync with [State].
  /// 保留一个 dispose 方法与 [State] 同步。
  ///
  /// Be aware that the method will do nothing when [keepScrollOffset] is true.
  /// 注意当 [keepScrollOffset] 为 true 时方法不会进行释放。
  void dispose() {
    Constants.scrollPosition = null;
    gridScrollController.dispose();
    permission.dispose();
    permissionOverlayHidden.dispose();
  }

  /// Yes, the build method.
  /// 没错，是它是它就是它，我们亲爱的 build 方法~
  Widget build(BuildContext context) {
    // Schedule the scroll position's restoration callback if this feature
    // is enabled and offsets are different.
    if (keepScrollOffset && Constants.scrollPosition != null) {
      SchedulerBinding.instance!.addPostFrameCallback((_) {
        // Update only if the controller has clients.
        if (gridScrollController.hasClients) {
          gridScrollController.jumpTo(Constants.scrollPosition!.pixels);
        }
      });
    }
    return AnnotatedRegion<SystemUiOverlayStyle>(
      value: overlayStyle,
      child: Theme(
        data: theme,
        child: ChangeNotifierProvider<LySortedAssetPickerProvider>.value(
          value: provider,
          builder: (BuildContext c, __) => Material(
            color: theme.canvasColor,
            child: Stack(
              fit: StackFit.expand,
              children: <Widget>[
                // if (isAppleOS) appleOSLayout(c) else androidLayout(c),
                androidLayout(c),
                if (isAppleOS) iOSPermissionOverlay(c),
              ],
            ),
          ),
        ),
      ),
    );
  }

  /// The overlay when the permission is limited on iOS.
  Widget iOSPermissionOverlay(BuildContext context) {
    final Size size = MediaQuery.of(context).size;
    final Widget _closeButton = Container(
      margin: const EdgeInsetsDirectional.only(start: 16, top: 4),
      alignment: AlignmentDirectional.centerStart,
      child: IconButton(
        onPressed: Navigator.of(context).maybePop,
        icon: const Icon(Icons.close),
        padding: EdgeInsets.zero,
        constraints: BoxConstraints.tight(const Size.square(32)),
      ),
    );

    final Widget _limitedTips = Padding(
      padding: const EdgeInsets.symmetric(horizontal: 30),
      child: Column(
        mainAxisAlignment: MainAxisAlignment.center,
        children: <Widget>[
          ScaleText(
            Constants.textDelegate.unableToAccessAll,
            style: const TextStyle(fontSize: 22),
            textAlign: TextAlign.center,
          ),
          SizedBox(height: size.height / 30),
          ScaleText(
            Constants.textDelegate.accessAllTip,
            style: const TextStyle(fontSize: 18),
            textAlign: TextAlign.center,
          ),
        ],
      ),
    );

    final Widget _goToSettingsButton = MaterialButton(
      elevation: 0,
      minWidth: size.width / 2,
      height: appBarItemHeight * 1.25,
      padding: const EdgeInsets.symmetric(horizontal: 24),
      color: themeColor,
      shape: RoundedRectangleBorder(
        borderRadius: BorderRadius.circular(5),
      ),
      child: ScaleText(
        Constants.textDelegate.goToSystemSettings,
        style: const TextStyle(fontSize: 17),
      ),
      onPressed: PhotoManager.openSetting,
      materialTapTargetSize: MaterialTapTargetSize.shrinkWrap,
    );

    final Widget _accessLimitedButton = GestureDetector(
      onTap: () => permissionOverlayHidden.value = true,
      child: ScaleText(
        Constants.textDelegate.accessLimitedAssets,
        style: TextStyle(color: interactiveTextColor(context)),
      ),
    );

    return ValueListenableBuilder2<PermissionState, bool>(
      firstNotifier: permission,
      secondNotifier: permissionOverlayHidden,
      builder: (_, PermissionState ps, bool isHidden, __) {
        if (ps.isAuth || isHidden) {
          return const SizedBox.shrink();
        }
        return Positioned.fill(
          child: Container(
            padding: MediaQuery.of(context).padding,
            color: context.themeData.canvasColor,
            child: Column(
              children: <Widget>[
                _closeButton,
                Expanded(child: _limitedTips),
                _goToSettingsButton,
                SizedBox(height: size.height / 18),
                _accessLimitedButton,
              ],
            ),
          ),
        );
      },
    );
  }
}
