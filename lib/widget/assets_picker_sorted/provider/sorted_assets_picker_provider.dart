import 'dart:async';
import 'dart:io';
import 'dart:math';

import 'package:flutter/cupertino.dart';
import 'package:zyocore/widget/assets_picker/constants/constants.dart';
import 'package:zyocore/widget/assets_picker/delegates/sort_path_delegate.dart';
import 'package:zyocore/widget/assets_picker/entity/asset_file_entity.dart';
import 'package:zyocore/widget/assets_picker/entity/group_entity.dart';
import 'package:zyocore/widget/assets_picker/entity/ly_asset_entity.dart';
import "package:collection/collection.dart";
import 'package:zyocore/widget/assets_picker/entity/path_wrapper.dart';
import 'package:zyocore/widget/assets_picker/ly_assets_picker.dart';
import 'package:zyocore/widget/assets_picker/provider/ly_asset_picker_provider.dart';

import '../../../zyocore.dart';

/// 排序相册的数据提供者

/// 对应DefaultAssetPickerProvider,
class LySortedAssetPickerProvider extends LyAssetPickerProvider {
  /// Call [getAssetList] with route duration when constructing.
  /// 构造时根据路由时长延时获取资源
  LySortedAssetPickerProvider({
    List<LyAssetEntity>? selectedAssets,
    RequestType requestType = RequestType.common,
    SortPathDelegate<AssetPathEntity>? sortPathDelegate = SortPathDelegate.common,
    FilterOptionGroup? filterOptions,
    int maxAssets = 9,
    int maxMinute = 5,
    int pageSize = 60,
    ThumbnailSize pathThumbnailSize = defaultPathThumbnailSize,
    Duration initializeDelayDuration = const Duration(milliseconds: 250),
    CloudCheckApi? cloudCheckApi,
    String? classId,
    List<String>? allowTypes,
  }) : super(
          selectedAssets: selectedAssets,
          requestType: requestType,
          sortPathDelegate: sortPathDelegate,
          filterOptions: filterOptions,
          maxAssets: maxAssets,
          maxMinute: maxMinute,
          pageSize: pageSize,
          pathThumbnailSize: pathThumbnailSize,
          initializeDelayDuration: initializeDelayDuration,
          cloudCheckApi: cloudCheckApi,
          classId: classId,
          allowTypes: allowTypes,
        ) {
    Future<void>.delayed(initializeDelayDuration, () async {
      Logger.d('LSAPP--------------------maxAssets----$maxAssets');
      await getPaths();
      await getAssetsFromCurrentPath();
    });
  }

  /// Clear all fields when dispose.
  /// 销毁时重置所有内容
  @override
  void dispose() {
    super.dispose();
  }

  /// Assets under current path entity.
  /// ------------------------------------------------
  /// 正在查看的资源路径下的所有资源
  List<GroupEntity> _currentGroup = <GroupEntity>[];

  List<GroupEntity> get currentGroup => _currentGroup;

  set currentGroup(List<GroupEntity> value) {
    if (value == currentGroup) {
      return;
    }
    _currentGroup = List<GroupEntity>.from(value);
    notifyListeners();
  }

  /// --------------------按照3个一组进行优化-start---------------------------
  /// Assets under current path entity.
  /// ------------------------------------------------
  /// 正在查看的资源路径下的所有资源
  List<GroupEntity> _groupList = <GroupEntity>[];

  List<GroupEntity> get groupList => _groupList;

  set groupList(List<GroupEntity> value) {
    if (value == groupList) {
      return;
    }
    _groupList = List<GroupEntity>.from(value);
    notifyListeners();
  }

  /// --------------------按照3个一组进行优化-end---------------------------

  ///  点击图片时保存当前分组
  GroupEntity? _curGroupEntity;

  GroupEntity? get curGroupEntity => _curGroupEntity;

  set curGroupEntity(GroupEntity? value) {
    if (value == _curGroupEntity) {
      return;
    }
    _curGroupEntity = value;
    notifyListeners();
  }

  @override
  selectAsset(LyAssetEntity item) {
    super.selectAsset(item);

    /// 从预览页面进行选择
    _curGroupEntity = _findAssetsGroup(item);
    if (_curGroupEntity != null) {
      if (selectedAssets.length >= maxAssets) {
        ToastUtil.showToast('已达到最大数量');
        return;
      }
      onAssetTap(_curGroupEntity!, item, true);
    }
  }

  @override
  unSelectAsset(LyAssetEntity item) {
    /// 从预览页面进行取消选择
    _curGroupEntity = _findAssetsGroup(item);
    if (_curGroupEntity != null) {
      onAssetTap(_curGroupEntity!, item, false);
    }
  }

  /// 路径列表
  @override
  Future<void> getPaths() async {
    final PMFilter options;
    final PMFilter? fog = filterOptions;
    if (fog is FilterOptionGroup?) {
      // Initial base options.
      // Enable need title for audios and image to get proper display.
      final FilterOptionGroup newOptions = FilterOptionGroup(
        imageOption: const FilterOption(
          needTitle: true,
          // sizeConstraint: SizeConstraint(ignoreSize: true),
        ),
        videoOption: const FilterOption(
          needTitle: true,
          // sizeConstraint: SizeConstraint(ignoreSize: true),
        ),
        containsLivePhotos: true,
        containsPathModified: sortPathsByModifiedDate,
        createTimeCond: DateTimeCond.def().copyWith(ignore: true),
        updateTimeCond: DateTimeCond.def().copyWith(ignore: true),
      );
      // Merge user's filter options into base options if it's not null.
      if (fog != null) {
        newOptions.merge(fog);
      }
      options = newOptions;
    } else {
      options = fog;
    }
    // Logger.d('LSPP=options**********${options.toMap()}');

    /// 路径列表
    final List<AssetPathEntity> list = await PhotoManager.getAssetPathList(
      onlyAll: true,
      type: requestType,
      filterOption: options,
    );

    List<PathWrapper<AssetPathEntity>> tempPaths = list.map((AssetPathEntity p) => PathWrapper<AssetPathEntity>(path: p)).toList();

    tempPaths
      ..forEach(getAssetCountFromPath)
      ..forEach(getThumbnailFromPath);

    /// 赋值paths
    paths = tempPaths;
    // Set first path entity as current path entity.
    if (tempPaths.isNotEmpty) {
      currentPath ??= tempPaths.first;
    }
    // Logger.d(
    //     'getPaths+currentPath****************${currentPath?.toString()}');
  }

  /// Get assets list from current path entity.
  /// 从当前已选路径获取资源列表--->getAssetList
  @override
  Future<void> getAssetsFromCurrentPath() async {
    if (currentPath == null || paths.isEmpty) {
      isAssetsEmpty = true;
      return;
    }
    final PathWrapper<AssetPathEntity> wrapper = currentPath!;
    final int assetCount = wrapper.assetCount ?? await wrapper.path.assetCountAsync;
    totalAssetsCount = assetCount;
    Logger.d('totalAssetsCount****************$totalAssetsCount');
    isAssetsEmpty = assetCount == 0;
    if (wrapper.assetCount == null) {
      currentPath = currentPath!.copyWith(assetCount: assetCount);
    }
    if (isSubmitting) return;
    isSubmitting = true;
    await getAssetsFromPath(0, currentPath!.path);
    isSubmitting = false;
  }

  Completer<void>? _getSortedAssetsFromPathCompleter;

  /// 根据路径去获取路径下图片
  @override
  Future<void> getAssetsFromPath([int? page, AssetPathEntity? path]) async {
    // Logger.d('LSPP>>>>>>>>>>>>>>>>>>>>>>page=$page');
    Future<void> run() async {
      final int currentPage = page ?? currentAssetsListPage;
      final AssetPathEntity currentPath = path ?? this.currentPath!.path;
      final List<AssetEntity> list = await currentPath.getAssetListPaged(
        page: currentPage,
        size: pageSize,
      );

      /// 过滤部分资源的格式
      List<AssetEntity> localValidAssets = await AssetUtil.filterAssetsList(
          assets: list,
          allowTypes: allowTypes ?? [],
          maxMinute: maxMinute,
          onInvalidSave: (data) {
            // Logger.d('onInvalidSave=data=====$data');
            var invalidList = invalidFormats;
            invalidList.addAll(data);
            invalidFormats = invalidList;
            // Logger.d('invalidFormats=final======$invalidFormats');
          },
          onNotLocallyAvailableSave: (data) {
            // Logger.d('onInvalidSave=data=====$data');
            var notLocallyAvailableList = notLocallyAvailables;
            notLocallyAvailableList.addAll(data);
            notLocallyAvailables = notLocallyAvailableList;
            // Logger.d('notLocallyAvailables=final======$notLocallyAvailables');
          });
      if (currentPage == 0) {
        currentAssets.clear();
      }

      /// 最终分组的列表
      currentGroup = translateAsset(localValidAssets);
      groupList = _spreadGroupToList(currentGroup);

      currentAssets = localValidAssets.map((e) => LyAssetEntity.fromAsset(e)).toList();
      hasAssetsToDisplay = currentGroup.isNotEmpty;
      notifyListeners();

      /// 检测云端标记
      checkCloudTag(targetList: localValidAssets, loadMore: false);
    }

    if (_getSortedAssetsFromPathCompleter == null) {
      _getSortedAssetsFromPathCompleter = Completer<void>();
      run().then((_) {
        _getSortedAssetsFromPathCompleter!.complete();
      }).catchError((Object e, StackTrace s) {
        _getSortedAssetsFromPathCompleter!.completeError(e, s);
      }).whenComplete(() {
        _getSortedAssetsFromPathCompleter = null;
      });
    }
    return _getSortedAssetsFromPathCompleter!.future;
  }

  Completer<void>? _getSortedMoreAssetsFromPathCompleter;

  /// 加载下一页
  @override
  Future<void> loadMoreAssets() async {
    // Logger.d('LSPP>currentAssetsListPage>$currentAssetsListPage');
    Future<void> run() async {
      /// 防止重复触发
      if (loadMoreIndex == currentAssetsListPage) return;
      loadMoreIndex = currentAssetsListPage;
      final int currentPage = currentAssetsListPage;
      final AssetPathEntity currentPath = this.currentPath!.path;
      final List<AssetEntity> list = await currentPath.getAssetListPaged(
        page: currentPage,
        size: pageSize,
      );
      final List<LyAssetEntity> tempList = <LyAssetEntity>[];
      tempList.addAll(currentAssets);

      List<AssetEntity> tempLocalValidAssets = await AssetUtil.filterAssetsList(
          assets: list,
          allowTypes: allowTypes ?? [],
          maxMinute: maxMinute,
          onInvalidSave: (data) {
            // Logger.d('onInvalidSave=data=====$data');
            var invalidList = invalidFormats;
            invalidList.addAll(data);
            invalidFormats = invalidList;
            // Logger.d('invalidFormats=final======$invalidFormats');
          },
          onNotLocallyAvailableSave: (data) {
            // Logger.d('onInvalidSave=data=====$data');
            var notLocallyAvailableList = notLocallyAvailables;
            notLocallyAvailableList.addAll(data);
            notLocallyAvailables = notLocallyAvailableList;
            // Logger.d('notLocallyAvailables=final======$notLocallyAvailables');
          });

      tempList.addAll(tempLocalValidAssets.map((e) => LyAssetEntity.fromAsset(e)).toList());
      currentAssets = tempList;

      List<GroupEntity> newGallery = translateAsset(tempLocalValidAssets);
      final List<GroupEntity> tempGalleryList = <GroupEntity>[];
      tempGalleryList.addAll(currentGroup);

      if (tempGalleryList.last.createDate == newGallery.first.createDate) {
        if (newGallery.length > 1) {
          tempGalleryList.last.child.addAll(newGallery.first.child);
          newGallery.removeAt(0);
          tempGalleryList.addAll(newGallery);
        } else {
          tempGalleryList.last.child.addAll(newGallery.first.child);
        }
      } else {
        tempGalleryList.addAll(newGallery);
      }
      currentGroup = tempGalleryList;
      groupList = _spreadGroupToList(currentGroup);
      notifyListeners();

      /// 检测云端标记
      checkCloudTag(targetList: tempLocalValidAssets, loadMore: true);
    }

    if (_getSortedMoreAssetsFromPathCompleter == null) {
      _getSortedMoreAssetsFromPathCompleter = Completer<void>();
      run().then((_) {
        _getSortedMoreAssetsFromPathCompleter!.complete();
      }).catchError((Object e, StackTrace s) {
        _getSortedMoreAssetsFromPathCompleter!.completeError(e, s);
      }).whenComplete(() {
        _getSortedMoreAssetsFromPathCompleter = null;
      });
    }
    return _getSortedMoreAssetsFromPathCompleter!.future;
  }

  /// List<AssetEntity>转List<GroupEntity>,按照日期分组
  List<GroupEntity> translateAsset(List<AssetEntity> assetList) {
    List<AssetFileEntity> fileModelList = assetList.map((e) {
      // Logger.d(
      //     'translateAsset asset=${e.toString()}+ctime=${utils.date.formatDate(e.createDateTime, format: DateFormats.y_mo_d)}+mtime=${utils.date.formatDate(e.modifiedDateTime, format: DateFormats.y_mo_d)}');
      return AssetFileEntity(
          id: e.id,
          createDate: utils.date.formatDate(Platform.isAndroid ? e.modifiedDateTime : e.createDateTime, format: DateFormats.y_mo_d),
          asset: LyAssetEntity.fromAsset(e));
    }).toList();

    /// 分组的map
    Map<String, List<AssetFileEntity>> assetsMap = fileModelList.groupListsBy<String>((e) => e.createDate);

    /// 分组List
    List<GroupEntity> assets = [];
    for (var key in assetsMap.keys) {
      var childFile = assetsMap[key]!.map((e) {
        var assetEntity = LyAssetEntity.fromFileAsset(e);
        var selectedIndex = selectedAssets.indexWhere((element) => element.id == e.id);
        assetEntity.notSelected = selectedIndex < 0;
        return assetEntity;
      }).toList();
      var notAllSelected = true;
      for (var element in childFile) {
        // Logger.d('====handleImageTap.cur id==${element.id}');
        // Logger.d('====handleImageTap.notSelected==${(element.notSelected)}');
        if (element.notSelected == true) {
          notAllSelected = true;
          break;
        } else {
          notAllSelected = false;
        }
      }
      var groupEntity = GroupEntity(child: childFile, createDate: key);

      groupEntity.notAllSelected = notAllSelected;

      assets.add(groupEntity);
    }
    return assets;
  }

  ///分组项被点击
  onGroupTap(GroupEntity group) {
    List<GroupEntity> _groupList = currentGroup;
    var index = _groupList.indexWhere((element) => element.createDate == group.createDate);
    // Logger.d("maxAssets====$maxAssets");
    // Logger.d("selectedAssets.length====${selectedAssets.length}");

    /// 当前组
    GroupEntity groupEntity = _groupList[index];

    if (index >= 0) {
      /// 未选--->已选
      if (groupEntity.notAllSelected == true) {
        if (selectedAssets.length >= maxAssets) {
          ToastUtil.showToast('已达到最大数量');
          return;
        }
        List<LyAssetEntity> groupAssets = groupEntity.child;
        List<LyAssetEntity> groupAvailableList = [];
        for (var element in groupAssets) {
          if (AssetUtil.isAssetAvailable(element, invalidFormats, notLocallyAvailables)) {
            groupAvailableList.add(element);
          }
        }
        // Logger.d("groupAvailableList.length====${groupAvailableList.length}");
        if (groupAvailableList.isNotEmpty) {
          List<LyAssetEntity> tempAssets = [];
          if (groupAvailableList.length > (maxAssets - selectedAssets.length)) {
            var selectedList = [];
            for (var element in groupAvailableList) {
              if (!element.notSelected) {
                selectedList.add(element);
              }

              /// 先让其他的都取消选中???
              element.notSelected = true;
            }

            tempAssets = groupAvailableList.sublist(0, min(((maxAssets - selectedAssets.length) + selectedList.length), groupAvailableList.length));

            /// 部分选中、or 全部选中
            groupEntity.notAllSelected = ((maxAssets - selectedAssets.length) + selectedList.length) < groupAvailableList.length;

            if (!groupEntity.notAllSelected) {
              /// 部分选中、or 全部选中
              List<String> tempTitleList = titleSelected;
              tempTitleList.add(group.createDate);
              titleSelected = tempTitleList;
            }
          } else {
            tempAssets = groupAvailableList;

            /// 全部选中
            groupEntity.notAllSelected = false;

            /// 部分选中、or 全部选中
            List<String> tempTitleList = titleSelected;
            tempTitleList.add(group.createDate);
            titleSelected = tempTitleList;
          }
          for (var element in tempAssets) {
            element.notSelected = false;
          }
        }
      } else {
        /// 已选--->未选
        List<LyAssetEntity> groupAssets = _groupList[index].child;
        if (groupAssets.isNotEmpty) {
          for (var element in groupAssets) {
            element.notSelected = true;
          }
          groupEntity.notAllSelected = true;

          /// 部分选中、or 全部选中
          List<String> tempTitleList = titleSelected;
          tempTitleList.remove(groupEntity.createDate);
          titleSelected = tempTitleList;
        }
      }
    }

    /// 修改当前分组
    _groupList[index] = groupEntity;

    /// 已选中的
    selectedAssets = _transAssetsList(_groupList);

    /// 更新groupList
    currentGroup = _groupList;
  }

  /// Select asset.
  /// 选中资源
  onAssetTap(
    GroupEntity group,
    LyAssetEntity assetEntity,
    bool isSelected,
  ) {
    // Logger.d('====isSelected==$isSelected');
    _curGroupEntity = group;
    List<GroupEntity> _groupList = currentGroup;
    var index = _groupList.indexWhere((element) => element.createDate == group.createDate);
    GroupEntity groupEntity = _groupList[index];
    var notAllSelected = true;
    if (groupEntity.child.isNotEmpty) {
      var curFileIndex = groupEntity.child.indexWhere((element) => element.id == assetEntity.id);
      // Logger.d('====child.l==${groupEntity.child.length}');
      List<LyAssetEntity> availableList = [];
      for (var element in groupEntity.child) {
        if (AssetUtil.isAssetAvailable(element, invalidFormats, notLocallyAvailables)) {
          availableList.add(element);
        }
      }
      // Logger.d('====availableList.l==${availableList.length}');

      /// 达到最大数量
      if (selectedAssets.length >= maxAssets) {
        /// 已选
        if (!groupEntity.child[curFileIndex].notSelected) {
          /// 当前项已选--->未选
          groupEntity.child[curFileIndex].notSelected = !(groupEntity.child[curFileIndex].notSelected);
          var firstNotSelected = availableList.firstWhereOrNull((element) => element.notSelected);
          if (firstNotSelected != null) {
            notAllSelected = true;
            List<String> tempTitleList = titleSelected;
            tempTitleList.remove(groupEntity.createDate);
            titleSelected = tempTitleList;
          }
        } else {
          // ToastUtil.showToast(
          //     '最多只能选择$maxToSelect${(requestType == RequestType.common ? '个图片和视频' : (requestType == RequestType.video ? '个视频' : '张图'))}');
          ToastUtil.showToast('已超过可选数量，不可进行选择！');
        }
      } else {
        groupEntity.child[curFileIndex].notSelected = !isSelected;
        var firstNotSelected = availableList.firstWhereOrNull((element) => element.notSelected);
        if (firstNotSelected == null) {
          notAllSelected = false;

          /// 修改title
          List<String> tempTitleList = titleSelected;
          tempTitleList.add(group.createDate);
          titleSelected = tempTitleList;
        } else {
          notAllSelected = true;

          /// 修改title
          List<String> tempTitleList = titleSelected;
          tempTitleList.remove(group.createDate);
          titleSelected = tempTitleList;
        }
      }
    }
    // var notAllSelected = true;
    // Logger.d('====child.l==${groupEntity.child.length}');
    // List<LyAssetEntity> availableList=[];
    // for (var element in groupEntity.child) {
    //   if (AssetUtil.isAssetAvailable(element, invalidFormats, notLocallyAvailables)) {
    //     availableList.add(element);
    //   }
    // }

    // for (var element in availableList) {
    //   // Logger.d('====handleImageTap.cur id==${element.id}');
    //   // Logger.d('====handleImageTap.notSelected==${(element.notSelected)}');
    //   if (element.notSelected == true) {
    //     notAllSelected = true;
    //     List<String> tempTitleList = titleSelected;
    //     tempTitleList.remove(groupEntity.createDate);
    //     titleSelected = tempTitleList;
    //     break;
    //   } else {
    //     var selectedList = groupEntity.child.map((e) => !e.notSelected);
    //     if (selectedList.length == availableList.length) {
    //       /// 变为全选
    //       notAllSelected = false;
    //
    //       /// 修改title
    //       List<String> tempTitleList = titleSelected;
    //       tempTitleList.add(group.createDate);
    //       titleSelected = tempTitleList;
    //     }
    //     // notAllSelected = false;
    //   }
    // }
    // Logger.d('notAllSelected===$notAllSelected');
    groupEntity.notAllSelected = notAllSelected;

    /// 修改当前分组
    _groupList[index] = groupEntity;

    /// 已选中的
    selectedAssets = _transAssetsList(_groupList);

    /// 更新groupList
    currentGroup = _groupList;
  }

  /// 取出选中项
  List<LyAssetEntity> _transAssetsList(List<GroupEntity> _groupList) {
    List<LyAssetEntity> assetList = [];
    for (var group in _groupList) {
      if (group.child.isNotEmpty) {
        for (var asset in group.child) {
          if (!asset.notSelected && AssetUtil.isAssetAvailable(asset, invalidFormats, notLocallyAvailables)) {
            assetList.add(asset);
          }
        }
      }
    }
    return assetList;
  }

  /// 通过assetEntity日期找到所在的分组
  GroupEntity _findAssetsGroup(LyAssetEntity assetEntity) {
    List<GroupEntity> _groupList = currentGroup;
    var assetCreateTime = utils.date.formatDate(Platform.isAndroid ? assetEntity.modifiedDateTime : assetEntity.createDateTime, format: DateFormats.y_mo_d);
    GroupEntity group = _groupList.firstWhere((element) => element.createDate == assetCreateTime);
    return group;
  }

  /// 分组的List<GroupEntity>铺开成日期、LyAssetEntity数据（3个）为一组的列表
  List<GroupEntity> _spreadGroupToList(List<GroupEntity> groupList) {
    List<GroupEntity> spreadList = [];
    for (var group in groupList) {
      spreadList.add(GroupEntity(child: [], createDate: group.createDate));
      if (group.child.isNotEmpty) {
        var tempList = AssetUtil.splitList(group.child, 3);
        if (tempList.isNotEmpty) {
          for (List<LyAssetEntity> threeAssetList in tempList) {
            spreadList.add(GroupEntity(child: threeAssetList, createDate: group.createDate));
          }
        }
      }
    }
    return spreadList;
  }

  /// 检测云端标记
  @override
  checkCloudTag({List<AssetEntity>? targetList, bool loadMore = false}) async {
    // Logger.d('LSPP checkCloudTag+loadMore****************$loadMore');
    if (cloudCheckApi == null || (targetList ?? []).isEmpty) return;
    List<GroupEntity> tempCurrentGroup = groupList;

    List<String> targetHashList = [];
    for (var asset in targetList!) {
      // var assetMd5 = await AssetUtil.getAssetMd5(asset);
      targetHashList.add(asset.id);
      // Logger.d('LySortedAssetPickerProvider asset.id****************${asset.id}');
      //
      // var assetCreateTime = utils.date.formatDate(asset.createDateTime, format: DateFormats.y_mo_d);
      // Logger.d('LySortedAssetPickerProvider asset.assetCreateTime****************$assetCreateTime');
      // // var indexGroup = tempCurrentGroup.indexWhere((element) =>
      // //     element.createDate == assetCreateTime && element.child.isNotEmpty);
      //
      // /// 找出同一天的数据
      // var sameDateList = tempCurrentGroup.where((element) =>
      //     element.createDate == assetCreateTime && element.child.isNotEmpty);
      // var sameDateAssetList = [];
      // for (var group in sameDateList) {
      //   sameDateAssetList.addAll(group.child);
      // }
      //
      // var assetIndex = sameDateAssetList.indexWhere((a) => a.id == asset.id);
      // if (assetIndex != -1) {
      //   LyAssetEntity curAsset = sameDateAssetList[assetIndex];
      //   // curAsset.hashId = assetMd5;
      //
      //   // var assetCreateTimeNew = utils.date.formatDate(curAsset.createDateTime, format: DateFormats.y_mo_d);
      //
      //   var indexGroupNew = tempCurrentGroup
      //       .indexWhere((element) => element.child.contains(curAsset));
      //   var assetIndexNew = tempCurrentGroup[indexGroupNew]
      //       .child
      //       .indexWhere((b) => b.id == curAsset.id);
      //   if (assetIndexNew != -1) {
      //     tempCurrentGroup[indexGroupNew].child[assetIndexNew] = curAsset;
      //   }
      // }
    }

    /// 分组
    // groupList = tempCurrentGroup;

    /// 暂时未用上
    // currentAssets = tempCurrentAssets;

    var data = await cloudCheckApi!(classId: classId, hashId: targetHashList) ?? [];
    if (data.isEmpty) return;
    final List<String> tempList = [];
    tempList.addAll(currentUploaded);
    for (var hashId in data) {
      tempList.add(hashId);
    }
    // Logger.d('LSPP checkCloudTag+tempList****************$tempList');

    /// 更新列表
    currentUploaded = tempList;
  }
}
