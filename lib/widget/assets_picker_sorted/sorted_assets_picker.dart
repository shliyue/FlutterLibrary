import 'dart:io';

import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:zyocore/widget/assets_picker/asset_picker.dart';
import 'package:zyocore/widget/assets_picker/constants/constants.dart';
import 'package:zyocore/widget/assets_picker/delegates/asset_picker_builder_delegate.dart';
import 'package:zyocore/widget/assets_picker/delegates/assets_picker_text_delegate.dart';
import 'package:zyocore/widget/assets_picker/entity/ly_asset_entity.dart';
import 'package:zyocore/widget/assets_picker/ly_assets_picker.dart';
import 'package:zyocore/widget/assets_picker/widget/asset_picker_page_route.dart';
import 'package:zyocore/widget/assets_picker_sorted/delegates/sorted_asset_picker_builder_delegate.dart';
import 'package:zyocore/widget/assets_picker_sorted/provider/sorted_assets_picker_provider.dart';

import '../../zyocore.dart';

/// 带排序的assets_picker
class SortedAssetsPicker extends StatefulWidget {
  const SortedAssetsPicker({Key? key, required this.builder}) : super(key: key);

  final SortedAssetPickerBuilderDelegate builder;

  static Future<List<LyAssetEntity>?> pickSortedAssets(
    BuildContext context, {
    List<LyAssetEntity>? selectedAssets,
    int maxAssets = 50,
    int pageSize = 30,
    int gridCount = 3,
    Color? themeColor,
    ThemeData? pickerTheme,
    AssetsPickerTextDelegate? textDelegate,
    FilterOptionGroup? filterOptions,
    IndicatorBuilder? loadingIndicatorBuilder,
    ThumbnailSize gridThumbnailSize = defaultAssetGridPreviewSize,
    ThumbnailSize pathThumbnailSize = defaultPathThumbnailSize,
    RequestType requestType = RequestType.common,
    ThumbnailSize previewThumbnailSize = defaultAssetGridPreviewSize,
    bool useRootNavigator = true,
    Curve routeCurve = Curves.easeIn,
    Duration initializeDelayDuration = const Duration(milliseconds: 250),
    CloudCheckApi? cloudCheckApi,
    String? classId,
    List<String>? allowTypes,
    String? packageName,
  }) async {
    if (maxAssets < 1) {
      throw ArgumentError(
        'maxAssets must be greater than 1.',
      );
    }

    if (pageSize % gridCount != 0) {
      throw ArgumentError(
        'pageSize must be a multiple of gridCount.',
      );
    }

    if (pickerTheme != null && themeColor != null) {
      throw ArgumentError(
        'Theme and theme color cannot be set at the same time.',
      );
    }
    // final PermissionState _ps = await AssetPicker.permissionCheck(reqType: requestType);
    var p = false;
    if (requestType == RequestType.common) {
      p = await LyPermission.photoVideos();
    } else if (requestType == RequestType.image) {
      p = await LyPermission.photos();
    } else if (requestType == RequestType.video) {
      p = await LyPermission.videos();
    }
    if (!p) {
      throw ArgumentError('pickSortedAssets-----没有权限');
    }
    PermissionState _ps = p ? PermissionState.authorized : PermissionState.denied;

    final FilterOptionGroup defaultOption = FilterOptionGroup(
        containsLivePhotos: requestType.containsImage(),
        imageOption: FilterOption(
          needTitle: Platform.isIOS,

          /// 过滤后，会与系统自带相册不一致
          // sizeConstraint: SizeConstraint(minWidth: 750, minHeight: 750),
        ),
        videoOption: FilterOption(
          needTitle: Platform.isIOS,
          // durationConstraint: DurationConstraint(min: Duration(seconds: 5), max: Duration(minutes: 5, seconds: 0, milliseconds: 500)),
        ),
        // orders: [const OrderOption(type: OrderOptionType.updateDate)]);
        orders: [
          OrderOption(
              type: Platform.isAndroid
                  ? OrderOptionType.updateDate
                  : OrderOptionType.createDate)
        ]);

    final LySortedAssetPickerProvider provider = LySortedAssetPickerProvider(
        selectedAssets: selectedAssets,
        maxAssets: maxAssets,
        pageSize: pageSize,
        requestType: requestType,
        filterOptions: filterOptions ?? defaultOption,
        initializeDelayDuration: initializeDelayDuration,
        cloudCheckApi: cloudCheckApi,
        classId: classId,
        allowTypes: allowTypes ?? defaultAllowTypes);

    final Widget picker =
        ChangeNotifierProvider<LySortedAssetPickerProvider>.value(
      value: provider,
      child: SortedAssetsPicker(
        key: Constants.sortedKey,
        builder: SortedAssetPickerBuilderDelegate(
            provider: provider,
            initialPermission: _ps,
            gridCount: gridCount,
            textDelegate: textDelegate,
            // themeColor: themeColor,
            pickerTheme: pickerTheme ??
                LyAssetsPicker.themeData(themeColor ?? LibColor.colorMain),
            gridThumbnailSize: gridThumbnailSize,
            loadingIndicatorBuilder: loadingIndicatorBuilder,
            previewThumbnailSize: ThumbnailSize(
                ScreenUtil().screenWidth.toInt(),
                ScreenUtil().screenHeight.toInt()),
            packageName: packageName),
      ),
    );

    final List<LyAssetEntity>? result = await Navigator.of(
      context,
      rootNavigator: useRootNavigator,
    ).push<List<LyAssetEntity>>(
      AssetPickerPageRoute<List<LyAssetEntity>>(
          builder: picker,
          transitionCurve: routeCurve,
          transitionDuration: initializeDelayDuration,
          settings: const RouteSettings(name: 'sorted_assets_picker')),
    );
    return result;
  }

  @override
  SortedAssetsPickerState createState() => SortedAssetsPickerState();
}

class SortedAssetsPickerState extends State<SortedAssetsPicker>
    with WidgetsBindingObserver {
  @override
  void initState() {
    super.initState();
    WidgetsBinding.instance.addObserver(this);
    AssetPicker.registerObserve(_onLimitedAssetsUpdated);
    widget.builder.initSortedState(this);
  }

  @override
  void didChangeAppLifecycleState(AppLifecycleState state) {
    super.didChangeAppLifecycleState(state);
    if (state == AppLifecycleState.resumed) {
      PhotoManager.requestPermissionExtend().then(
        (PermissionState ps) => widget.builder.permission.value = ps,
      );
    }
  }

  @override
  void dispose() {
    WidgetsBinding.instance.removeObserver(this);
    AssetPicker.unregisterObserve(_onLimitedAssetsUpdated);
    // Skip delegate's dispose when it's keeping scroll offset.
    if (!widget.builder.keepScrollOffset) {
      widget.builder.dispose();
    }
    super.dispose();
  }

  Future<void> _onLimitedAssetsUpdated(MethodCall call) async {
    return widget.builder.onAssetsChanged(call, (VoidCallback fn) {
      fn();
      if (mounted) {
        setState(() {});
      }
    });
  }

  @override
  Widget build(BuildContext context) {
    return widget.builder.build(context);
  }
}
