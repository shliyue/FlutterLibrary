///
/// [Author] Alex (https://github.com/Alex525)
/// [Date] 2020/3/31 16:27
///
import 'dart:async';

import 'package:flutter/material.dart';
import 'package:zyocore/widget/assets_picker/asset_picker.dart';
import 'package:zyocore/widget/assets_picker/constants/enums.dart';
import 'package:zyocore/widget/assets_picker/constants/typedefs.dart';
import 'package:zyocore/widget/assets_picker/delegates/asset_picker_viewer_builder_delegate.dart';
import 'package:zyocore/widget/assets_picker/entity/ly_asset_entity.dart';
import 'package:zyocore/widget/assets_picker/provider/asset_picker_viewer_provider.dart';
import 'package:zyocore/widget/assets_picker_sorted/delegates/sorted_asset_picker_viewer_builder_delegate.dart';
import 'package:zyocore/widget/assets_picker_sorted/provider/sorted_assets_picker_provider.dart';

import '../../zyocore.dart';

class SortedAssetPickerViewer<Asset, Path> extends StatefulWidget {
  const SortedAssetPickerViewer({
    Key? key,
    required this.builder,
  }) : super(key: key);

  final AssetPickerViewerBuilderDelegate<Asset, Path> builder;

  @override
  SortedAssetPickerViewerState<Asset, Path> createState() =>
      SortedAssetPickerViewerState<Asset, Path>();

  /// Static method to push with the navigator.
  /// 跳转至选择预览的静态方法
  static Future<List<LyAssetEntity>?> pushToViewer(
    BuildContext context, {
    int currentIndex = 0,
    required List<LyAssetEntity> previewAssets,
    required ThemeData themeData,
    LySortedAssetPickerProvider? selectorProvider,
    ThumbnailSize? previewThumbnailSize,
    List<LyAssetEntity>? selectedAssets,
    SpecialPickerType? specialPickerType,
    int? maxAssets,
    bool shouldReversePreview = false,
    AssetSelectPredicate<AssetEntity>? selectPredicate,
    String? packageName,
  }) async {
    await AssetPicker.permissionCheck();
    final Widget viewer = SortedAssetPickerViewer<AssetEntity, AssetPathEntity>(
      builder: SortedAssetPickerViewerBuilderDelegate(
          currentIndex: currentIndex,
          previewAssets: previewAssets,
          provider: selectedAssets != null
              ? AssetPickerViewerProvider<LyAssetEntity>(
                  selectedAssets, selectorProvider!.notLocallyAvailables)
              : null,
          themeData: themeData,
          previewThumbnailSize: previewThumbnailSize,
          specialPickerType: specialPickerType,
          selectedAssets: selectedAssets,
          selectorProvider: selectorProvider,
          maxAssets: maxAssets,
          shouldReversePreview: shouldReversePreview,
          selectPredicate: selectPredicate,
          packageName: packageName),
    );
    final PageRouteBuilder<List<LyAssetEntity>> pageRoute =
        PageRouteBuilder<List<LyAssetEntity>>(
      pageBuilder: (_, __, ___) => viewer,
      transitionsBuilder: (_, Animation<double> animation, __, Widget child) {
        return FadeTransition(opacity: animation, child: child);
      },
    );
    final List<LyAssetEntity>? result =
        await Navigator.of(context).push<List<LyAssetEntity>>(pageRoute);
    return result;
  }

  /// Call the viewer with provided delegate and provider.
  /// 通过指定的 [delegate] 调用查看器
  static Future<List<A>?> pushToViewerWithDelegate<A, P>(
    BuildContext context, {
    required AssetPickerViewerBuilderDelegate<A, P> delegate,
  }) async {
    await AssetPicker.permissionCheck();
    final Widget viewer = SortedAssetPickerViewer<A, P>(builder: delegate);
    final PageRouteBuilder<List<A>> pageRoute = PageRouteBuilder<List<A>>(
      pageBuilder: (_, __, ___) => viewer,
      transitionsBuilder: (_, Animation<double> animation, __, Widget child) {
        return FadeTransition(opacity: animation, child: child);
      },
    );
    final List<A>? result = await Navigator.of(context).push<List<A>>(
      pageRoute,
    );
    return result;
  }
}

class SortedAssetPickerViewerState<Asset, Path>
    extends State<SortedAssetPickerViewer<Asset, Path>>
    with TickerProviderStateMixin {
  AssetPickerViewerBuilderDelegate<Asset, Path> get builder => widget.builder;

  @override
  void initState() {
    super.initState();
    builder.initStateAndTicker(this);
  }

  @override
  void dispose() {
    builder.dispose();
    super.dispose();
  }

  @override
  Widget build(BuildContext context) {
    return builder.build(context);
  }
}
