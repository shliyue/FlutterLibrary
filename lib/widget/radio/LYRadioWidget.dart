import 'package:flutter/material.dart';
import 'package:flutter_screenutil/flutter_screenutil.dart';
import 'package:get/get.dart';

import '../icons/ly_radio_icon.dart';
import '../text/ly_text.dart';

class LYRadioWidget extends StatefulWidget {
  String? title = "是否圆角";
  String? firstTitle = "是";
  String? secondTitle = "否";
  double? titleSpace = 10.w; //标题间隔
  double? itemSpace = 50.w; //选项间隔
  double? titleFontSize = 15.w;
  double? itemFontSize = 15.w;
  Color? titleColor;
  Color? itemColor;
  bool defaultSelect;
  Function(bool select)? callBack;

  LYRadioWidget(
      {super.key,
      this.title = "是否圆角:",
      this.titleSpace,
      this.firstTitle = "是",
      this.secondTitle = "否",
      this.itemSpace,
      this.titleFontSize,
      this.itemFontSize,
      this.titleColor,
      this.itemColor,
      this.defaultSelect = true,
      this.callBack});

  @override
  State<LYRadioWidget> createState() => _LYRadioWidgetState();
}

class _LYRadioWidgetState extends State<LYRadioWidget> {
  var isRadioValue = true.obs;

  @override
  void initState() {
    // TODO: implement initState
    super.initState();
    isRadioValue.value = widget.defaultSelect;
  }

  @override
  Widget build(BuildContext context) {
    widget.titleSpace = widget.titleSpace ?? 10.w;
    widget.itemSpace = widget.itemSpace ?? 50.w;
    widget.titleFontSize = widget.titleFontSize ?? 15.w;
    widget.itemFontSize = widget.itemFontSize ?? 15.w;
    return Row(
      children: [
        LYText(
          text: widget.title,
          fontSize: widget.titleFontSize,
          color: widget.titleColor,
        ),
        SizedBox(width: widget.titleSpace),
        Obx(() {
          return InkWell(
            child: LYRadioIcon(status: isRadioValue.value ? 1 : 0),
            onTap: () {
              isRadioValue.value = true;
              if (widget.callBack != null) {
                widget.callBack!(true);
              }
            },
          );
        }),
        LYText(
          text: widget.firstTitle,
          fontSize: widget.itemFontSize,
          color: widget.itemColor,
        ),
        SizedBox(width: widget.itemSpace),
        Obx(() {
          return InkWell(
            child: LYRadioIcon(status: isRadioValue.value ? 0 : 1),
            onTap: () {
              isRadioValue.value = false;
              if (widget.callBack != null) {
                widget.callBack!(false);
              }
            },
          );
        }),
        LYText(
          text: widget.secondTitle,
          fontSize: widget.itemFontSize,
          color: widget.itemColor,
        ),
      ],
    );
  }
}
