import 'package:flutter/material.dart';
import 'package:flutter_screenutil/flutter_screenutil.dart';
import 'package:zyocore/widget/assets_picker/entity/ly_asset_entity.dart';
import 'package:zyocore/widget/index.dart';

import '../../values/text.dart';

///tab组件
class TabWidget extends StatefulWidget {
  TabWidget({
    Key? key,
    required this.tabs,
    this.activeIndex = 0,
    this.selectStyle,
    this.textStyle,
    this.tabItemBottom,
    this.selectTabItemBottom,
    this.textBottomMargin = 12,
    this.selectTextBottomMargin = 3,
    this.mainAxisAlignment = MainAxisAlignment.start,
    required this.onTap,
    this.color,
  }) : super(key: key) {
    selectStyle ??= cfont3black17w500;
    textStyle ??= cfont6black17w400;
  }

  final List<String> tabs;
  final int activeIndex;
  late TextStyle? selectStyle;
  late TextStyle? textStyle;
  final Widget? tabItemBottom;
  final Widget? selectTabItemBottom;
  final double? textBottomMargin;
  final double? selectTextBottomMargin;
  final MainAxisAlignment mainAxisAlignment;
  final Function(int index) onTap;
  final Color? color;

  @override
  State<TabWidget> createState() => _TabWidgetState();
}

class _TabWidgetState extends State<TabWidget> {
  var tabItemBottom, selectTabItemBottom;

  @override
  Widget build(BuildContext context) {
    selectTabItemBottom =
        widget.tabItemBottom ?? LYAssetsImage(name: "top_arc_purple.png", height: 8.w, width: 21.w, color: widget.color);
    tabItemBottom = widget.selectTabItemBottom ?? Container();
    return Row(
      mainAxisAlignment: MainAxisAlignment.spaceAround,
      children: _getTabItems(),
    );
  }

  ///获取tab选项卡内容显示
  List<Widget> _getTabItems() {
    List<Widget> result = <Widget>[];
    for (var i = 0; i < widget.tabs.length; i++) {
      result.add(
        Expanded(
          child: InkWell(
            child: Column(
              mainAxisAlignment: widget.mainAxisAlignment,
              children: [
                Text(
                  widget.tabs[i],
                  style: widget.activeIndex == i ? widget.selectStyle : widget.textStyle,
                  overflow: TextOverflow.ellipsis,
                  softWrap: true,
                ),
                SizedBox(
                  height: widget.activeIndex == i ? widget.selectTextBottomMargin : widget.textBottomMargin,
                ),
                widget.activeIndex == i ? selectTabItemBottom : tabItemBottom,
              ],
            ),
            onTap: () {
              widget.onTap(i);
            },
          ),
        ),
      );
    }
    return result;
  }
}
