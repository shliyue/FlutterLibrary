import 'dart:async';
import 'dart:io';

import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:zyocore/widget/assets_picker/asset_picker.dart';
import 'package:zyocore/widget/assets_picker/constants/constants.dart';
import 'package:zyocore/widget/assets_picker/constants/enums.dart';
import 'package:zyocore/widget/assets_picker/delegates/asset_picker_builder_delegate.dart';
import 'package:zyocore/widget/assets_picker/delegates/assets_picker_text_delegate.dart';
import 'package:zyocore/widget/assets_picker/delegates/ly_asset_picker_builder_delegate.dart';
import 'package:zyocore/widget/assets_picker/delegates/sort_path_delegate.dart';
import 'package:zyocore/widget/assets_picker/entity/ly_asset_entity.dart';
import 'package:zyocore/widget/assets_picker/provider/ly_asset_picker_provider.dart';
import 'package:zyocore/widget/assets_picker/widget/asset_picker_page_route.dart';
import 'package:zyocore/widget/camera_picker/internals/enums.dart';
import 'package:zyocore/widget/camera_picker/widgets/camera_picker.dart';

import '../../core/configs/config.dart';
import '../../zyocore.dart';
import 'constants/typedefs.dart';

typedef CloudCheckApi = Future<List<dynamic>?> Function({List<String>? hashId, String? classId});

var defaultAllowTypes = ['jpg', 'jpeg', 'jpe', 'png', 'webp', 'bmp', 'heic', 'heif', 'dng'];

/// 自定义asset_picker
class LyAssetsPicker<Asset, Path> extends StatefulWidget {
  const LyAssetsPicker({Key? key, required this.builder}) : super(key: key);

  final AssetPickerBuilderDelegate<Asset, Path> builder;

  static late LyAssetPickerProvider lyProvider;

  static Future<List<LyAssetEntity>?> pickAssets(
    BuildContext context, {
    List<LyAssetEntity>? selectedAssets,
    int maxAssets = 9,
    int pageSize = 30,
    int maxMinute = 5,
    ThumbnailSize gridThumbnailSize = defaultAssetGridPreviewSize,
    ThumbnailSize pathThumbnailSize = defaultPathThumbnailSize,
    int gridCount = 3,
    RequestType requestType = RequestType.image,
    ThumbnailSize previewThumbnailSize = defaultAssetGridPreviewSize,
    SpecialPickerType? specialPickerType,
    Color? themeColor,
    ThemeData? pickerTheme,
    SortPathDelegate<AssetPathEntity>? sortPathDelegate,
    AssetsPickerTextDelegate? textDelegate,
    PMFilter? filterOptions,
    SpecialItemBuilder? specialItemBuilder,
    IndicatorBuilder? loadingIndicatorBuilder,
    SpecialItemPosition specialItemPosition = SpecialItemPosition.prepend,
    bool allowSpecialItemWhenEmpty = false,
    AssetSelectPredicate<LyAssetEntity>? selectPredicate,
    bool useRootNavigator = true,
    Curve routeCurve = Curves.easeIn,
    Duration initializeDelayDuration = const Duration(milliseconds: 250),
    CloudCheckApi? cloudCheckApi,
    String? classId,
    List<String>? allowTypes,
    String? packageName,
    List<String>? appendAllowTypes,
  }) async {
    if (appendAllowTypes != null) {
      defaultAllowTypes.addAll(appendAllowTypes);
    }
    if (specialItemPosition != SpecialItemPosition.none) {
      specialItemBuilder = LyAssetsPicker.specialItemBuilder;
    }
    if (maxAssets < 1) {
      throw ArgumentError(
        'maxAssets must be greater than 1.',
      );
    }
    if (pageSize % gridCount != 0) {
      throw ArgumentError(
        'pageSize must be a multiple of gridCount.',
      );
    }
    if (pickerTheme != null && themeColor != null) {
      throw ArgumentError(
        'Theme and theme color cannot be set at the same time.',
      );
    }
    if (specialPickerType == SpecialPickerType.wechatMoment) {
      if (requestType != RequestType.image) {
        throw ArgumentError(
          'SpecialPickerType.wechatMoment and requestType cannot be set at the same time.',
        );
      }
      requestType = RequestType.common;
    }
    if ((specialItemBuilder == null && specialItemPosition != SpecialItemPosition.none) ||
        (specialItemBuilder != null && specialItemPosition == SpecialItemPosition.none)) {
      throw ArgumentError('Custom item did not set properly.');
    }
    var p = false;
    if (requestType == RequestType.common) {
      p = await LyPermission.photoVideos();
    } else if (requestType == RequestType.image) {
      p = await LyPermission.photos();
    } else if (requestType == RequestType.video) {
      p = await LyPermission.videos();
    }
    if (!p) {
      throw ArgumentError('pickAssets-----没有权限');
    }
    PermissionState _ps = p ? PermissionState.authorized : PermissionState.denied;
    // final PermissionState _ps = await AssetPicker.permissionCheck(reqType: requestType);
    // final PermissionState _ps = PermissionState.authorized;

    FilterOptionGroup defaultOption = FilterOptionGroup(
        containsLivePhotos: requestType == RequestType.image,
        imageOption: FilterOption(
          needTitle: Platform.isIOS,

          /// 过滤后，会与系统自带相册不一致
          // sizeConstraint: SizeConstraint(minWidth: 750, minHeight: 750),
        ),
        videoOption: FilterOption(
          needTitle: Platform.isIOS,
          // durationConstraint: DurationConstraint(min: Duration(seconds: 5), max: Duration(minutes: 5, seconds: 0, milliseconds: 500)),
        ),
        // orders: [const OrderOption(type: OrderOptionType.updateDate)]);
        orders: [OrderOption(type: Platform.isAndroid ? OrderOptionType.updateDate : OrderOptionType.createDate)]);
    lyProvider = LyAssetPickerProvider(
        maxAssets: maxAssets,
        pageSize: pageSize,
        pathThumbnailSize: pathThumbnailSize,
        selectedAssets: selectedAssets,
        requestType: requestType,
        sortPathDelegate: sortPathDelegate,
        filterOptions: filterOptions ?? defaultOption,
        initializeDelayDuration: initializeDelayDuration,
        cloudCheckApi: cloudCheckApi,
        classId: classId,
        maxMinute: maxMinute,
        allowTypes: allowTypes ?? defaultAllowTypes);
    final Widget picker = ChangeNotifierProvider<LyAssetPickerProvider>.value(
      value: lyProvider,
      child: AssetPicker<LyAssetEntity, AssetPathEntity>(
        key: Constants.lyPickerKey,
        builder: LyAssetPickerBuilderDelegate(
            provider: lyProvider,
            initialPermission: _ps,
            gridCount: gridCount,
            textDelegate: textDelegate,
            // themeColor: themeColor,
            pickerTheme: pickerTheme ?? LyAssetsPicker.themeData(themeColor ?? LibColor.colorMain),
            gridThumbnailSize: gridThumbnailSize,
            previewThumbnailSize: ThumbnailSize(ScreenUtil().screenWidth.toInt(), ScreenUtil().screenHeight.toInt()),
            specialPickerType: specialPickerType,
            specialItemPosition: specialItemPosition,
            specialItemBuilder: specialItemBuilder,
            loadingIndicatorBuilder: loadingIndicatorBuilder,
            allowSpecialItemWhenEmpty: allowSpecialItemWhenEmpty,
            selectPredicate: selectPredicate,
            shouldRevertGrid: false,
            packageName: packageName),
      ),
    );
    final List<LyAssetEntity>? result = await Navigator.of(
      context,
      rootNavigator: useRootNavigator,
    ).push<List<LyAssetEntity>>(
      AssetPickerPageRoute<List<LyAssetEntity>>(
          builder: picker, transitionCurve: routeCurve, transitionDuration: initializeDelayDuration, settings: const RouteSettings(name: 'ly_assets_picker')),
    );
    return result;
  }

  static FutureOr<dynamic> _onEntitySaving({BuildContext? context, CameraPickerViewType? viewType, File? file}) {
    // return null;
  }

  /// 特殊项
  static Widget? specialItemBuilder(BuildContext context, dynamic path, int length) {
    var h = (MediaQuery.of(context).size.width - 4) / 3;
    return GestureDetector(
      onTap: () async {
        try {
          bool p;
          if (lyProvider.requestType == RequestType.video) {
            p = await LyPermission.video();
          } else {
            p = await LyPermission.cameras();
          }
          if (!p) return;

          /// 默认值会引起部分机型崩溃
          /// resolutionPreset: ResolutionPreset.medium,
          AssetEntity? assetEntity = await CameraPicker.pickFromCamera(context,
              theme: LyAssetsPicker.themeCameraData(LibColor.colorMain),
              resolutionPreset: ResolutionPreset.veryHigh,
              imageFormatGroup: ImageFormatGroup.yuv420,
              maximumRecordingDuration: const Duration(minutes: 5, seconds: 0, milliseconds: 500),
              enableRecording: lyProvider.requestType == RequestType.video);
          //
          // Logger.d(
          //     "==========assets=inner=assetEntity==========${(await assetEntity?.file)?.path}");
          List<LyAssetEntity> images = [];

          /// 已选择的图片
          if (lyProvider.selectedAssets.isNotEmpty) {
            images.addAll(lyProvider.selectedAssets);
          }
          if (assetEntity != null) {
            LyAssetEntity lyAssetEntity = LyAssetEntity.fromAsset(assetEntity);
            images.add(lyAssetEntity);
          }
          // Logger.d("==========assets=inner===========$images");
          Navigator.of(context).pop(images);
        } catch (e) {
          Logger.d(e.toString());
        }
      },
      child: Container(
        width: h,
        height: h,
        alignment: Alignment.center,
        color: lightF8F8F8,
        child: Image(
          image: AssetImage('assets/images/take_picture.png', package: Config.packageName),
          width: 46,
          height: 46,
        ),
      ),
    );
  }

  // Build a dark theme according to the theme color.
  /// 通过主题色构建一个默认的暗黑主题
  static ThemeData themeData(Color themeColor) {
    return ThemeData.light().copyWith(
      primaryColor: Colors.grey[100],
      primaryColorLight: Colors.grey[100],
      primaryColorDark: Colors.grey[100],
      canvasColor: Colors.grey[300],
      scaffoldBackgroundColor: Colors.grey[100],
      bottomAppBarColor: Colors.grey[100],
      cardColor: Colors.grey[100],
      highlightColor: Colors.transparent,
      toggleableActiveColor: themeColor,
      textSelectionTheme: TextSelectionThemeData(
        cursorColor: themeColor,
        selectionColor: themeColor.withAlpha(100),
        selectionHandleColor: themeColor,
      ),
      iconTheme: IconThemeData(
        color: Colors.grey[900],
      ),
      indicatorColor: themeColor,
      appBarTheme: const AppBarTheme(
          systemOverlayStyle: SystemUiOverlayStyle(
            statusBarBrightness: Brightness.light,
            statusBarIconBrightness: Brightness.dark,
          ),
          elevation: 0,
          backgroundColor: lightFFFFFF),
      buttonTheme: ButtonThemeData(buttonColor: themeColor),
      colorScheme: ColorScheme(
        primary: Colors.grey[900]!,
        secondary: themeColor,
        background: Colors.grey[100]!,
        surface: Colors.grey[100]!,
        brightness: Brightness.dark,
        error: const Color(0xffcf6679),
        onPrimary: Colors.white,
        onSecondary: Colors.white,
        onSurface: Colors.black,
        onBackground: Colors.black,
        onError: Colors.white,
      ),
    );
  }

  /// 相册打开相机时样式
  static ThemeData themeCameraData(Color themeColor) {
    return ThemeData.dark().copyWith(
      primaryColor: Colors.grey[900],
      primaryColorLight: Colors.grey[900],
      primaryColorDark: Colors.grey[900],
      canvasColor: Colors.grey[850],
      scaffoldBackgroundColor: Colors.grey[900],
      bottomAppBarColor: Colors.grey[900],
      cardColor: Colors.grey[900],
      highlightColor: Colors.transparent,
      toggleableActiveColor: themeColor,
      textSelectionTheme: TextSelectionThemeData(
        cursorColor: themeColor,
        selectionColor: themeColor.withAlpha(100),
        selectionHandleColor: themeColor,
      ),
      indicatorColor: themeColor,
      appBarTheme: const AppBarTheme(
        systemOverlayStyle: SystemUiOverlayStyle(
          statusBarBrightness: Brightness.dark,
          statusBarIconBrightness: Brightness.light,
        ),
        elevation: 0,
      ),
      buttonTheme: ButtonThemeData(buttonColor: themeColor),
      colorScheme: ColorScheme(
        primary: Colors.grey[900]!,
        secondary: themeColor,
        background: Colors.grey[900]!,
        surface: Colors.grey[900]!,
        brightness: Brightness.dark,
        error: const Color(0xffcf6679),
        onPrimary: Colors.black,
        onSecondary: Colors.black,
        onSurface: Colors.white,
        onBackground: Colors.white,
        onError: Colors.black,
      ),
    );
  }

  static String pathNameBuilder(AssetPathEntity pathEntity) {
    var transPathName = pathEntity.name;
    switch (pathEntity.name) {
      case 'Recent':
        transPathName = '最近项目';
        break;
      case 'Camera':
        transPathName = '相机';
        break;
      case 'Pictures':
        transPathName = '相片';
        break;
      case 'Movies':
        transPathName = '电影';
        break;
      case 'ScreenRecorder':
        transPathName = '录屏';
        break;
      case 'Screenshots':
        transPathName = '截屏';
        break;
      case 'WeiXin':
        transPathName = '微信';
        break;
      case 'Download':
        transPathName = '下载';
        break;
    }
    return transPathName;
  }

  @override
  LyAssetsPickerState<Asset, Path> createState() => LyAssetsPickerState<Asset, Path>();
}

class LyAssetsPickerState<Asset, Path> extends State<LyAssetsPicker<Asset, Path>> with WidgetsBindingObserver {
  @override
  void initState() {
    super.initState();
    // Logger.d('AssetPickerState initState--------------------');
    WidgetsBinding.instance.addObserver(this);
    AssetPicker.registerObserve(_onLimitedAssetsUpdated);
    widget.builder.initLyState(this);
  }

  @override
  void didChangeAppLifecycleState(AppLifecycleState state) {
    // Logger.d('AssetPickerState didChangeAppLifecycleState--------------------');
    super.didChangeAppLifecycleState(state);
    if (state == AppLifecycleState.resumed) {
      PhotoManager.requestPermissionExtend().then(
        (PermissionState ps) => widget.builder.permission.value = ps,
      );
    }
  }

  @override
  void dispose() {
    // Logger.d('AssetPickerState dispose--------------------');
    WidgetsBinding.instance.removeObserver(this);
    AssetPicker.unregisterObserve(_onLimitedAssetsUpdated);
    widget.builder.dispose();
    super.dispose();
  }

  Future<void> _onLimitedAssetsUpdated(MethodCall call) async {
    return widget.builder.onAssetsChanged(call, (VoidCallback fn) {
      fn();
      if (mounted) {
        setState(() {});
      }
    });
  }

  @override
  Widget build(BuildContext context) {
    // Logger.d('AssetPickerState build--------------------');
    return widget.builder.build(context);
  }
}
