///
/// [Author] Alex (https://github.com/AlexV525)
/// [Date] 2021/7/23 16:07
///
import 'dart:async';
import 'dart:io';

import 'package:flutter/material.dart';
import 'package:zyocore/core/configs/config.dart';
import 'package:zyocore/widget/assets_picker/constants/constants.dart';
import 'package:zyocore/widget/assets_picker/entity/group_entity.dart';
import 'package:zyocore/widget/assets_picker/entity/ly_asset_entity.dart';
import 'package:zyocore/widget/assets_picker/provider/asset_picker_provider.dart';
import 'package:zyocore/widget/assets_picker_sorted/provider/sorted_assets_picker_provider.dart';
import 'package:zyocore/zyocore.dart';

import '../scale_text.dart';

/// 图片列表网格---用户手动下载icloud远程图片到本地
class LocallyHandlebleAvailableBuilder extends StatefulWidget {
  const LocallyHandlebleAvailableBuilder({
    Key? key,
    required this.assetEntityImageProvider,
    required this.assetPickerProvider,
    required this.builder,
    this.isOriginal = true,
    this.maxMinute = 5,
  }) : super(key: key);

  final AssetEntityImageProvider assetEntityImageProvider;
  final Widget Function(BuildContext context, AssetEntity asset) builder;
  final bool isOriginal;
  final AssetPickerProvider<LyAssetEntity, AssetPathEntity> assetPickerProvider;
  final int? maxMinute;

  @override
  _LocallyHandlebleAvailableBuilderState createState() => _LocallyHandlebleAvailableBuilderState();
}

class _LocallyHandlebleAvailableBuilderState extends State<LocallyHandlebleAvailableBuilder> {
  bool _isLocallyAvailable = false;
  PMProgressHandler? _progressHandler;
  late StreamSubscription ssRefreshIcloud;
  File? file;

  @override
  void dispose() {
    ssRefreshIcloud.cancel();
    super.dispose();
  }

  @override
  void initState() {
    super.initState();
    initEventSubscribe();
    _checkLocallyAvailable();
  }

  initEventSubscribe() {
    ssRefreshIcloud = lyBus.on(eventName: Constants.refreshIcloudById).listen(refreshIcloudAssetById);
  }

  void refreshIcloudAssetById(LyEvent e) {
    var id = widget.assetEntityImageProvider.entity.id;
    if (id == e.data['id']) {
      _downloadIcloud();
    }
  }

  Future<void> _checkLocallyAvailable() async {
    var cacheAvailableList = lySp.getList(key: SK_LOCALLY_AVAILABLE_CACHE);
    // Logger.d('cacheAvailableList====$cacheAvailableList');
    // Logger.d(
    //     'widget.assetEntityImageProvider.entity.id${widget.assetEntityImageProvider.entity.id}');
    var assetAvailable = await widget.assetEntityImageProvider.entity.isLocallyAvailable();
    var inCache = cacheAvailableList.contains(widget.assetEntityImageProvider.entity.id);

    /// 当同时满足在icloud和不符合规格时，优先展示不符合规格
    /// 此时隐藏底层（照片在iCloud）
    _isLocallyAvailable = assetAvailable || inCache;
    // Logger.d(
    //     '_checkLocallyAvailable_isLocallyAvailable====$_isLocallyAvailable');
    if (!mounted) {
      return;
    }
    setState(() {});
    if (!_isLocallyAvailable) {
      _progressHandler = PMProgressHandler();
    }
  }

  _downloadIcloud() async {
    file = await widget.assetEntityImageProvider.entity.loadFile(
      isOrigin: widget.isOriginal,
      withSubtype: true,
      progressHandler: _progressHandler,
    );
    // Logger.d('Produced file: $file');
    if (file != null) {
      _isLocallyAvailable = true;
      if (mounted) {
        setState(() {});
      }

      /// 修改状态触发列表刷新
      changeLocalState();

      /// 更新分组title
      updateGroupAfterDownload();
    }
    // _progressHandler?.stream.listen((PMProgressState s) async {
    //   // Logger.d('eq2=${notLocallyAvailableList.hashCode}');
    //   // Logger.d('id=${widget.assetEntityImageProvider.entity.id}');
    //   // Logger.d('1111=$index');
    //   // Logger.d('state=${s.state}');
    //   // Logger.d('xprogress=${s.progress}');
    //   if (s.state == PMRequestState.success || s.progress >= 0.99) {
    //     _isLocallyAvailable = true;
    //     file = null;
    //     if (mounted) {
    //       setState(() {});
    //     }
    //
    //     // Logger.d(
    //     //     'hashCode前=${widget.assetPickerProvider.notLocallyAvailables.hashCode}');
    //     var notLocallyAvailableList =
    //         List<String>.from(widget.assetPickerProvider.notLocallyAvailables);
    //     var index = notLocallyAvailableList
    //         .indexOf(widget.assetEntityImageProvider.entity.id);
    //
    //     // Logger.d('hashCode后=${notLocallyAvailableList.hashCode}');
    //     // Logger.d('1111=$index');
    //     var cacheAvailableList =
    //         await StorageUtil.getList(key: SK_LOCALLY_AVAILABLE_CACHE) ?? [];
    //
    //     if (index != -1) {
    //       if (!cacheAvailableList
    //           .contains(widget.assetEntityImageProvider.entity.id)) {
    //         cacheAvailableList.add(widget.assetEntityImageProvider.entity.id);
    //         await StorageUtil.setList(
    //             key: SK_LOCALLY_AVAILABLE_CACHE, value: cacheAvailableList);
    //
    //         Logger.d(
    //             'move 前=${widget.assetPickerProvider.notLocallyAvailables}');
    //
    //         notLocallyAvailableList.removeAt(index);
    //         Logger.d('当前=$notLocallyAvailableList');
    //         widget.assetPickerProvider.notLocallyAvailables =
    //             notLocallyAvailableList;
    //
    //         Logger.d('赋值=${widget.assetPickerProvider.notLocallyAvailables}');
    //       }
    //     }
    //   }
    // });
  }

  changeLocalState() async {
    /// 更新列表页icloud遮盖
    // Logger.d(
    //     'hashCode前=${widget.assetPickerProvider.notLocallyAvailables.hashCode}');
    var notLocallyAvailableList = List<String>.from(widget.assetPickerProvider.notLocallyAvailables);
    var index = notLocallyAvailableList.indexOf(widget.assetEntityImageProvider.entity.id);

    // Logger.d('hashCode后=${notLocallyAvailableList.hashCode}');
    // Logger.d('1111=$index');
    // Logger.d('当前ID=${widget.assetEntityImageProvider.entity.id}');
    if (index != -1) {
      // Logger.d('当前=$notLocallyAvailableList');
      // Logger.d('move 前l=${notLocallyAvailableList.length}');
      notLocallyAvailableList.removeAt(index);
      widget.assetPickerProvider.notLocallyAvailables = notLocallyAvailableList;
      // Logger.d('move 后l=${widget.assetPickerProvider.notLocallyAvailables.length}');

      var cacheAvailableList = lySp.getList(key: SK_LOCALLY_AVAILABLE_CACHE) ?? [];
      if (!cacheAvailableList.contains(widget.assetEntityImageProvider.entity.id)) {
        cacheAvailableList.add(widget.assetEntityImageProvider.entity.id);
        lySp.setList(key: SK_LOCALLY_AVAILABLE_CACHE, value: cacheAvailableList);
      }
    }
  }

  /// 更新分组
  updateGroupAfterDownload() {
    if (widget.assetPickerProvider is LySortedAssetPickerProvider) {
      LySortedAssetPickerProvider pickerProvider = widget.assetPickerProvider as LySortedAssetPickerProvider;
      LyAssetEntity currentAsset = LyAssetEntity.fromAsset(widget.assetEntityImageProvider.entity);

      /// 当前分组
      List<GroupEntity> assetGroupList = pickerProvider.currentGroup;
      var assetCreateTime = utils.date.formatDate(Platform.isAndroid ? currentAsset.modifiedDateTime : currentAsset.createDateTime, format: DateFormats.y_mo_d);
      var index = assetGroupList.indexWhere((element) => element.createDate == assetCreateTime);

      GroupEntity groupEntity = assetGroupList[index];

      var notAllSelected = true;
      if (groupEntity.child.isNotEmpty) {
        // var curFileIndex = groupEntity.child
        //     .indexWhere((element) => element.id == currentAsset.id);
        // Logger.d('====child.l==${groupEntity.child.length}');
        List<LyAssetEntity> availableList = [];
        List<LyAssetEntity> selectedList = [];
        for (var element in groupEntity.child) {
          if (AssetUtil.isAssetAvailable(element, pickerProvider.invalidFormats, pickerProvider.notLocallyAvailables)) {
            availableList.add(element);
          }

          if (!element.notSelected) {
            selectedList.add(element);
          }
        }
        if (availableList.length != selectedList.length) {
          notAllSelected = true;

          /// 修改title
          List<String> tempTitleList = pickerProvider.titleSelected;
          tempTitleList.remove(assetCreateTime);
          pickerProvider.titleSelected = tempTitleList;
        } else {
          notAllSelected = false;

          /// 修改title
          List<String> tempTitleList = pickerProvider.titleSelected;
          tempTitleList.add(assetCreateTime);
          pickerProvider.titleSelected = tempTitleList;
        }
      }
      groupEntity.notAllSelected = notAllSelected;

      /// 修改当前分组
      assetGroupList[index] = groupEntity;

      /// 已选中的
      pickerProvider.selectedAssets = AssetUtil.transAssetsList(assetGroupList, pickerProvider.invalidFormats, pickerProvider.notLocallyAvailables);

      /// 更新groupList
      pickerProvider.currentGroup = assetGroupList;
    }
  }

  Widget _indicator(BuildContext context) {
    return StreamBuilder<PMProgressState>(
      stream: _progressHandler!.stream,
      initialData: const PMProgressState(0, PMRequestState.prepare),
      builder: (BuildContext c, AsyncSnapshot<PMProgressState> s) {
        if (s.hasData) {
          // final double progress = s.data!.progress;
          final PMRequestState state = s.data!.state;
          if (state == PMRequestState.prepare) {
            var formatInvalid = widget.assetPickerProvider.invalidFormats.contains(widget.assetEntityImageProvider.entity.id);

            var videoTooLong = widget.assetEntityImageProvider.entity.duration > (widget.maxMinute! * 60);
            // Logger.d(
            //     '_checkLocallyAvailable_formatInvalid====$formatInvalid');
            if (formatInvalid || videoTooLong) return const SizedBox.shrink();
            return Column(
              mainAxisSize: MainAxisSize.min,
              children: <Widget>[
                Image(
                  image: AssetImage('assets/images/icon_icloud.png', package: Config.packageName),
                  width: 24.w,
                  height: 24.w,
                ),
                SizedBox(height: 2.w),
                ScaleText(
                  '照片视频在iCloud',
                  style: TextStyle(
                    fontSize: 12.sp,
                    color: dark1A1A1A,
                  ),
                ),
              ],
            );
          } else if (state != PMRequestState.success && state != PMRequestState.failed) {
            return Image.asset(
              'assets/images/icon_uploading.png',
              width: 24.w,
              height: 24.w,
              package: Config.packageName,
            );
          } else {
            return failedItemBuilder(context);
          }
        }
        return const SizedBox.shrink();
      },
    );
  }

  @override
  Widget build(BuildContext context) {
    if (_isLocallyAvailable) {
      return widget.builder(context, widget.assetEntityImageProvider.entity);
    }
    if (_progressHandler != null) {
      return Stack(
        children: <Widget>[
          Positioned.fill(
            child: ExtendedImage(
              image: widget.assetEntityImageProvider,
              fit: BoxFit.cover,
              clearMemoryCacheWhenDispose: true,
              filterQuality: FilterQuality.low,
              loadStateChanged: (ExtendedImageState state) {
                Widget loader = const SizedBox.shrink();
                switch (state.extendedImageLoadState) {
                  case LoadState.loading:
                    loader = const ColoredBox(color: Color(0x10ffffff));
                    break;
                  case LoadState.completed:
                    loader = RepaintBoundary(child: state.completedWidget);
                    break;
                  case LoadState.failed:
                    // widget.image.resolve(ImageConfiguration.empty).addListener(listener);
                    loader = failedItemBuilder(context);
                    break;
                }
                return loader;
              },
            ),
          ),
          GestureDetector(
            onTap: _downloadIcloud,
            child: Container(child: _indicator(context), alignment: Alignment.center, decoration: BoxDecoration(color: Colors.white.withOpacity(0.6))),
          ),
          Positioned(right: 5.w, bottom: 5.w, child: _videoIcloudIndicator(context, widget.assetEntityImageProvider.entity))
        ],
      );
    }
    return const SizedBox.shrink();
  }

  /// Item widgets when the thumb data load failed.
  /// 资源缩略数据加载失败时使用的部件
  Widget failedItemBuilder(BuildContext context) {
    return Container(
      color: const Color(0xFFEAEAEA),
      alignment: Alignment.center,
      child: Image(
        image: AssetImage('assets/images/load_fail.png', package: Config.packageName),
        width: 48.w,
        height: 48.w,
      ),
    );
  }

  /// image 云端标识
  /// 将指示器的图标和文字设置为 [Colors.white]。
  Widget _videoIcloudIndicator(BuildContext context, AssetEntity asset) {
    final List<String> currentUploaded = widget.assetPickerProvider.currentUploaded;
    if (!currentUploaded.contains(asset.id)) return Container();
    return Image(
      image: AssetImage('assets/images/cloud_upload.png', package: Config.packageName),
      width: 24.w,
      height: 24.w,
    );
  }
}
