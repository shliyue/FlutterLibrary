///
/// [Author] Alex (https://github.com/AlexV525)
/// [Date] 2021/5/11 11:37
///
import 'package:flutter/material.dart';
import 'package:zyocore/core/configs/config.dart';
import 'package:zyocore/widget/assets_picker/entity/ly_asset_entity.dart';
import 'package:zyocore/widget/assets_picker/provider/asset_picker_provider.dart';
import 'package:zyocore/widget/assets_picker/widget/builder/locally_handlable_available_builder.dart';
import 'package:zyocore/zyocore.dart';

class AssetEntityGridItemBuilder extends StatefulWidget {
  const AssetEntityGridItemBuilder({
    Key? key,
    required this.image,
    required this.assetPickerProvider,
    required this.failedItemBuilder,
    this.maxMinute,
  }) : super(key: key);

  final AssetEntityImageProvider image;
  final WidgetBuilder failedItemBuilder;
  final AssetPickerProvider<LyAssetEntity, AssetPathEntity> assetPickerProvider;
  final int? maxMinute;

  @override
  AssetEntityGridItemWidgetState createState() =>
      AssetEntityGridItemWidgetState();
}

class AssetEntityGridItemWidgetState extends State<AssetEntityGridItemBuilder> {
  AssetEntityImageProvider get imageProvider => widget.image;

  Widget? child;

  Widget get newChild {
    return LocallyHandlebleAvailableBuilder(
      assetEntityImageProvider: imageProvider,
      isOriginal: imageProvider.isOriginal,
      assetPickerProvider: widget.assetPickerProvider,
      maxMinute: widget.maxMinute,
      builder: (BuildContext context, AssetEntity asset) {
        return ExtendedImage(
          image: imageProvider,
          fit: BoxFit.cover,
          clearMemoryCacheWhenDispose: true,
          filterQuality: FilterQuality.medium,
          loadStateChanged: (ExtendedImageState state) {
            Widget loader = const SizedBox.shrink();
            switch (state.extendedImageLoadState) {
              case LoadState.loading:
                loader = const ColoredBox(color: Color(0x10ffffff));
                break;
              case LoadState.completed:
                loader = RepaintBoundary(child: state.completedWidget);
                break;
              case LoadState.failed:
                // widget.image.resolve(ImageConfiguration.empty).addListener(listener);
                loader = widget.failedItemBuilder(context);
                break;
            }
            return loader;
          },
        );
      },
    );
  }

  // ImageStreamListener listener=ImageStreamListener((info,b){
  //   Logger.d('>>>>>>>>>>>>>>>>>>>onImage=${info.toString()}');
  // },onError: (o,s){
  //   Logger.d('>>>>>>>>>>>>>>>>>>>onError=o=$o===s=$s');
  // });

  /// Item widgets when the thumb data load failed.
  /// 资源缩略数据加载失败时使用的部件
  Widget failedItemBuilder(BuildContext context) {
    return Container(
      color: const Color(0xFFEAEAEA),
      alignment: Alignment.center,
      child: Image(
        image: AssetImage('assets/images/load_fail.png',
            package: Config.packageName),
        width: 48.w,
        height: 48.w,
      ),
    );
  }

  @override
  @mustCallSuper
  Widget build(BuildContext context) {
    child ??= newChild;
    return child!;
  }
}
