// Copyright 2019 The FlutterCandies author. All rights reserved.
// Use of this source code is governed by an Apache license that can be found
// in the LICENSE file.

import 'package:flutter/material.dart';
import 'package:zyocore/zyocore.dart';

import '../../delegates/asset_picker_viewer_builder_delegate.dart';

/// 在用版本
class SimpleImagePageBuilder extends StatefulWidget {
  const SimpleImagePageBuilder({
    super.key,
    required this.asset,
    required this.delegate,
    this.previewThumbnailSize,
  });

  /// Asset currently displayed.
  /// 展示的资源
  final AssetEntity asset;

  final AssetPickerViewerBuilderDelegate<AssetEntity, AssetPathEntity> delegate;

  final ThumbnailSize? previewThumbnailSize;

  @override
  State<SimpleImagePageBuilder> createState() => _SimpleImagePageBuilderState();
}

class _SimpleImagePageBuilderState extends State<SimpleImagePageBuilder> {
  Widget _imageBuilder(BuildContext context, AssetEntity asset) {
    return ExtendedImage(
      image: AssetEntityImageProvider(
        asset,
        isOriginal: widget.previewThumbnailSize == null,
        thumbnailSize: widget.previewThumbnailSize,
      ),
      fit: BoxFit.contain,
      mode: ExtendedImageMode.gesture,
      onDoubleTap: widget.delegate.updateAnimation,
      initGestureConfigHandler: (ExtendedImageState state) => GestureConfig(
        minScale: 1.0,
        maxScale: 3.0,
        animationMinScale: 0.6,
        animationMaxScale: 4.0,
        inPageView: true,
      ),
      loadStateChanged: (ExtendedImageState state) {
        return widget.delegate.previewWidgetLoadStateChanged(
          context,
          state,
          hasLoaded: state.extendedImageLoadState == LoadState.completed,
        );
      },
    );
  }

  @override
  Widget build(BuildContext context) {
    return GestureDetector(
      behavior: HitTestBehavior.opaque,
      onTap: widget.delegate.switchDisplayingDetail,
      child: Builder(
        builder: (BuildContext context) {
          return _imageBuilder(context, widget.asset);
        },
      ),
    );
  }
}
