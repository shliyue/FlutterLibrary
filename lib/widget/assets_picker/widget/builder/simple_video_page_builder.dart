// Copyright 2019 The FlutterCandies author. All rights reserved.
// Use of this source code is governed by an Apache license that can be found
// in the LICENSE file.

import 'dart:async';
import 'dart:io';

import 'package:flutter/material.dart';
import 'package:zyocore/zyocore.dart';
import 'package:zyocore/core/configs/config.dart';
import 'package:zyocore/widget/assets_picker/constants/constants.dart';

import '../../delegates/asset_picker_viewer_builder_delegate.dart';

/// 在用版本
class SimpleVideoPageBuilder extends StatefulWidget {
  const SimpleVideoPageBuilder({
    super.key,
    required this.asset,
    required this.delegate,
    this.hasOnlyOneVideoAndMoment = false,
  });

  /// Asset currently displayed.
  /// 展示的资源
  final AssetEntity asset;

  final AssetPickerViewerBuilderDelegate<AssetEntity, AssetPathEntity> delegate;

  /// Only previewing one video and with the [SpecialPickerType.wechatMoment].
  /// 是否处于 [SpecialPickerType.wechatMoment] 且只有一个视频
  final bool hasOnlyOneVideoAndMoment;

  @override
  State<SimpleVideoPageBuilder> createState() => _SimpleVideoPageBuilderState();
}

class _SimpleVideoPageBuilderState extends State<SimpleVideoPageBuilder> {
  /// Controller for the video player.
  /// 视频播放的控制器
  VideoPlayerController get controller => _controller!;
  VideoPlayerController? _controller;

  /// Whether the controller has initialized.
  /// 控制器是否已初始化
  bool hasLoaded = false;

  /// Whether there's any error when initialize the video controller.
  /// 初始化视频控制器时是否发生错误
  bool hasErrorWhenInitializing = false;

  /// Whether the player is playing.
  /// 播放器是否在播放
  final ValueNotifier<bool> isPlaying = ValueNotifier<bool>(false);

  /// Whether the controller is playing.
  /// 播放控制器是否在播放
  bool get isControllerPlaying => _controller?.value.isPlaying ?? false;

  bool _isInitializing = false;
  bool _isLocallyAvailable = false;

  late StreamSubscription ssRefreshVideo;

  @override
  void initState() {
    initEventSubscribe();
    super.initState();
  }

  initEventSubscribe() {
    ssRefreshVideo= lyBus.on(
        eventName: Constants.refreshVideoAfterDownload,
    ).listen(refreshVideoAfterDownload);
  }

  void refreshVideoAfterDownload(LyEvent arg) {
    // Logger.d('refreshVideoAfterDownload= id=${widget.asset.id}');
    // Logger.d('refreshVideoAfterDownload=arg id=${arg['id']}');
    var id = widget.asset.id;
    if (id == arg.data['id']) {
      initializeVideoPlayerController();
    }
  }

  @override
  void dispose() {
    /// Remove listener from the controller and dispose it when widget dispose.
    /// 部件销毁时移除控制器的监听并销毁控制器。
    _controller
      ?..removeListener(videoPlayerListener)
      ..pause()
      ..dispose();
    ssRefreshVideo.cancel();
    super.dispose();
  }

  /// Get media url from the asset, then initialize the controller and add with a listener.
  /// 从资源获取媒体url后初始化，并添加监听。
  Future<void> initializeVideoPlayerController() async {
    _isInitializing = true;
    _isLocallyAvailable = true;
    final String? url = await widget.asset.getMediaUrl();
    if (url == null) {
      hasErrorWhenInitializing = true;
      if (mounted) {
        setState(() {});
      }
      return;
    }
    if (Platform.isAndroid) {
      _controller = VideoPlayerController.contentUri(Uri.parse(url));
    } else {
      _controller = VideoPlayerController.network(url);
    }
    try {
      await controller.initialize();
      hasLoaded = true;
      // controller
      //   ..addListener(videoPlayerListener)
      //   ..setLooping(widget.hasOnlyOneVideoAndMoment);
      controller
        .addListener(videoPlayerListener);
      if (widget.hasOnlyOneVideoAndMoment) {
        controller.play();
      }
      if (Platform.isIOS) {
        controller.seekTo(Duration.zero);
        // controller.play();
        // Future.delayed(const Duration(milliseconds: 100), () {
        //   controller.pause();
        // });
      }
    } catch (e) {
      Logger.d('Error when initialize video controller: $e');
      hasErrorWhenInitializing = true;
    } finally {
      if (mounted) {
        setState(() {});
      }
    }
  }

  /// Listener for the video player.
  /// 播放器的监听方法
  void videoPlayerListener() {
    if (isControllerPlaying != isPlaying.value) {
      isPlaying.value = isControllerPlaying;
    }
  }

  /// Callback for the play button.
  /// 播放按钮的回调
  ///
  /// Normally it only switches play state for the player. If the video reaches the end,
  /// then click the button will make the video replay.
  /// 一般来说按钮只切换播放暂停。当视频播放结束时，点击按钮将从头开始播放。
  Future<void> playButtonCallback() async {
    if (isPlaying.value) {
      controller.pause();
      return;
    }
    if (widget.delegate.isDisplayingDetail.value) {
      widget.delegate.switchDisplayingDetail(value: false);
    }
    if (controller.value.duration == controller.value.position) {
      controller
        ..seekTo(Duration.zero)
        ..play();
      return;
    }
    controller.play();
  }

  Widget _contentBuilder(BuildContext context) {
    return Stack(
      fit: StackFit.expand,
      children: <Widget>[
        Positioned.fill(
          child: Center(
            child: AspectRatio(
              aspectRatio: controller.value.aspectRatio,
              child: VideoPlayer(controller),
            ),
          ),
        ),
        if (!widget.hasOnlyOneVideoAndMoment)
          ValueListenableBuilder<bool>(
            valueListenable: isPlaying,
            builder: (_, bool value, __) => GestureDetector(
              behavior: HitTestBehavior.opaque,
              onTap: value
                  ? playButtonCallback
                  : widget.delegate.switchDisplayingDetail,
              child: Center(
                child: AnimatedOpacity(
                  duration: kThemeAnimationDuration,
                  opacity: value ? 0.0 : 1.0,
                  child: GestureDetector(
                    onTap: playButtonCallback,
                    child: DecoratedBox(
                      decoration: const BoxDecoration(
                        boxShadow: <BoxShadow>[
                          BoxShadow(color: Colors.black12)
                        ],
                        shape: BoxShape.circle,
                      ),
                      child: Icon(
                        value
                            ? Icons.pause_circle_outline
                            : Icons.play_circle_filled,
                        size: 70.0,
                        color: Colors.white,
                      ),
                    ),
                  ),
                ),
              ),
            ),
          ),
      ],
    );
  }

  @override
  Widget build(BuildContext context) {
    if (hasErrorWhenInitializing) {
      return failedItemBuilder(context);
    }
    if (!_isLocallyAvailable && !_isInitializing) {
      return placeHoldWidget;
    }
    if (!hasLoaded) {
      return Image.asset(
        'assets/images/icon_uploading.png',
        width: 50.w,
        height: 50.w,
        package: Config.packageName,
      );
    }
    return GestureDetector(
      onLongPress: MediaQuery.of(context).accessibleNavigation
          ? playButtonCallback
          : null,
      child: _contentBuilder(context),
    );
  }

  Widget get placeHoldWidget {
    final AssetEntityImageProvider imageProvider = AssetEntityImageProvider(
        widget.asset,
        isOriginal: false,
        thumbnailSize: ThumbnailSize(ScreenUtil().screenWidth.toInt(),
            ScreenUtil().screenWidth.toInt()));
    return ValueListenableBuilder<bool>(
        valueListenable: isPlaying,
        builder: (_, bool value, __) => GestureDetector(
              onTap: value
                  ? playButtonCallback
                  : widget.delegate.switchDisplayingDetail,
              child: Container(
                alignment: Alignment.center,
                color: Colors.black,
                child: AspectRatio(
                  aspectRatio: 1.0,
                  child: ExtendedImage(
                    image: imageProvider,
                    fit: BoxFit.fitWidth,
                    clearMemoryCacheWhenDispose: true,
                    filterQuality: FilterQuality.medium,
                    loadStateChanged: (ExtendedImageState state) {
                      Widget loader = const SizedBox.shrink();
                      switch (state.extendedImageLoadState) {
                        case LoadState.loading:
                          loader = const ColoredBox(color: Color(0x10ffffff));
                          break;
                        case LoadState.completed:
                          loader =
                              RepaintBoundary(child: state.completedWidget);
                          break;
                        case LoadState.failed:
                          // widget.image.resolve(ImageConfiguration.empty).addListener(listener);
                          loader = failedItemBuilder(context);
                          break;
                      }
                      return loader;
                    },
                  ),
                ),
              ),
            ));
  }

  /// Item widgets when the thumb data load failed.
  /// 资源缩略数据加载失败时使用的部件
  Widget failedItemBuilder(BuildContext context) {
    return Container(
      color: const Color(0xFFEAEAEA),
      alignment: Alignment.center,
      child: Image(
        image: AssetImage('assets/images/load_fail.png',
            package: Config.packageName),
        width: 48.w,
        height: 48.w,
      ),
    );
  }
}
