import 'package:zyocore/widget/assets_picker/entity/ly_asset_entity.dart';

class AssetFileEntity {
  String id = '';
  String createDate = '';
  LyAssetEntity asset;

  AssetFileEntity({
    required this.id,
    required this.createDate,
    required this.asset,
  });

  Map toJson() {
    Map map = {};
    map["id"] = id;
    map["createDate"] = createDate;
    map["asset"] = asset;
    return map;
  }
}
