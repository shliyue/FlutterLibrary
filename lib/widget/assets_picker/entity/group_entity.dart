import 'package:zyocore/widget/assets_picker/entity/ly_asset_entity.dart';

class GroupEntity {
  List<LyAssetEntity> child = [];
  String createDate = '';
  bool notAllSelected = true;

  GroupEntity(
      {required this.child,
      required this.createDate,
      this.notAllSelected = true});

  Map toJson() {
    Map map = {};
    map["child"] = child;
    map["createDate"] = createDate;
    map["notAllSelected"] = notAllSelected;
    return map;
  }
}
