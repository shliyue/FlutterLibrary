import 'package:flutter/material.dart';
import 'package:zyocore/widget/assets_picker/entity/asset_file_entity.dart';
import 'package:zyocore/zyocore.dart';

/// 对AssetEntity进行扩展，增加自定义对字段
class LyAssetEntity extends AssetEntity {
  @override
  String id;
  @override
  int typeInt; //1 img  2:video 3 audio
  @override
  int width;
  @override
  int height;
  @override
  int duration;
  @override
  int orientation;
  @override
  bool isFavorite;
  @override
  String? title;
  int? createDateSecond;
  @override
  int? modifiedDateSecond;
  @override
  String? relativePath;
  @override
  String? mimeType;
  bool notSelected;

  /// 扩展字段
  bool? uploadCloud;
  bool? isAvailable; //本地是否可用

  /// 文件的md5计算后的hash值
  String? hashId;

  //服务器图片地址
  String? netUrl;

  LyAssetEntity({
    required this.id,
    required this.typeInt,
    required this.width,
    required this.height,
    this.duration = 0,
    this.orientation = 0,
    this.isFavorite = false,
    this.notSelected = true,
    this.uploadCloud,
    this.isAvailable,
    this.hashId,
    this.title,
    this.createDateSecond,
    this.modifiedDateSecond,
    this.relativePath,
    double? latitude,
    double? longitude,
    this.mimeType,
    this.netUrl,
  }) : super(
    id: id,
    typeInt: typeInt,
    width: width,
    height: height,
    duration: duration,
    orientation: orientation,
    isFavorite: isFavorite,
    title: title,
    createDateSecond: createDateSecond,
    modifiedDateSecond: modifiedDateSecond,
    relativePath: relativePath,
    latitude: latitude,
    longitude: longitude,
    mimeType: mimeType,
  );

  @override
  int get hashCode => hashValues(id, isFavorite, uploadCloud, hashId);

  @override
  bool operator ==(other) {
    if (other is! LyAssetEntity) {
      return false;
    }
    // if (hashId != null && other.hashId != null) {
    //   return hashId == other.hashId;
    // }
    return id == other.id &&
        isFavorite == other.isFavorite &&
        uploadCloud == other.uploadCloud;
  }

  factory LyAssetEntity.fromJson(dynamic json) {
    return LyAssetEntity(
        id: json['id'],
        width: json['width'],
        height: json['height'],
        typeInt: json['typeInt'],
        duration: json['duration'],
        isFavorite: json['isFavorite'],
        title: json['title'],
        createDateSecond: json['createDateSecond'],
        modifiedDateSecond: json['modifiedDateSecond'],
        latitude: json['latitude'],
        longitude: json['longitude'],
        mimeType: json['mimeType'],
        relativePath: json['relativePath'],
        orientation: json['orientation'],
        hashId: json['hashId'],
        netUrl: json['netUrl']
    );
  }

  Map toJson() {
    Map map = {};
    map["id"] = id;
    map["typeInt"] = typeInt;
    map["width"] = width;
    map["height"] = height;
    map["duration"] = duration;
    map["isFavorite"] = isFavorite;
    map["title"] = title;
    map["createDateSecond"] = createDateSecond;
    map["modifiedDateSecond"] = modifiedDateSecond;
    map["latitude"] = latitude;
    map["longitude"] = longitude;
    map["mimeType"] = mimeType;
    map["uploadCloud"] = uploadCloud;
    map["notSelected"] = notSelected;
    map["relativePath"] = relativePath;
    map["orientation"] = orientation;
    map["hashId"] = hashId;
    map["netUrl"] = netUrl;
    return map;
  }

  factory LyAssetEntity.fromAsset(AssetEntity entity,
      {bool isAvailable = true}) {
    return LyAssetEntity(
      id: entity.id,
      width: entity.width,
      height: entity.height,
      typeInt: entity.typeInt,
      duration: entity.duration,
      isFavorite: entity.isFavorite,
      title: entity.title,
      createDateSecond: entity.createDateSecond,
      modifiedDateSecond: entity.modifiedDateSecond,
      latitude: entity.latitude,
      longitude: entity.longitude,
      mimeType: entity.mimeType,
      relativePath: entity.relativePath,
      orientation: entity.orientation,
      isAvailable: isAvailable,
    );
  }

  factory LyAssetEntity.fromFileAsset(AssetFileEntity a) {
    return LyAssetEntity(
      id: a.id,
      width: a.asset.width,
      height: a.asset.height,
      typeInt: a.asset.typeInt,
      duration: a.asset.duration,
      isFavorite: a.asset.isFavorite,
      title: a.asset.title,
      createDateSecond: a.asset.createDateSecond,
      modifiedDateSecond: a.asset.modifiedDateSecond,
      latitude: a.asset.latitude,
      longitude: a.asset.longitude,
      mimeType: a.asset.mimeType,
      relativePath: a.asset.relativePath,
      orientation: a.asset.orientation,
    );
  }
}
