import 'package:flutter/material.dart';
import 'package:photo_manager/photo_manager.dart';
import 'package:zyocore/widget/assets_picker/delegates/assets_picker_text_delegate.dart';
import 'package:zyocore/widget/assets_picker/delegates/sort_path_delegate.dart';

const int defaultAssetsPerPage = 80;
const int defaultMaxAssetsCount = 9;

/// Default theme color from WeChat.
const Color defaultThemeColorWeChat = Color(0xff00bc56);

const ThumbnailSize defaultAssetGridPreviewSize = ThumbnailSize.square(200);
const ThumbnailSize defaultPathThumbnailSize = ThumbnailSize.square(80);

class Constants {
  const Constants._();

  static GlobalKey pickerKey = GlobalKey();
  static GlobalKey lyPickerKey = GlobalKey();
  static GlobalKey sortedKey = GlobalKey();

  // 用于预览页icloud资源下载后，通知列表资源刷新
  static const String refreshIcloudById = 'refresh_icloud_by_id';
  static const String refreshVideoAfterDownload = 'refresh_video_after_download';

  static AssetsPickerTextDelegate textDelegate = AssetsPickerTextDelegate();
  static SortPathDelegate<dynamic> sortPathDelegate = SortPathDelegate.common;

  static ScrollPosition? scrollPosition;

  static const int defaultGridThumbSize = 200;

  static const List<int> viewerGridThumbSize = [
    200,
    200,
  ];
}
