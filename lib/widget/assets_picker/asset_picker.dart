///
/// [Author] Alex (https://github.com/Alex525)
/// [Date] 2020/3/31 15:39
///

import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:photo_manager/photo_manager.dart';
import 'package:provider/provider.dart';
import 'package:zyocore/widget/assets_picker/constants/enums.dart';
import 'package:zyocore/widget/assets_picker/constants/typedefs.dart';
import 'package:zyocore/widget/assets_picker/delegates/asset_picker_builder_delegate.dart';
import 'package:zyocore/widget/assets_picker/delegates/assets_picker_text_delegate.dart';
import 'package:zyocore/widget/assets_picker/delegates/sort_path_delegate.dart';
import 'package:zyocore/widget/assets_picker/entity/ly_asset_entity.dart';
import 'package:zyocore/widget/assets_picker/provider/asset_picker_provider.dart';

import '../../util/logger.dart';
import 'constants/constants.dart';
import 'widget/asset_picker_page_route.dart';

/// 默认的微信图片选择
class AssetPicker<Asset, Path> extends StatefulWidget {
  const AssetPicker({Key? key, required this.builder}) : super(key: key);

  final AssetPickerBuilderDelegate<Asset, Path> builder;

  static Future<PermissionState> permissionCheck({RequestType reqType = RequestType.common}) async {
    final PermissionState _ps = await PhotoManager.requestPermissionExtend(
      requestOption: PermissionRequestOption(
        androidPermission: AndroidPermission(
          type: reqType,
          mediaLocation: false,
        ),
      ),
    );
    if (_ps != PermissionState.authorized && _ps != PermissionState.limited) {
      throw StateError('Permission state error with $_ps.');
    }
    return _ps;
  }

  /// Static method to push with the navigator.
  /// 跳转至选择器的静态方法
  static Future<List<LyAssetEntity>?> pickAssets(
    BuildContext context, {
    List<LyAssetEntity>? selectedAssets,
    int maxAssets = 9,
    int pageSize = 80,
    int maxMinute = 5,
    ThumbnailSize gridThumbnailSize = defaultAssetGridPreviewSize,
    ThumbnailSize pathThumbnailSize = defaultPathThumbnailSize,
    int gridCount = 4,
    RequestType requestType = RequestType.image,
    ThumbnailSize previewThumbnailSize = defaultAssetGridPreviewSize,
    SpecialPickerType? specialPickerType,
    Color? themeColor,
    ThemeData? pickerTheme,
    SortPathDelegate<AssetPathEntity>? sortPathDelegate,
    AssetsPickerTextDelegate? textDelegate,
    FilterOptionGroup? filterOptions,
    SpecialItemBuilder? specialItemBuilder,
    IndicatorBuilder? loadingIndicatorBuilder,
    SpecialItemPosition specialItemPosition = SpecialItemPosition.none,
    bool allowSpecialItemWhenEmpty = false,
    AssetSelectPredicate<LyAssetEntity>? selectPredicate,
    bool? shouldRevertGrid,
    bool useRootNavigator = true,
    Curve routeCurve = Curves.easeIn,
    Duration initializeDelayDuration = const Duration(milliseconds: 300),
  }) async {
    if (maxAssets < 1) {
      throw ArgumentError(
        'maxAssets must be greater than 1.',
      );
    }
    if (pageSize % gridCount != 0) {
      throw ArgumentError(
        'pageSize must be a multiple of gridCount.',
      );
    }
    if (pickerTheme != null && themeColor != null) {
      throw ArgumentError(
        'Theme and theme color cannot be set at the same time.',
      );
    }
    if (specialPickerType == SpecialPickerType.wechatMoment) {
      if (requestType != RequestType.image) {
        throw ArgumentError(
          'SpecialPickerType.wechatMoment and requestType cannot be set at the same time.',
        );
      }
      requestType = RequestType.common;
    }
    if ((specialItemBuilder == null && specialItemPosition != SpecialItemPosition.none) ||
        (specialItemBuilder != null && specialItemPosition == SpecialItemPosition.none)) {
      throw ArgumentError('Custom item did not set properly.');
    }

    final PermissionState _ps = await permissionCheck();

    final DefaultAssetPickerProvider provider = DefaultAssetPickerProvider(
      maxAssets: maxAssets,
      pageSize: pageSize,
      pathThumbnailSize: pathThumbnailSize,
      selectedAssets: selectedAssets,
      requestType: requestType,
      sortPathDelegate: sortPathDelegate,
      filterOptions: filterOptions,
      initializeDelayDuration: initializeDelayDuration,
      maxMinute: maxMinute,
    );
    final Widget picker = ChangeNotifierProvider<DefaultAssetPickerProvider>.value(
      value: provider,
      child: AssetPicker<LyAssetEntity, AssetPathEntity>(
        key: Constants.pickerKey,
        builder: DefaultAssetPickerBuilderDelegate(
          provider: provider,
          initialPermission: _ps,
          gridCount: gridCount,
          textDelegate: textDelegate,
          themeColor: themeColor,
          pickerTheme: pickerTheme,
          gridThumbnailSize: gridThumbnailSize,
          previewThumbnailSize: previewThumbnailSize,
          specialPickerType: specialPickerType,
          specialItemPosition: specialItemPosition,
          specialItemBuilder: specialItemBuilder,
          loadingIndicatorBuilder: loadingIndicatorBuilder,
          allowSpecialItemWhenEmpty: allowSpecialItemWhenEmpty,
          selectPredicate: selectPredicate,
          shouldRevertGrid: shouldRevertGrid,
        ),
      ),
    );
    final List<LyAssetEntity>? result = await Navigator.of(
      context,
      rootNavigator: useRootNavigator,
    ).push<List<LyAssetEntity>>(
      AssetPickerPageRoute<List<LyAssetEntity>>(
        builder: picker,
        transitionCurve: routeCurve,
        transitionDuration: initializeDelayDuration,
      ),
    );
    return result;
  }

  /// Call the picker with provided [delegate] and [provider].
  /// 通过指定的 [delegate] 和 [provider] 调用选择器
  static Future<List<Asset>?> pickAssetsWithDelegate<Asset, Path, PickerProvider extends AssetPickerProvider<Asset, Path>>(
    BuildContext context, {
    required AssetPickerBuilderDelegate<Asset, Path> delegate,
    required PickerProvider provider,
    bool useRootNavigator = true,
    Curve routeCurve = Curves.easeIn,
    Duration routeDuration = const Duration(milliseconds: 300),
  }) async {
    await permissionCheck();

    final Widget picker = ChangeNotifierProvider<PickerProvider>.value(
      value: provider,
      child: AssetPicker<Asset, Path>(
        key: Constants.pickerKey,
        builder: delegate,
      ),
    );
    final List<Asset>? result = await Navigator.of(
      context,
      rootNavigator: useRootNavigator,
    ).push<List<Asset>>(
      AssetPickerPageRoute<List<Asset>>(
        builder: picker,
        transitionCurve: routeCurve,
        transitionDuration: routeDuration,
      ),
    );
    return result;
  }

  /// Register observe callback with assets changes.
  /// 注册资源（图库）变化的监听回调
  static void registerObserve([ValueChanged<MethodCall>? callback]) {
    if (callback == null) {
      return;
    }
    try {
      PhotoManager.addChangeCallback(callback);
      PhotoManager.startChangeNotify();
    } catch (e) {
      Logger.d('Error when registering assets callback: $e');
    }
  }

  /// Unregister the observation callback with assets changes.
  /// 取消注册资源（图库）变化的监听回调
  static void unregisterObserve([ValueChanged<MethodCall>? callback]) {
    if (callback == null) {
      return;
    }
    try {
      PhotoManager.removeChangeCallback(callback);
      PhotoManager.stopChangeNotify();
    } catch (e) {
      Logger.d('Error when unregistering assets callback: $e');
    }
  }

  /// Build a dark theme according to the theme color.
  /// 通过主题色构建一个默认的暗黑主题
  static ThemeData themeData(Color themeColor) {
    return ThemeData.dark().copyWith(
      primaryColor: Colors.grey[900],
      // primaryColorBrightness: Brightness.dark,
      primaryColorLight: Colors.grey[900],
      primaryColorDark: Colors.grey[900],
      canvasColor: Colors.grey[850],
      scaffoldBackgroundColor: Colors.grey[900],
      bottomAppBarTheme: BottomAppBarTheme(color: Colors.grey[900]),
      cardColor: Colors.grey[900],
      highlightColor: Colors.transparent,
      // toggleableActiveColor: themeColor,
      textSelectionTheme: TextSelectionThemeData(
        cursorColor: themeColor,
        selectionColor: themeColor.withAlpha(100),
        selectionHandleColor: themeColor,
      ),
      indicatorColor: themeColor,
      appBarTheme: const AppBarTheme(
        systemOverlayStyle: SystemUiOverlayStyle(
          statusBarBrightness: Brightness.dark,
          statusBarIconBrightness: Brightness.light,
        ),
        elevation: 0,
      ),
      buttonTheme: ButtonThemeData(buttonColor: themeColor),
      colorScheme: ColorScheme(
        primary: Colors.grey[900]!,
        // primaryVariant: Colors.grey[900]!,
        secondary: themeColor,
        // secondaryVariant: themeColor,
        background: Colors.grey[900]!,
        surface: Colors.grey[900]!,
        brightness: Brightness.dark,
        error: const Color(0xffcf6679),
        onPrimary: Colors.black,
        onSecondary: Colors.black,
        onSurface: Colors.white,
        onBackground: Colors.white,
        onError: Colors.black,
      ),
    );
  }

  @override
  AssetPickerState<Asset, Path> createState() => AssetPickerState<Asset, Path>();
}

class AssetPickerState<Asset, Path> extends State<AssetPicker<Asset, Path>> with WidgetsBindingObserver {
  @override
  void initState() {
    super.initState();
    // Logger.d('AssetPickerState initState--------------------');
    WidgetsBinding.instance.addObserver(this);
    AssetPicker.registerObserve(_onLimitedAssetsUpdated);
    widget.builder.initState(this);
  }

  @override
  void didChangeAppLifecycleState(AppLifecycleState state) {
    // Logger.d('AssetPickerState didChangeAppLifecycleState--------------------');
    super.didChangeAppLifecycleState(state);
    if (state == AppLifecycleState.resumed) {
      PhotoManager.requestPermissionExtend().then(
        (PermissionState ps) => widget.builder.permission.value = ps,
      );
    }
  }

  @override
  void dispose() {
    // Logger.d('AssetPickerState dispose--------------------');
    WidgetsBinding.instance.removeObserver(this);
    AssetPicker.unregisterObserve(_onLimitedAssetsUpdated);
    widget.builder.dispose();
    super.dispose();
  }

  Future<void> _onLimitedAssetsUpdated(MethodCall call) async {
    return widget.builder.onAssetsChanged(call, (VoidCallback fn) {
      fn();
      if (mounted) {
        setState(() {});
      }
    });
  }

  @override
  Widget build(BuildContext context) {
    // Logger.d('AssetPickerState build--------------------');
    return widget.builder.build(context);
  }
}
