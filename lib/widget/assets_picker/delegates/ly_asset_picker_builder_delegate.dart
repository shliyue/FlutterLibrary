///
/// [Author] Alex (https://github.com/AlexV525)
/// [Date] 2020-10-29 21:50
///
import 'dart:async';
import 'dart:io';
import 'dart:math' as math;
import 'dart:ui' as ui;

import 'package:flutter/gestures.dart';
import 'package:flutter/material.dart';
import 'package:flutter/scheduler.dart';
import 'package:flutter/services.dart';
import 'package:zyocore/values/style.dart';
import 'package:zyocore/values/text.dart';
import 'package:zyocore/widget/assets_picker/constants/enums.dart';
import 'package:zyocore/widget/assets_picker/constants/extensions.dart';
import 'package:zyocore/widget/assets_picker/constants/constants.dart';
import 'package:zyocore/widget/assets_picker/delegates/asset_picker_builder_delegate.dart';
import 'package:zyocore/widget/assets_picker/entity/ly_asset_entity.dart';
import 'package:zyocore/widget/assets_picker/entity/path_wrapper.dart';
import 'package:zyocore/widget/assets_picker/ly_asset_picker_viewer.dart';
import 'package:zyocore/widget/assets_picker/ly_assets_picker.dart';
import 'package:zyocore/widget/assets_picker/provider/ly_asset_picker_provider.dart';
import 'package:zyocore/widget/assets_picker/widget/builder/asset_entity_grid_item_builder.dart';
import 'package:zyocore/widget/assets_picker/widget/fixed_appbar.dart';
import 'package:zyocore/widget/assets_picker/widget/gaps.dart';
import 'package:zyocore/widget/assets_picker/widget/platform_progress_indicator.dart';
import 'package:zyocore/widget/assets_picker/widget/scale_text.dart';
import 'package:zyocore/zyocore.dart';

import '../../../core/configs/config.dart';

class LyAssetPickerBuilderDelegate extends AssetPickerBuilderDelegate<LyAssetEntity, AssetPathEntity> {
  LyAssetPickerBuilderDelegate({
    required this.provider,
    required super.initialPermission,
    super.gridCount,
    super.pickerTheme,
    super.themeColor,
    super.specialItemPosition,
    super.specialItemBuilder,
    super.loadingIndicatorBuilder,
    super.selectPredicate,
    super.shouldRevertGrid,
    super.pathNameBuilder,
    super.textDelegate,
    super.allowSpecialItemWhenEmpty,
    this.gridThumbnailSize = defaultAssetGridPreviewSize,
    this.previewThumbnailSize,
    this.specialPickerType,
    this.keepScrollOffset = false,
    this.packageName,
  }) {
    // Add the listener if [keepScrollOffset] is true.
    if (keepScrollOffset) {
      gridScrollController.addListener(keepScrollOffsetListener);
    }
  }

  /// [ChangeNotifier] for asset picker.
  /// 资源选择器状态保持
  final LyAssetPickerProvider provider;

  /// Whether the picker should save the scroll offset between pushes and pops.
  /// 选择器是否可以从同样的位置开始选择
  final bool keepScrollOffset;

  /// Whether the picker is under the single asset mode.
  /// 选择器是否为单选模式
  bool get isSingleAssetMode => provider.maxAssets == 1;

  /// 本次可以选择的最大数量
  int maxToSelect = 0;

  /// Thumbnail size in the grid.
  /// 预览时网络的缩略图大小
  ///
  /// This only works on images and videos since other types does not have to
  /// request for the thumbnail data. The preview can speed up by reducing it.
  /// 该参数仅生效于图片和视频类型的资源，因为其他资源不需要请求缩略图数据。
  /// 预览图片的速度可以通过适当降低它的数值来提升。
  ///
  /// This cannot be `null` or a large value since you shouldn't use the
  /// original data for the grid.
  /// 该值不能为空或者非常大，因为在网格中使用原数据不是一个好的决定。
  final ThumbnailSize gridThumbnailSize;

  /// Preview thumbnail size in the viewer.
  /// 预览时图片的缩略图大小
  ///
  /// This only works on images and videos since other types does not have to
  /// request for the thumbnail data. The preview can speed up by reducing it.
  /// 该参数仅生效于图片和视频类型的资源，因为其他资源不需要请求缩略图数据。
  /// 预览图片的速度可以通过适当降低它的数值来提升。
  ///
  /// Default is `null`, which will request the origin data.
  /// 默认为空，即读取原图。
  final ThumbnailSize? previewThumbnailSize;

  /// The current special picker type for the picker.
  /// 当前特殊选择类型
  ///
  /// Several types which are special:
  /// * [SpecialPickerType.wechatMoment] When user selected video, no more images
  /// can be selected.
  /// * [SpecialPickerType.noPreview] Disable preview of asset; Clicking on an
  /// asset selects it.
  ///
  /// 这里包含一些特殊选择类型：
  /// * [SpecialPickerType.wechatMoment] 微信朋友圈模式。当用户选择了视频，将不能选择图片。
  /// * [SpecialPickerType.noPreview] 禁用资源预览。多选时单击资产将直接选中，单选时选中并返回。
  final SpecialPickerType? specialPickerType;

  /// [Duration] when triggering path switching.
  /// 切换路径时的动画时长
  Duration get switchingPathDuration => kThemeAnimationDuration;

  /// [Curve] when triggering path switching.
  /// 切换路径时的动画曲线
  Curve get switchingPathCurve => Curves.easeInOut;

  /// Whether the [SpecialPickerType.wechatMoment] is enabled.
  /// 当前是否为微信朋友圈选择模式
  bool get isWeChatMoment => specialPickerType == SpecialPickerType.wechatMoment;

  /// Whether the preview of assets is enabled.
  /// 资源的预览是否启用
  bool get isPreviewEnabled => specialPickerType != SpecialPickerType.noPreview;

  /// GlobalKey
  final GlobalKey customScrollKey = GlobalKey();

  /// 手指最后滑动的资源
  LyAssetEntity? _lastScrollAsset;

  /// 最后一次滑动位置
  DragUpdateDetails? _lastScrollDetails;

  /// 滑动选择处理
  bool? _scrollAction;

  /// 滑动Timer
  Timer? _scrollTimer;

  /// 资源包名
  String? packageName;

  @override
  Widget androidLayout(BuildContext context) {
    return FixedAppBarWrapper(
      appBar: appBar(context),
      body: Consumer<LyAssetPickerProvider>(
        builder: (BuildContext context, LyAssetPickerProvider p, __) {
          final bool shouldDisplayAssets = p.hasAssetsToDisplay || shouldBuildSpecialItem;
          return AnimatedSwitcher(
            duration: switchingPathDuration,
            child: shouldDisplayAssets
                ? Stack(
                    children: <Widget>[
                      RepaintBoundary(
                        child: Column(
                          children: <Widget>[
                            Expanded(child: assetsGridBuilder(context)),
                            if (isPreviewEnabled) bottomActionBar(context),
                          ],
                        ),
                      ),
                      pathEntityListBackdrop(context),
                      pathEntityListWidget(context),
                    ],
                  )
                : loadingIndicator(context),
          );
        },
      ),
    );
  }

  /// Back button.
  /// 返回按钮
  @override
  Widget backButton(BuildContext context) {
    return GestureDetector(
      onTap: Navigator.of(context).maybePop,
      child: Container(
        height: kToolbarHeight,
        width: kToolbarHeight,
        color: theme.colorScheme.onPrimary,
        child: Icon(
          Icons.arrow_back_ios,
          size: 20,
          color: theme.colorScheme.onSurface,
        ),
      ),
    );
  }

  /// Action bar widget aligned to bottom.
  /// 底部操作栏部件
  @override
  Widget bottomActionBar(BuildContext context) {
    Widget child = Container(
      height: 60 + context.bottomPadding,
      padding: const EdgeInsets.symmetric(horizontal: 16).copyWith(
        bottom: context.bottomPadding,
      ),
      color: theme.colorScheme.onPrimary,
      child: Row(
        mainAxisAlignment: MainAxisAlignment.spaceBetween,
        children: <Widget>[
          previewButton(context),
          confirmButton(context),
        ],
      ),
    );
    if (isPermissionLimited) {
      child = Column(
        mainAxisSize: MainAxisSize.min,
        children: <Widget>[accessLimitedBottomTip(context), child],
      );
    }
    child = ClipRect(
      child: BackdropFilter(
        filter: ui.ImageFilter.blur(
          sigmaX: appleOSBlurRadius,
          sigmaY: appleOSBlurRadius,
        ),
        child: child,
      ),
    );
    return child;
  }

  @override
  PreferredSizeWidget appBar(BuildContext context) {
    return FixedAppBar(
      backgroundColor: theme.appBarTheme.backgroundColor,
      centerTitle: true,
      title: pathEntitySelector(context),
      leading: backButton(context),
      actionsPadding: const EdgeInsetsDirectional.only(end: 14),
      blurRadius: 0,
    );
  }

  @override
  Widget appleOSLayout(BuildContext context) {
    Widget _gridLayout(BuildContext context) {
      return ValueListenableBuilder<bool>(
        valueListenable: isSwitchingPath,
        builder: (_, bool isSwitchingPath, __) => RepaintBoundary(
          child: Stack(
            children: <Widget>[
              Positioned.fill(child: assetsGridBuilder(context)),
              if (isPreviewEnabled)
                Positioned.fill(
                  top: null,
                  child: bottomActionBar(context),
                ),
            ],
          ),
        ),
      );
    }

    Widget _layout(BuildContext context) {
      return Stack(
        children: <Widget>[
          Positioned.fill(
            child: Consumer<LyAssetPickerProvider>(
              builder: (_, LyAssetPickerProvider p, __) {
                final Widget _child;
                final bool shouldDisplayAssets = p.hasAssetsToDisplay;
                if (shouldDisplayAssets) {
                  _child = Stack(
                    children: <Widget>[
                      _gridLayout(context),
                      pathEntityListBackdrop(context),
                      pathEntityListWidget(context),
                    ],
                  );
                } else {
                  _child = loadingIndicator(context);
                }
                return AnimatedSwitcher(
                  duration: switchingPathDuration,
                  child: _child,
                );
              },
            ),
          ),
          appBar(context),
        ],
      );
    }

    return ValueListenableBuilder<bool>(
      valueListenable: permissionOverlayDisplay,
      builder: (_, bool value, Widget? child) {
        if (value) {
          return ExcludeSemantics(child: child);
        }
        return child!;
      },
      child: _layout(context),
    );
  }

  @override
  Widget assetsGridBuilder(BuildContext context) {
    return Selector<LyAssetPickerProvider, PathWrapper<AssetPathEntity>?>(
      selector: (_, LyAssetPickerProvider p) => p.currentPath,
      builder: (BuildContext _c, PathWrapper<AssetPathEntity>? wrapper, __) {
        // First, we need the count of the assets.
        int totalCount = wrapper?.assetCount ?? 0;
        // If user chose a special item's position, add 1 count.
        final Widget? specialItem;
        // If user chose a special item's position, add 1 count.
        if (specialItemPosition != SpecialItemPosition.none && (wrapper?.path.isAll ?? false) == true) {
          specialItem = specialItemBuilder?.call(
            _c,
            wrapper?.path,
            totalCount,
          );
          if (specialItem != null) {
            totalCount += 1;
          }
        } else {
          specialItem = null;
        }
        if (totalCount == 0 && specialItem == null) {
          return loadingIndicator(_c);
        }
        // Then we use the [totalCount] to calculate placeholders we need.
        final int placeholderCount;
        if (effectiveShouldRevertGrid && totalCount % gridCount != 0) {
          // When there are left items that not filled into one row,
          // filled the row with placeholders.
          placeholderCount = gridCount - totalCount % gridCount;
        } else {
          // Otherwise, we don't need placeholders.
          placeholderCount = 0;
        }
        // Calculate rows count.
        final int row = (totalCount + placeholderCount) ~/ gridCount;
        // Here we got a magic calculation. [itemSpacing] needs to be divided by
        // [gridCount] since every grid item is squeezed by the [itemSpacing],
        // and it's actual size is reduced with [itemSpacing / gridCount].
        final double dividedSpacing = itemSpacing / gridCount;
        final double topPadding = context.topPadding + kToolbarHeight;

        Widget _sliverGrid(BuildContext ctx, List<LyAssetEntity> assets) {
          return SliverGrid(
            delegate: SliverChildBuilderDelegate(
              (_, int index) => Builder(
                builder: (BuildContext c) {
                  if (effectiveShouldRevertGrid) {
                    if (index < placeholderCount) {
                      return const SizedBox.shrink();
                    }
                    index -= placeholderCount;
                  }
                  return Directionality(
                    textDirection: Directionality.of(context),
                    child: assetGridItemBuilder(c, index, assets, specialItem: specialItem),
                  );
                },
              ),
              childCount: assetsGridItemCount(context: ctx, assets: assets, placeholderCount: placeholderCount, specialItem: specialItem),
              findChildIndexCallback: (Key? key) {
                if (key is ValueKey<String>) {
                  return findChildIndexBuilder(
                    id: key.value,
                    assets: assets,
                    placeholderCount: placeholderCount,
                  );
                }
                return null;
              },
              addSemanticIndexes: false,
            ),
            gridDelegate: SliverGridDelegateWithFixedCrossAxisCount(
              crossAxisCount: gridCount,
              mainAxisSpacing: itemSpacing,
              crossAxisSpacing: itemSpacing,
            ),
          );
        }

        return LayoutBuilder(
          builder: (BuildContext c, BoxConstraints constraints) {
            final double itemSize = constraints.maxWidth / gridCount;
            // Check whether all rows can be placed at the same time.
            final bool onlyOneScreen = row * itemSize <= constraints.maxHeight - context.bottomPadding - topPadding - permissionLimitedBarHeight;
            final double height;
            if (onlyOneScreen) {
              height = constraints.maxHeight;
            } else {
              // Reduce [permissionLimitedBarHeight] for the final height.
              height = constraints.maxHeight - permissionLimitedBarHeight;
            }
            // Use [ScrollView.anchor] to determine where is the first place of
            // the [SliverGrid]. Each row needs [dividedSpacing] to calculate,
            // then minus one times of [itemSpacing] because spacing's count in the
            // cross axis is always less than the rows.
            final double anchor = math.min(
              (row * (itemSize + dividedSpacing) + topPadding - itemSpacing) / height,
              1,
            );

            return Directionality(
              textDirection: effectiveGridDirection(context),
              child: ColoredBox(
                color: theme.canvasColor,
                child: Selector<LyAssetPickerProvider, List<String>>(
                    selector: (_, LyAssetPickerProvider p) => p.currentUploaded,
                    builder: (_, List<String> uploadsAssets, __) {
                      return Selector<LyAssetPickerProvider, List<String>>(
                          selector: (_, LyAssetPickerProvider p) => p.notLocallyAvailables,
                          builder: (_, List<String> notLocallyAvailables, __) {
                            return Selector<LyAssetPickerProvider, List<LyAssetEntity>>(
                              selector: (_, LyAssetPickerProvider p) => p.currentAssets,
                              builder: (_, List<LyAssetEntity> assets, __) {
                                final SliverGap _bottomGap = SliverGap.v(
                                  context.bottomPadding + bottomSectionHeight,
                                );
                                return GestureDetector(
                                  onPanStart: (details) {
                                    _scrollAction = null;
                                  },
                                  onPanUpdate: (details) {
                                    _onPanUpdate(
                                      assets: assets,
                                      details: details,
                                      context: context,
                                    );
                                  },
                                  onPanEnd: (details) {
                                    _lastScrollAsset = null;
                                    _stopScrollTimer();
                                  },
                                  child: CustomScrollView(
                                    key: customScrollKey,
                                    physics: const AlwaysScrollableScrollPhysics(),
                                    controller: gridScrollController,
                                    anchor: effectiveShouldRevertGrid ? anchor : 0,
                                    center: effectiveShouldRevertGrid ? gridRevertKey : null,
                                    slivers: <Widget>[
                                      if (isAppleOS) SliverGap.v(context.topPadding + kToolbarHeight),
                                      _sliverGrid(_, assets),
                                      // Ignore the gap when the [anchor] is not equal to 1.
                                      if (effectiveShouldRevertGrid && anchor == 1) _bottomGap,
                                      if (effectiveShouldRevertGrid)
                                        SliverToBoxAdapter(
                                          key: gridRevertKey,
                                          child: const SizedBox.shrink(),
                                        ),
                                      if (isAppleOS && !effectiveShouldRevertGrid) _bottomGap,
                                    ],
                                  ),
                                );
                              },
                            );
                          });
                    }),
              ),
            );
          },
        );
      },
    );
  }

  /// 滑动选择手势处理
  void _onPanUpdate({
    required List<LyAssetEntity> assets,
    required DragUpdateDetails details,
    required BuildContext context,
  }) {
    if (shouldRevertGrid == false) {
      var fingerX = details.localPosition.dx;
      var scrollHeight = customScrollKey.currentContext?.size?.height ?? 0.0;
      var fingerY = details.localPosition.dy + gridScrollController.offset;
      if (isAppleOS) {
        scrollHeight -= (context.topPadding + kToolbarHeight);
        scrollHeight -= (context.bottomPadding + bottomSectionHeight);
        fingerY -= (context.topPadding + kToolbarHeight);
      }
      // Logger.d(
      //     '---  dx ${details.localPosition.dx} dy ${details.localPosition.dy} offset ${gridScrollController.offset} ');
      var fingerOffset = Offset(fingerX, fingerY);
      // Logger.d('---  finger $fingerOffset');
      var assetWidth = (MediaQuery.of(context).size.width - 4.0) / 3.0;
      var assetHeight = assetWidth;
      var x = 0.0;
      var y = 0.0;
      int? childIndex;
      var count = assets.length;
      if (specialItemPosition != SpecialItemPosition.none && provider.currentPath!.path.isAll == true) {
        count += 1;
      }
      for (var i = 0; i < count; i++) {
        if (i % 3 == 0) {
          // 换行
          x = 0;
          if (i != 0) {
            y += (assetHeight + itemSpacing);
          }
        }
        var rect = Rect.fromLTWH(x, y, assetWidth, assetHeight);
        // Logger.d(
        //     "-- rect i:$i  ${rect.left} ${rect.top} ${rect.width} ${rect.height}");
        if (rect.contains(fingerOffset)) {
          childIndex = i;
          break;
        }
        x += (assetWidth + itemSpacing);
      }
      if (childIndex != null) {
        if (specialItemPosition != SpecialItemPosition.none && provider.currentPath!.path.isAll == true) {
          childIndex -= 1;
        }
        if (childIndex < 0) {
          return;
        }
        // Logger.d('---  当前位置 $childIndex');
        var asset = assets[childIndex];
        if (_lastScrollAsset == null || _lastScrollAsset!.id != asset.id) {
          _lastScrollAsset = asset;
          var selected = provider.selectedAssets.contains(asset);
          _scrollAction ??= !selected;
          if (_scrollAction!) {
            var invalid = provider.invalidFormats.contains(asset.id);
            var notLocallyAvailable = provider.notLocallyAvailables.contains(asset.id);
            final List<String>? allowTypes = provider.allowTypes;
            var isAllow = (asset.type == AssetType.image && allowTypes != null && allowTypes.contains(AssetUtil.assetSuffix(asset)));
            if (((asset.type == AssetType.image && !invalid) || isAllow || (asset.type == AssetType.video && asset.duration <= (provider.maxMinute * 60))) &&
                !notLocallyAvailable) {
              provider.selectAsset(asset);
            }
          } else {
            provider.unSelectAsset(asset);
          }
        }
      } else {
        _lastScrollAsset = null;
      }
      if (fingerY < gridScrollController.offset + assetHeight) {
        _startScrollTimer(isToTop: true, assets: assets, context: context);
      } else if (fingerY > gridScrollController.offset + scrollHeight - assetHeight) {
        _startScrollTimer(isToTop: false, assets: assets, context: context);
      } else {
        _stopScrollTimer();
      }
    }
  }

  void _startScrollTimer({
    required bool isToTop,
    required List<LyAssetEntity> assets,
    required BuildContext context,
  }) {
    var duration = const Duration(milliseconds: 16);
    _scrollTimer ??= Timer.periodic(duration, (timer) async {
      var y = gridScrollController.offset;
      var max = gridScrollController.position.maxScrollExtent;
      if (isToTop) {
        y -= ScreenUtil().pixelRatio ?? 1;
        if (y < 0) {
          y = 0;
        }
      } else {
        y += ScreenUtil().pixelRatio ?? 1;
        if (y >= max) {
          y = max;
        }
      }
      if (y != gridScrollController.offset) {
        gridScrollController.jumpTo(y);
        if (_lastScrollDetails != null) {
          _onPanUpdate(
            assets: assets,
            details: _lastScrollDetails!,
            context: context,
          );
        }
      } else {
        _stopScrollTimer();
      }
    });
  }

  void _stopScrollTimer() {
    _scrollTimer?.cancel();
    _scrollTimer = null;
  }

  /// There are several conditions within this builder:
  ///  * Return [specialItemBuilder] while the current path is all and
  ///    [specialItemPosition] is not equal to [SpecialItemPosition.none].
  ///  * Return item builder according to the asset's type.
  ///    * [AssetType.audio] -> [audioItemBuilder]
  ///    * [AssetType.image], [AssetType.video] -> [imageAndVideoItemBuilder]
  ///  * Load more assets when the index reached at third line counting
  ///    backwards.
  ///
  /// 资源构建有几个条件：
  ///  * 当前路径是全部资源且 [specialItemPosition] 不等于
  ///    [SpecialItemPosition.none] 时，将会通过 [specialItemBuilder] 构建内容。
  ///  * 根据资源类型返回对应类型的构建：
  ///    * [AssetType.audio] -> [audioItemBuilder] 音频类型
  ///    * [AssetType.image], [AssetType.video] -> [imageAndVideoItemBuilder]
  ///      图片和视频类型
  ///  * 在索引到达倒数第三列的时候加载更多资源。
  @override
  Widget assetGridItemBuilder(
    BuildContext context,
    int index,
    List<LyAssetEntity> currentAssets, {
    Widget? specialItem,
    int? maxMinute,
  }) {
    final LyAssetPickerProvider p = context.read<LyAssetPickerProvider>();
    final int length = currentAssets.length;
    final PathWrapper<AssetPathEntity>? currentWrapper = p.currentPath;
    final AssetPathEntity? currentPathEntity = currentWrapper?.path;
    // Logger.d('assetGridItemBuilder=是否进入了=======');

    if (specialItem != null) {
      if ((index == 0 && specialItemPosition == SpecialItemPosition.prepend) || (index == length && specialItemPosition == SpecialItemPosition.append)) {
        return specialItem;
      }
    }

    final int currentIndex;
    if (specialItem != null && specialItemPosition == SpecialItemPosition.prepend) {
      currentIndex = index - 1;
    } else {
      currentIndex = index;
    }

    if (currentPathEntity == null) {
      return const SizedBox.shrink();
    }

    // // Directly return the special item when it's empty.
    // if (currentPathEntity == null && specialItemBuilder != null) {
    //   if (allowSpecialItemWhenEmpty &&
    //       specialItemPosition != SpecialItemPosition.none) {
    //     int totalCount = currentWrapper?.assetCount ?? 0;
    //     return specialItemBuilder!(context, currentPathEntity, totalCount)!;
    //   }
    //   return const SizedBox.shrink();
    // }
    if (p.hasMoreToLoad) {
      if ((p.pageSize <= gridCount * 3 && index == length - 1) || index == length - gridCount * 3) {
        p.loadMoreAssets();
      }
    }
    // if (currentIndex >= currentAssets.length) {
    //   return const SizedBox.shrink();
    // }
    final LyAssetEntity asset = currentAssets.elementAt(currentIndex);
    Widget builder;
    switch (asset.type) {
      case AssetType.audio:
        builder = audioItemBuilder(context, currentIndex, asset);
        break;
      case AssetType.image:
      case AssetType.video:
        builder = imageAndVideoItemBuilder(context, currentIndex, asset, maxMinute: maxMinute);
        break;
      case AssetType.other:
        builder = const SizedBox.shrink();
        break;
    }
    final List<String> notLocallyAvailables = p.notLocallyAvailables;
    var notLocalAvailble = notLocallyAvailables.contains(asset.id);
    // Logger.d('asset.id=${asset.id}');
    // Logger.d('notLocalAvailble=$notLocalAvailble');
    return Stack(
      key: ValueKey<String>(asset.id),
      children: notLocalAvailble
          ? <Widget>[builder]
          : <Widget>[
              builder,
              selectedBackdrop(context, currentIndex, asset),
              if (!isWeChatMoment || asset.type != AssetType.video) selectIndicator(context, asset),
              itemBannedIndicator(context, asset),
            ],
    );
  }

  @override
  int findChildIndexBuilder({
    required String id,
    required List<LyAssetEntity> assets,
    int placeholderCount = 0,
  }) {
    int index = assets.indexWhere((LyAssetEntity e) => e.id == id);
    if (specialItemPosition == SpecialItemPosition.prepend) {
      index += 1;
    }
    index += placeholderCount;
    return index;
  }

  @override
  int assetsGridItemCount({
    required BuildContext context,
    required List<LyAssetEntity> assets,
    int placeholderCount = 0,
    Widget? specialItem,
  }) {
    final PathWrapper<AssetPathEntity>? currentWrapper = context.select<LyAssetPickerProvider, PathWrapper<AssetPathEntity>?>(
      (LyAssetPickerProvider p) => p.currentPath,
    );
    final AssetPathEntity? currentPathEntity = currentWrapper?.path;
    if (currentPathEntity == null && specialItem != null) {
      return placeholderCount + 1;
    }

    /// Return actual length if current path is all.
    /// 如果当前目录是全部内容，则返回实际的内容数量。
    final int _length = assets.length + placeholderCount;
    if (currentPathEntity?.isAll != true && specialItem == null) {
      return _length;
    }
    switch (specialItemPosition) {
      case SpecialItemPosition.none:
        return _length;
      case SpecialItemPosition.prepend:
      case SpecialItemPosition.append:
        return _length + 1;
    }
  }

  @override
  Widget audioIndicator(BuildContext context, LyAssetEntity asset) {
    return Container(
      width: double.maxFinite,
      alignment: AlignmentDirectional.bottomStart,
      padding: const EdgeInsets.symmetric(horizontal: 2, vertical: 8),
      decoration: BoxDecoration(
        gradient: LinearGradient(
          begin: AlignmentDirectional.bottomCenter,
          end: AlignmentDirectional.topCenter,
          colors: <Color>[theme.dividerColor, Colors.transparent],
        ),
      ),
      child: Padding(
        padding: const EdgeInsetsDirectional.only(start: 4),
        child: ScaleText(
          Constants.textDelegate.durationIndicatorBuilder(
            Duration(seconds: asset.duration),
          ),
          style: const TextStyle(fontSize: 16),
        ),
      ),
    );
  }

  @override
  Widget audioItemBuilder(BuildContext context, int index, LyAssetEntity asset) {
    return Stack(
      children: <Widget>[
        Container(
          width: double.maxFinite,
          alignment: AlignmentDirectional.topStart,
          padding: const EdgeInsets.symmetric(horizontal: 2, vertical: 8),
          decoration: BoxDecoration(
            gradient: LinearGradient(
              begin: AlignmentDirectional.topCenter,
              end: AlignmentDirectional.bottomCenter,
              colors: <Color>[theme.dividerColor, Colors.transparent],
            ),
          ),
          child: Padding(
            padding: const EdgeInsetsDirectional.only(start: 4, end: 30),
            child: ScaleText(
              asset.title ?? '',
              style: const TextStyle(fontSize: 16),
              maxLines: 3,
              overflow: TextOverflow.ellipsis,
            ),
          ),
        ),
        const Align(
          alignment: AlignmentDirectional(0.9, 0.8),
          child: Icon(Icons.audiotrack),
        ),
        audioIndicator(context, asset),
      ],
    );
  }

  /// It'll pop with [AssetPickerProvider.selectedAssets]
  /// when there are any assets were chosen.
  /// 当有资源已选时，点击按钮将把已选资源通过路由返回。
  @override
  Widget confirmButton(BuildContext context) {
    return Consumer<LyAssetPickerProvider>(
      builder: (_, LyAssetPickerProvider provider, __) {
        return GestureDetector(
          onTap: () {
            if (provider.isSelectedNotEmpty) {
              Navigator.of(context).maybePop(provider.selectedAssets);
            }
          },
          child: Container(
            height: 30,
            alignment: Alignment.center,
            constraints: BoxConstraints(
              minWidth: provider.isSelectedNotEmpty ? 124 : 50,
            ),
            decoration: BoxDecoration(color: provider.isSelectedNotEmpty ? themeColor : themeColor.withOpacity(0.6), borderRadius: BorderRadius.circular(15)),
            padding: const EdgeInsets.symmetric(horizontal: 17),
            child: ScaleText(
              provider.isSelectedNotEmpty
                  ? '${Constants.textDelegate.complete}'
                      ' (${provider.selectedAssets.length}/${provider.maxAssets})'
                  : Constants.textDelegate.complete,
              style: TextStyle(
                color: theme.colorScheme.onPrimary,
                fontSize: 15,
                fontFamily: AppFontFamily.pingFangMedium,
              ),
            ),
          ),
        );
      },
    );
  }

  @override
  Widget imageAndVideoItemBuilder(BuildContext context, int index, LyAssetEntity asset, {int? maxMinute}) {
    final AssetEntityImageProvider imageProvider = AssetEntityImageProvider(
      asset,
      isOriginal: false,
      thumbnailSize: gridThumbnailSize,
    );
    SpecialImageType? type;
    if (imageProvider.imageFileType == ImageFileType.gif) {
      type = SpecialImageType.gif;
    } else if (imageProvider.imageFileType == ImageFileType.heic) {
      type = SpecialImageType.heic;
    }
    return Stack(
      children: <Widget>[
        Positioned.fill(
          child: RepaintBoundary(
            child: AssetEntityGridItemBuilder(
              image: imageProvider,
              assetPickerProvider: provider,
              failedItemBuilder: failedItemBuilder,
              maxMinute: maxMinute,
            ),
          ),
        ),
        // if (type == SpecialImageType.gif) // 如果为GIF则显示标识
        //   gifIndicator(context, asset),
        if (asset.type == AssetType.video) // 如果为视频则显示标识
          videoIndicator(context, asset),

        if (asset.type == AssetType.image) // 如果为图片，只展示云端标识
          imageIndicator(context, asset),
      ],
    );
  }

  @override
  Widget loadingIndicator(BuildContext context) {
    return Selector<LyAssetPickerProvider, bool>(
      selector: (_, LyAssetPickerProvider p) => p.isAssetsEmpty,
      builder: (BuildContext c, bool isAssetsEmpty, Widget? w) {
        if (loadingIndicatorBuilder != null) {
          return loadingIndicatorBuilder!(c, isAssetsEmpty);
        }
        return Center(child: isAssetsEmpty ? emptyIndicator(context) : w);
      },
      child: PlatformProgressIndicator(
        color: theme.iconTheme.color,
        size: MediaQuery.of(context).size.width / gridCount / 3,
        radius: Platform.isIOS ? 20 : 10,
        brightness: Brightness.light,
      ),
    );
  }

  /// While the picker is switching path, this will displayed.
  /// If the user tapped on it, it'll collapse the list widget.
  ///
  /// 当选择器正在选择路径时，它会出现。用户点击它时，列表会折叠收起。
  @override
  Widget pathEntityListBackdrop(BuildContext context) {
    return ValueListenableBuilder<bool>(
      valueListenable: isSwitchingPath,
      builder: (_, bool isSwitchingPath, __) => Positioned.fill(
        child: IgnorePointer(
          ignoring: !isSwitchingPath,
          ignoringSemantics: true,
          child: GestureDetector(
            onTap: () => this.isSwitchingPath.value = false,
            child: AnimatedOpacity(
              duration: switchingPathDuration,
              opacity: isSwitchingPath ? .75 : 0,
              child: ColoredBox(color: theme.colorScheme.onSurface),
            ),
          ),
        ),
      ),
    );
  }

  @override
  Widget pathEntityListWidget(BuildContext context) {
    return Positioned.fill(
      top: isAppleOS ? context.topPadding + kToolbarHeight : 0,
      bottom: null,
      child: ValueListenableBuilder<bool>(
        valueListenable: isSwitchingPath,
        builder: (_, bool isSwitchingPath, Widget? w) => AnimatedAlign(
          duration: switchingPathDuration,
          curve: switchingPathCurve,
          alignment: Alignment.bottomCenter,
          heightFactor: isSwitchingPath ? 1 : 0,
          child: AnimatedOpacity(
            duration: switchingPathDuration,
            curve: switchingPathCurve,
            opacity: !isAppleOS || isSwitchingPath ? 1 : 0,
            child: Container(
              constraints: BoxConstraints(
                maxHeight: MediaQuery.of(context).size.height * (isAppleOS ? .6 : .8),
              ),
              color: theme.colorScheme.background,
              child: w,
            ),
          ),
        ),
        child: Column(
          mainAxisSize: MainAxisSize.min,
          children: <Widget>[
            ValueListenableBuilder<PermissionState>(
              valueListenable: permission,
              builder: (_, PermissionState ps, Widget? child) {
                if (isPermissionLimited) {
                  return child!;
                }
                return const SizedBox.shrink();
              },
              child: Padding(
                padding: const EdgeInsets.symmetric(
                  horizontal: 20,
                  vertical: 12,
                ),
                child: Text.rich(
                  TextSpan(
                    children: <TextSpan>[
                      TextSpan(
                        text: Constants.textDelegate.viewingLimitedAssetsTip,
                      ),
                      TextSpan(
                        text: ' '
                            '${Constants.textDelegate.changeAccessibleLimitedAssets}',
                        style: TextStyle(color: interactiveTextColor(context)),
                        recognizer: TapGestureRecognizer()..onTap = PhotoManager.presentLimited,
                      ),
                    ],
                  ),
                  style: context.themeData.textTheme.caption?.copyWith(
                    fontSize: 14,
                  ),
                ),
              ),
            ),
            Flexible(
              child: Selector<LyAssetPickerProvider, int>(
                selector: (_, LyAssetPickerProvider p) => p.validPathThumbCount,
                builder: (_, int count, __) => Selector<LyAssetPickerProvider, List<PathWrapper<AssetPathEntity>>>(
                  selector: (_, LyAssetPickerProvider p) => p.paths,
                  builder: (_, List<PathWrapper<AssetPathEntity>> paths, __) {
                    final List<PathWrapper<AssetPathEntity>> filtered = paths
                        .where(
                          (PathWrapper<AssetPathEntity> p) => (p.assetCount ?? 0) != 0,
                        )
                        .toList();
                    return ListView.builder(
                      padding: const EdgeInsetsDirectional.only(top: 1),
                      shrinkWrap: true,
                      itemCount: filtered.length,
                      itemBuilder: (BuildContext c, int i) => pathEntityWidget(
                        context: c,
                        list: filtered,
                        index: i,
                      ),
                    );
                  },
                ),
              ),
            ),
          ],
        ),
      ),
    );
  }

  @override
  Widget pathEntitySelector(BuildContext context) {
    Widget _text(BuildContext context, String text) {
      return Flexible(
        child: ScaleText(
          text,
          style: TextStyle(
            fontSize: 17,
            fontFamily: AppFontFamily.pingFangRegular,
          ),
          maxLines: 1,
          overflow: TextOverflow.fade,
          maxScaleFactor: 1.2,
        ),
      );
    }

    return UnconstrainedBox(
      child: GestureDetector(
        onTap: () {
          if (isPermissionLimited && provider.isAssetsEmpty) {
            PhotoManager.presentLimited();
            return;
          }
          if (provider.currentPath == null) {
            return;
          }
          isSwitchingPath.value = !isSwitchingPath.value;
        },
        child: Container(
          height: appBarItemHeight,
          constraints: BoxConstraints(
            maxWidth: MediaQuery.of(context).size.width * 0.5,
          ),
          padding: const EdgeInsetsDirectional.only(start: 12, end: 6),
          child: Selector<LyAssetPickerProvider, PathWrapper<AssetPathEntity>?>(
            selector: (_, LyAssetPickerProvider p) => p.currentPath,
            builder: (_, PathWrapper<AssetPathEntity>? p, Widget? w) {
              final AssetPathEntity? path = p?.path;
              return Row(
                mainAxisSize: MainAxisSize.min,
                children: <Widget>[
                  if (path == null && isPermissionLimited)
                    _text(
                      context,
                      Constants.textDelegate.changeAccessibleLimitedAssets,
                    ),
                  if (path != null)
                    Flexible(
                      child: ScaleText(
                        isPermissionLimited && path.isAll ? Constants.textDelegate.accessiblePathName : LyAssetsPicker.pathNameBuilder(path),
                        style: TextStyle(
                          fontSize: 17,
                          color: theme.colorScheme.onSurface,
                          fontFamily: AppFontFamily.pingFangRegular,
                        ),
                        maxLines: 1,
                        overflow: TextOverflow.ellipsis,
                        maxScaleFactor: 1.2,
                      ),
                    ),
                  w!,
                ],
              );
            },
            child: Padding(
              padding: const EdgeInsetsDirectional.only(start: 0),
              child: ValueListenableBuilder<bool>(
                valueListenable: isSwitchingPath,
                builder: (_, bool isSwitchingPath, Widget? w) => Transform.rotate(
                  angle: isSwitchingPath ? math.pi : 0,
                  alignment: Alignment.center,
                  child: w,
                ),
                child: const Icon(
                  Icons.keyboard_arrow_down,
                  size: 25,
                  color: dark666666,
                ),
              ),
            ),
          ),
        ),
      ),
    );
  }

  @override
  Widget pathEntityWidget({
    required BuildContext context,
    required List<PathWrapper<AssetPathEntity>> list,
    required int index,
  }) {
    final PathWrapper<AssetPathEntity> wrapper = list[index];
    final AssetPathEntity pathEntity = wrapper.path;
    final Uint8List? data = wrapper.thumbnailData;
    final int? assetCount = wrapper.assetCount;

    Widget builder() {
      if (data != null) {
        // The reason that the `thumbData` should be checked at here to see if it
        // is null is that even the image file is not exist, the `File` can still
        // returned as it exist, which will cause the thumb bytes return null.
        //
        // 此处需要检查缩略图为空的原因是：尽管文件可能已经被删除，
        // 但通过 `File` 读取的文件对象仍然存在，使得返回的数据为空。
        return Image.memory(data, fit: BoxFit.cover);
      }
      if (pathEntity.type.containsAudio()) {
        return ColoredBox(
          color: theme.colorScheme.primary.withOpacity(0.12),
          child: const Center(child: Icon(Icons.audiotrack)),
        );
      }
      return ColoredBox(color: theme.colorScheme.primary.withOpacity(0.12));
    }

    return Selector<LyAssetPickerProvider, PathWrapper<AssetPathEntity>?>(
      selector: (_, LyAssetPickerProvider p) => p.currentPath,
      builder: (_, PathWrapper<AssetPathEntity>? currentWrapper, __) {
        final bool isSelected = currentWrapper?.path == pathEntity;
        return Material(
          type: MaterialType.transparency,
          child: InkWell(
            splashFactory: InkSplash.splashFactory,
            onTap: () {
              context.read<LyAssetPickerProvider>().switchPath(wrapper);
              isSwitchingPath.value = false;
              gridScrollController.jumpTo(0);
            },
            child: Container(
              color: theme.colorScheme.onPrimary,
              padding: const EdgeInsetsDirectional.only(top: 8, bottom: 8, start: 16, end: 0),
              height: 64,
              child: Row(
                children: <Widget>[
                  RepaintBoundary(
                    child: AspectRatio(aspectRatio: 1, child: builder()),
                  ),
                  Expanded(
                    child: Padding(
                      padding: const EdgeInsetsDirectional.only(
                        start: 12,
                      ),
                      child: Column(
                        mainAxisAlignment: MainAxisAlignment.center,
                        crossAxisAlignment: CrossAxisAlignment.start,
                        children: <Widget>[
                          Flexible(
                            child: ScaleText(
                              isPermissionLimited && pathEntity.isAll ? Constants.textDelegate.accessiblePathName : LyAssetsPicker.pathNameBuilder(pathEntity),
                              style: const TextStyle(fontSize: 17, color: dark333333),
                              maxLines: 1,
                              overflow: TextOverflow.ellipsis,
                            ),
                          ),
                          ScaleText(
                            '$assetCount',
                            style: const TextStyle(
                              color: dark999999,
                              fontSize: 12,
                            ),
                            maxLines: 1,
                            overflow: TextOverflow.ellipsis,
                          ),
                        ],
                      ),
                    ),
                  ),
                  if (isSelected)
                    AspectRatio(
                      aspectRatio: 1,
                      child: Icon(Icons.check, color: themeColor, size: 20),
                    ),
                ],
              ),
            ),
          ),
        );
      },
    );
  }

  @override
  Widget previewButton(BuildContext context) {
    return Selector<LyAssetPickerProvider, bool>(
      selector: (_, LyAssetPickerProvider p) => p.isSelectedNotEmpty,
      builder: (BuildContext c, bool isSelectedNotEmpty, Widget? child) {
        return GestureDetector(
          onTap: () async {
            if (!isSelectedNotEmpty) {
              return;
            }
            final List<LyAssetEntity> _selected;
            if (isWeChatMoment) {
              _selected = provider.selectedAssets.where((LyAssetEntity e) => e.type == AssetType.image).toList();
            } else {
              _selected = provider.selectedAssets;
            }

            /// 预览
            final List<LyAssetEntity>? result = await LyAssetPickerViewer.pushToViewer(context,
                currentIndex: 0,
                previewAssets: _selected,
                previewThumbnailSize: previewThumbnailSize,
                selectedAssets: _selected,
                selectorProvider: provider,
                themeData: theme,
                maxAssets: provider.maxAssets,
                maxMinute: provider.maxMinute,
                packageName: packageName);
            if (result != null) {
              Navigator.of(context).maybePop(result);
            }
          },
          child: Selector<LyAssetPickerProvider, String>(
            selector: (_, LyAssetPickerProvider p) => p.selectedDescriptions,
            builder: (_, __, ___) => ScaleText(
              Constants.textDelegate.preview,
              style: TextStyle(
                color: isSelectedNotEmpty ? themeColor : themeColor.withOpacity(0.6),
                fontSize: 17,
                fontFamily: AppFontFamily.pingFangMedium,
              ),
              maxScaleFactor: 1.2,
            ),
          ),
        );
      },
    );
  }

  @override
  Widget itemBannedIndicator(BuildContext context, LyAssetEntity asset) {
    return Consumer<LyAssetPickerProvider>(
      builder: (_, LyAssetPickerProvider p, __) {
        if (asset.type == AssetType.video) {
          if (asset.duration > (p.maxMinute * 60)) {
            return Container(
              color: theme.colorScheme.onPrimary.withOpacity(.8),
              alignment: Alignment.center,
              child: LYText.withStyle(
                '视频超过${p.maxMinute}分钟\n不允许上传',
                style: text12.textColor(Colors.black),
                textAlign: TextAlign.center,
              ),
            );
          }
          return const SizedBox.shrink();
        } else if (asset.type == AssetType.image) {
          var isNotAllow = (p.allowTypes != null && !p.allowTypes!.contains(AssetUtil.assetSuffix(asset)));
          if (p.invalidFormats.contains(asset.id) || isNotAllow) {
            return Container(
              color: theme.colorScheme.onPrimary.withOpacity(.8),
              alignment: Alignment.center,
              child: LYText.withStyle(
                '不支持${AssetUtil.pureAssetSuffix(asset)}格式',
                style: text12.textColor(Colors.black),
              ),
            );
          }
          return const SizedBox.shrink();
        }

        return const SizedBox.shrink();
      },
    );
  }

  @override
  Widget selectIndicator(BuildContext context, LyAssetEntity asset) {
    final Duration duration = switchingPathDuration * 0.75;
    return Selector<LyAssetPickerProvider, String>(
      selector: (_, LyAssetPickerProvider p) => p.selectedDescriptions,
      builder: (BuildContext context, String descriptions, __) {
        final LyAssetPickerProvider p = context.read<LyAssetPickerProvider>();
        final bool selected = descriptions.contains(asset.toString());

        final List<String> invalidFormats = p.invalidFormats;
        final List<String>? allowTypes = p.allowTypes;
        var package = packageName == "" ? null : packageName ?? Config.packageName;
        // final double indicatorSize =
        //     MediaQuery.of(context).size.width / gridCount / 3;
        final Widget innerSelector = AnimatedSwitcher(
          duration: duration,
          reverseDuration: duration,
          child: selected
              ? Image(
                  image: AssetImage('assets/images/public/icon_radio_1.png', package: package),
                  width: 22,
                  height: 22,
                )
              : Image(
                  image: AssetImage('assets/images/public/icon_radio_f0.png', package: package),
                  width: 22,
                  height: 22,
                ),
        );
        final GestureDetector selectorWidget = GestureDetector(
          behavior: HitTestBehavior.opaque,
          onTap: () async {
            final bool? selectPredicateResult = await selectPredicate?.call(
              context,
              asset,
              selected,
            );
            if (selectPredicateResult == false) {
              return;
            }
            if (selected) {
              provider.unSelectAsset(asset);
              return;
            }
            provider.selectAsset(asset);
          },
          child: Container(
            color: Colors.transparent,
            padding: const EdgeInsets.all(8),
            width: isPreviewEnabled ? 36 : null,
            height: isPreviewEnabled ? 36 : null,
            alignment: AlignmentDirectional.topEnd,
            child: (!isPreviewEnabled && !selected) ? const SizedBox.shrink() : innerSelector,
          ),
        );
        if (isPreviewEnabled) {
          if (invalidFormats.contains(asset.id) ||
              (asset.type == AssetType.video && asset.duration > (provider.maxMinute * 60)) ||
              (asset.type == AssetType.image && allowTypes != null && !allowTypes.contains(AssetUtil.assetSuffix(asset)))) {
            return const SizedBox.shrink();
          }
          return PositionedDirectional(
            top: 0,
            start: 0,
            child: selectorWidget,
          );
        }
        return selectorWidget;
      },
    );
  }

  @override
  Widget selectedBackdrop(BuildContext context, int index, LyAssetEntity asset) {
    bool selectedAllAndNotSelected() => !provider.selectedAssets.contains(asset) && provider.selectedMaximumAssets;
    // bool selectedPhotosAndIsVideo() =>
    //     isWeChatMoment &&
    //     asset.type == AssetType.video &&
    //     provider.selectedAssets.isNotEmpty;

    return Positioned.fill(
      child: GestureDetector(
        onTap: () async {
          // When we reached the maximum select count and the asset
          // is not selected, do nothing.
          // When the special type is WeChat Moment, pictures and videos cannot
          // be selected at the same time. Video select should be banned if any
          // pictures are selected.
          if (selectedAllAndNotSelected()) {
            // ToastUtil.showToast(
            //     '最多只能选择${(maxToSelect)}${(provider.requestType == RequestType.common ? '个图片和视频' : (provider.requestType == RequestType.video ? '个视频' : '张图'))}');
            ToastUtil.showToast('已超过可选数量，不可进行选择！');
            return;
          }
          var aa = await asset.isLocallyAvailable();
          // Logger.d("x====id=${asset.id}");
          // Logger.d("x====isLocallyAvailable=$aa");
          // Logger.d("x====title=${asset.title}");
          final List<LyAssetEntity> _current;
          final List<LyAssetEntity>? _selected;
          final int _index;
          if (isWeChatMoment) {
            if (asset.type == AssetType.video) {
              _current = <LyAssetEntity>[asset];
              _selected = null;
              _index = 0;
            } else {
              _current = provider.currentAssets.where((LyAssetEntity e) => e.type == AssetType.image).toList();
              _selected = provider.selectedAssets;
              _index = _current.indexOf(asset);
            }
          } else {
            _current = provider.currentAssets;
            _selected = provider.selectedAssets;
            _index = index;
          }

          ///预览
          final List<LyAssetEntity>? result = await LyAssetPickerViewer.pushToViewer(context,
              currentIndex: _index,
              previewAssets: _current,
              themeData: theme,
              previewThumbnailSize: previewThumbnailSize,
              selectedAssets: _selected,
              selectorProvider: provider,
              specialPickerType: specialPickerType,
              maxAssets: provider.maxAssets,
              maxMinute: provider.maxMinute,
              packageName: packageName
              // shouldReversePreview: isAppleOS,
              );
          if (result != null) {
            Navigator.of(context).maybePop(result);
          }
        },
        child: Consumer<LyAssetPickerProvider>(
          builder: (_, LyAssetPickerProvider p, __) {
            final int index = p.selectedAssets.indexOf(asset);
            final bool selected = index != -1;
            return AnimatedContainer(
              duration: switchingPathDuration,
              color: selected ? theme.colorScheme.primary.withOpacity(.45) : theme.colorScheme.onSurface.withOpacity(.1),
              child: const SizedBox.shrink(),
            );
          },
        ),
      ),
    );
  }

  /// Videos often contains various of color in the cover,
  /// so in order to keep the content visible in most cases,
  /// the color of the indicator has been set to [Colors.white].
  ///
  /// 视频封面通常包含各种颜色，为了保证内容在一般情况下可见，此处
  /// 将指示器的图标和文字设置为 [Colors.white]。
  @override
  Widget videoIndicator(BuildContext context, LyAssetEntity asset) {
    return PositionedDirectional(
      start: 0,
      end: 0,
      bottom: 0,
      child: Container(
        width: double.maxFinite,
        height: 26,
        padding: const EdgeInsets.symmetric(horizontal: 2, vertical: 2),
        decoration: BoxDecoration(
          gradient: LinearGradient(
            begin: AlignmentDirectional.bottomCenter,
            end: AlignmentDirectional.topCenter,
            colors: <Color>[theme.dividerColor, Colors.transparent],
          ),
        ),
        child: Row(
          crossAxisAlignment: CrossAxisAlignment.center,
          children: <Widget>[
            Image(
              image: AssetImage('assets/images/icon_play.png', package: Config.packageName),
              width: 12.w,
              height: 12.w,
            ),
            Expanded(
              child: Padding(
                padding: const EdgeInsetsDirectional.only(start: 4),
                child: Text(
                  Constants.textDelegate.durationIndicatorBuilder(
                    Duration(seconds: asset.duration),
                  ),
                  style: TextStyle(color: theme.colorScheme.onPrimary, fontSize: 11),
                  maxLines: 1,
                ),
              ),
            ),
            _videoIcloudIndicator(context, asset),
          ],
        ),
      ),
    );
  }

  /// image 云端标识
  /// 将指示器的图标和文字设置为 [Colors.white]。
  Widget _videoIcloudIndicator(BuildContext context, LyAssetEntity asset) {
    final List<String> currentUploaded = context.select<LyAssetPickerProvider, List<String>>(
      (LyAssetPickerProvider p) => p.currentUploaded,
    );
    if (!currentUploaded.contains(asset.id)) return Container();
    return Image(
      image: AssetImage('assets/images/cloud_upload.png', package: Config.packageName),
      width: 24.w,
      height: 24.w,
    );
  }

  /// image 云端标识
  /// 将指示器的图标和文字设置为 [Colors.white]。
  Widget imageIndicator(BuildContext context, LyAssetEntity asset) {
    final List<String> currentUploaded = context.select<LyAssetPickerProvider, List<String>>(
      (LyAssetPickerProvider p) => p.currentUploaded,
    );
    if (!currentUploaded.contains(asset.id)) return Container();

    return PositionedDirectional(
      start: 0,
      end: 0,
      bottom: 0,
      child: Container(
        width: double.maxFinite,
        height: 26,
        padding: const EdgeInsets.symmetric(horizontal: 2, vertical: 2),
        decoration: BoxDecoration(
          gradient: LinearGradient(
            begin: AlignmentDirectional.bottomCenter,
            end: AlignmentDirectional.topCenter,
            colors: <Color>[theme.dividerColor, Colors.transparent],
          ),
        ),
        child: Row(
          mainAxisAlignment: MainAxisAlignment.spaceBetween,
          crossAxisAlignment: CrossAxisAlignment.center,
          children: <Widget>[
            Container(),
            Image(
              image: AssetImage('assets/images/cloud_upload.png', package: Config.packageName),
              width: 24.w,
              height: 24.w,
            ),
          ],
        ),
      ),
    );
  }

  /// Yes, the build method.
  /// 没错，是它是它就是它，我们亲爱的 build 方法~
  @override
  Widget build(BuildContext context) {
    // Schedule the scroll position's restoration callback if this feature
    // is enabled and offsets are different.
    if (keepScrollOffset && Constants.scrollPosition != null) {
      SchedulerBinding.instance.addPostFrameCallback((_) {
        // Update only if the controller has clients.
        if (gridScrollController.hasClients) {
          gridScrollController.jumpTo(Constants.scrollPosition!.pixels);
        }
      });
    }
    return AnnotatedRegion<SystemUiOverlayStyle>(
      value: overlayStyle,
      child: Theme(
        data: theme,
        child: ChangeNotifierProvider<LyAssetPickerProvider>.value(
          value: provider,
          builder: (BuildContext c, __) => Material(
            color: theme.canvasColor,
            child: Stack(
              fit: StackFit.expand,
              children: <Widget>[
                if (isAppleOS) appleOSLayout(c) else androidLayout(c),
                if (Platform.isIOS) iOSPermissionOverlay(c),
              ],
            ),
          ),
        ),
      ),
    );
  }
}
