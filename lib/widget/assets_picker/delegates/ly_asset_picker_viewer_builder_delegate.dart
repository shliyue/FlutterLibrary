///
/// [Author] Alex (https://github.com/AlexV525)
/// [Date] 2020-10-31 00:15
///
import 'dart:io';
import 'dart:math' as math;

import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:zyocore/values/style.dart';
import 'package:zyocore/values/text.dart';
import 'package:zyocore/widget/assets_picker/constants/constants.dart';
import 'package:zyocore/widget/assets_picker/constants/custom_scroll_physics.dart';
import 'package:zyocore/widget/assets_picker/constants/enums.dart';
import 'package:zyocore/widget/assets_picker/constants/typedefs.dart';
import 'package:zyocore/widget/assets_picker/delegates/asset_picker_viewer_builder_delegate.dart';
import 'package:zyocore/widget/assets_picker/entity/ly_asset_entity.dart';
import 'package:zyocore/widget/assets_picker/provider/asset_picker_provider.dart';
import 'package:zyocore/widget/assets_picker/provider/asset_picker_viewer_provider.dart';
import 'package:zyocore/widget/assets_picker/provider/ly_asset_picker_provider.dart';
import 'package:zyocore/widget/assets_picker/widget/builder/audio_page_builder.dart';
import 'package:zyocore/widget/assets_picker/widget/builder/simple_image_page_builder.dart';
import 'package:zyocore/widget/assets_picker/widget/builder/simple_video_page_builder.dart';
import 'package:zyocore/widget/assets_picker/widget/builder/video_page_builder.dart';

import '../../../core/configs/config.dart';
import '../../../zyocore.dart';
import '../widget/builder/value_listenable_builder_2.dart';
import '../widget/scale_text.dart';

class LyAssetPickerViewerBuilderDelegate extends AssetPickerViewerBuilderDelegate<LyAssetEntity, AssetPathEntity> {
  LyAssetPickerViewerBuilderDelegate({
    required int currentIndex,
    required List<LyAssetEntity> previewAssets,
    LyAssetPickerProvider? selectorProvider,
    required ThemeData themeData,
    AssetPickerViewerProvider<LyAssetEntity>? provider,
    List<LyAssetEntity>? selectedAssets,
    this.previewThumbnailSize,
    this.specialPickerType,
    int? maxAssets,
    int? maxMinute,
    bool shouldReversePreview = false,
    AssetSelectPredicate<LyAssetEntity>? selectPredicate,
    String? packageName,
  }) : super(
          currentIndex: currentIndex,
          previewAssets: previewAssets,
          provider: provider,
          themeData: themeData,
          selectedAssets: selectedAssets,
          selectorProvider: selectorProvider,
          maxAssets: maxAssets,
          shouldReversePreview: shouldReversePreview,
          selectPredicate: selectPredicate,
          packageName: packageName,
          maxMinute: maxMinute,
        );

  /// Thumb size for the preview of images in the viewer.
  /// 预览时图片的缩略图大小
  final ThumbnailSize? previewThumbnailSize;

  /// The current special picker type for the viewer.
  /// 当前特殊选择类型
  ///
  /// If the type is not null, the title of the viewer will not display.
  /// 如果类型不为空，则标题将不会显示。
  final SpecialPickerType? specialPickerType;

  /// Whether the [SpecialPickerType.wechatMoment] is enabled.
  /// 当前是否为微信朋友圈选择模式
  bool get isWeChatMoment => specialPickerType == SpecialPickerType.wechatMoment;

  /// Whether there are videos in preview/selected assets.
  /// 当前正在预览或已选的资源是否有视频
  bool get hasVideo =>
      previewAssets.any((LyAssetEntity e) => e.type == AssetType.video) || (selectedAssets?.any((LyAssetEntity e) => e.type == AssetType.video) ?? false);

  List<GlobalKey> _keys = [];

  @override
  Widget assetPageBuilder(BuildContext context, int index) {
    final LyAssetEntity asset = previewAssets.elementAt(index);
    Widget _builder;
    switch (asset.type) {
      case AssetType.audio:
        _builder = AudioPageBuilder(asset: asset);
        break;
      case AssetType.image:
        _builder = SimpleImagePageBuilder(
          asset: asset,
          delegate: this,
          // assetPickerProvider: selectorProvider!,
          previewThumbnailSize: previewThumbnailSize,
          // previewProvider: provider
        );
        break;
      case AssetType.video:
        final bool notLocallyAvailable = provider!.notLocallyAvailables.contains(asset.id);
        _builder = Platform.isAndroid || (!notLocallyAvailable && asset.duration <= (maxMinute! * 60))
            ? VideoPageBuilder(
                asset: asset,
                delegate: this,
                // assetPickerProvider: selectorProvider!,
                hasOnlyOneVideoAndMoment: isWeChatMoment && hasVideo,
                // previewProvider: provider
              )
            : SimpleVideoPageBuilder(
                asset: asset,
                delegate: this,
                // assetPickerProvider: selectorProvider!,
                hasOnlyOneVideoAndMoment: isWeChatMoment && hasVideo,
                // previewProvider: provider
              );
        break;
      case AssetType.other:
        _builder = Center(
          child: ScaleText(Constants.textDelegate.unSupportedAssetType),
        );
        break;
    }
    return _builder;
  }

  /// Preview item widgets for audios.
  /// 音频的底部预览部件
  Widget _audioPreviewItem(LyAssetEntity asset) {
    return const ColoredBox(
      color: lightE1E1E1,
      child: Center(child: Icon(Icons.audiotrack)),
    );
  }

  /// Preview item widgets for images.
  /// 图片的底部预览部件
  Widget _imagePreviewItem(LyAssetEntity asset) {
    return Positioned.fill(
      child: RepaintBoundary(
        child: ExtendedImage(
          image: AssetEntityImageProvider(asset, isOriginal: false),
          fit: BoxFit.cover,
        ),
      ),
    );
  }

  /// Preview item widgets for video.
  /// 视频的底部预览部件
  Widget _videoPreviewItem(LyAssetEntity asset) {
    return Positioned.fill(
      child: Stack(
        children: <Widget>[
          _imagePreviewItem(asset),
          Center(
            child: Icon(
              Icons.video_library,
              color: themeData.iconTheme.color?.withOpacity(0.54),
            ),
          ),
        ],
      ),
    );
  }

  /// The back button when previewing video in [SpecialPickerType.wechatMoment].
  /// 使用 [SpecialPickerType.wechatMoment] 预览视频时的返回按钮
  Widget momentVideoBackButton(BuildContext context) {
    return PositionedDirectional(
      start: 16,
      top: MediaQuery.of(context).padding.top + 16,
      child: GestureDetector(
        onTap: Navigator.of(context).maybePop,
        child: Container(
          padding: const EdgeInsets.all(5),
          decoration: BoxDecoration(
            color: themeData.iconTheme.color,
            shape: BoxShape.circle,
          ),
          child: Icon(
            Icons.keyboard_return_rounded,
            color: themeData.canvasColor,
            size: 18,
          ),
        ),
      ),
    );
  }

  @override
  Widget bottomDetailBuilder(BuildContext context) {
    Color _backgroundColor = Colors.black.withOpacity(0.8);
    return ValueListenableBuilder2<bool, int>(
      firstNotifier: isDisplayingDetail,
      secondNotifier: selectedNotifier,
      builder: (_, bool v, __, Widget? child) => AnimatedPositionedDirectional(
        duration: kThemeAnimationDuration,
        curve: Curves.easeInOut,
        bottom: v ? 0 : -(MediaQuery.of(context).padding.bottom + bottomDetailHeight),
        start: 0,
        end: 0,
        height: MediaQuery.of(context).padding.bottom + bottomDetailHeight,
        child: child!,
      ),
      child: ChangeNotifierProvider<AssetPickerViewerProvider<LyAssetEntity>?>.value(
        value: provider,
        child: Column(
          mainAxisSize: MainAxisSize.min,
          mainAxisAlignment: MainAxisAlignment.end,
          children: <Widget>[
            if (provider != null)
              ValueListenableBuilder<int>(
                valueListenable: selectedNotifier,
                builder: (_, int count, __) {
                  _keys = List.generate(count, (index) => GlobalKey());
                  return Container(
                    width: count > 0 ? double.maxFinite : 0,
                    height: bottomPreviewHeight,
                    color: _backgroundColor,
                    child: ListView.builder(
                      controller: previewingListController,
                      scrollDirection: Axis.horizontal,
                      padding: const EdgeInsets.symmetric(horizontal: 16.0),
                      physics: const ClampingScrollPhysics(),
                      itemCount: count,
                      itemBuilder: bottomDetailItemBuilder,
                    ),
                  );
                },
              ),
            Container(
              height: bottomBarHeight + MediaQuery.of(context).padding.bottom,
              padding: const EdgeInsets.symmetric(horizontal: 16.0).copyWith(bottom: MediaQuery.of(context).padding.bottom),
              color: _backgroundColor,
              child: Row(
                mainAxisAlignment: MainAxisAlignment.spaceBetween,
                children: <Widget>[
                  const Spacer(),
                  // if (isAppleOS && (provider != null || isWeChatMoment))
                  //   confirmButton(context)
                  // else
                  // selectButton(context),
                  confirmButton(context)
                ],
              ),
            ),
          ],
        ),
      ),
    );
  }

  @override
  Widget bottomDetailItemBuilder(BuildContext context, int index) {
    const double padding = 6.0;
    return Padding(
      key: _keys[index],
      padding: const EdgeInsets.symmetric(
        horizontal: padding,
        vertical: padding * 2,
      ),
      child: AspectRatio(
        aspectRatio: 1.0,
        child: StreamBuilder<int>(
          initialData: currentIndex,
          stream: pageStreamController.stream,
          builder: (_, AsyncSnapshot<int> snapshot) {
            final LyAssetEntity asset = selectedAssets!.elementAt(index);
            final bool isViewing = previewAssets[snapshot.data!] == asset;
            final Widget _item = () {
              switch (asset.type) {
                case AssetType.image:
                  return _imagePreviewItem(asset);
                case AssetType.video:
                  return _videoPreviewItem(asset);
                case AssetType.audio:
                  return _audioPreviewItem(asset);
                default:
                  return const SizedBox.shrink();
              }
            }();
            return GestureDetector(
              onTap: () {
                final int page;
                if (previewAssets != selectedAssets) {
                  page = previewAssets.indexOf(asset);
                } else {
                  page = index;
                }
                if (pageController.page == page.toDouble()) {
                  return;
                }

                /// 切换顶部path路径后，再去预览，点击底部选中的图片时，
                /// 上方预览大图中，可能不存在该点击的图片资源，此时不做预览图的切换处理
                if (previewAssets.contains(asset)) {
                  pageController.jumpToPage(page);
                }

                // final double offset =
                //     (index - 0.5) * (bottomPreviewHeight - padding * 3) -
                //         MediaQuery.of(context).size.width / 4;
                // previewingListController.animateTo(
                //   math.max(0, offset),
                //   curve: Curves.ease,
                //   duration: kThemeChangeDuration,
                // );
                Scrollable.ensureVisible(_keys[index].currentContext!, alignment: 0.5);
              },
              child: Selector<AssetPickerViewerProvider<LyAssetEntity>?, List<LyAssetEntity>?>(
                selector: (_, AssetPickerViewerProvider<LyAssetEntity>? p) => p?.currentlySelectedAssets,
                child: _item,
                builder: (
                  _,
                  List<LyAssetEntity>? currentlySelectedAssets,
                  Widget? w,
                ) {
                  // final bool isSelected =
                  //     currentlySelectedAssets?.contains(asset) ?? false;
                  return Stack(
                    children: <Widget>[
                      w!,
                      AnimatedContainer(
                        duration: kThemeAnimationDuration,
                        curve: Curves.easeInOut,
                        decoration: BoxDecoration(
                          border: isViewing
                              ? Border.all(
                                  color: themeData.colorScheme.secondary,
                                  width: 4,
                                )
                              : null,
                          // color: isSelected
                          //     ? null
                          //     : themeData.colorScheme.surface.withOpacity(0.54),
                        ),
                      ),
                    ],
                  );
                },
              ),
            );
          },
        ),
      ),
    );
  }

  /// AppBar widget.
  /// 顶栏部件
  Widget appBar(BuildContext context) {
    return ValueListenableBuilder<bool>(
      valueListenable: isDisplayingDetail,
      builder: (_, bool value, Widget? child) => AnimatedPositionedDirectional(
        duration: kThemeAnimationDuration,
        curve: Curves.easeInOut,
        top: value ? 0.0 : -(MediaQuery.of(context).padding.top + kToolbarHeight),
        start: 0.0,
        end: 0.0,
        height: MediaQuery.of(context).padding.top + kToolbarHeight,
        child: child!,
      ),
      child: Container(
        padding: EdgeInsetsDirectional.only(top: MediaQuery.of(context).padding.top),
        color: Colors.black,
        child: Stack(
          fit: StackFit.expand,
          children: <Widget>[
            StreamBuilder<int>(
                initialData: currentIndex,
                stream: pageStreamController.stream,
                builder: (BuildContext _, AsyncSnapshot<int> snapshot) {
                  return ChangeNotifierProvider<AssetPickerViewerProvider<LyAssetEntity>>.value(
                      value: provider!,
                      child: Selector<AssetPickerViewerProvider<LyAssetEntity>, List<String>>(
                          selector: (_, AssetPickerViewerProvider<LyAssetEntity> p) => p.notLocallyAvailables,
                          builder: (_, List<String> notLocallyAvailables, ___) {
                            final LyAssetEntity asset = previewAssets.elementAt(
                              snapshot.data!,
                            );
                            final bool isInvalid = asset.type == AssetType.image && selectorProvider!.invalidFormats.contains(asset.id);
                            final bool videoTooLong = (asset.type == AssetType.video && asset.duration > (maxMinute! * 60));

                            final bool notLocallyAvailable = notLocallyAvailables.contains(
                              asset.id,
                            );
                            return Row(
                              mainAxisAlignment: MainAxisAlignment.start,
                              children: <Widget>[
                                GestureDetector(
                                  onTap: Navigator.of(context).maybePop,
                                  child: Container(
                                    height: kToolbarHeight,
                                    width: kToolbarHeight,
                                    color: Colors.black,
                                    child: const Icon(
                                      Icons.arrow_back_ios,
                                      size: 20,
                                      color: Colors.white,
                                    ),
                                  ),
                                ),
                                isInvalid || videoTooLong || notLocallyAvailable
                                    ? Expanded(
                                        child: Container(
                                        alignment: Alignment.center,
                                        child: Row(
                                          mainAxisSize: MainAxisSize.min,
                                          mainAxisAlignment: MainAxisAlignment.end,
                                          children: [
                                            Image(
                                              image: AssetImage('assets/images/icon_warn.png', package: Config.packageName),
                                              width: 22.w,
                                              height: 22.w,
                                            ),
                                            const SizedBox(width: 5),
                                            LYText.withStyle(
                                              isInvalid
                                                  ? '不支持${AssetUtil.pureAssetSuffix(currentAsset)}格式'
                                                  : (videoTooLong ? '视频超过${maxMinute!}分钟，不允许上传' : (notLocallyAvailable ? '照片视频在iCloud' : '')),
                                              style: text17,
                                            )
                                          ],
                                        ),
                                      ))
                                    : const Spacer(),
                                if (provider != null) selectButton(context),
                                // if (!isAppleOS && (provider != null || isWeChatMoment))
                                //   Padding(
                                //     padding: const EdgeInsetsDirectional.only(end: 16),
                                //     child: confirmButton(context),
                                //   ),
                              ],
                            );
                          }));
                }),
          ],
        ),
      ),
    );
  }

  /// It'll pop with [AssetPickerProvider.selectedAssets] when there are
  /// any assets were chosen. Then, the assets picker will pop too.
  /// 当有资源已选时，点击按钮将把已选资源通过路由返回。
  /// 资源选择器将识别并一同返回。
  @override
  Widget confirmButton(BuildContext context) {
    return ChangeNotifierProvider<AssetPickerViewerProvider<LyAssetEntity>?>.value(
      value: provider,
      child: Consumer<AssetPickerViewerProvider<LyAssetEntity>?>(
        builder: (_, AssetPickerViewerProvider<LyAssetEntity>? provider, __) {
          assert(
            isWeChatMoment || provider != null,
            'Viewer provider must not be null'
            'when the special type is not WeChat moment.',
          );
          return GestureDetector(
            onTap: () {
              if (isWeChatMoment && hasVideo) {
                Navigator.of(context).pop(<LyAssetEntity>[currentAsset]);
                return;
              }
              if (provider.isSelectedNotEmpty) {
                Navigator.of(context).pop(provider.currentlySelectedAssets);
                return;
              }
              // selectAsset(currentAsset);
              // Navigator.of(context).pop(
              //   selectedAssets ?? <LyAssetEntity>[currentAsset],
              // );
            },
            child: Container(
              height: 30,
              alignment: Alignment.center,
              constraints: BoxConstraints(
                minWidth: provider!.isSelectedNotEmpty ? 124 : 50,
              ),
              decoration: BoxDecoration(
                  color: provider.isSelectedNotEmpty ? themeData.colorScheme.secondary : themeData.colorScheme.secondary.withOpacity(0.6),
                  borderRadius: BorderRadius.circular(15)),
              padding: const EdgeInsets.symmetric(horizontal: 17),
              child: ScaleText(
                provider.isSelectedNotEmpty
                    ? '${Constants.textDelegate.complete}(${provider.currentlySelectedAssets.length})'
                    : Constants.textDelegate.complete,
                style: TextStyle(
                  color: provider.isSelectedNotEmpty ? themeData.colorScheme.onPrimary : themeData.colorScheme.onPrimary.withOpacity(0.6),
                  fontSize: 15,
                  fontFamily: AppFontFamily.pingFangMedium,
                ),
              ),
            ),
          );
        },
      ),
    );
  }

  /// Select button for apple OS.
  /// 苹果系列系统的选择按钮
  Widget _appleOSSelectButton(
    BuildContext context,
    bool isSelected,
    LyAssetEntity asset,
  ) {
    if (!isSelected && selectedMaximumAssets) {
      return const SizedBox.shrink();
    }
    return Padding(
      padding: const EdgeInsetsDirectional.only(end: 10.0),
      child: GestureDetector(
        behavior: HitTestBehavior.opaque,
        onTap: () => onChangingSelected(context, asset, isSelected),
        child: AnimatedContainer(
          duration: kThemeAnimationDuration,
          width: 28.0,
          decoration: BoxDecoration(
            border: !isSelected ? Border.all(color: themeData.iconTheme.color!) : null,
            color: isSelected ? themeData.colorScheme.secondary : null,
            shape: BoxShape.circle,
          ),
          child: const Center(child: Icon(Icons.check, size: 20.0)),
        ),
      ),
    );
  }

  /// Select button for Android.
  /// 安卓系统的选择按钮
  Widget _androidSelectButton(
    BuildContext context,
    bool isSelected,
    LyAssetEntity asset,
  ) {
    var package = packageName == "" ? null : packageName ?? Config.packageName;
    Logger.d('_androidSelectButton==packageName=$packageName');
    return GestureDetector(
      onTap: () {
        onChangingSelected(context, asset, isSelected);
        final double offset = selectedCount * (bottomPreviewHeight - 8 * 3) - MediaQuery.of(context).size.width / 4;
        previewingListController.animateTo(
          math.max(0, offset),
          curve: Curves.ease,
          duration: kThemeChangeDuration,
        );
      },
      child: Container(
        color: Colors.black,
        padding: const EdgeInsets.symmetric(horizontal: 16),
        child: isSelected
            ? Image(
                image: AssetImage('assets/images/public/icon_radio_1.png', package: package),
                width: 24.w,
                height: 24.w,
              )
            : Image(
                image: AssetImage('assets/images/public/icon_radio_f0.png', package: package),
                width: 24.w,
                height: 24.w,
              ),
      ),
    );
  }

  /// 下载icloud图片到本地
  Widget _downloadLocalButton(
    BuildContext context,
    LyAssetEntity asset,
  ) {
    return GestureDetector(
      onTap: () {
        _downloadIcloud();
      },
      child: Container(
        padding: const EdgeInsets.symmetric(horizontal: 16),
        child: Image(
          image: AssetImage('assets/images/icon_icloud.png', package: Config.packageName),
          width: 32.w,
          height: 32.w,
        ),
      ),
    );
  }

  _downloadIcloud() async {
    PMProgressHandler _progressHandler = PMProgressHandler();
    File? file = await currentAsset.loadFile(
      isOrigin: previewThumbnailSize == null,
      withSubtype: true,
      progressHandler: _progressHandler,
    );
    Logger.d('Produced file: $file.');
    if (file != null) {
      changeLocalState();
    }
  }

  changeLocalState() async {
    /// 更新列表页icloud遮盖
    lyBus.emit(eventName: Constants.refreshIcloudById, data: {'id': currentAsset.id});

    // Logger.d(
    //     'hashCode前=${widget.assetPickerProvider.notLocallyAvailables.hashCode}');
    var notLocallyAvailableList = List<String>.from(provider!.notLocallyAvailables);
    var index = notLocallyAvailableList.indexOf(currentAsset.id);

    // Logger.d('hashCode后=${notLocallyAvailableList.hashCode}');
    // Logger.d('id=${currentAsset.id}');
    // Logger.d('1111=$index');
    // Logger.d('xprogress=${s.progress}');

    if (index != -1) {
      notLocallyAvailableList.removeAt(index);
      // Logger.d('当前=$notLocallyAvailableList');
      selectorProvider!.notLocallyAvailables = notLocallyAvailableList;
      provider?.notLocallyAvailables = notLocallyAvailableList;

      /// 更新预览页去下载icloud视频到本地
      lyBus.emit(eventName: Constants.refreshVideoAfterDownload, data: {'id': currentAsset.id});

      var cacheAvailableList = lySp.getList(key: SK_LOCALLY_AVAILABLE_CACHE);
      if (!cacheAvailableList.contains(currentAsset.id)) {
        cacheAvailableList.add(currentAsset.id);
        lySp.setList(key: SK_LOCALLY_AVAILABLE_CACHE, value: cacheAvailableList);
      }

      // Logger.d(
      //     'move 前=${widget.assetPickerProvider.notLocallyAvailables}');

      // Logger.d('赋值=${widget.assetPickerProvider.notLocallyAvailables}');
    }
  }

  @override
  Widget selectButton(BuildContext context) {
    return Row(
      children: <Widget>[
        StreamBuilder<int>(
          initialData: currentIndex,
          stream: pageStreamController.stream,
          builder: (BuildContext _, AsyncSnapshot<int> snapshot) {
            return ChangeNotifierProvider<AssetPickerViewerProvider<LyAssetEntity>>.value(
              value: provider!,
              child: Selector<AssetPickerViewerProvider<LyAssetEntity>, List<String>>(
                  selector: (_, AssetPickerViewerProvider<LyAssetEntity> p) => p.notLocallyAvailables,
                  builder: (_, List<String> notLocallyAvailables, ___) {
                    return Selector<AssetPickerViewerProvider<LyAssetEntity>, List<LyAssetEntity>>(
                      selector: (
                        _,
                        AssetPickerViewerProvider<LyAssetEntity> p,
                      ) =>
                          p.currentlySelectedAssets,
                      builder: (
                        BuildContext c,
                        List<LyAssetEntity> currentlySelectedAssets,
                        __,
                      ) {
                        final LyAssetEntity asset = previewAssets.elementAt(
                          snapshot.data!,
                        );
                        // final List<String> invalidFormats =
                        // context.select<LyAssetPickerProvider, List<String>>(
                        //       (LyAssetPickerProvider p) => p.invalidFormats,
                        // );

                        final bool isInvalid = selectorProvider!.invalidFormats.contains(
                          asset.id,
                        );

                        if (isInvalid || (asset.type == AssetType.video && asset.duration > (maxMinute! * 60))) {
                          return const SizedBox.shrink();
                        }
                        final bool notLocallyAvailable = notLocallyAvailables.contains(
                          asset.id,
                        );
                        if (notLocallyAvailable) {
                          return _downloadLocalButton(context, asset);
                        }
                        final bool isSelected = currentlySelectedAssets.contains(
                          asset,
                        );
                        // if (isAppleOS) {
                        //   return _appleOSSelectButton(c, isSelected, asset);
                        // }
                        return _androidSelectButton(c, isSelected, asset);
                      },
                    );
                  }),
            );
          },
        ),
        // ScaleText(
        //   Constants.textDelegate.select,
        //   style: const TextStyle(fontSize: 17, height: 1),
        // ),
      ],
    );
  }

  @override
  Widget build(BuildContext context) {
    return WillPopScope(
      onWillPop: syncSelectedAssetsWhenPop,
      child: Theme(
        data: themeData,
        child: AnnotatedRegion<SystemUiOverlayStyle>(
          value: SystemUiOverlayStyle.light,
          child: Material(
            color: Colors.black,
            child: Stack(
              children: <Widget>[
                Positioned.fill(
                  child: ExtendedImageGesturePageView.builder(
                    physics: previewAssets.length == 1 ? const CustomClampingScrollPhysics() : const CustomBouncingScrollPhysics(),
                    controller: pageController,
                    itemCount: previewAssets.length,
                    itemBuilder: assetPageBuilder,
                    reverse: shouldReversePreview,
                    // preloadPagesCount: 1,
                    onPageChanged: (int index) {
                      currentIndex = index;
                      pageStreamController.add(index);

                      /// 底部缩略图滚动到指定位置
                      if ((selectedAssets ?? []).isNotEmpty) {
                        var curPageAsset = previewAssets[index];
                        var curSelectedIndex = selectedAssets!.indexWhere((element) => element.id == curPageAsset.id);

                        /// 同步跳到指定的缩略图
                        // schoolToThumbnail(context, index, 8);
                        var scrollIndex = curSelectedIndex != -1 ? curSelectedIndex : (selectedAssets!.length - 1);
                        if (_keys[scrollIndex].currentContext != null) {
                          Scrollable.ensureVisible(_keys[scrollIndex].currentContext!, alignment: 0.5);
                        }
                      }
                    },
                    scrollDirection: Axis.horizontal,
                  ),
                ),
                ...<Widget>[
                  appBar(context),
                  if (selectedAssets != null || (isWeChatMoment && hasVideo && isAppleOS)) bottomDetailBuilder(context),
                ],
              ],
            ),
          ),
        ),
      ),
    );
  }
}
