///
/// [Author] Alex (https://github.com/Alex525)
/// [Date] 2020/3/31 15:37
///
import 'package:flutter/material.dart';

/// [ChangeNotifier] for assets picker viewer.
/// 资源选择查看器的 provider model.
class LyAssetPickerViewerProvider<A> extends ChangeNotifier {
  /// Copy selected assets for editing when constructing.
  /// 构造时深拷贝已选择的资源集合，用于后续编辑。
  LyAssetPickerViewerProvider(List<A>? assets) {
    _currentlySelectedAssets = List<A>.from(assets ?? <A>[]);
  }

  /// 本地无效的资源
  List<String> _notLocallyAvailables = <String>[];

  List<String> get notLocallyAvailables => _notLocallyAvailables;

  set notLocallyAvailables(List<String> value) {
    // Logger.d('sorted value*$value');
    // Logger.d('sorted _invalidFormats*$_notLocallyAvailables');
    // Logger.d('value == _notLocallyAvailables*${value == _notLocallyAvailables}');
    if (value == _notLocallyAvailables) {
      return;
    }
    _notLocallyAvailables = List<String>.from(value);
    // Logger.d('sorted invalidFormats*$_invalidFormats');
    notifyListeners();
  }

  /// Selected assets in the viewer.
  /// 查看器中已选择的资源
  late List<A> _currentlySelectedAssets;

  List<A> get currentlySelectedAssets => _currentlySelectedAssets;

  set currentlySelectedAssets(List<A> value) {
    if (value == _currentlySelectedAssets) {
      return;
    }
    _currentlySelectedAssets = value;
    notifyListeners();
  }

  /// 不支持的格式
  List<String> _invalidFormats = <String>[];

  List<String> get invalidFormats => _invalidFormats;

  set invalidFormats(List<String> value) {
    if (value == _invalidFormats) {
      return;
    }
    _invalidFormats = List<String>.from(value);
    // Logger.d('preview invalidFormats*$_invalidFormats');
    notifyListeners();
  }

  /// 选中资源是否为空
  bool get isSelectedNotEmpty => currentlySelectedAssets.isNotEmpty;

  /// Select asset.
  /// 选中资源
  void selectAssetEntity(A entity) {
    final List<A> set = List<A>.from(currentlySelectedAssets);
    set.add(entity);
    currentlySelectedAssets = List<A>.from(set);
  }

  /// Un-select asset.
  /// 取消选中资源
  void unSelectAssetEntity(A entity) {
    final List<A> set = List<A>.from(currentlySelectedAssets);
    set.remove(entity);
    currentlySelectedAssets = List<A>.from(set);
  }
}
