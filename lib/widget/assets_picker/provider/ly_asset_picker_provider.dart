import 'dart:async';
import 'dart:typed_data';

import 'package:flutter/material.dart';
import 'package:zyocore/widget/assets_picker/constants/constants.dart';
import 'package:zyocore/widget/assets_picker/delegates/sort_path_delegate.dart';
import 'package:zyocore/widget/assets_picker/entity/ly_asset_entity.dart';
import 'package:zyocore/widget/assets_picker/entity/path_wrapper.dart';
import 'package:zyocore/widget/assets_picker/ly_assets_picker.dart';
import 'package:zyocore/widget/assets_picker/provider/asset_picker_provider.dart';

import '../../../zyocore.dart';

///对应DefaultAssetPickerProvider
class LyAssetPickerProvider extends AssetPickerProvider<LyAssetEntity, AssetPathEntity> {
  /// Call [getAssetList] with route duration when constructing.
  /// 构造时根据路由时长延时获取资源
  LyAssetPickerProvider({
    List<LyAssetEntity>? selectedAssets,
    this.requestType = RequestType.image,
    this.sortPathDelegate = SortPathDelegate.common,
    this.filterOptions,
    int maxAssets = 9,
    int pageSize = 60,
    int maxMinute = 5,
    ThumbnailSize pathThumbnailSize = defaultPathThumbnailSize,
    Duration initializeDelayDuration = const Duration(milliseconds: 250),
    this.sortPathsByModifiedDate = false,
    this.cloudCheckApi,
    this.classId,
    List<String>? allowTypes,
  }) : super(
            maxAssets: maxAssets,
            pageSize: pageSize,
            pathThumbnailSize: pathThumbnailSize,
            selectedAssets: selectedAssets,
            allowTypes: allowTypes,
            maxMinute: maxMinute) {
    Future<void>.delayed(initializeDelayDuration, () async {
      await getPaths();
      await getAssetsFromCurrentPath();
    });
  }

  /// 会触发两次
  bool isSubmitting = false;

  /// Request assets type.
  /// 请求的资源类型
  final RequestType requestType;

  /// Delegate to sort asset path entities.
  /// 资源路径排序的实现
  final SortPathDelegate<AssetPathEntity>? sortPathDelegate;

  /// Filter options for the picker.
  /// 选择器的筛选条件
  ///
  /// Will be merged into the base configuration.
  /// 将会与基础条件进行合并。
  final PMFilter? filterOptions;

  /// 云端标识检测
  final CloudCheckApi? cloudCheckApi;

  /// 班级id
  final String? classId;

  /// 当前loadMore的索引，防止重复触发
  int loadMoreIndex = 0;

  /// {@macro wechat_assets_picker.constants.AssetPickerConfig.sortPathsByModifiedDate}
  final bool sortPathsByModifiedDate;

  /// Clear all fields when dispose.
  /// 销毁时重置所有内容
  @override
  void dispose() {
    super.dispose();
  }

  @override
  Future<void> getPaths() async {
    final PMFilter options;
    final PMFilter? fog = filterOptions;
    if (fog is FilterOptionGroup?) {
      // Initial base options.
      // Enable need title for audios and image to get proper display.
      final FilterOptionGroup newOptions = FilterOptionGroup(
        imageOption: const FilterOption(
          needTitle: true,
          // sizeConstraint: SizeConstraint(ignoreSize: true),
        ),
        videoOption: const FilterOption(
          needTitle: true,
          // sizeConstraint: SizeConstraint(ignoreSize: true),
        ),
        // containsLivePhotos: false,
        containsPathModified: sortPathsByModifiedDate,
        createTimeCond: DateTimeCond.def().copyWith(ignore: true),
        updateTimeCond: DateTimeCond.def().copyWith(ignore: true),
      );
      // Merge user's filter options into base options if it's not null.
      if (fog != null) {
        newOptions.merge(fog);
      }
      // Logger.d('options**********new}');
      options = newOptions;
    } else {
      options = fog;
    }
    // Logger.d('options**********${options.toMap()}');
    final List<AssetPathEntity> list = await PhotoManager.getAssetPathList(
      type: requestType,
      filterOption: options,
    );
    // Logger.d('path**********${list.length}');
    // for (var i = 0; i < list.length; i++) {
    //   var element = list[i];
    //   // Logger.d('path element=${element.toString()}');
    // }

    List<PathWrapper<AssetPathEntity>> tempPaths = list.map((AssetPathEntity p) => PathWrapper<AssetPathEntity>(path: p)).toList();

    // Sort path using sort path delegate.
    Constants.sortPathDelegate.sort(tempPaths);
    tempPaths
      ..forEach(getAssetCountFromPath)
      ..forEach(getThumbnailFromPath);

    /// 赋值paths
    paths = tempPaths;

    // Set first path entity as current path entity.
    if (tempPaths.isNotEmpty) {
      currentPath ??= tempPaths.first;
    }
    // Logger.d(
    //     'getPaths+currentPath****************${currentPath?.toString()}');
  }

  // /// Get assets list from current path entity.
  // /// 从当前已选路径获取资源列表
  // Future<void> getAssetList() async {
  //   if (pathEntityList.isNotEmpty) {
  //     currentPathEntity = pathEntityList.keys.elementAt(0);
  //     totalAssetsCount = currentPathEntity!.assetCount;
  //     if (isSubmitting) return;
  //     isSubmitting = true;
  //     await getAssetsFromEntity(0, currentPathEntity!);
  //     // Logger.d(
  //     //     'LySortedAssetPickerProvider>>>>>>>>>>>>>>>>>>>>>>totalAssetsCount=$totalAssetsCount');
  //     isSubmitting = false;
  //     // Update total assets count.
  //   } else {
  //     isAssetsEmpty = true;
  //   }
  // }

  /// Get assets list from current path entity.
  /// 从当前已选路径获取资源列表--->getAssetList
  Future<void> getAssetsFromCurrentPath() async {
    if (currentPath == null || paths.isEmpty) {
      isAssetsEmpty = true;
      return;
    }
    final PathWrapper<AssetPathEntity> wrapper = currentPath!;
    final int assetCount = wrapper.assetCount ?? await wrapper.path.assetCountAsync;
    totalAssetsCount = assetCount;
    Logger.d('totalAssetsCount****************$totalAssetsCount');
    isAssetsEmpty = assetCount == 0;
    if (wrapper.assetCount == null) {
      currentPath = currentPath!.copyWith(assetCount: assetCount);
    }
    if (isSubmitting) return;
    isSubmitting = true;
    await getAssetsFromPath(0, currentPath!.path);
    isSubmitting = false;
  }

  Completer<void>? _getAssetsFromPathCompleter;

  @override
  Future<void> getAssetsFromPath([int? page, AssetPathEntity? path]) async {
    Future<void> run() async {
      final int currentPage = page ?? currentAssetsListPage;
      final AssetPathEntity currentPath = path ?? this.currentPath!.path;
      final List<AssetEntity> list = await currentPath.getAssetListPaged(
        page: currentPage,
        size: pageSize,
      );
      // var pageIndex = page;
      // Logger.d('当前是第几页=$pageIndex');
      // for (var i = 0; i < list.length; i++) {
      //   var element = list[i];
      //   Logger.d('当前第$page页第$i/30张');
      //   Logger.d('asset id=${element.id}');
      //   Logger.d('asset type=${element.type}');
      //   Logger.d('isLocallyAvailable=${await element.isLocallyAvailable()}');
      //   Logger.d('asset file path=${(await element.file)?.path}');
      //   Logger.d('asset fileWithSubtype path=${(await element.fileWithSubtype)?.path}');
      //   Logger.d('asset originFile path=${(await element.originFile)?.path}');
      //   Logger.d('asset originFileWithSubtype path=${(await element.originFileWithSubtype)?.path}');
      //   Logger.d('asset title=${element.title}');
      //   Logger.d('asset mimeType=${element.mimeType}');
      //   Logger.d('asset width=${element.width}');
      //   Logger.d('asset height=${element.height}');
      // }

      /// 过滤部分资源的格式
      List<AssetEntity> localValidAssets = await AssetUtil.filterAssetsList(
          assets: list,
          allowTypes: allowTypes ?? [],
          maxMinute: maxMinute,
          onInvalidSave: (data) {
            // Logger.d('onInvalidSave=data=========$data');
            if (page != null && page == 0 && invalidFormats.isNotEmpty) {
              invalidFormats.clear();
            }
            var invalidList = invalidFormats;
            invalidList.addAll(data);
            invalidFormats = invalidList;
            // Logger.d('invalidFormats=final======$invalidFormats');
          },
          onNotLocallyAvailableSave: (data) async {
            // Logger.d('onInvalidSave=data=================$data');
            if (page != null && page == 0 && notLocallyAvailables.isNotEmpty) {
              notLocallyAvailables.clear();
            }
            var notLocallyAvailableList = notLocallyAvailables;
            notLocallyAvailableList.addAll(data);
            notLocallyAvailables = notLocallyAvailableList;
            // Logger.d('notLocallyAvailables=final======$notLocallyAvailables');
          });

      if (currentPage == 0) {
        currentAssets.clear();
      }
      currentAssets.addAll(list.map((e) => LyAssetEntity.fromAsset(e)).toList());
      hasAssetsToDisplay = currentAssets.isNotEmpty;
      notifyListeners();
      // Logger.d('当前图片总数total=${currentAssets.length}');
      // Logger.d('hasMoreToLoad========$hasMoreToLoad');
      /// 异步去检查云端标记
      checkCloudTag(targetList: localValidAssets, loadMore: false);
    }

    if (_getAssetsFromPathCompleter == null) {
      _getAssetsFromPathCompleter = Completer<void>();
      run().then((_) {
        _getAssetsFromPathCompleter!.complete();
      }).catchError((Object e, StackTrace s) {
        _getAssetsFromPathCompleter!.completeError(e, s);
      }).whenComplete(() {
        _getAssetsFromPathCompleter = null;
      });
    }
    return _getAssetsFromPathCompleter!.future;
  }

  Completer<void>? _getMoreAssetsFromPathCompleter;

  @override
  Future<void> loadMoreAssets() async {
    /// 防止重复触发
    if (loadMoreIndex == currentAssetsListPage) return;
    loadMoreIndex = currentAssetsListPage;
    // Logger.d('hasMoreToLoad****************$hasMoreToLoad');
    // Logger.d('currentAssetsListPage****************$currentAssetsListPage');
    var lastPath = currentPath;
    Future<void> run() async {
      final int currentPage = currentAssetsListPage;
      final AssetPathEntity currentPath = this.currentPath!.path;
      final List<AssetEntity> list = await currentPath.getAssetListPaged(
        page: currentPage,
        size: pageSize,
      );
      // var pageIndex = currentAssetsListPage;
      // Logger.d('当前是第几页=$pageIndex');
      // for (var i = 0; i < list.length; i++) {
      //   var element = list[i];
      //   Logger.d('当前第$currentPage页第$i/30张');
      //   Logger.d('asset id=${element.id}');
      //   Logger.d('isLocallyAvailable=${await element.isLocallyAvailable()}');
      //   Logger.d('asset path=${(await element.file)?.path}');
      //   Logger.d('asset title=${element.title}');
      //   Logger.d('asset mimeType=${element.mimeType}');
      //   Logger.d('asset width=${element.width}');
      //   Logger.d('asset height=${element.height}');
      // }

      /// 过滤部分资源的格式
      List<AssetEntity> localValidAssets = await AssetUtil.filterAssetsList(
          assets: list,
          allowTypes: allowTypes ?? [],
          maxMinute: maxMinute,
          onInvalidSave: (data) {
            // Logger.d('onInvalidSave=data=====$data');
            var invalidList = invalidFormats;
            invalidList.addAll(data);
            invalidFormats = invalidList;
            // Logger.d('invalidFormats=final======$invalidFormats');
          },
          onNotLocallyAvailableSave: (data) async {
            // Logger.d('onInvalidSave=data=====$data');
            var notLocallyAvailableList = notLocallyAvailables;
            notLocallyAvailableList.addAll(data);
            notLocallyAvailables = notLocallyAvailableList;
            // Logger.d('notLocallyAvailables=final======$notLocallyAvailables');
          });
      List<LyAssetEntity> tempAssetList = <LyAssetEntity>[];

      /// 切换路径时，第二页的数据可能加载在切换路径后的页面中，这里需要去除
      tempAssetList.addAll(lastPath != this.currentPath ? [] : currentAssets);
      tempAssetList.addAll(list.map((e) => LyAssetEntity.fromAsset(e)).toList());
      // Logger.d('tempAssetList=l======${tempAssetList.length}');
      // Logger.d('currentAssets=l======${currentAssets.length}');
      currentAssets = tempAssetList;
      hasAssetsToDisplay = currentAssets.isNotEmpty;

      notifyListeners();
      // notifyListeners();
      // notifyListeners();
      // Logger.d('当前图片总数total=${currentAssets.length}');
      // Logger.d('hasMoreToLoad========$hasMoreToLoad');
      /// 异步去检查云端标记
      checkCloudTag(targetList: localValidAssets, loadMore: true);
    }

    if (_getMoreAssetsFromPathCompleter == null) {
      _getMoreAssetsFromPathCompleter = Completer<void>();
      run().then((_) {
        _getMoreAssetsFromPathCompleter!.complete();
      }).catchError((Object e, StackTrace s) {
        _getMoreAssetsFromPathCompleter!.completeError(e, s);
      }).whenComplete(() {
        _getMoreAssetsFromPathCompleter = null;
      });
    }
    return _getMoreAssetsFromPathCompleter!.future;
  }

  @override
  Future<void> switchPath([PathWrapper<AssetPathEntity>? path]) async {
    assert(
      () {
        if (path == null && currentPath == null) {
          throw FlutterError.fromParts(<DiagnosticsNode>[
            ErrorSummary('Switching empty path.'),
            ErrorDescription(
              'Neither "path" nor "currentPathEntity" is non-null, '
              'which makes this method useless.',
            ),
            ErrorHint(
              'You need to pass a non-null path or call this method '
              'when the "currentPath" is not null.',
            ),
          ]);
        }
        return true;
      }(),
    );
    if (path == null && currentPath == null) {
      return;
    }

    path ??= currentPath!;
    currentPath = path;
    isSwitchingPath = false;
    // currentPathEntity = pathEntity;
    // totalAssetsCount = pathEntity.assetCount;
    /// 切换路径后，清除上次数据
    invalidFormats.clear();
    notLocallyAvailables.clear();
    notifyListeners();
    loadMoreIndex = 0;
    await getAssetsFromCurrentPath();
    // await getAssetsFromEntity(0, currentPathEntity!);
  }

  @override
  Future<Uint8List?> getThumbnailFromPath(
    PathWrapper<AssetPathEntity> path,
  ) async {
    if (requestType == RequestType.audio) {
      return null;
    }
    final int assetCount = path.assetCount ?? await path.path.assetCountAsync;
    if (assetCount == 0) {
      return null;
    }
    final List<AssetEntity> assets = await path.path.getAssetListRange(
      start: 0,
      end: 1,
    );
    if (assets.isEmpty) {
      return null;
    }
    final AssetEntity asset = assets.single;
    // Obtain the thumbnail only when the asset is image or video.
    if (asset.type != AssetType.image && asset.type != AssetType.video) {
      return null;
    }
    final Uint8List? data = await asset.thumbnailDataWithSize(
      pathThumbnailSize,
    );
    final int index = paths.indexWhere(
      (PathWrapper<AssetPathEntity> p) => p.path == path.path,
    );
    if (index != -1) {
      paths[index] = paths[index].copyWith(thumbnailData: data);
      notifyListeners();
    }
    return data;
  }

  Future<void> getAssetCountFromPath(PathWrapper<AssetPathEntity> path) async {
    final int assetCount = await path.path.assetCountAsync;
    final int index = paths.indexWhere(
      (PathWrapper<AssetPathEntity> p) => p == path,
    );
    if (index != -1) {
      paths[index] = paths[index].copyWith(assetCount: assetCount);
      if (index == 0) {
        currentPath = currentPath?.copyWith(assetCount: assetCount);
      }
      // Logger.d('getAssetCountFromPath+count*${currentPath?.assetCount}');
      notifyListeners();
    }
  }

  /// 检测云端标记
  checkCloudTag({List<AssetEntity>? targetList, bool loadMore = false}) async {
    // Logger.d('checkCloudTag+loadMore*$loadMore');
    if (cloudCheckApi == null || (targetList ?? []).isEmpty) return;

    List<String> targetHashList = [];
    for (var asset in targetList!) {
      targetHashList.add(asset.id);
    }

    /// 暂时未用上
    // currentAssets = tempCurrentAssets;
    // Logger.d('checkCloudTag*targetHashList**$targetHashList');
    var data = await cloudCheckApi!(classId: classId, hashId: targetHashList) ?? [];
    // Logger.d('checkCloudTag*data********$data');
    if (data.isEmpty) return;
    final List<String> tempList = [];
    tempList.addAll(currentUploaded);
    for (var hashId in data) {
      tempList.add(hashId);
    }
    // Logger.d('checkCloudTag+tempList****************$tempList');

    /// 更新列表
    currentUploaded = tempList;
  }
}
