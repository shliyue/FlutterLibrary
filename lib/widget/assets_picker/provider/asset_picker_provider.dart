///
/// [Author] Alex (https://github.com/Alex525)
/// [Date] 2020/3/31 15:28
///
import 'dart:async';
import 'dart:math' as math;
import 'dart:typed_data';

import 'package:flutter/material.dart';
import 'package:photo_manager/photo_manager.dart';
import 'package:zyocore/util/asset_util.dart';
import 'package:zyocore/util/toast_util.dart';
import 'package:zyocore/widget/assets_picker/delegates/sort_path_delegate.dart';
import 'package:zyocore/widget/assets_picker/entity/ly_asset_entity.dart';
import 'package:zyocore/widget/assets_picker/entity/path_wrapper.dart';

import '../constants/constants.dart';
import '../ly_assets_picker.dart';

/// [ChangeNotifier] for assets picker.
///
/// The provider maintain all methods that control assets and paths.
/// By extepnding it you can customize how you can get all assets or paths,
/// how to fetch the next page of assets, how to get the thumb data of a path.
abstract class AssetPickerProvider<Asset, Path> extends ChangeNotifier {
  AssetPickerProvider({
    this.maxAssets = 9,
    this.pageSize = 320,
    this.pathThumbnailSize = defaultPathThumbnailSize,
    List<Asset>? selectedAssets,
    this.allowTypes,
    this.maxMinute = 5,
  }) {
    if (selectedAssets != null && selectedAssets.isNotEmpty) {
      _selectedAssets = List<Asset>.from(selectedAssets);
    }

    /// 本次可以选择的数量
    maxToSelect = maxAssets - (selectedAssets ?? []).length;
  }

  /// 本次可以选择的最大数量
  int maxToSelect = 0;

  /// Maximum count for asset selection.
  /// 资源选择的最大数量
  final int maxAssets;

  /// Assets should be loaded per page.
  /// 资源选择的最大数量
  ///
  /// Use `null` to display all assets into a single grid.
  final int pageSize;

  // Thumbnail size for path selector.
  /// 路径选择器中缩略图的大小
  final ThumbnailSize pathThumbnailSize;

  /// 允许类型
  final List<String>? allowTypes;

  ///视频最大时间(分钟)
  final int maxMinute;

  /// Clear all fields when dispose.
  /// 销毁时重置所有内容
  @override
  void dispose() {
    _isAssetsEmpty = false;
    _isSwitchingPath = false;
    _paths.clear();
    _currentPath = null;
    _currentAssets.clear();
    _selectedAssets.clear();
    super.dispose();
  }

  /// Whether there are assets on the devices.
  /// 设备上是否有资源文件
  bool _isAssetsEmpty = false;

  bool get isAssetsEmpty => _isAssetsEmpty;

  set isAssetsEmpty(bool value) {
    if (value == _isAssetsEmpty) {
      return;
    }
    _isAssetsEmpty = value;
    notifyListeners();
  }

  /// Whether there are any assets can be displayed.
  /// 是否有资源可供显示
  bool _hasAssetsToDisplay = false;

  bool get hasAssetsToDisplay => _hasAssetsToDisplay;

  set hasAssetsToDisplay(bool value) {
    if (value == _hasAssetsToDisplay) {
      return;
    }
    _hasAssetsToDisplay = value;
    notifyListeners();
  }

  /// Whether more assets are waiting for a load.
  /// 是否还有更多资源可以加载
  bool get hasMoreToLoad => _currentAssets.length < _totalAssetsCount!;

  /// The current page for assets list.
  /// 当前加载的资源列表分页数
  int get currentAssetsListPage => (math.max(1, _currentAssets.length) / pageSize).ceil();

  /// Total count for assets.
  /// 资源总数
  int? _totalAssetsCount;

  int? get totalAssetsCount => _totalAssetsCount;

  set totalAssetsCount(int? value) {
    if (value == _totalAssetsCount) {
      return;
    }
    _totalAssetsCount = value;
    notifyListeners();
  }

  /// If path switcher opened.
  /// 是否正在进行路径选择
  bool _isSwitchingPath = false;

  bool get isSwitchingPath => _isSwitchingPath;

  set isSwitchingPath(bool value) {
    if (value == _isSwitchingPath) {
      return;
    }
    _isSwitchingPath = value;
    notifyListeners();
  }

  /// Map for all path entity.
  /// 所有包含资源的路径里列表
  ///
  /// Using [Map] in order to save the thumbnail data
  /// for the first asset under the path.
  /// 使用 [Map] 来保存路径下第一个资源的缩略图数据
  List<PathWrapper<Path>> get paths => _paths;
  List<PathWrapper<Path>> _paths = <PathWrapper<Path>>[];

  set paths(List<PathWrapper<Path>> value) {
    if (value == _paths) {
      return;
    }
    _paths = value;
    notifyListeners();
  }

  /// Set thumbnail [data] for the specific [path].
  /// 为指定的路径设置缩略图数据
  void setPathThumbnail(Path path, Uint8List? data) {
    final int index = _paths.indexWhere(
      (PathWrapper<Path> w) => w.path == path,
    );
    if (index != -1) {
      final PathWrapper<Path> newWrapper = _paths[index].copyWith(
        thumbnailData: data,
      );
      _paths[index] = newWrapper;
      notifyListeners();
    }
  }

  /// How many path has a valid thumb data.
  /// 当前有多少目录已经正常载入了缩略图
  ///
  /// This getter provides a "Should Rebuild" condition judgement to [Selector]
  /// with the path entities widget.
  /// 它为目录部件展示部分的 [Selector] 提供了是否重建的条件。
  int get validPathThumbCount => _paths.where((PathWrapper? d) => d != null).length;

  /// The path which is currently using.
  /// 正在查看的资源路径
  PathWrapper<Path>? _currentPath;

  PathWrapper<Path>? get currentPath => _currentPath;

  set currentPath(PathWrapper<Path>? value) {
    if (value == _currentPath) {
      return;
    }
    _currentPath = value;
    notifyListeners();
  }

  /// Assets under current path entity.
  /// 正在查看的资源路径下的所有资源
  List<Asset> _currentAssets = <Asset>[];

  List<Asset> get currentAssets => _currentAssets;

  set currentAssets(List<Asset> value) {
    if (value == _currentAssets) {
      return;
    }
    _currentAssets = List<Asset>.from(value);
    notifyListeners();
  }

  /// 已经上传到云端的asset资源的hashId
  List<String> _currentUploaded = <String>[];

  List<String> get currentUploaded => _currentUploaded;

  set currentUploaded(List<String> value) {
    if (value == _currentUploaded) {
      return;
    }
    _currentUploaded = List<String>.from(value);
    notifyListeners();
  }

  /// 分组标题栏的全选的时间（该组数据已全部修改为可以选中的状态）
  List<String> _titleSelected = <String>[];

  List<String> get titleSelected => _titleSelected;

  set titleSelected(List<String> value) {
    if (value == _titleSelected) {
      return;
    }
    _titleSelected = List<String>.from(value);
    notifyListeners();
  }

  /// 不支持的格式
  List<String> _invalidFormats = <String>[];

  List<String> get invalidFormats => _invalidFormats;

  set invalidFormats(List<String> value) {
    if (value == _invalidFormats) {
      return;
    }
    _invalidFormats = List<String>.from(value);
    notifyListeners();
  }

  /// 本地无效的资源
  List<String> _notLocallyAvailables = <String>[];

  List<String> get notLocallyAvailables => _notLocallyAvailables;

  set notLocallyAvailables(List<String> value) {
    if (value == _notLocallyAvailables) {
      return;
    }
    _notLocallyAvailables = List<String>.from(value);
    notifyListeners();
  }

  /// Selected assets.
  /// 已选中的资源
  List<Asset> _selectedAssets = <Asset>[];

  List<Asset> get selectedAssets => _selectedAssets;

  set selectedAssets(List<Asset> value) {
    if (value == _selectedAssets) {
      return;
    }
    _selectedAssets = List<Asset>.from(value);
    notifyListeners();
  }

  /// Descriptions for selected assets currently.
  /// 当前已被选中的资源的描述
  ///
  /// This getter provides a "Should Rebuild" condition judgement to [Selector]
  /// with the preview widget's selective part.
  /// 它为预览部件的选中部分的 [Selector] 提供了是否重建的条件。
  String get selectedDescriptions => _selectedAssets.fold(
        <String>[],
        (List<String> list, Asset a) => list..add(a.toString()),
      ).join();

  /// 选中资源是否为空
  bool get isSelectedNotEmpty => selectedAssets.isNotEmpty;

  /// 是否已经选择了最大数量的资源
  bool get selectedMaximumAssets => selectedAssets.length == maxAssets;

  /// Get assets path entities.
  /// 获取所有的资源路径
  Future<void> getPaths();

  /// Get thumb data from the first asset under the specific path entity.
  /// 获取指定路径下的第一个资源的缩略数据
  Future<Uint8List?> getThumbnailFromPath(PathWrapper<Path> path);

  /// Switch between paths.
  /// 切换路径
  Future<void> switchPath([PathWrapper<Path>? path]);

  /// Get assets under the specific path entity.
  /// 获取指定路径下的资源
  Future<void> getAssetsFromPath(int page, Path path);

  /// Load more assets.
  /// 加载更多资源
  Future<void> loadMoreAssets();

  /// Select asset.
  /// 选中资源
  void selectAsset(Asset item) {
    if (!selectedAssets.contains(item)) {
      if (selectedAssets.length == maxAssets) {
        ToastUtil.showToast('已超过可选数量，不可进行选择！');
        return;
      }
      final List<Asset> _set = List<Asset>.from(selectedAssets);
      _set.add(item);
      selectedAssets = _set;
    }
  }

  /// Un-select asset.
  /// 取消选中资源
  void unSelectAsset(Asset item) {
    final List<Asset> _set = List<Asset>.from(selectedAssets);
    _set.remove(item);
    selectedAssets = _set;
  }
}

class DefaultAssetPickerProvider extends AssetPickerProvider<LyAssetEntity, AssetPathEntity> {
  /// Call [getAssetList] with route duration when constructing.
  /// 构造时根据路由时长延时获取资源
  DefaultAssetPickerProvider({
    super.selectedAssets,
    super.maxAssets,
    super.pageSize,
    super.pathThumbnailSize,
    super.maxMinute,
    this.requestType = RequestType.image,
    this.sortPathDelegate = SortPathDelegate.common,
    this.sortPathsByModifiedDate = false,
    this.filterOptions,
    Duration initializeDelayDuration = const Duration(milliseconds: 250),
  }) {
    Constants.sortPathDelegate = sortPathDelegate ?? SortPathDelegate.common;
    Future<void>.delayed(initializeDelayDuration, () async {
      await getPaths();
      // await getAssetList();
      await getAssetsFromCurrentPath();
    });
  }

  /// Request assets type.
  /// 请求的资源类型
  final RequestType requestType;

  /// Delegate to sort asset path entities.
  /// 资源路径排序的实现
  final SortPathDelegate<AssetPathEntity>? sortPathDelegate;

  /// {@macro wechat_assets_picker.constants.AssetPickerConfig.sortPathsByModifiedDate}
  final bool sortPathsByModifiedDate;

  /// Filter options for the picker.
  /// 选择器的筛选条件
  ///
  /// Will be merged into the base configuration.
  /// 将会与基础条件进行合并。
  final PMFilter? filterOptions;

  @override
  set currentPath(PathWrapper<AssetPathEntity>? value) {
    if (value == _currentPath) {
      return;
    }
    _currentPath = value;
    if (value != null) {
      final int index = _paths.indexWhere(
        (PathWrapper<AssetPathEntity> p) => p.path.id == value.path.id,
      );
      if (index != -1) {
        _paths[index] = value;
        getThumbnailFromPath(value);
      }
    }
    notifyListeners();
  }

  @override
  Future<void> getPaths() async {
    final PMFilter options;
    final PMFilter? fog = filterOptions;
    if (fog is FilterOptionGroup?) {
      // Initial base options.
      // Enable need title for audios and image to get proper display.
      final FilterOptionGroup newOptions = FilterOptionGroup(
        imageOption: const FilterOption(
          needTitle: true,
          sizeConstraint: SizeConstraint(ignoreSize: true),
        ),
        audioOption: const FilterOption(
          needTitle: true,
          sizeConstraint: SizeConstraint(ignoreSize: true),
        ),
        containsPathModified: sortPathsByModifiedDate,
        createTimeCond: DateTimeCond.def().copyWith(ignore: true),
        updateTimeCond: DateTimeCond.def().copyWith(ignore: true),
      );
      // Merge user's filter options into base options if it's not null.
      if (fog != null) {
        newOptions.merge(fog);
      }
      options = newOptions;
    } else {
      options = fog;
    }

    final List<AssetPathEntity> list = await PhotoManager.getAssetPathList(
      type: requestType,
      filterOption: options,
    );
    _paths = list.map((AssetPathEntity p) => PathWrapper<AssetPathEntity>(path: p)).toList();
    // Sort path using sort path delegate.
    Constants.sortPathDelegate.sort(_paths);
    // Use sync method to avoid unnecessary wait.
    _paths
      ..forEach(getAssetCountFromPath)
      ..forEach(getThumbnailFromPath);

    // Set first path entity as current path entity.
    if (_paths.isNotEmpty) {
      _currentPath ??= _paths.first;
    }
  }

  Completer<void>? _getAssetsFromPathCompleter;

  @override
  Future<void> getAssetsFromPath([int? page, AssetPathEntity? path]) {
    Future<void> run() async {
      final int currentPage = page ?? currentAssetsListPage;
      final AssetPathEntity currentPath = path ?? this.currentPath!.path;
      final List<AssetEntity> list = await currentPath.getAssetListPaged(
        page: currentPage,
        size: pageSize,
      );
      if (currentPage == 0) {
        _currentAssets.clear();
      }
      _currentAssets.addAll(list.map((e) => LyAssetEntity.fromAsset(e)).toList());
      _hasAssetsToDisplay = _currentAssets.isNotEmpty;
      notifyListeners();
    }

    if (_getAssetsFromPathCompleter == null) {
      _getAssetsFromPathCompleter = Completer<void>();
      run().then((_) {
        _getAssetsFromPathCompleter!.complete();
      }).catchError((Object e, StackTrace s) {
        _getAssetsFromPathCompleter!.completeError(e, s);
      }).whenComplete(() {
        _getAssetsFromPathCompleter = null;
      });
    }
    return _getAssetsFromPathCompleter!.future;
  }

  @override
  Future<void> loadMoreAssets() => getAssetsFromPath();

  @override
  Future<void> switchPath([PathWrapper<AssetPathEntity>? path]) async {
    assert(
      () {
        if (path == null && _currentPath == null) {
          throw FlutterError.fromParts(<DiagnosticsNode>[
            ErrorSummary('Switching empty path.'),
            ErrorDescription(
              'Neither "path" nor "currentPathEntity" is non-null, '
              'which makes this method useless.',
            ),
            ErrorHint(
              'You need to pass a non-null path or call this method '
              'when the "currentPath" is not null.',
            ),
          ]);
        }
        return true;
      }(),
    );
    if (path == null && _currentPath == null) {
      return;
    }
    path ??= _currentPath!;
    _currentPath = path;
    await getAssetsFromCurrentPath();
  }

  @override
  Future<Uint8List?> getThumbnailFromPath(
    PathWrapper<AssetPathEntity> path,
  ) async {
    if (requestType == RequestType.audio) {
      return null;
    }
    final int assetCount = path.assetCount ?? await path.path.assetCountAsync;
    if (assetCount == 0) {
      return null;
    }
    final List<AssetEntity> assets = await path.path.getAssetListRange(
      start: 0,
      end: 1,
    );
    if (assets.isEmpty) {
      return null;
    }
    final AssetEntity asset = assets.single;
    // Obtain the thumbnail only when the asset is image or video.
    if (asset.type != AssetType.image && asset.type != AssetType.video) {
      return null;
    }
    final Uint8List? data = await asset.thumbnailDataWithSize(
      pathThumbnailSize,
    );
    final int index = _paths.indexWhere(
      (PathWrapper<AssetPathEntity> p) => p.path == path.path,
    );
    if (index != -1) {
      _paths[index] = _paths[index].copyWith(thumbnailData: data);
      notifyListeners();
    }
    return data;
  }

  Future<void> getAssetCountFromPath(PathWrapper<AssetPathEntity> path) async {
    final int assetCount = await path.path.assetCountAsync;
    final int index = _paths.indexWhere(
      (PathWrapper<AssetPathEntity> p) => p == path,
    );
    if (index != -1) {
      _paths[index] = _paths[index].copyWith(assetCount: assetCount);
      if (index == 0) {
        _currentPath = _currentPath?.copyWith(assetCount: assetCount);
      }
      notifyListeners();
    }
  }

  /// Get assets list from current path entity.
  /// 从当前已选路径获取资源列表
  Future<void> getAssetsFromCurrentPath() async {
    if (_currentPath == null || _paths.isEmpty) {
      isAssetsEmpty = true;
      return;
    }
    final PathWrapper<AssetPathEntity> wrapper = _currentPath!;
    final int assetCount = wrapper.assetCount ?? await wrapper.path.assetCountAsync;
    totalAssetsCount = assetCount;
    isAssetsEmpty = assetCount == 0;
    if (wrapper.assetCount == null) {
      currentPath = _currentPath!.copyWith(assetCount: assetCount);
    }
    await getAssetsFromPath(0, currentPath!.path);
  }
}
