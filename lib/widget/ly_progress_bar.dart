// ignore_for_file: public_member_api_docs, sort_constructors_first
import 'package:flutter/cupertino.dart';
import 'package:zyocore/widget/ly_ratio_bar_chart.dart';

class LYProgressBar extends StatefulWidget {
  final Color backgroudColor;
  final Color foregroundColor;
  final num total;
  final num current;
  const LYProgressBar({
    Key? key,
    required this.backgroudColor,
    required this.foregroundColor,
    required this.total,
    required this.current,
  }) : super(key: key);

  @override
  State<LYProgressBar> createState() => _LYProgressBarState();
}

class _LYProgressBarState extends State<LYProgressBar> {
  @override
  Widget build(BuildContext context) {
    bool showSpace = (widget.total - widget.current) > 0;
    return Stack(
      children: [
        Positioned.fill(
          child: LYRatioBar(
              model: LYRatioBarModel(datas: [
            LYRatioBarData(number: 1, color: widget.backgroudColor)
          ])),
        ),
        if (widget.current != 0)
          Positioned.fill(
            child: Row(
              crossAxisAlignment: CrossAxisAlignment.stretch,
              children: [
                Expanded(
                  child: LYRatioBar(
                    model: LYRatioBarModel(datas: [
                      LYRatioBarData(number: 1, color: widget.foregroundColor)
                    ]),
                  ),
                  flex: widget.current.toInt(),
                ),
                if (showSpace)
                  Spacer(
                    flex: (widget.total - widget.current).toInt(),
                  ),
              ],
            ),
          ),
      ],
    );
  }
}
