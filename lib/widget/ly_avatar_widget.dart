import 'package:flutter/material.dart';
import 'package:zyocore/zyocore.dart';


class LYAvatarWidget extends StatelessWidget {
  final double width;
  final double height;
  final String src;
  final double radius;
  final AssetImage? error;
  final BoxFit? fit;
  final LYPlaceholderWidgetBuilder? placeholderBuilder;
  final LYLoadingErrorWidgetBuilder? errorWidgetBuilder;
  const LYAvatarWidget({
    Key? key,
    required this.src,
    this.width = 52,
    this.height = 52,
    this.radius = 0,
    this.error,
    this.fit,
    this.placeholderBuilder,
    this.errorWidgetBuilder,
  }) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Container(
      width: width,
      height: height,
      clipBehavior: Clip.hardEdge,
      decoration: BoxDecoration(
        borderRadius: BorderRadius.all(
          Radius.circular(radius),
        ),
      ),
      child: LYAvatarIndex.getAvatar(src,
          placeholderBuilder: placeholderBuilder,
          errorWidgetBuilder: errorWidgetBuilder,
          fit: fit ?? BoxFit.cover),
    );
  }
}

///Cloud默认头像
class LYAvatarWidgetCloud extends StatelessWidget {
  final double width;
  final double height;
  final String src;
  final double radius;
  final AssetImage? error;

  const LYAvatarWidgetCloud(
      {Key? key,
      required this.src,
      this.width = 52,
      this.height = 52,
      this.radius = 0,
      this.error})
      : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Container(
      width: width,
      height: height,
      clipBehavior: Clip.hardEdge,
      decoration: BoxDecoration(
        borderRadius: BorderRadius.all(
          Radius.circular(radius),
        ),
      ),
      child: CachedNetworkImage(
          placeholder: (context, url) {
            return const Image(
                image: AssetImage(
              "assets/images/baby_avatar.png",
              package: "zyocore",
            ));
          },
          width: width,
          height: height,
          fit: BoxFit.cover,
          imageUrl: src,
          errorWidget: (a, b, c) => error == null
              ? const Image(
                  image: AssetImage(
                  "assets/images/baby_avatar.png",
                  package: "zyocore",
                ))
              : Image(image: error!)),
    );
  }
}

class LYIcon extends StatelessWidget {
  final double width;
  final double height;
  final String src;
  final double radius;
  final AssetImage? error;

  const LYIcon(
      {Key? key,
      required this.src,
      this.width = 52,
      this.height = 52,
      this.radius = 0,
      this.error})
      : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Container(
      width: width,
      height: height,
      clipBehavior: Clip.hardEdge,
      decoration: BoxDecoration(
        borderRadius: BorderRadius.all(
          Radius.circular(radius),
        ),
      ),
      child: CachedNetworkImage(
        fit: BoxFit.cover,
        imageUrl: src,
      ),
    );
  }
}
