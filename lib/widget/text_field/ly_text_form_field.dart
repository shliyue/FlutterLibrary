import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:zyocore/values/style.dart';
import 'package:zyocore/values/color.dart';
import 'package:flutter_screenutil/flutter_screenutil.dart';

class LyTextFormField extends StatelessWidget {
  final String? title;
  final TextEditingController? tec;
  final String? hintText;
  final TextAlign textAlign;
  final TextInputType? keyboardType;
  final FocusNode? focusNode;
  final bool? enable;
  final ValueChanged<String>? onChanged;
  final List<TextInputFormatter>? inputFormatters;
  int? maxLines;
  int? maxLength;
  final double? fontSize;
  final GestureTapCallback? onTap;
  bool hasUnderline;
  String? counterText;
  Color? textColor;

  LyTextFormField({
    Key? key,
    @Deprecated('属性弃用') this.title,
    this.tec,
    this.hintText = '请填写',
    this.onChanged,
    this.textAlign = TextAlign.left,
    this.keyboardType,
    this.inputFormatters,
    this.focusNode,
    this.enable,
    this.maxLines = 1,
    this.maxLength,
    this.fontSize,
    this.onTap,
    this.hasUnderline = true,
    this.counterText = "",
    this.textColor,
  }) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return TextFormField(
      controller: tec,
      enabled: enable,
      textAlign: textAlign,
      style: TextStyle(
        fontSize: fontSize ?? 17.sp,
        color: textColor ?? dark191919,
        fontFamily: AppFontFamily.pingFangRegular,
        package: 'zyocore',
      ),
      decoration: InputDecoration(
        contentPadding: EdgeInsets.zero,
        counterText: counterText,
        hintStyle: TextStyle(
          fontSize: fontSize ?? 17.sp,
          color: dark999999,
          fontFamily: AppFontFamily.pingFangRegular,
          package: 'zyocore',
        ),
        hintText: hintText,
        border: hasUnderline ? null : InputBorder.none,
      ),
      maxLines: maxLines,
      maxLength: maxLength,
      inputFormatters: inputFormatters,
      textInputAction: TextInputAction.next,
      keyboardType: keyboardType,
      onChanged: onChanged,
      onTap: onTap,
    );
  }
}
