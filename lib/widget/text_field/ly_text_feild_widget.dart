import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:zyocore/values/edge_insets.dart';
import 'package:zyocore/values/style.dart';
import 'package:zyocore/values/text.dart';
import 'package:zyocore/widget/common/flex/row.dart';
import 'package:zyocore/zyocore.dart';

import 'ly_text_form_field.dart';

typedef TextChangeCallBack = void Function(String text);

/// 输入框（园所注册、我的）等模块使用
class LyTextFieldWidget extends StatelessWidget {
  final VoidCallback? onSelect;
  final TextChangeCallBack? onChange;
  final String? leading;
  final double? leadingMinWidth;
  final TextEditingController? textController;
  final String? placeHolder;
  final bool showStar;
  final bool hideDiv;
  final bool enable;
  final bool isTextArea;
  final int maxLength;
  final double? height;
  final Color? bgColor;
  final TextStyle? hintStyle;
  final TextInputType? textInputType;
  final List<TextInputFormatter>? inputFormatters;
  final String? counterText;
  final int maxLines;
  final double? fontSize;
  final GestureTapCallback? onTap;
  final TextAlign textAlign;
  final Color? textColor;

  const LyTextFieldWidget({Key? key,
    this.onSelect,
    this.onChange,
    this.leading,
    this.leadingMinWidth,
    this.placeHolder,
    this.showStar = true,
    this.hideDiv = false,
    this.enable = true,
    this.maxLength = 20,
    this.isTextArea = false,
    this.height,
    this.bgColor,
    this.hintStyle,
    this.textController,
    this.textInputType,
    this.counterText = '',
    this.maxLines = 5,
    this.fontSize,
    this.onTap,
    this.textAlign = TextAlign.left,
    this.inputFormatters,
    this.textColor,})
      : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Container(
      alignment: isTextArea ? Alignment.topLeft : Alignment.centerLeft,
      height: height ?? (isTextArea ? null : 56.w),
      margin: EdgeInsets.only(left: 16.w),
      padding: right16,
      decoration: BoxDecoration(
          color: bgColor ?? lightFFFFFF,
          border: Border(
            bottom: BorderSide(
              width: 1.w,
              color: hideDiv ? Colors.white : lightE1E1E1,
            ),
          )),
      child: isTextArea ? _textArea : _textField,
    );
  }

  Widget get _textField {
    return RSC(
      children: [_leadingWidget, onSelect != null ? _buildTextSelect : _buildTextField],
    );
  }

  Widget get _textArea {
    return Container(
      margin: top16,
      child: RSS(
        children: [
          _leadingWidget,
          _buildTextField,
        ],
      ),
    );
  }

  Widget get _leadingWidget {
    return TextUtil.isEmpty(leading)
        ? const SizedBox()
        : Container(
      constraints: BoxConstraints(minWidth: leadingMinWidth ?? 105.w),
      child: RSC(
        mainAxisSize: MainAxisSize.min,
        children: [
          LYText(
            text: leading,
            fontFamily: AppFontFamily.pingFangMedium,
            fontSize: fontSize ?? 17.sp,
          ),
          showStar
              ? LYText.withStyle(
            '*',
            style: text17.textColor(redFA5151),
          )
              : Container(),
        ],
      ),
    );
  }

  Widget get _buildTextSelect {
    return Expanded(
        child: GestureDetector(
          onTap: () {
            if (onSelect != null) {
              onSelect!();
            }
          },
          child: Container(
            color: bgColor ?? lightFFFFFF,
            padding: right8,
            child: RBC(
              children: [
                LYText.withStyle(
                  placeHolder,
                  style: (placeHolder == '请选择' || placeHolder == '请选择省市区') ? text17.gray : (hintStyle ?? text17.dark),
                ),
                Icon(
                  Icons.keyboard_arrow_right_rounded,
                  size: 20.w,
                  color: dark999999,
                )
              ],
            ),
          ),
        ));
  }

  Widget get _buildTextField {
    return Expanded(
      child: LyTextFormField(
        enable: enable,
        fontSize: fontSize,
        hintText: placeHolder,
        textAlign: textAlign,
        inputFormatters: inputFormatters,
        keyboardType: textInputType,
        maxLength: maxLength,
        maxLines: isTextArea ? maxLines : null,
        tec: textController,
        onChanged: onChange,
        onTap: onTap,
        hasUnderline: false,
        textColor: textColor ?? dark666666,
      ),

      // child: TextField(
      //   enabled: enable,
      //   style: TextStyle(
      //     fontSize: fontSize ?? 17.sp,
      //     color: dark191919,
      //     fontFamily: AppFontFamily.pingFangRegular,
      //     package: 'zyocore',
      //   ),
      //   inputFormatters: inputFormatters ?? [],
      //   keyboardType: textInputType ?? TextInputType.text,
      //   autofocus: false,
      //   onChanged: (text) {
      //     if (onChange != null) {
      //       onChange!(text);
      //     }
      //   },
      //   onTap: () {
      //     if (onTap != null) {
      //       onTap!();
      //     }
      //   },
      //   maxLength: maxLength,
      //   controller: textController,
      //   textInputAction: TextInputAction.done,
      //   textAlignVertical: TextAlignVertical.center,
      //   cursorColor: dark666666,
      //   decoration: InputDecoration(
      //     counterText: '',
      //     hintText: placeHolder,
      //     hintStyle: text17.gray.size(fontSize ?? 17.sp),
      //     isDense: true,
      //     border: InputBorder.none,
      //   ),
      // ),
    );
  }

  Widget get _buildTextArea {
    return Expanded(
      child: LyTextFormField(
        enable: enable,
        fontSize: fontSize,
        hintText: placeHolder,
        inputFormatters: inputFormatters,
        keyboardType: textInputType,
        maxLength: maxLength,
        maxLines: maxLines,
        tec: textController,
        onChanged: onChange,
        onTap: onTap,
        hasUnderline: false,
      ),

      // child: TextField(
      //   enabled: enable,
      //   style: TextStyle(
      //     fontSize: fontSize ?? 17.sp,
      //     color: dark191919,
      //     fontFamily: AppFontFamily.pingFangRegular,
      //     package: 'zyocore',
      //   ),
      //   keyboardType: TextInputType.text,
      //   autofocus: false,
      //   onChanged: (text) {
      //     if (onChange != null) {
      //       onChange!(text);
      //     }
      //   },
      //   onTap: () {
      //     if (onTap != null) {
      //       onTap!();
      //     }
      //   },
      //   maxLines: maxLines,
      //   maxLength: maxLength,
      //   controller: textController,
      //   textInputAction: TextInputAction.done,
      //   cursorColor: dark666666,
      //   decoration: InputDecoration(
      //     contentPadding: EdgeInsets.zero,
      //     counterText: counterText,
      //     hintText: placeHolder,
      //     hintStyle: text17.gray.size(fontSize ?? 17.sp),
      //     isDense: true,
      //     border: InputBorder.none,
      //   ),
      // ),
    );
  }
}
