import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:flutter_screenutil/flutter_screenutil.dart';
import '../text/ly_label_hjc.dart';
import 'ly_text_form_field.dart';

@Deprecated('建议使用LyTextFieldWidget')
class LYFormTextFiled extends StatelessWidget {
  final String title;
  final double titleWidth;
  final TextEditingController tec;
  final String hintText;
  final bool must;
  final bool showBorder;
  final TextAlign textAlign;
  final TextInputType? keyboardType;
  final FocusNode? focusNode;
  final bool enable;
  final ValueChanged<String>? onChanged;
  final List<TextInputFormatter>? inputFormatters;
  final GestureTapCallback? onTap;
  int? maxLines;
  int? maxLength;
  final double? fontSize;

  LYFormTextFiled(
      {Key? key,
      required this.title,
      required this.tec,
      this.titleWidth = 92,
      this.hintText = '请填写',
      this.showBorder = true,
      this.onChanged,
      this.textAlign = TextAlign.left,
      this.keyboardType,
      this.inputFormatters,
      this.focusNode,
      this.onTap,
      this.maxLines = 1,
      this.maxLength,
      this.fontSize,
      this.enable = true,
      this.must = false})
      : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Container(
      margin: EdgeInsets.only(left: 16.w),
      padding: EdgeInsets.only(top: 4.w, bottom: 4.w, right: 16.w),
      decoration: showBorder
          ? const BoxDecoration(
              border: Border(
                bottom: BorderSide(
                  width: 0.5,
                  color: Color(0xFFE1E1E1),
                ),
              ),
            )
          : null,
      child: Row(
        children: [
          SizedBox(
            width: titleWidth,
            child: LYLabelHjc(title: title, hjc: must),
          ),
          SizedBox(width: 12.w),
          Expanded(
            child: LyTextFormField(
              enable: enable,
              textAlign: textAlign,
              fontSize: fontSize,
              hintText: hintText,
              inputFormatters: inputFormatters,
              keyboardType: keyboardType,
              maxLength: maxLength,
              maxLines: maxLines,
              tec: tec,
              onChanged: onChanged,
              onTap: onTap,
              hasUnderline: false,
            ),
            // child: TextField(
            //   focusNode: focusNode,
            //   controller: tec,
            //   textAlign: textAlign,
            //   keyboardType: keyboardType,
            //   enabled: enable,
            //   style: TextStyle(
            //     fontFamily: AppFontFamily.pingFangRegular,
            //     fontSize: 17.sp,
            //     color: dark1A1A1A,
            //   ),
            //   decoration: InputDecoration(
            //     border: InputBorder.none,
            //     focusedBorder: InputBorder.none,
            //     enabledBorder: InputBorder.none,
            //     errorBorder: InputBorder.none,
            //     disabledBorder: InputBorder.none,
            //     hintStyle: TextStyle(
            //       fontFamily: AppFontFamily.pingFangRegular,
            //       fontSize: 17.sp,
            //       color: dark999999,
            //     ),
            //     hintText: hintText,
            //   ),
            //   onChanged: onChanged,
            //   onTap: onTap,
            //   inputFormatters: inputFormatters,
            // ),
          ),
        ],
      ),
    );
  }
}
