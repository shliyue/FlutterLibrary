import 'package:flutter/material.dart';

class LyPageView extends StatelessWidget {
  final int itemCount;
  final PageController controller;
  final IndexedWidgetBuilder itemBuilder;

  const LyPageView({
    Key? key,
    required this.itemCount,
    required this.controller,
    required this.itemBuilder,
  }) : super(key: key);

  @override
  Widget build(BuildContext context) {
    Map<int, Widget> pages = {};
    return PageView.builder(
      physics: const NeverScrollableScrollPhysics(),
      controller: controller,
      itemCount: itemCount,
      itemBuilder: (context, index) {
        if (!pages.containsKey(index)) {
          pages[index] = itemBuilder(context, index);
        }
        return pages[index]!;
      },
    );
  }
}
