import 'package:flutter/material.dart';
import 'package:zyocore/values/edge_insets.dart';
import 'package:zyocore/values/text.dart';
import 'package:zyocore/widget/common/flex/column.dart';
import 'package:zyocore/widget/common/flex/row.dart';

import '../../core/configs/config.dart';
import '../../zyocore.dart';
import 'logic.dart';

/// 图片剪裁
class LyClipImagePage extends GetView<LyClipImageLogic> {
  const LyClipImagePage({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return SafeArea(
        child: Theme(
      data: ThemeData(scaffoldBackgroundColor: Colors.black),
      child: Scaffold(
        backgroundColor: Colors.black,
        body: Stack(
          alignment: Alignment.center,
          children: <Widget>[
            Obx(() {
              if (controller.state.file.value.path.isEmpty) {
                return Container();
              }
              return ExtendedImage.file(
                controller.state.file.value,
                fit: BoxFit.contain,
                mode: ExtendedImageMode.editor,
                enableLoadState: true,
                extendedImageEditorKey: controller.editorKey,
                cacheRawData: true,
                initEditorConfigHandler: (ExtendedImageState? state) {
                  return EditorConfig(
                      lineColor: dark0D0D0D,
                      maxScale: 8.0,
                      cropRectPadding: EdgeInsets.all(20.w),
                      hitTestSize: 20.0,
                      cornerColor: blue63B7FF,
                      cropAspectRatio: controller.state.scale.value ?? CropAspectRatios.ratio1_1);
                },
              );
            }),
            Positioned(
              top: 30.w,
              child: topAction,
            ),
            if (controller.state.showActions.value)
              Positioned(
                bottom: 20.w,
                child: Container(
                  padding: EdgeInsets.symmetric(horizontal: 62.w),
                  width: ScreenUtil().screenWidth,
                  child: RBC(
                    children: actionList,
                  ),
                ),
              ),
          ],
        ),
      ),
    ));
  }

  Widget get topAction {
    return Container(
      width: ScreenUtil().screenWidth,
      alignment: Alignment.topLeft,
      child: RBC(
        children: [
          GestureDetector(
            onTap: () {
              Get.back();
            },
            child: Container(
              padding: horizontal16,
              child: LYText.withStyle(
                '取消',
                style: text17,
              ),
            ),
          ),
          GestureDetector(
            onTap: () {
              controller.cropImage();
            },
            child: Container(
              padding: horizontal16,
              child: LYText.withStyle(
                '完成',
                style: text17,
              ),
            ),
          )
        ],
      ),
    );
  }

  List<Widget> get actionList {
    return controller.state.actions.map((e) {
      return GestureDetector(
        onTap: () {
          controller.handleAction(e['title'] ?? "");
        },
        child: Container(
          alignment: Alignment.center,
          width: 68.w,
          height: 68.w,
          decoration: BoxDecoration(color: dark333333, borderRadius: BorderRadius.all(Radius.circular(34.w))),
          child: CCC(
            children: [
              Image(
                image: AssetImage('assets/images/${e['icon']}.png', package: Config.packageName),
                fit: BoxFit.cover,
                width: 18.w,
                height: 18.w,
              ),
              LYText.withStyle(
                e['title'],
                style: text12,
              )
            ],
          ),
        ),
      );
    }).toList();
  }
}
