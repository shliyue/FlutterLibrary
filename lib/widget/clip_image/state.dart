import 'dart:io';

import 'package:get/get_rx/src/rx_types/rx_types.dart';

class LyClipImageState {
  Rx<File> file = File('').obs;

  var scale = Rxn<double>();
  late bool avatar;
  late bool compress;
  late int maxWidth;
  late bool isOnlyPhoto;

  /// 剪裁完是否直接上传，而不是返回到上一页才上传
  var upload = false;

  var cropping = false;
  var actions = [{"title":'左旋转',"icon":'clip_left'}, {"title":'右旋转',"icon":'clip_right'}, {"title":'重置',"icon":'clip_reset'}];
  /// 招生模块
  var emActions =  [{"title":'替换',"icon":'clip_replace'}, {"title":'重置',"icon":'clip_reset'}];
  var showActions = true.obs;
  String? actionType;

  LyClipImageState() {}
}
