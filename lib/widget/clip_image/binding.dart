import 'package:get/get.dart';

import 'logic.dart';

class LyClipImageBinding extends Bindings {
  @override
  void dependencies() {
    Get.lazyPut(() => LyClipImageLogic());
  }
}
