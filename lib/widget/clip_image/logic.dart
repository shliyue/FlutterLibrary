import 'dart:io';

import 'package:flutter/foundation.dart';
import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:zyocore/core/model/file_model.dart';
import 'package:zyocore/zyocore.dart';

import 'state.dart';

class LyClipImageLogic extends GetxController {
  final LyClipImageState state = LyClipImageState();

  final GlobalKey<ExtendedImageEditorState> editorKey = GlobalKey<ExtendedImageEditorState>();

  @override
  void onInit() {
    super.onInit();
    Map<String, dynamic> arguments = Get.arguments;
    state.file.value = arguments['file'];
    state.upload = arguments['upload'];
    state.scale.value = arguments['scale'];
    state.avatar = arguments['avatar'] ?? true;
    state.compress = arguments['compress'] ?? false;
    state.maxWidth = arguments['maxWidth'] ?? 2000;
    state.showActions.value = arguments['showActions'] ?? true;
    state.actionType = arguments['actionType'];
    state.isOnlyPhoto = arguments['isOnlyPhoto'] ?? false;

    if (state.actionType == 'enrollment') {
      state.actions = state.emActions;
    }
    SystemChrome.setSystemUIOverlayStyle(const SystemUiOverlayStyle(
      statusBarColor: Colors.black,
      statusBarIconBrightness: Brightness.light,
    ));
  }

  @override
  void onClose() {
    SystemChrome.setSystemUIOverlayStyle(const SystemUiOverlayStyle(
      statusBarColor: Colors.transparent,
      statusBarIconBrightness: Brightness.dark,
    ));
    super.onClose();
  }

  /// 重拍
  resumeCamera() {
    Get.back();
  }

  handleAction(String title) {
    switch (title) {
      case "左旋转":
        editorKey.currentState!.rotate(right: false);
        break;
      case "右旋转":
        editorKey.currentState!.rotate(right: true);
        break;
      case "重置":
        editorKey.currentState!.reset();
        break;
      case "替换":
        onTapHandle();
        break;
    }
  }

  Future onTapHandle() async {
    onTapPictureHandle();
  }

  /// 选择照片
  void onTapPictureHandle() async {
    AssetUtil.onSelectAssets(
        context: Get.context!,
        isOnlyPhoto: state.isOnlyPhoto,
        onCallBack: (v) {
          if (v != null) {
            state.file.value = v;
            editorKey.currentState?.reset();
          }
        });
  }

  /// 剪裁完直接上传
  Future uploadImage(String filePath, {String? suffix}) async {
    try {
      var filename = FileUtil.formatImageNameWithWH(width: 1080, height: 1080, suffix: suffix ?? "jpg");
      File file = File(filePath);
      //文件上传七牛
      ToastUtil.loading('');
      FileModel model = FileModel(file: file, type: 1, key: filename);
      // List<String> uploadedFiles = await UploadUtil.uploadImageFileModel([model]);
      ToastUtil.dismiss();

      // if (uploadedFiles.isNotEmpty && state.avatar) {
      //    submitAvatar(uploadedFiles.first);
      // } else {
      //   Get.back(result: uploadedFiles.first);
      // }
    } catch (e) {
      ToastUtil.dismiss();
      Logger.d(e.toString());
    }
  }

  cropImage() async {
    if (state.cropping) return;
    ToastUtil.loading("");
    state.cropping = true;
    final Uint8List fileData =
        Uint8List.fromList((await ImageUtil.cropImageDataWithNativeLibrary(state: editorKey.currentState!))!);
    File cropImage = await _listToFile(fileData);
    if (cropImage.path.isNotEmpty) {
      var filePath = cropImage.path;
      if (state.compress) {
        var image = await decodeImageFromList(fileData);
        double coefficient = 1.0;
        if (image.width > state.maxWidth) {
          coefficient = state.maxWidth / image.width;
        }
        int compressedW = (coefficient * image.width).toInt();
        int compressedH = (coefficient * image.height).toInt();
        var compressedFile = await ImageUtil.compressAndGetFile(cropImage, minWidth: compressedW, minHeight: compressedH);
        filePath = compressedFile!.path;
      }

      /// 直接上传还是返回本地文件路径
      if (state.upload) {
        var suffix = filePath.substring(filePath.lastIndexOf(".") + 1, filePath.length);
        await uploadImage(filePath, suffix: suffix);
        state.cropping = false;
        ToastUtil.dismiss();
      } else {
        ToastUtil.dismiss();
        Get.back(result: filePath);
        state.cropping = false;
      }
      ToastUtil.dismiss();
    }
  }

  /// 将Uint8List直接写入到文件
  Future<File> _listToFile(Uint8List list) async {
    String path = state.file.value.path;
    String name = path.substring(path.lastIndexOf("/") + 1, path.length);
    String? suffix = name.lastIndexOf(".") == -1 ? 'jpg' : name.substring(name.lastIndexOf(".") + 1, name.length);
    int timestamp = DateTime.now().millisecondsSinceEpoch;

    Directory tempDir = await FileUtil.getUploadCachePath();
    await CacheUtil.delDir(tempDir, isDelDir: false);
    var finalName = tempDir.path + '/$timestamp.$suffix';
    File finalFile = File(finalName);

    File fixedFile = await finalFile.writeAsBytes(
      list,
      flush: true,
    );

    return fixedFile;
  }
}
