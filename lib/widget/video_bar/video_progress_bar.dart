import 'dart:math';

import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'dart:ui' as ui;
import 'package:video_player/video_player.dart';
import 'package:zyocore/widget/video_bar/video_progress_style.dart';

import '../../util/logger.dart';

typedef VideoProgressDragHandle = void Function(
    Duration position, Duration duration);

/// 自定义线性进度条
class VideoProgressBar extends StatefulWidget {
  VideoProgressBar(
    this.vpc, {
    Key? key,
    VideoProgressStyle? progressStyle,
    this.onProgressDrag,
    this.onDragEnd,
    this.proImage,
  })  : progressStyle = progressStyle ?? VideoProgressStyle(),
        super(key: key);

  final VideoPlayerController vpc;
  final ui.Image? proImage;

  final VideoProgressStyle progressStyle;

  final VideoProgressDragHandle? onProgressDrag;
  final VoidCallback? onDragEnd;

  @override
  _VideoProgressBarState createState() => _VideoProgressBarState();
}

class _VideoProgressBarState extends State<VideoProgressBar> {
  VideoProgressStyle get style => widget.progressStyle;
  ui.Image? image;

  @override
  void initState() {
    super.initState();
    loadAsset();
  }

  @override
  void deactivate() {
    super.deactivate();
  }

  loadAsset() async {
    image = await loadAssetImage('packages/zyocore/assets/images/icon_progress.png');
    Logger.d('loadAsset=======image == null=${image == null}');
    Logger.d(image.toString());
  }

  Future<ui.Image> loadAssetImage(String asset) async {
    final imageData = await rootBundle.load(asset);
    final imageCodec = await ui.instantiateImageCodec(
        imageData.buffer.asUint8List(),
        targetHeight: 15,
        targetWidth: 15);
    final imageInfo = await imageCodec.getNextFrame();
    return imageInfo.image;
  }

  @override
  Widget build(BuildContext context) {
    return _VideoScrubber(
      vpc: widget.vpc,
      handleDrag: widget.onProgressDrag,
      onDragEnd: widget.onDragEnd,
      child: CustomPaint(
        foregroundPainter: _ProgressBarPainter(style, widget.vpc, image),
        child: Container(
          color: Colors.transparent,
          height: 24,
        ),
      ),
    );
  }
}

/// 处理进度条手势
class _VideoScrubber extends StatefulWidget {
  const _VideoScrubber(
      {required this.child,
      this.handleDrag,
      this.onDragEnd,
      required this.vpc});

  final Widget child;
  final VideoPlayerController vpc;
  final VideoProgressDragHandle? handleDrag;
  final VoidCallback? onDragEnd;

  @override
  _VideoScrubberState createState() => _VideoScrubberState();
}

class _VideoScrubberState extends State<_VideoScrubber> {
  bool _controllerWasPlaying = false;

  @override
  Widget build(BuildContext context) {
    /// 滑动到指定位置
    void seekToRelativePosition(Offset globalPosition) {
      // videoSource = widget.vpc.dataSource;
      var sizeBox = context.findRenderObject() as RenderBox;
      final Offset tapPos = sizeBox.globalToLocal(globalPosition);
      final double relative = tapPos.dx / sizeBox.size.width;
      final Duration position = widget.vpc.value.duration * min(relative, 1);
      int finalPosition =
          position.inSeconds > widget.vpc.value.duration.inSeconds
              ? widget.vpc.value.duration.inSeconds
              : position.inSeconds;
      widget.vpc.seekTo(Duration(seconds: finalPosition));
    }

    return GestureDetector(
      behavior: HitTestBehavior.deferToChild,
      child: widget.child,
      onTap: () {
        Logger.d('-------onTap--onTap');
      },
      onHorizontalDragStart: (DragStartDetails details) {
        if (!widget.vpc.value.isInitialized) {
          return;
        }
        _controllerWasPlaying = widget.vpc.value.isPlaying;
        if (_controllerWasPlaying) {
          widget.vpc.pause();
        }
      },
      onHorizontalDragUpdate: (DragUpdateDetails details) {
        // Logger.d(
        //     '-------onHorizontalDragUpdate--isInitialized---${widget.vpc.value.isInitialized}');
        if (!widget.vpc.value.isInitialized) {
          return;
        }
        seekToRelativePosition(details.globalPosition);
        // emitDragUpdate(details.globalPosition);
      },
      onHorizontalDragEnd: (DragEndDetails details) {
        // Logger.d(
        //     '-------onHorizontalDragUpdate--_controllerWasPlaying---$_controllerWasPlaying');
        if (_controllerWasPlaying) {
          widget.vpc.play();
        }
      },
      onTapDown: (TapDownDetails details) {
        // Logger.d(
        //     '-------onTapDown--isInitialized---${widget.vpc.value.isInitialized}');
        if (!widget.vpc.value.isInitialized) {
          return;
        }
        seekToRelativePosition(details.globalPosition);
      },
    );
  }
}

/// 绘制进度条
class _ProgressBarPainter extends CustomPainter {
  _ProgressBarPainter(this.style, this.vpc, this.proImage);

  final VideoProgressStyle style;
  final VideoPlayerController vpc;
  ui.Image? proImage;
  late Path mPath;

  //刷新布局的时候告诉flutter 是否需要重绘
  @override
  bool shouldRepaint(CustomPainter painter) {
    return true;
  }

  //绘制流程
  @override
  void paint(Canvas canvas, Size size) {
    /// y轴起点，外层整个进度条
    final baseOffset = size.height / 2 - style.height / 2.0;

    /// 已经播放的进度条
    final playedBaseOffset = size.height / 2 - style.playedHeight / 2.0;

    /// 外层进度条，包含未播放的部分
    canvas.drawRRect(
      RRect.fromRectAndRadius(
        Rect.fromPoints(
          /// 起点，左上角
          Offset(0.0, baseOffset),

          /// 终点，右下角
          Offset(size.width, baseOffset + style.height),
        ),
        Radius.circular(style.progressRadius),
      ),
      Paint()..color = style.backgroundColor,
    );
    if (!(vpc.value.isInitialized)) {
      return;
    }

    /// 已播放部分（比例）
    final double playedPartPercent =
        vpc.value.position.inMilliseconds / vpc.value.duration.inMilliseconds;

    /// 已播放部分（宽度）
    final double playedPart =
        playedPartPercent > 1 ? size.width : playedPartPercent * size.width;

    /// 缓冲部分UI
    for (DurationRange range in vpc.value.buffered) {
      final double start = range.startFraction(vpc.value.duration) * size.width;
      final double end = range.endFraction(vpc.value.duration) * size.width;
      canvas.drawRRect(
        RRect.fromRectAndRadius(
          Rect.fromPoints(
            Offset(start, baseOffset),
            Offset(end, baseOffset + style.height),
          ),
          Radius.circular(style.progressRadius),
        ),
        Paint()..color = style.bufferedColor,
      );
    }

    /// 已播放部分
    canvas.drawRRect(
      RRect.fromRectAndRadius(
        Rect.fromPoints(
          Offset(0.0, playedBaseOffset),
          Offset(playedPart, playedBaseOffset + style.playedHeight),
        ),
        Radius.circular(style.progressRadius),
      ),
      Paint()..color = style.playedColor,
    );

    /// 画滑动的圆形
    // mPath = Path()
    //   ..addOval(Rect.fromCircle(
    //       center: Offset(playedPart, baseOffset + style.height / 2),
    //       radius: style.dragHeight));
    // canvas.drawShadow(mPath, Colors.black, 0.8, true);
    if (proImage == null) {
      canvas.drawCircle(
        Offset(
            playedPart + style.dragHeight / 2, baseOffset + style.height / 2),
        style.dragHeight,
        Paint()..color = style.dragBarColor,
      );
    } else {
      canvas.drawImage(
        proImage!,
        Offset(
            playedPart - style.dragHeight / 2, baseOffset / 2 - style.height),
        Paint()..color = style.dragBarColor,
      );
    }
  }
}
