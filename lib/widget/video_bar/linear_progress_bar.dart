import 'package:flutter/material.dart';
import 'package:video_player/video_player.dart';
import 'package:zyocore/widget/video_bar/video_progress_style.dart';

import '../../util/logger.dart';

typedef VideoProgressDragHandle = void Function(
    Duration position, Duration duration);

/// 自定义线性进度条
class VideoLinearProgressBar extends StatefulWidget {
  VideoLinearProgressBar(
    this.videoPlayerController, {
    Key? key,
    VideoProgressStyle? progressStyle,
    this.allowScrubbing,
    this.padding = const EdgeInsets.only(top: 5.0),
    this.onProgressDrag,
  })  : progressStyle = progressStyle ?? VideoProgressStyle(),
        super(key: key);

  final VideoPlayerController videoPlayerController;

  final VideoProgressStyle progressStyle;

  final bool? allowScrubbing;

  final EdgeInsets padding;

  final VideoProgressDragHandle? onProgressDrag;

  @override
  _VideoLinearProgressBarState createState() => _VideoLinearProgressBarState();
}

class _VideoLinearProgressBarState extends State<VideoLinearProgressBar> {
  VoidCallback? listener;

  VideoProgressStyle get style => widget.progressStyle;

  @override
  void initState() {
    super.initState();
  }

  @override
  void deactivate() {
    super.deactivate();
  }

  /// 这里需要保留，否则无法响应点击事件，原因不明、待查
  /// child: Container(
  //           color: Colors.yellow,
  //           height: 4,
  //         ),
  @override
  Widget build(BuildContext context) {
    return _VideoScrubber(
      videoPlayerController: widget.videoPlayerController,
      handleDrag: widget.onProgressDrag,
      child: CustomPaint(
        painter: _ProgressBarPainter(
          style,
          widget.videoPlayerController,
        ),
        child: Container(
          color: Colors.transparent,
          height: 30,
        ),
      ),
    );
  }
}

/// 处理进度条手势
class _VideoScrubber extends StatefulWidget {
  const _VideoScrubber(
      {required this.child, this.handleDrag, required this.videoPlayerController});

  final Widget child;
  final VideoProgressDragHandle? handleDrag;
  final VideoPlayerController videoPlayerController;

  @override
  _VideoScrubberState createState() => _VideoScrubberState();
}

class _VideoScrubberState extends State<_VideoScrubber> {
  bool _controllerWasPlaying = false;

  @override
  Widget build(BuildContext context) {
    void seekToRelativePosition(Offset globalPosition) {
      final RenderBox box = context.findRenderObject() as RenderBox;
      final Offset tapPos = box.globalToLocal(globalPosition);
      final double relative = tapPos.dx / box.size.width;
      final Duration position =
          widget.videoPlayerController.value.duration * relative;
      widget.videoPlayerController.seekTo(position);
    }

    return GestureDetector(
      behavior: HitTestBehavior.deferToChild,
      child: widget.child,
      onTap: () {
        Logger.d('-------onHorizontalDragStart--onTap');
      },
      onHorizontalDragStart: (DragStartDetails details) {
        Logger.d(
            '-------onHorizontalDragStart--isInitialized---${widget.videoPlayerController.value.isInitialized}');
        if (!widget.videoPlayerController.value.isInitialized) {
          return;
        }
        _controllerWasPlaying = widget.videoPlayerController.value.isPlaying;
        if (_controllerWasPlaying) {
          widget.videoPlayerController.pause();
        }
      },
      onHorizontalDragUpdate: (DragUpdateDetails details) {
        Logger.d(
            '-------onHorizontalDragUpdate--isInitialized---${widget.videoPlayerController.value.isInitialized}');
        if (!widget.videoPlayerController.value.isInitialized) {
          return;
        }
        // emitDragUpdate(details.globalPosition);
        seekToRelativePosition(details.globalPosition);
      },
      onHorizontalDragEnd: (DragEndDetails details) {
        Logger.d(
            '-------onHorizontalDragUpdate--_controllerWasPlaying---${_controllerWasPlaying}');
        if (_controllerWasPlaying) {
          widget.videoPlayerController.play();
        }
      },
      onTapDown: (TapDownDetails details) {
        Logger.d(
            '-------onTapDown--isInitialized---${widget.videoPlayerController.value.isInitialized}');
        if (!widget.videoPlayerController.value.isInitialized) {
          return;
        }
        seekToRelativePosition(details.globalPosition);
      },
    );
  }
}

/// 绘制进度条
class _ProgressBarPainter extends CustomPainter {
  _ProgressBarPainter(this.style, this.videoPlayerController);

  final VideoProgressStyle style;
  final VideoPlayerController videoPlayerController;
  late Path mPath;

  //刷新布局的时候告诉flutter 是否需要重绘
  @override
  bool shouldRepaint(CustomPainter painter) {
    return true;
  }

  //绘制流程
  @override
  void paint(Canvas canvas, Size size) {
    final baseOffset = size.height / 2 - style.height / 2.0;
    final playedBaseOffset = size.height / 2 - style.playedHeight / 2.0;

    canvas.drawRRect(
      RRect.fromRectAndRadius(
        Rect.fromPoints(
          Offset(0.0, baseOffset),
          Offset(size.width, baseOffset + style.height),
        ),
        Radius.circular(style.progressRadius),
      ),
      Paint()..color = style.backgroundColor,
    );
    if (!(videoPlayerController.value.isInitialized)) {
      return;
    }
    final double playedPartPercent =
        videoPlayerController.value.position.inMilliseconds /
            videoPlayerController.value.duration.inMilliseconds;
    final double playedPart =
        playedPartPercent > 1 ? size.width : playedPartPercent * size.width;
    for (DurationRange range in videoPlayerController.value.buffered) {
      final double start =
          range.startFraction(videoPlayerController.value.duration) *
              size.width;
      final double end =
          range.endFraction(videoPlayerController.value.duration) * size.width;
      canvas.drawRRect(
        RRect.fromRectAndRadius(
          Rect.fromPoints(
            Offset(start, baseOffset),
            Offset(end, baseOffset + style.height),
          ),
          Radius.circular(style.progressRadius),
        ),
        Paint()..color = style.bufferedColor,
      );
    }
    canvas.drawRRect(
      RRect.fromRectAndRadius(
        Rect.fromPoints(
          Offset(0.0, playedBaseOffset),
          Offset(playedPart, playedBaseOffset + style.playedHeight),
        ),
        Radius.circular(style.progressRadius),
      ),
      Paint()..color = style.playedColor,
    );

    mPath = Path()
      ..addOval(Rect.fromCircle(
          center: Offset(playedPart, baseOffset + style.height / 2),
          radius: style.dragHeight));
    canvas.drawShadow(mPath, Colors.black, 0.8, true);
    canvas.drawCircle(
      Offset(playedPart, baseOffset + style.height / 2),
      style.dragHeight,
      Paint()..color = style.dragBarColor,
    );
  }
}
