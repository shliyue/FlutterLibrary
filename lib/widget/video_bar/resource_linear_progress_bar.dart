import 'package:flutter/material.dart';
import 'package:video_player/video_player.dart';
import 'package:zyocore/widget/video_bar/video_progress_style.dart';

import '../../util/logger.dart';

typedef VideoProgressDragHandle = void Function(
    Duration position, Duration duration);

/// 自定义线性进度条
class ResourceVideoLinearProgressBar extends StatefulWidget {
  ResourceVideoLinearProgressBar(
    this.vpc, {
    Key? key,
    VideoProgressStyle? progressStyle,
    this.padding = const EdgeInsets.only(top: 5.0),
    this.onProgressDrag,
  })  : progressStyle = progressStyle ?? VideoProgressStyle(),
        super(key: key);

  final VideoPlayerController vpc;

  final VideoProgressStyle progressStyle;

  final EdgeInsets padding;

  final VideoProgressDragHandle? onProgressDrag;

  @override
  _ResourceVideoLinearProgressBarState createState() => _ResourceVideoLinearProgressBarState();
}

class _ResourceVideoLinearProgressBarState extends State<ResourceVideoLinearProgressBar> {
  VoidCallback? listener;

  VideoProgressStyle get style => widget.progressStyle;

  @override
  void initState() {
    super.initState();
  }

  @override
  void deactivate() {
    super.deactivate();
  }

  /// 这里需要保留，否则无法响应点击事件，原因不明、待查
  /// child: Container(
  //           color: Colors.yellow,
  //           height: 4,
  //         ),
  @override
  Widget build(BuildContext context) {
    return _VideoScrubber(
      vpc: widget.vpc,
      handleDrag: widget.onProgressDrag,
      child: CustomPaint(
        foregroundPainter: _ProgressBarPainter(
          style,
          widget.vpc,
        ),
        // painter: _ProgressBarPainter(
        //   style,
        //   widget.vpc,
        // ),
        child: Container(
          color: Colors.yellow,
          height: 8,
        ),
      ),
    );
  }
}

/// 处理进度条手势
class _VideoScrubber extends StatefulWidget {
  const _VideoScrubber(
      {required this.child, this.handleDrag, required this.vpc});

  final Widget child;
  final VideoProgressDragHandle? handleDrag;
  final VideoPlayerController vpc;

  @override
  _VideoScrubberState createState() => _VideoScrubberState();
}

class _VideoScrubberState extends State<_VideoScrubber> {
  bool _controllerWasPlaying = false;

  @override
  Widget build(BuildContext context) {
    void seekToRelativePosition(Offset globalPosition) {
      final RenderBox box = context.findRenderObject() as RenderBox;
      final Offset tapPos = box.globalToLocal(globalPosition);
      final double relative = tapPos.dx / box.size.width;
      final Duration position =
          widget.vpc.value.duration * relative;
      widget.vpc.seekTo(position);
    }

    void emitDragUpdate(globalPosition) {
      if (widget.handleDrag != null) {
        final RenderBox box = context.findRenderObject() as RenderBox;
        final Offset tapPos = box.globalToLocal(globalPosition);
        final double relative = tapPos.dx / box.size.width;
        final Duration position =
            widget.vpc.value.duration * relative;
        widget.handleDrag!(
            position, widget.vpc.value.duration);
      }
    }

    return GestureDetector(
      behavior: HitTestBehavior.opaque,
      child: widget.child,
      onTap: () {
        Logger.d('-------onHorizontalDragStart--onTap');
      },
      onHorizontalDragStart: (DragStartDetails details) {
        Logger.d(
            '-------onHorizontalDragStart--isInitialized---${widget.vpc.value.isInitialized}');
        if (!widget.vpc.value.isInitialized) {
          return;
        }
        _controllerWasPlaying = widget.vpc.value.isPlaying;
        if (_controllerWasPlaying) {
          widget.vpc.pause();
        }
      },
      onHorizontalDragUpdate: (DragUpdateDetails details) {
        Logger.d(
            '-------onHorizontalDragUpdate--isInitialized---${widget.vpc.value.isInitialized}');
        if (!widget.vpc.value.isInitialized) {
          return;
        }
        // emitDragUpdate(details.globalPosition);
        seekToRelativePosition(details.globalPosition);
      },
      onHorizontalDragEnd: (DragEndDetails details) {
        Logger.d(
            '-------onHorizontalDragUpdate--_controllerWasPlaying---${_controllerWasPlaying}');
        if (_controllerWasPlaying) {
          widget.vpc.play();
        }
      },
      onTapDown: (TapDownDetails details) {
        Logger.d(
            '-------onTapDown--isInitialized---${widget.vpc.value.isInitialized}');
        if (!widget.vpc.value.isInitialized) {
          return;
        }
        seekToRelativePosition(details.globalPosition);
      },
    );
  }
}

/// 绘制进度条
class _ProgressBarPainter extends CustomPainter {
  _ProgressBarPainter(this.style, this.vpc);

  final VideoProgressStyle style;
  final VideoPlayerController vpc;
  late Path mPath;

  //刷新布局的时候告诉flutter 是否需要重绘
  @override
  bool shouldRepaint(CustomPainter painter) {
    return true;
  }

  //绘制流程
  @override
  void paint(Canvas canvas, Size size) {
    /// y轴起点，外层整个进度条
    final baseOffset = size.height / 2 - style.height / 2.0;
    /// 已经播放的进度条
    final playedBaseOffset = size.height / 2 - style.playedHeight / 2.0;

    /// 外层进度条，包含未播放的部分
    canvas.drawRRect(
      RRect.fromRectAndRadius(
        Rect.fromPoints(
          /// 起点，左上角
          Offset(0.0, baseOffset),
          /// 终点，右下角
          Offset(size.width, baseOffset + style.height),
        ),
        Radius.circular(style.progressRadius),
      ),
      Paint()..color = style.backgroundColor,
    );
    if (!(vpc.value.isInitialized)) {
      return;
    }
    /// 已播放部分（比例）
    final double playedPartPercent =
        vpc.value.position.inMilliseconds / vpc.value.duration.inMilliseconds;
    /// 已播放部分（宽度）
    final double playedPart =
        playedPartPercent > 1 ? size.width : playedPartPercent * size.width;
    /// 缓冲部分UI
    for (DurationRange range in vpc.value.buffered) {
      final double start =
          range.startFraction(vpc.value.duration) *
              size.width;
      final double end =
          range.endFraction(vpc.value.duration) * size.width;
      canvas.drawRRect(
        RRect.fromRectAndRadius(
          Rect.fromPoints(
            Offset(start, baseOffset),
            Offset(end, baseOffset + style.height),
          ),
          Radius.circular(style.progressRadius),
        ),
        Paint()..color = style.bufferedColor,
      );
    }
    /// 已播放部分
    canvas.drawRRect(
      RRect.fromRectAndRadius(
        Rect.fromPoints(
          Offset(0.0, playedBaseOffset),
          Offset(playedPart, playedBaseOffset + style.playedHeight),
        ),
        Radius.circular(style.progressRadius),
      ),
      Paint()..color = style.playedColor,
    );

    /// 画滑动的圆形
    mPath = Path()
      ..addOval(Rect.fromCircle(
          center: Offset(playedPart, baseOffset + style.height / 2),
          radius: style.dragHeight));
    canvas.drawShadow(mPath, Colors.black, 0.2, true);
    canvas.drawCircle(
      Offset(playedPart, baseOffset + style.height / 2),
      style.dragHeight,
      Paint()..color = style.dragBarColor,
    );
  }
}
