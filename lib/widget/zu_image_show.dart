import 'package:flutter/material.dart';
import 'package:zyocore/widget/assets_picker/entity/ly_asset_entity.dart';
import 'package:zyocore/zyocore.dart';

@Deprecated('后面版本将弃用,请使用LYImageWidget显示图片')
class ZuImageShow extends StatelessWidget {
  ZuImageShow(
      {Key? key,
      required this.imgUrl,
      this.defaultUrl = "wei_bg.png",
      this.imgWidth,
      this.imgHeight,
      this.isShowDefault = true,
      this.fit = BoxFit.cover,
      this.headPortrait = false,
      this.assetEntity,
      this.package,
      this.radius = 0,
      this.isOriginal = false})
      : super(key: key);

  String? imgUrl;
  String defaultUrl;
  final double? imgWidth;
  final double? imgHeight;
  final bool isShowDefault;
  final BoxFit? fit;
  final double radius;
  String? package;
  bool headPortrait;
  LyAssetEntity? assetEntity;
  bool isOriginal;

  @override
  Widget build(BuildContext context) {
    return LYImageWidget(
      src: imgUrl,
      width: imgWidth,
      height: imgHeight,
      defaultUrl: defaultUrl,
      isShowDefault: isShowDefault,
      fit: fit,
      headPortrait: headPortrait,
      assetEntity: assetEntity,
      radius: radius,
      isOriginal: isOriginal,
      package: package,
    );
  }
}
