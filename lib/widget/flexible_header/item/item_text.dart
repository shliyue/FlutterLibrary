part of "../flexible_header.dart";

class FlexibleTextItem extends FlexibleItem {
  const FlexibleTextItem({
    required this.text,
    this.collapsedStyle,
    this.expandedStyle,
    this.maxLines,
    this.textScaleFactor,
    this.overflow,
    this.softWrap,
    this.locale,
    this.textDirection,
    this.textAlign,
    Alignment? alignment,
    Alignment? expandedAlignment,
    Alignment? collapsedAlignment,
    EdgeInsets? padding,
    EdgeInsets? expandedPadding,
    EdgeInsets? collapsedPadding,
    EdgeInsets? margin,
    EdgeInsets? expandedMargin,
    EdgeInsets? collapsedMargin,
    List<HeaderToolTransform> transform = const [],
    Key? key,
  }) : super(
          alignment: alignment,
          expandedAlignment: expandedAlignment,
          collapsedAlignment: collapsedAlignment,
          padding: padding,
          expandedPadding: expandedPadding,
          collapsedPadding: collapsedPadding,
          margin: margin,
          expandedMargin: expandedMargin,
          collapsedMargin: collapsedMargin,
          transform: transform,
          key: key,
        );

  final String text;

  final TextStyle? collapsedStyle;
  final TextStyle? expandedStyle;

  final int? maxLines;
  final double? textScaleFactor;
  final TextOverflow? overflow;
  final bool? softWrap;
  final Locale? locale;
  final TextDirection? textDirection;
  final TextAlign? textAlign;

  @override
  Widget onProgress(double progress) {
    return FlexibleTextItemWidget(this, progress);
  }
}

class FlexibleTextItemWidget extends StatelessWidget {
  const FlexibleTextItemWidget(
    this._item,
    this._progress, {
    Key? key,
  }) : super(key: key);

  final FlexibleTextItem? _item;
  final double _progress;

  @override
  Widget build(BuildContext context) {
    return AnimatedContainer(
      duration: const Duration(milliseconds: 100),
      padding: _item!.paddingValue(_progress),
      margin: _item!.marginValue(_progress),
      alignment: _item!.alignmentValue(_progress),
      child: Text(
        _item!.text,
        style: TextStyle.lerp(
          _item!.expandedStyle,
          _item!.collapsedStyle,
          _progress,
        ),
        maxLines: _item!.maxLines,
        textScaleFactor: _item!.textScaleFactor,
        overflow: _item!.overflow,
        softWrap: _item!.softWrap,
        locale: _item!.locale,
        textDirection: _item!.textDirection,
        textAlign: _item!.textAlign,
      ),
    );
  }
}
