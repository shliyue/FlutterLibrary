part of "../flexible_header.dart";

enum HeaderToolTransform {
  scale,
  hide,
}

class FlexibleItem extends IFlexibleProgress {
  const FlexibleItem({
    this.child,
    this.alignment,
    this.expandedAlignment,
    this.collapsedAlignment,
    this.padding,
    this.expandedPadding,
    this.collapsedPadding,
    this.margin,
    this.expandedMargin,
    this.collapsedMargin,
    this.transform = const [],
    Key? key,
  })  : assert(alignment == null ||
            (expandedAlignment == null && collapsedAlignment == null)),
        assert(padding == null ||
            (expandedPadding == null && collapsedPadding == null)),
        assert(margin == null ||
            (expandedMargin == null && collapsedMargin == null)),
        super(key: key);

  final Alignment? alignment;
  final Alignment? expandedAlignment;
  final Alignment? collapsedAlignment;

  final EdgeInsets? padding;
  final EdgeInsets? expandedPadding;
  final EdgeInsets? collapsedPadding;

  final EdgeInsets? margin;
  final EdgeInsets? expandedMargin;
  final EdgeInsets? collapsedMargin;

  final List<HeaderToolTransform> transform;

  final Widget? child;

  EdgeInsets? paddingValue(double progress) => EdgeInsets.lerp(
        expandedPadding ?? padding,
        collapsedPadding ?? padding,
        progress,
      );

  EdgeInsets? marginValue(double progress) => EdgeInsets.lerp(
        expandedMargin ?? margin,
        collapsedMargin ?? margin,
        progress,
      );

  Alignment? alignmentValue(double progress) => Alignment.lerp(
        expandedAlignment ?? alignment,
        collapsedAlignment ?? alignment,
        progress,
      );

  @override
  Widget onProgress(double progress) {
    return FlexibleItemWidget(this, progress);
  }
}

class FlexibleItemWidget extends StatelessWidget {
  const FlexibleItemWidget(
    this._item,
    this._progress, {
    Key? key,
  }) : super(key: key);

  final FlexibleItem? _item;
  final double _progress;

  @override
  Widget build(BuildContext context) {
    return AnimatedContainer(
      duration: const Duration(milliseconds: 100),
      padding: _item!.paddingValue(_progress),
      margin: _item!.marginValue(_progress),
      alignment: _item!.alignmentValue(_progress),
      child: _item!.child != null ? _wrapWithOpacity() : const SizedBox(),
    );
  }

  Widget? _wrapWithOpacity() {
    return _item!.transform.contains(HeaderToolTransform.hide)
        ? AnimatedOpacity(
            opacity: 1 - _progress,
            duration: const Duration(milliseconds: 150),
            child: _wrapWithScale())
        : _wrapWithScale();
  }

  Widget? _wrapWithScale() {
    return _item!.transform.contains(HeaderToolTransform.scale)
        ? Transform.scale(
            scale: Tween<double>(begin: 1, end: 0).transform(_progress),
            child: _item!.child,
          )
        : _item!.child;
  }
}
