part of "../flexible_header.dart";

class FlexibleGradientBackground extends IFlexibleProgress {
  const FlexibleGradientBackground({
    required this.gradient,
    this.modifyGradient = true,
    Key? key,
  }) : super(key: key);

  final Gradient gradient;
  final bool modifyGradient;

  @override
  Widget onProgress(double progress) {
    return FlexibleGradientBackgroundWidget(this, progress);
  }
}

class FlexibleGradientBackgroundWidget extends StatelessWidget {
  const FlexibleGradientBackgroundWidget(
    this._gradientBackground,
    this._progress, {
    Key? key,
  }) : super(key: key);

  final double _progress;
  final FlexibleGradientBackground? _gradientBackground;

  @override
  Widget build(BuildContext context) {
    return _gradientBackground!.modifyGradient
        ? ShaderMask(
            blendMode: BlendMode.src,
            shaderCallback: (bounds) {
              return _gradientBackground!.gradient.createShader(
                Rect.fromLTWH(
                  0,
                  0,
                  Tween<double>(begin: bounds.width, end: 0)
                      .transform(_progress),
                  Tween<double>(begin: bounds.height, end: 0)
                      .transform(_progress),
                ),
              );
            },
            child: Container(
              width: double.infinity,
              height: double.infinity,
              color: Colors.white,
            ),
          )
        : Container(
            width: double.infinity,
            height: double.infinity,
            decoration: BoxDecoration(
              gradient: _gradientBackground!.gradient,
            ),
          );
  }
}
