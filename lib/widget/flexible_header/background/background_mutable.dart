part of "../flexible_header.dart";

class FlexibleMutableBackground extends IFlexibleProgress {
  const FlexibleMutableBackground({
    this.expandedWidget,
    this.expandedColor,
    this.collapsedWidget,
    this.collapsedColor,
    this.animationDuration = const Duration(milliseconds: 150),
    Key? key,
  })  : assert(expandedColor == null || expandedWidget == null),
        assert(collapsedColor == null || collapsedWidget == null),
        super(key: key);

  final Widget? expandedWidget;
  final Widget? collapsedWidget;
  final Color? expandedColor;
  final Color? collapsedColor;
  final Duration animationDuration;

  @override
  Widget onProgress(double progress) {
    return FlexibleMutableBackgroundWidget(this, progress);
  }
}

class FlexibleMutableBackgroundWidget extends StatelessWidget {
  const FlexibleMutableBackgroundWidget(
    this._mutableBackground,
    this._progress, {
    Key? key,
  }) : super(key: key);

  final double _progress;
  final FlexibleMutableBackground? _mutableBackground;

  @override
  Widget build(BuildContext context) {
    return Stack(
      fit: StackFit.expand,
      children: [
        AnimatedOpacity(
          duration: _mutableBackground!.animationDuration,
          opacity: _progress,
          child: _buildWidget(
            _mutableBackground!.collapsedWidget,
            _mutableBackground!.collapsedColor,
          ),
        ),
        AnimatedOpacity(
          duration: _mutableBackground!.animationDuration,
          opacity: 1 - _progress,
          child: _buildWidget(
            _mutableBackground!.expandedWidget,
            _mutableBackground!.expandedColor,
          ),
        ),
      ],
    );
  }

  Widget _buildWidget(Widget? widget, Color? color) {
    return widget ??
        (color == null ? const SizedBox() : Container(color: color));
  }
}
