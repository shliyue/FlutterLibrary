import 'dart:math';

import 'package:flutter/material.dart';
import 'package:flutter/services.dart';

part 'item/item.dart';

part 'item/item_text.dart';

part 'background/background_gradient.dart';

part 'background/background_mutable.dart';

class FlexibleStyle {
  final SystemUiOverlayStyle? systemOverlayStyle;
  final IconThemeData? iconTheme;
  final IconThemeData? actionsIconTheme;

  FlexibleStyle({
    this.systemOverlayStyle,
    this.iconTheme,
    this.actionsIconTheme,
  });
}

typedef FlexibleBuilder = Widget Function(
  BuildContext context,
  double progress,
);

typedef StyleBuilder = FlexibleStyle Function(
  BuildContext context,
  double progress,
);

abstract class IFlexibleProgress extends Widget {
  const IFlexibleProgress({Key? key}) : super(key: key);

  Widget onProgress(double progress);

  @override
  Element createElement() =>
      throw Exception('Unable to wrap $this with other widgets');
}

class FlexibleHeaderDelegate extends SliverPersistentHeaderDelegate {
  FlexibleHeaderDelegate({
    this.collapsedHeight = kToolbarHeight,
    this.expandedHeight = kToolbarHeight * 3,
    this.leading,
    this.actions,
    this.title,
    this.children,
    this.backgroundColor,
    this.background,
    this.collapsedElevation = 0,
    this.expandedElevation = 0,
    this.statusBarHeight = 0,
    this.builder,
    this.styleBuilder,
  });

  final List<Widget>? actions;
  final Widget? leading;
  final Widget? title;

  final double expandedHeight;
  final double collapsedHeight;
  final List<Widget>? children;
  final Color? backgroundColor;
  final Widget? background;
  final double expandedElevation;
  final double collapsedElevation;
  final FlexibleBuilder? builder;
  final StyleBuilder? styleBuilder;

  final double statusBarHeight;

  @override
  Widget build(
    BuildContext context,
    double shrinkOffset,
    bool overlapsContent,
  ) {
    final offset = min(shrinkOffset, maxExtent - minExtent);
    final progress = offset / (maxExtent - minExtent);

    final style =
        styleBuilder == null ? null : styleBuilder!(context, progress);

    return Material(
      elevation: progress < 1 ? expandedElevation : collapsedElevation,
      child: Stack(
        fit: StackFit.expand,
        children: [
          if (background != null) transform(background!, progress),
          Column(
            children: [
              AppBar(
                backgroundColor: Colors.transparent,
                systemOverlayStyle: style?.systemOverlayStyle,
                actions: actions,
                leading: leading,
                title: title,
                iconTheme: style?.iconTheme,
                actionsIconTheme: style?.actionsIconTheme,
                elevation: 0,
                toolbarHeight: collapsedHeight,
              )
            ],
          ),
          if (builder != null) builder!(context, progress),
          if (children != null)
            ...children!.map((item) => transform(item, progress)).toList(),
        ],
      ),
    );
  }

  @override
  double get maxExtent => expandedHeight + statusBarHeight;

  @override
  double get minExtent => collapsedHeight + statusBarHeight;

  @override
  bool shouldRebuild(covariant SliverPersistentHeaderDelegate oldDelegate) =>
      true;

  Widget transform(Widget w, double progress) {
    if (w is IFlexibleProgress) {
      return w.onProgress(progress);
    }
    return w;
  }
}
