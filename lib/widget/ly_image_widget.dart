import 'dart:io';

import 'package:flutter/material.dart';
import 'package:zyocore/widget/assets_picker/entity/ly_asset_entity.dart';
import 'package:zyocore/zyocore.dart';

ImageProvider zuAssetImage(String imgName, {
  bool isExact = false,
  String? package,
}) {
  imgName = imgName.replaceAll("assets/images/", "");
  return isExact
      ? ExactAssetImage(
    "assets/images/$imgName",
    package: package,
  )
      : AssetImage(
    "assets/images/$imgName",
    package: package,
  );
}

/// 图片显示组件(支持网络图、资源图、本地图)
class LYImageWidget extends StatelessWidget {
  LYImageWidget({Key? key,
    required this.src,
    this.defaultUrl = "wei_bg.png",
    this.width,
    this.height,
    this.isShowDefault = true,
    this.fit = BoxFit.cover,
    this.headPortrait = false,
    this.assetEntity,
    this.package,
    this.radius = 0,
    this.bgColor,
    this.color,
    this.onLongPress,
    this.onTap,
    this.alignment = Alignment.center,
    this.isOriginal = false})
      : super(key: key);

  String? src;
  String defaultUrl;
  final double? width;
  final double? height;
  bool isShowDefault;
  final BoxFit? fit;
  final double? radius;
  String? package;
  bool headPortrait;
  LyAssetEntity? assetEntity;
  bool isOriginal;
  Color? bgColor;
  Color? color;
  final GestureTapCallback? onLongPress;
  final GestureTapCallback? onTap;
  final Alignment alignment;

  @override
  Widget build(BuildContext context) {
    ImageProvider? showImg;
    Widget? child;

    if (!TextUtil.isEmpty(src) || assetEntity != null) {
      var imgType = AppUtils().getFileType(src);
      switch (imgType) {
        case EnumFileType.network:
          child = networkImage(src ?? '');
          break;
        case EnumFileType.system:
          showImg = zuAssetImage(src!, package: package);
          isShowDefault = false;
          break;
        case EnumFileType.phone:
          showImg = FileImage(File(src!));
          break;
        case EnumFileType.asset:
          showImg = AssetEntityImageProvider(assetEntity!, isOriginal: isOriginal);
          break;
      }
    } else {
      showImg = zuAssetImage(defaultUrl, package: package ?? "zyocore");
    }
    if (child == null) {
      if (isShowDefault) {
        child = FadeInImage(
          image: showImg!,
          placeholder: zuAssetImage(defaultUrl, package: package ?? "zyocore"),
          imageErrorBuilder: _imageErrorBuilder,
          //占位图
          fadeOutDuration: const Duration(milliseconds: 200),
          fadeInDuration: const Duration(milliseconds: 1),
          fadeInCurve: Curves.easeIn,
          fadeOutCurve: Curves.easeOut,
          fit: fit,
          width: width,
          height: height,
          alignment: alignment,
        );
      } else {
        child = Image(
          image: showImg!,
          fit: fit,
          width: width,
          height: height,
          color: color,
          alignment: alignment,
        );
      }
    }
    return GestureDetector(
      onTap: onTap,
      onLongPress: onLongPress,
      child: Container(
        clipBehavior: Clip.hardEdge,
        decoration: BoxDecoration(
          color: bgColor,
          borderRadius: BorderRadius.all(
            Radius.circular(radius ?? 0),
          ),
        ),
        child: child,
      ),
    );
  }

  Widget _imageErrorBuilder(BuildContext context, Object error, StackTrace? stackTrace) {
    return errorWidget();
  }

  Widget errorWidget() {
    return Image(
      image: zuAssetImage(headPortrait ? "baby_avatar.png" : "image_placeholder_1.png", package: "zyocore"),
      fit: BoxFit.fill,
      width: width,
      height: height,
    );
  }

  /// 提供cacheNetWorkImageProvider
  CachedNetworkImage networkImage(String url) {
    return CachedNetworkImage(
      imageUrl: url,
      //占位图
      placeholder: !isShowDefault ? null : (context, url) {
        return Image(image: zuAssetImage(defaultUrl, package: package ?? "zyocore"));
      },
      errorWidget: (context, url, error) {
        return errorWidget();
      },
      // fadeOutDuration: const Duration(milliseconds: 200),
      // fadeInDuration: const Duration(milliseconds: 1),
      // fadeInCurve: Curves.easeIn,
      // fadeOutCurve: Curves.easeOut,
      fit: fit,
      width: width,
      height: height,
      alignment: alignment,
    );
  }
}
