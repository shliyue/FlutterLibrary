// ignore_for_file: public_member_api_docs, sort_constructors_first
import 'dart:async';

import 'package:flutter/foundation.dart';
import 'package:flutter/material.dart';
import 'package:flutter/services.dart';

import '../ly_cache_manager.dart';
import 'ly_avatar_image_cache_provider.dart';
import 'dart:ui' as ui show Codec;

class LyImageStreamCompleter extends ImageStreamCompleter {
  LyImageStreamCompleter({
    required this.key,
    required this.decode,
    required double scale,
    Stream<ImageChunkEvent>? chunkEvents,
    InformationCollector? informationCollector,
  }) : super();
  DecoderBufferCallback decode;
  LYAvatarImageProvider key;
  Stream<ui.Codec>? stream;
  StreamSubscription? subscription;
  // CacheObject cacheObj = CacheObject();
  DateTime? lastFetchTime;
  String? eTag;
  bool get needUpdate {
    if (lastFetchTime == null) {
      return true;
    }
    final liveDuration = LyCacheManager.single.cacheDuration;
    final isExpired = lastFetchTime!.add(liveDuration).isBefore(DateTime.now());
    return isExpired;
  }

  tryRefresh() async {
    doTry();
  }

  doTry() async {
    bool hasNew = true;
    if (key.url.isEmpty) {
      hasNew = false;
    }
    if (hasNew) {
      loadAndhandleData();
    }
  }

  void loadAndhandleData() {
    /// 如果cacheObject.imageData不为null且不需要更新的话
    if (!needUpdate) {
      return;
    }
    _loadAsync(key, decode);
  }

  void _loadAsync(
    LYAvatarImageProvider key,
    DecoderBufferCallback decode,
  ) async {
    if (requestIsOnRoad) return;
    requestIsOnRoad = true;
    try {
      /// 1. 直接从缓存返回
      // if (cacheObj != null && cacheObj!.cacheType == CacheType.cache) {
      //   assert(
      //       cacheObj!.imageData != null, '既然cache拿到了cacheObject，那么image数据必定存在');
      //   if (cacheObj?.imageInfo != null) {
      //     // setImage(cacheObj.imageInfo!);
      //   }
      // }

      /// 2. 更新最新数据（可能不更新）
      final imageData = await key.httpHelper.doFetch(key.url, completer: this);
      // key.cacheManager.putCacheObj(key.url, cacheObj);
      if (imageData != null) {
        final imageInfo = await convertToImageInfo(imageData);
        if (imageInfo != null) {
          // cacheObj.imageInfo = imageInfo;
          setImage(imageInfo);
        }
      }
    } finally {
      requestIsOnRoad = false;
    }
  }

  Future<ImageInfo?> convertToImageInfo(Uint8List? list) async {
    if (list == null) {
      return null;
    }
    final codec = await decode(await ImmutableBuffer.fromUint8List(list));
    // Logger.d("JMT - IMAGE convert ---");
    try {
      final _nextFrame = await codec.getNextFrame();
      final imageInfo = ImageInfo(
        image: _nextFrame.image.clone(),
      );
      _nextFrame.image.dispose();
      return imageInfo;
    } catch (exception) {
      throw ErrorDescription('resolving an image frame');
    }
  }

  bool requestIsOnRoad = false;
}
