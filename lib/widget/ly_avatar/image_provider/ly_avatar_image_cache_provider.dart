import 'dart:async';
import 'dart:ui' as ui show Codec, ImmutableBuffer, hashValues;

import 'package:flutter/foundation.dart';
import 'package:flutter/material.dart';

import '../http_helper.dart';
import '../ly_cache_manager.dart';
import 'ly_image_stream_completer.dart';

class LYAvatarImageProvider extends ImageProvider<LYAvatarImageProvider> {
  LYAvatarImageProvider(
    this.url, {
    required this.httpHelper,
    required this.cacheManager,
    this.scale = 1.0,
  });

  final String url;
  final double scale;
  final HttpHelper httpHelper;
  final LyCacheManager cacheManager;

  @override
  Future<LYAvatarImageProvider> obtainKey(ImageConfiguration configuration) {
    return SynchronousFuture(this);
  }

  @override
  void resolveStreamForKey(ImageConfiguration configuration, ImageStream stream,
      LYAvatarImageProvider key, ImageErrorListener handleError) {
    // This is an unusual edge case where someone has told us that they found
    // the image we want before getting to this method. We should avoid calling
    // load again, but still update the image cache with LRU information.
    if (stream.completer != null) {
      final ImageStreamCompleter completer = stream.completer!;
      if (completer is LyImageStreamCompleter) {
        completer.tryRefresh();
      }
      assert(identical(completer, stream.completer));
      return;
    }
    CompleterHandlerObject? handlerObject = cacheManager.getCacheObj(key.url);
    if (handlerObject == null) {
      final completer = loadBuffer(
          key, PaintingBinding.instance.instantiateImageCodecFromBuffer);
      handlerObject = CompleterHandlerObject(completer: completer);
    }
    LyImageStreamCompleter completer = handlerObject.completer;
    stream.setCompleter(completer);
    cacheManager.putCacheObj(key.url, handlerObject);
    completer.tryRefresh();
  }

  @override
  LyImageStreamCompleter loadBuffer(
      LYAvatarImageProvider key, DecoderBufferCallback decode) {
    return LyImageStreamCompleter(
      key: key,
      decode: decode,
      scale: key.scale,
      informationCollector: () sync* {
        yield DiagnosticsProperty<ImageProvider>(
          'Image provider: $this \n Image key: $key',
          this,
          style: DiagnosticsTreeStyle.errorProperty,
        );
      },
    );
  }

  @override
  bool operator ==(Object other) {
    if (other.runtimeType != runtimeType) {
      return false;
    }
    return other is LYAvatarImageProvider &&
        other.url == url &&
        other.scale == scale;
  }

  @override
  int get hashCode => ui.hashValues(url, scale);

  @override
  String toString() =>
      '${objectRuntimeType(this, 'LYAvatarImageProvider')}("$url", scale: $scale)';
}
