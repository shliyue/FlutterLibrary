import 'package:flutter/material.dart';
import 'package:zyocore/widget/ly_avatar/image_provider/ly_avatar_image_cache_provider.dart';
import 'ly_avatar_index.dart';
import 'ly_cache_manager.dart';

import 'http_helper.dart';

// ignore: must_be_immutable
class LYAvatarCacheImage extends StatelessWidget {
  LYAvatarCacheImage({
    Key? key,
    required String imageUrl,
    this.fit = BoxFit.cover,
    required this.placeholderBuilder,
    required this.errorWidgetBuilder,
  })  : _image = LYAvatarImageProvider(imageUrl,
            cacheManager: LyCacheManager.single,
            httpHelper: LYHttpHelper.single),
        super(key: key);
  final ImageProvider _image;
  final BoxFit fit;
  LYPlaceholderWidgetBuilder placeholderBuilder;
  LYLoadingErrorWidgetBuilder errorWidgetBuilder;
  @override
  Widget build(BuildContext context) {
    return Image(
      image: _image,
      gaplessPlayback: true,
      fit: fit,
      frameBuilder: (context, child, frame, wasSynchronouslyLoaded) {
        if (frame == null) {
          return placeholderBuilder(context, child);
        } else {
          return child;
        }
      },
      errorBuilder: (context, error, stackTrace) {
        return errorWidgetBuilder(context, error, stackTrace);
      },
    );
  }
}
