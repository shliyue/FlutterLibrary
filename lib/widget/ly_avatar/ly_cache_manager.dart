// ignore_for_file: public_member_api_docs, sort_constructors_first
import 'dart:async';

import 'package:flutter/material.dart';
import 'package:flutter/scheduler.dart';

import 'image_provider/ly_image_stream_completer.dart';

class LyCacheManager {
  LyCacheManager._();

  static LyCacheManager single = LyCacheManager._();
  final Map<String, CompleterHandlerObject> memCache = {};
  final int maxCount = 60;
  final Duration cacheDuration = const Duration(minutes: 30);

  CompleterHandlerObject? getCacheObj(String key) {
    return memCache[key];
  }

  void putCacheObj(String key, CompleterHandlerObject obj) {
    /// 移动到最新位置
    memCache.remove(key);
    obj.checkKeepAlive();
    memCache[key] = obj;
    Future.delayed(const Duration(seconds: 3), () {
      scheduleMicrotask(() {
        clean();
      });
    });
  }

  void evictImageByUrl(String url) {
    doClean(url);
  }

  void evictAll() {
    for (var key in memCache.keys) {
      doClean(key);
    }
  }

  clean() {
    while (memCache.length > maxCount) {
      final Object key = memCache.keys.first;
      doClean(key);
    }
  }

  doClean(Object key) {
    final obj = memCache[key];
    if (obj != null) {
      obj.clean();
    }
    memCache.remove(key);
  }
}

enum CacheType {
  cache,
  internet,
}

// class CacheObject {
//   String? eTag;
//   // Uint8List? imageData;
//   // ImageInfo? imageInfo;
//   // set imageInfo(ImageInfo value){
//   //   _imageInfo = value;
//   // }
//   // ImageInfo get imageInfo(){
//   //   _imageInfo
//   // }
//   // CacheType cacheType = CacheType.cache;
//   DateTime? lastFetchTime;

//   CacheObject({
//     this.eTag,
//     // this.imageData,
//   });
//   bool get needUpdate {
//     if (lastFetchTime == null) {
//       return true;
//     }
//     final liveDuration = LyCacheManager.single.cacheDuration;
//     final isExpired = lastFetchTime!.add(liveDuration).isBefore(DateTime.now());
//     return isExpired;
//   }
// }

class CompleterHandlerObject {
  ImageStreamCompleterHandle? handle;
  LyImageStreamCompleter completer;
  CompleterHandlerObject({
    this.handle,
    required this.completer,
  });

  checkKeepAlive() {
    handle ??= completer.keepAlive();
  }

  clean() {
    SchedulerBinding.instance.addPostFrameCallback((timeStamp) {
      handle?.dispose();
    });
  }
}
