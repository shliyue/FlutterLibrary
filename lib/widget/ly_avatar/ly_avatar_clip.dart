import 'package:flutter/material.dart';
import '../../core/configs/config.dart';
import '../../zyocore.dart';

class LyAvatarClip extends StatefulWidget {
  BuildContext? mContext;
  bool isOnlyPhoto = false;
  bool isCircle = true;
  String? avatar;
  Function? onCallBack;
  double? height;
  Color? bgColor;
  String? packageName;
  String? hintText;
  bool isEdit;
  int? maxWidth;
  bool compress;

  var showAvatarUrl = "".obs;

  LyAvatarClip({super.key,
    this.mContext,
    this.isOnlyPhoto = false,
    this.isCircle = true,
    this.isEdit = true,
    this.avatar,
    this.height,
    this.bgColor,
    this.packageName,
    this.hintText,
    this.onCallBack,
    this.maxWidth,
    this.compress = true,}) {
    showAvatarUrl.value = avatar ?? "";
  }

  @override
  State<LyAvatarClip> createState() => _LyAvatarClipState();

  /// 头像
  Future<void> onTapAvatarHandle() async {
    if (isEdit) {
      AssetUtil.onSelectAssets(
          context: mContext!,
          isOnlyPhoto: isOnlyPhoto,
          isClip: true,
          compress: compress,
          maxWidth: maxWidth,
          onCallBack: (v) {
            showAvatarUrl.value = v;
            if (onCallBack != null) {
              onCallBack!(showAvatarUrl.value);
            }
          });
    }
  }
}

class _LyAvatarClipState extends State<LyAvatarClip> {
  @override
  Widget build(BuildContext context) {
    widget.mContext = context;
    return _buildContentWidget();
  }

  /// top
  Widget _buildContentWidget() {
    return Container(
      color: widget.bgColor,
      width: double.infinity,
      height: widget.height ?? 250.w,
      alignment: Alignment.center,
      child: Stack(
        alignment: AlignmentDirectional.center,
        children: [
          _buildAvatarWidget(),
          widget.isEdit ? _buildAvatarCamera() : const SizedBox(),
        ],
      ),
    );
  }

  Widget _buildAvatarCamera() {
    return Positioned(
      right: 0,
      bottom: 0,
      child: GestureDetector(
        onTap: () => widget.onTapAvatarHandle(),
        child: Image.asset(
          'assets/images/camera_icon.png',
          width: 22.w,
          package: Config.packageName,
        ),
      ),
    );
  }

  Widget _buildAvatarWidget() {
    return GestureDetector(
      onTap: () => widget.onTapAvatarHandle(),
      child: Container(
        margin: EdgeInsets.all(6.w),
        alignment: Alignment.center,
        clipBehavior: widget.isCircle ? Clip.hardEdge : Clip.none,
        width: 72.w,
        height: 72.w,
        decoration: BoxDecoration(
          color: Colors.white,
          borderRadius: BorderRadius.all(
            Radius.circular(widget.isCircle ? 36.w : 0),
          ),
        ),
        child: Obx(() {
          return TextUtil.isEmpty(widget.showAvatarUrl.value)
              ? LYText(text: widget.hintText!, fontSize: 14.sp, color: const Color(0xFFBEBEBE))
              : LYImageWidget(
            src: widget.showAvatarUrl.value,
            height: 72.w,
            width: 72.w,
            package: widget.packageName ?? Config.packageName,
          );
          // LYAvatarWidget(src: widget.showAvatarUrl.value, width: 72.w, height: 72.w, radius: widget.isCircle
          //     ? 72.r : 0);
        }),
      ),
    );
  }
}
