import 'package:flutter/cupertino.dart';

import 'ly_avatar_cache_image.dart';
import 'ly_cache_manager.dart';

typedef LYPlaceholderWidgetBuilder = Widget Function(
  BuildContext context,
  Widget child,
);
typedef LYLoadingErrorWidgetBuilder = Widget Function(
  BuildContext context,
  Object error,
  StackTrace? stackTrace,
);

class LYAvatarIndex {
  LYAvatarIndex._();

  static Widget getAvatar(String src,
      {LYPlaceholderWidgetBuilder? placeholderBuilder,
      LYLoadingErrorWidgetBuilder? errorWidgetBuilder,
      BoxFit fit = BoxFit.cover}) {
    placeholderBuilder ??= (BuildContext context, Widget child) {
      return child;
    };
    errorWidgetBuilder ??=
        (BuildContext context, Object error, StackTrace? stackTrace) {
      return Container();
    };
    return LYAvatarCacheImage(
      imageUrl: src,
      fit: fit,
      placeholderBuilder: placeholderBuilder,
      errorWidgetBuilder: errorWidgetBuilder,
    );
    // child: CachedNetworkImage(
    //   imageUrl: src,
    //   cacheManager: AvatarCacheManager(),
    //   fit: BoxFit.cover,
    // ),
    // return ExtendedImage.network(
    //   src,
    //   fit: fit,
    //   cache: true,
    // );
  }

  static evictUrl(String url) {
    LyCacheManager.single.evictImageByUrl(url);
  }

  /// 当应用退出登录，重新登录的时候我们清空一下缓存
  static evictAll() {
    LyCacheManager.single.evictAll();
  }
}
