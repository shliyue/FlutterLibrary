import 'dart:io';

import 'package:flutter/foundation.dart';
import 'package:http/http.dart' as http;
import 'package:zyocore/widget/ly_avatar/image_provider/ly_image_stream_completer.dart';

abstract class HttpHelper {
  Future<Uint8List?> doFetch(String url,
      {Map<String, String>? headers,
      required LyImageStreamCompleter completer});
}

class LYHttpHelper extends HttpHelper {
  static LYHttpHelper single = LYHttpHelper._();

  LYHttpHelper._();

  // final http.Client _httpClient = http.Client();

  static const statusCodesNewFile = [HttpStatus.ok, HttpStatus.accepted];
  static const statusCodesFileNotChanged = [HttpStatus.notModified];

  @override
  Future<Uint8List?> doFetch(String url,
      {Map<String, String>? headers,
      required LyImageStreamCompleter completer}) async {
    final cacheObject = completer;
    // 判断是否需要更新数据，如果在过期时间内，则不更新
    // if (cacheObject.lastFetchTime != null) {
    //   final liveDuration = LyCacheManager.single.cacheDuration;
    //   final isExpired = cacheObject.lastFetchTime!.add(liveDuration).isBefore(DateTime.now());
    //   if (!isExpired) {
    //     return cacheObject;
    //   }
    // }
    final req = http.Request('GET', Uri.parse(url));
    if (headers != null) {
      req.headers.addAll(headers);
    }
    if (cacheObject.eTag != null) {
      req.headers[HttpHeaders.ifNoneMatchHeader] = cacheObject.eTag!;
    }
    // Logger.d(url + "发送请求", tag: "JMT - http_helper");
    try {
      final httpResponse = await http.Client().send(req);
      final newEtag = httpResponse.headers[HttpHeaders.etagHeader];
      int stautsCode = httpResponse.statusCode;
      bool hasNewFile = statusCodesNewFile.contains(stautsCode);
      final data = await httpResponse.stream.toBytes();
      cacheObject.lastFetchTime = DateTime.now();
      if (!hasNewFile || data.isEmpty) {
        // Logger.d(
        //     url +
        //         "没有新文件 statusCode:$stautsCode data-length:${data.length},header:${httpResponse.headers}",
        //     tag: "JMT - http_helper");
        return null;
      } else {
        // Logger.d(
        //     url +
        //         "有新文件 statusCode:$stautsCode data-length:${data.length},header:${httpResponse.headers}",
        //     tag: "JMT - http_helper");
      }
      cacheObject.eTag = newEtag;
      // cacheObject.cacheType = CacheType.internet;
      // cacheObject.imageData = data;
      return data;
    } catch (e) {
      // Logger.d("请求错误:$e", tag: "JMT - http_helper");
    } finally {}
  }
}
