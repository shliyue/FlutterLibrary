import 'package:flutter/material.dart';

import '../values/style.dart';

class LyButton extends StatelessWidget {
  const LyButton({
    Key? key,
    this.width,
    required this.height,
    required this.btnTitle,
    this.icon,
    this.alignment = Alignment.center,
    this.border,
    this.radius,
    this.gradient,
    this.btnTitleStyle,
    this.bgColor,
    this.textColor,
    this.fontFamily,
    this.padding,
    this.margin,
    this.boxShadow,
    this.onTap,
    this.mainAxisAlignment = MainAxisAlignment.center,
    this.fontSize = 14,
  }) : super(key: key);

  final double? width;
  final double height;
  final String btnTitle;
  final double? fontSize;
  final String? fontFamily;
  final Color? textColor;

  final AlignmentGeometry alignment;
  final BoxBorder? border;
  final double? radius;
  final Gradient? gradient;
  final TextStyle? btnTitleStyle;
  final EdgeInsetsGeometry? padding;
  final EdgeInsetsGeometry? margin;
  final Color? bgColor;
  final Widget? icon;
  final List<BoxShadow>? boxShadow;
  final Function()? onTap;
  final MainAxisAlignment mainAxisAlignment;

  @override
  Widget build(BuildContext context) {
    return InkWell(
      child: _getButton(),
      onTap: onTap,
    );
  }

  Widget _getButton() {
    var button = Container(
      width: width,
      height: height,
      alignment: alignment,
      padding: padding,
      margin: margin,
      decoration: BoxDecoration(
        border: border,
        borderRadius: BorderRadius.all(Radius.circular(radius ?? 0)),
        gradient: gradient,
        color: bgColor,
        boxShadow: boxShadow,
      ),
      child: Row(
        mainAxisAlignment: mainAxisAlignment,
        crossAxisAlignment: CrossAxisAlignment.center,
        children: [
          icon ?? const SizedBox(),
          Text(
            btnTitle,
            style: btnTitleStyle ?? TextStyle(fontFamily: fontFamily ?? AppFontFamily.pingFangRegular, fontSize: fontSize, color: textColor),
            strutStyle: const StrutStyle(leading: 0.2),
          ),
        ],
      ),
    );
    return button;
  }
}
