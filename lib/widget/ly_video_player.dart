import 'package:flutter/material.dart';
import 'package:zyocore/zyocore.dart';
import 'package:zyocore/widget/common/flex/row.dart';

import '../util/cache_manager_util.dart';

class LYVideoPlayer extends StatefulWidget {
  LYVideoPlayer({
    Key? key,
    required this.url,
    this.playBtnUrl = "play_video/video_play_blue",
    this.suspendBtnUrl = "play_video/video_suspend_blue",
    this.progressBtnUrl = "assets/images/play_video/bear_video.png",
    this.pauseBtnUrl = "play_video/button_pause",
    this.progressBgColor = Colors.white,
    this.progressColor = const Color(0xFF69CAFF),
    this.package = "zyocore",
    this.curTimeColor = const Color(0xFF69CAFF),
    this.totalTimeColor = Colors.white,
    this.progressBtnWidth = 24,
    this.progressBtnHeight = 21,
    this.fontSize = 12,
    this.porgressHeight = 3,
    this.btnHeight = 40,
    this.btnWidth = 40,
    this.endCallBack,
    this.onInitialize,
    this.progressMargin,
    this.pauseBtnWidth = 112,
    this.pauseBtnHeight = 112,
    this.isShowPauseBtn = false,
    this.hasDrag = true,
  }) : super(key: key);

  final Function()? endCallBack;
  final Function(VideoPlayerController)? onInitialize;
  final String url;
  final String playBtnUrl;
  final String suspendBtnUrl;
  final String progressBtnUrl;
  final Color progressBgColor;
  final Color progressColor;
  final Color curTimeColor;
  final Color totalTimeColor;
  final String? package;
  final double progressBtnWidth;
  final double progressBtnHeight;
  final double fontSize;
  final double porgressHeight;
  final double btnHeight;
  final double btnWidth;
  final EdgeInsetsGeometry? progressMargin;
  final String? pauseBtnUrl;
  final double pauseBtnWidth;
  final double pauseBtnHeight;
  final bool isShowPauseBtn;
  final bool hasDrag;

  @override
  State<LYVideoPlayer> createState() => _LYVideoPlayerState();
}

class _LYVideoPlayerState extends State<LYVideoPlayer> {
  late VideoPlayerController videoPlayerController;

  var progress = 0.0.obs;
  bool intermediatePause = false;
  String totalDuration = "0:00:00";
  var currentDuration = "0:00:00".obs;
  bool endPlay = false;
  var initFinish = false.obs;
  var isPlay = false.obs;
  var isBuffering = false.obs;

  @override
  void initState() {
    Logger.d('++++++++++++++视频初始化 initState+++++++++++++++++++++++++++++++');
    initVideoController();
    // TODO: implement initState
    super.initState();
  }

  initVideoController() async {
    var f = await CacheManagerUtil().getFileFromCache(widget.url);
    if (f == null) {
      if (!FileUtil.isM3U8Video(widget.url)) {
        bool flag = await LyPermission.storage();
        if (flag) {
          Logger.d('++++++++++++++LYVideoPlayer 视频缓存+++++++++++++++++++++++++++++++');
          CacheManagerUtil().downloadFile(widget.url).then((v) => Logger.d('++++++++++++++LYVideoPlayer 视频缓存 成功+++++++++++${v.file.path}++++++++++++++++++++'));
        }
      }
      videoPlayerController = VideoPlayerController.networkUrl(Uri.parse(widget.url));
    } else {
      Logger.d('++++++++++++++LYVideoPlayer 播放缓存++++++++++++${f.file.path}+++++++++++++++++++');
      videoPlayerController = VideoPlayerController.file(f.file);
    }
    videoPlayerController.initialize().then((_) {
      initFinish.value = true;
      var duration = videoPlayerController.value.duration;
      totalDuration = getFormatTime(duration);
      videoPlayerController.play();
      if (widget.onInitialize != null) {
        widget.onInitialize!(videoPlayerController);
      }
    });
    videoPlayerController.addListener(() {
      updateVideoPlayerState();
    });
  }

  String getFormatTime(Duration duration) {
    var h = duration.inHours;
    var m = (duration.inMinutes - h * 60).toString().padLeft(2, "0");
    var s = (duration.inSeconds % 60).toString().padLeft(2, "0");
    return "$h:$m:$s";
  }

  updateVideoPlayerState() {
    isPlay.value = videoPlayerController.value.isPlaying;
    isBuffering.value = videoPlayerController.value.isBuffering;
    if (!videoPlayerController.value.isPlaying) {
      intermediatePause = true;
    }
    if (videoPlayerController.value.duration.inSeconds > 0) {
      var position = videoPlayerController.value.position;
      var duration = videoPlayerController.value.duration;
      progress.value = position.inSeconds / duration.inSeconds;
      currentDuration.value = getFormatTime(position);
      if (!endPlay && position == duration && !isPlay.value && videoPlayerController.value.isInitialized && widget.endCallBack != null) {
        endPlay = true;
        widget.endCallBack!();
      }
    }
  }

  /// 播放点击
  void onPlayTap() {
    if (videoPlayerController.value.isPlaying) {
      videoPlayerController.pause();
    } else {
      videoPlayerController.play();
    }
  }

  @override
  void dispose() {
    videoPlayerController.dispose();
    // TODO: implement dispose
    super.dispose();
  }

  @override
  Widget build(BuildContext context) {
    Logger.d('++++++++++++++视频加载+++++++++++++++++++++++++++++++');
    return Stack(
      children: [
        GestureDetector(
          onTap: () {
            onPlayTap();
          },
          child: Container(
            alignment: Alignment.center,
            color: Colors.black,
            child: Obx(() {
              return initFinish.value
                  ? AspectRatio(
                      aspectRatio: videoPlayerController.value.aspectRatio,
                      child: VideoPlayer(videoPlayerController),
                    )
                  : const SizedBox();
            }),
          ),
        ),
        Obx(() {
          return (!isPlay.value && intermediatePause)
              ? Positioned.fill(
                  child: Center(
                    child: isBuffering.value
                        ? Loading(
                            tip: "视频加载中...",
                            width: 80.w,
                            height: 80.w,
                            bgColor: Colors.black,
                            radius: 8.w,
                          )
                        : (widget.isShowPauseBtn
                            ? LYButtonUtil.iconButton(
                                icon: widget.pauseBtnUrl,
                                width: widget.pauseBtnWidth.lywA,
                                height: widget.pauseBtnHeight.lywA,
                                package: widget.package,
                                onTap: () {
                                  onPlayTap();
                                },
                              )
                            : const SizedBox()),
                  ),
                )
              : const SizedBox();
        }),
        Positioned(
          left: 0,
          right: 0,
          bottom: 0,
          child: bottomWidget(),
        ),
      ],
    );
  }

  Widget bottomWidget() {
    return Container(
      height: 87.w,
      decoration: BoxDecoration(
        gradient: LinearGradient(
          begin: Alignment.topCenter,
          end: Alignment.bottomCenter,
          colors: [
            const Color(0xFFD8D8D8).withOpacity(0),
            const Color(0xFF000000).withOpacity(0.69),
          ],
        ),
      ),
      alignment: Alignment.bottomCenter,
      padding: EdgeInsets.only(bottom: 12.lywA),
      child: Container(
        height: 40.lywA,
        margin: widget.progressMargin ?? EdgeInsets.symmetric(horizontal: 12.lywA),
        child: Obx(() {
          return RSS(
            children: [
              LYButtonUtil.iconButton(
                  icon: isPlay.value ? widget.suspendBtnUrl : widget.playBtnUrl,
                  width: widget.btnWidth.lywA,
                  height: widget.btnHeight.lywA,
                  package: widget.package,
                  onTap: () {
                    onPlayTap();
                  }),
              SizedBox(width: 16.lywA),
              LyLinearProgress(
                progressBgColor: widget.progressBgColor,
                progressColor: widget.progressColor,
                progress: progress,
                hasDrag: widget.hasDrag,
                onDragEnd: (e) {
                  videoPlayerController.seekTo(videoPlayerController.value.duration * e);
                },
              ),
              SizedBox(width: 16.lywA),
              Container(
                alignment: Alignment.center,
                child: Obx(() {
                  return LYText.withStyle(
                    currentDuration.value,
                    style: TextStyle(fontSize: widget.fontSize.lywA, color: widget.curTimeColor),
                  );
                }),
              ),
              Container(
                alignment: Alignment.center,
                child: LYText.withStyle(
                  "/$totalDuration",
                  style: TextStyle(fontSize: widget.fontSize.lywA, color: widget.totalTimeColor),
                ),
              )
            ],
          );
        }),
      ),
    );
  }

  void resetController() {
    videoPlayerController.dispose();
    initFinish.value = false;
    endPlay = false;
    initVideoController();
  }

  @override
  void didUpdateWidget(covariant LYVideoPlayer oldWidget) {
    // TODO: implement didUpdateWidget
    super.didUpdateWidget(oldWidget);
    Logger.d('++++++++++++++didUpdateWidget+++++++++++2222222222222222222++++++++++++++++++++');
    resetController();
  }
}
