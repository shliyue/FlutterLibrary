import 'package:flutter/material.dart';
import 'package:flutter_screenutil/flutter_screenutil.dart';
import 'package:get/get.dart';
import 'package:zyocore/util/size_adapter_util.dart';

import '../util/logger.dart';
import 'ly_image_widget.dart';

class LyLinearProgress extends StatefulWidget {
  LyLinearProgress({
    Key? key,
    this.porgressHeight,
    required this.progressBgColor,
    required this.progressColor,
    required this.progress,
    this.progressBtnWidth = 24,
    this.progressBtnHeight = 21,
    this.package,
    this.progressBtnUrl = "assets/images/play_video/bear_video.png",
    this.onDragEnd,
    this.hasDrag = true,
  }) : super(key: key);

  final String progressBtnUrl;
  final double? porgressHeight;
  final Color progressBgColor;
  final Color progressColor;
  final int progressBtnWidth;
  final int progressBtnHeight;
  final String? package;
  Function(double)? onDragEnd;
  final bool hasDrag;

  final RxDouble progress;

  @override
  State<LyLinearProgress> createState() => _LyLinearProgressState();
}

class _LyLinearProgressState extends State<LyLinearProgress> {
  GlobalKey porgressKey = GlobalKey();
  double startDx = 0;
  late double startProgress;
  late double startLeft;
  double progressWidth = 0;

  var dragProgress = 0.0.obs;

  double getLeft(double progress) {
    var value = startLeft + progress * progressWidth;
    return value;
  }

  void _afterLayout(_) {
    var currentContext = porgressKey.currentContext;
    if (currentContext != null) {
      RenderBox renderBox = currentContext.findRenderObject() as RenderBox;
      if (renderBox.hasSize) {
        progressWidth = renderBox.size.width - widget.progressBtnWidth.w + 4;
        Logger.d("+++++++++progressWidth+++++++++$progressWidth+++++++++++++++++++");
      }
    }
  }

  @override
  void initState() {
    // TODO: implement initState
    super.initState();
    widget.progress.value = 0;
    startLeft = 0;
    startProgress = widget.progress.value;
    WidgetsBinding.instance.addPostFrameCallback(_afterLayout);
  }

  @override
  void didUpdateWidget(covariant LyLinearProgress oldWidget) {
    // TODO: implement didUpdateWidget
    super.didUpdateWidget(oldWidget);
    Logger.d("+++++++++didUpdateWidget++++++++++++++++++++++++++++");
    _afterLayout(null);
  }

  @override
  Widget build(BuildContext context) {
    return Expanded(
      child: Stack(
        clipBehavior: Clip.none,
        alignment: AlignmentDirectional.centerStart,
        children: [
          Center(
            child: Container(
              height: widget.porgressHeight,
              clipBehavior: Clip.hardEdge,
              decoration: BoxDecoration(
                borderRadius: BorderRadius.all(Radius.circular(2.w)),
              ),
              child: Obx(() {
                return LinearProgressIndicator(
                  key: porgressKey,
                  backgroundColor: widget.progressBgColor,
                  color: widget.progressColor,
                  value: dragProgress.value != 0 ? dragProgress.value : widget.progress.value,
                  // 如果进度是确定的，那么可以设置进度百分比，0-1
                  minHeight: widget.porgressHeight,
                );
              }),
            ),
          ),
          Obx(() {
            return Positioned(
              left: getLeft(dragProgress.value != 0 ? dragProgress.value : widget.progress.value),
              child: GestureDetector(
                onHorizontalDragDown: (details) {
                  if (!widget.hasDrag) {
                    return;
                  }
                  startDx = details.globalPosition.dx.w;
                  startProgress = widget.progress.value;
                },
                onHorizontalDragUpdate: (details) {
                  if (!widget.hasDrag) {
                    return;
                  }
                  double dx = details.globalPosition.dx.w - startDx;
                  dragProgress.value = dx / progressWidth.w + startProgress;
                  if (dragProgress.value < 0) {
                    dragProgress.value = 0;
                  } else if (dragProgress.value > 1) {
                    dragProgress.value = 1;
                  }
                },
                onHorizontalDragEnd: (d) {
                  if (!widget.hasDrag) {
                    return;
                  }
                  if (widget.onDragEnd != null) {
                    widget.onDragEnd!(dragProgress.value);
                  }
                  dragProgress.value = 0;
                },
                child: LYImageWidget(
                  src: widget.progressBtnUrl,
                  width: widget.progressBtnWidth.w,
                  height: widget.progressBtnHeight.w,
                  package: widget.package ?? "zyocore",
                  isShowDefault: false,
                ),
              ),
            );
          }),
        ],
      ),
    );
  }
}
