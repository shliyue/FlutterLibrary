import 'package:flutter/material.dart';
import 'package:flutter_screenutil/flutter_screenutil.dart';
import 'package:get/get.dart';
import 'package:zyocore/extension/color_extension.dart';
import 'package:zyocore/util/ly_button_util.dart';
import 'package:zyocore/values/index.dart';
import 'package:zyocore/widget/index.dart';

import '../core/configs/config.dart';
import '../core/enum/enum_icon_color.dart';

Widget ZUGradientButton(
  String text, {
  VoidCallback? onPressed,
  Color textColor = Colors.white,
  List<Color> colors = const [Color(0xff9F89FF), Color(0xff9F89FF)],
  double fontSize = 14,
  double radius = 5,
  double? width,
  double? height,
  EdgeInsetsGeometry? margin,
  EdgeInsetsGeometry padding = const EdgeInsets.fromLTRB(16, 7, 16, 7),
  bool enable = true,
}) {
  return GestureDetector(
    onTap: enable ? onPressed : null,
    child: Opacity(
      opacity: enable ? 1 : 0.3,
      child: Container(
        width: width,
        height: height,
        margin: margin,
        padding: padding,
        alignment: Alignment.center,
        clipBehavior: Clip.hardEdge,
        decoration: BoxDecoration(
          borderRadius: BorderRadius.all(Radius.circular(radius.r)),
          gradient: LinearGradient(colors: colors),
        ),
        child: Text(
          text,
          style: TextStyle(fontSize: fontSize.sp, color: textColor),
        ),
      ),
    ),
  );
}

Widget ZUTextButton({
  String? text,
  String? image,
  Widget? icon,
  VoidCallback? onPressed,
  Color color = const Color(0xFF1A1A1A),
  double fontSize = 14,
  double width = 24,
  double height = 24,
  FontWeight? fontWeight,
  double? space,
  bool rtl = false,
  bool enable = true,
  String? package,
  TextStyle? fontStyle,
  double? overAllheight,
}) {
  List<Widget> _buildChild() {
    List<Widget> list = <Widget>[];

    if (text != null && text.isNotEmpty) {
      list.add(LYText.withStyle(
        text,
        style: fontStyle ??
            TextStyle(
              fontSize: fontSize,
              fontWeight: fontWeight,
              color: color,
            ),
      ));
    }

    if (image != null && image.isNotEmpty) {
      // Image img = Image(
      //   image: AssetImage(
      //     "assets/images/$image",
      //     package: package,
      //   ),
      //   width: width.w,
      //   height: height.w,
      // );
      Widget img = LYAssetsImage(
        name: "assets/images/$image",
        package: package,
        width: width.w,
        height: height.w,
      );

      if (rtl) {
        list.insert(0, img);
      } else {
        list.add(img);
      }
    } else if (icon != null) {
      if (rtl) {
        list.insert(0, icon);
      } else {
        list.add(icon);
      }
    }

    if (list.length == 2 && space != null) {
      list.insert(1, SizedBox(width: space));
    }

    return list;
  }

  return InkWell(
    child: SizedBox(
      height: overAllheight,
      child: Row(
        mainAxisAlignment: MainAxisAlignment.center,
        children: _buildChild(),
      ),
    ),
    onTap: enable ? onPressed : null,
  );
}

///返回按钮
Widget zyoBackButton({
  EnumIconColor? enumIconColor = EnumIconColor.black,
  VoidCallback? backTap, //自定义返回事件
  EdgeInsetsGeometry? padding,
  double left = 16,
}) {
  String imgName = "";
  switch (enumIconColor) {
    case EnumIconColor.black:
      imgName = "assets/images/navi_back_icon.png";
      break;
    case EnumIconColor.white:
      imgName = "assets/images/navi_back_white.png";
      break;
    default:
      imgName = "assets/images/navi_back_icon.png";
      break;
  }
  padding ??= EdgeInsets.only(left: left, top: 8, right: 16, bottom: 8);
  return IconButton(
    alignment: Alignment.centerLeft,
    padding: padding,
    icon: Image.asset(
      imgName,
      width: 12.w,
      height: 24.w,
      fit: BoxFit.contain,
      package: Config.packageName,
    ),
    onPressed: () {
      if (backTap == null) {
        Get.back();
      } else {
        backTap();
      }
    },
  );
}

class LyButtonBorderWidget extends StatelessWidget {
  const LyButtonBorderWidget({
    Key? key,
    this.text,
    this.tstyle,
    this.bgcolor,
    this.width,
    this.height,
    this.border = true,
    this.borderRadius,
    this.enable = true,
    this.onTap,
  }) : super(key: key);

  final String? text;
  final TextStyle? tstyle;
  final Color? bgcolor;
  final double? width;
  final double? height;
  final bool border;
  final double? borderRadius;
  final bool enable; //是否启用
  final GestureTapCallback? onTap;

  @override
  Widget build(BuildContext context) {
    Color color = bgcolor ?? LibColor.colorMain;
    double btnH = height ?? 44.w;
    return LYButtonUtil.customButton(
      Container(
          width: width ?? 165.w,
          height: btnH,
          decoration: !border
              ? BoxDecoration(
                  border: Border.all(color: color, width: 1.w),
                  borderRadius: BorderRadius.circular(borderRadius ?? btnH / 2),
                )
              : BoxDecoration(
                  color: enable ? color : color.withOpacity(0.2),
                  borderRadius: BorderRadius.circular(borderRadius ?? btnH / 2),
                ),
          child: Center(
            child: Text(
              text ?? "确认",
              style: tstyle ?? TextStyle(fontSize: 15.sp, fontFamily: AppFontFamily.pingFangRegular, color: Colors.white),
              // strutStyle: const StrutStyle(leading: 0.6),
            ),
          )),
      onTap: onTap,
    );
  }
}
