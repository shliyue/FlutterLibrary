import 'package:flutter/material.dart';
import 'ly_assets_image.dart';

@Deprecated('后面版本将弃用,请使用LYAssetsImage显示资源图片')
class LYImage extends StatelessWidget {
  final String name;
  final double? radius;
  final double? width;
  final double? height;
  final BoxFit fit;
  final String? package;
  final Color? color;

  const LYImage(
      {Key? key, required this.name, this.radius, this.width, this.height, this.fit = BoxFit.fill, this.package, this.color})
      : super(key: key);

  @override
  Widget build(BuildContext context) {
    return LYAssetsImage(
      name: name,
      width: width,
      height: height,
      radius: radius,
      fit: fit,
      color: color,
      package: package,
    );
  }
}
