import 'package:flutter/material.dart';
import 'package:flutter/widgets.dart';
import 'package:get/get_state_manager/src/rx_flutter/rx_obx_widget.dart';
import 'package:skeletons/skeletons.dart';
import 'package:zyocore/zyocore.dart';

class LYSkeletonWrapper extends StatelessWidget {
  final Widget child;

  /// 必须是obs的变量
  RxBool loaded = false.obs;

  /// 骨架图组件
  final Widget? skeleton;

  LYSkeletonWrapper(
      {Key? key, required this.child, required this.loaded, this.skeleton})
      : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Obx(() {
      return Column(
        children: [
          Expanded(
              child: Skeleton(
            isLoading: !loaded.value,
            skeleton: skeleton ?? SkeletonListView(),
            child: child,
            themeMode: ThemeMode.light,
          ))
        ],
      );
    });
  }
}
