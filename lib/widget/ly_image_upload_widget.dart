import 'package:flutter/material.dart';
import 'package:flutter_screenutil/flutter_screenutil.dart';
import 'package:photo_manager_image_provider/photo_manager_image_provider.dart';
import 'package:zyocore/values/color.dart';

import '../core/configs/config.dart';
import '../util/logger.dart';
import '../values/edge_insets.dart';

///图片上传组件
class LyUploadImageWidget extends StatelessWidget {
  List images;
  int maxLength;
  bool canEdit;
  VoidCallback? addCallBack;
  Function(int)? delCallBack;
  Function(int)? onTapItemHandle;
  Function()? onDragStarted;
  Function()? onDraggableCanceled;

  LyUploadImageWidget({
    Key? key,
    required this.images,
    this.maxLength = 6,
    this.addCallBack,
    this.onTapItemHandle,
    this.onDragStarted,
    this.onDraggableCanceled,
    this.delCallBack,
    this.canEdit = true,
  }) : super(key: key);

  @override
  Widget build(BuildContext context) {
    int itemCount = (images.length >= maxLength)
        ? maxLength
        : canEdit
            ? images.length + 1
            : images.length;

    var row = (itemCount ~/ 4) + (itemCount % 4 == 0 ? 0 : 1);

    var itemW = ((375 - 32 - 15) / 4);

    var imageW = itemW - 5;

    var height = row * itemW + (row - 1) * 5;

    return SizedBox(
      height: height.w,
      child: GridView.builder(
        padding: EdgeInsets.zero,
        physics: const NeverScrollableScrollPhysics(),
        itemCount: itemCount,
        //SliverGridDelegateWithFixedCrossAxisCount 构建一个横轴固定数量Widget
        gridDelegate: SliverGridDelegateWithFixedCrossAxisCount(
          //横轴元素个数
          crossAxisCount: 4,
          //纵轴间距
          mainAxisSpacing: 5.w,
          //横轴间距
          crossAxisSpacing: 5.w,
          childAspectRatio: 1,
        ),
        itemBuilder: (BuildContext context, int index) {
          //Widget Function(BuildContext context, int index)
          if (index >= images.length) {
            return GestureDetector(
                onTap: addCallBack,
                child: Container(
                  width: itemW,
                  height: itemW,
                  alignment: Alignment.bottomLeft,
                  clipBehavior: Clip.hardEdge,
                  decoration: BoxDecoration(
                    borderRadius: BorderRadius.circular(5.w),
                  ),
                  child: Container(
                      clipBehavior: Clip.hardEdge,
                      margin: bottom5,
                      decoration: BoxDecoration(borderRadius: BorderRadius.circular(5.w), color: lightF8F8F8),
                      width: imageW.w,
                      height: imageW.w,
                      child: Image(
                        width: 20.w,
                        height: 20.w,
                        image: AssetImage("assets/images/add_img.png", package: Config.packageName),
                      )),
                ));
          }
          return buildGridItem(context, index, itemW, imageW);
        },
      ),
    );
  }

  Widget buildImageWidget({required int index, required double itemW, required double imageW}) {
    return Stack(children: [
      Container(
          width: itemW.w,
          height: itemW.w,
          padding: delCallBack == null ? null : EdgeInsets.only(top: 5.w, right: 5.w),
          alignment: Alignment.bottomLeft,
          child: Container(
            clipBehavior: Clip.hardEdge,
            decoration: BoxDecoration(
              borderRadius: BorderRadius.circular(5.w),
            ),
            child: Image(
              width: imageW.w,
              height: imageW.w,
              fit: BoxFit.cover,
              image: AssetEntityImageProvider(images[index], isOriginal: false),
            ),
          )),
      Visibility(
        visible: delCallBack != null,
        child: Positioned(
          top: 0,
          right: 0,
          child: GestureDetector(
            onTap: () => delCallBack!(index),
            child: Container(
              clipBehavior: Clip.hardEdge,
              decoration: BoxDecoration(borderRadius: BorderRadius.circular(10.r)),
              child: Image(
                image: const AssetImage(
                  "assets/images/icon_del.png",
                  package: "zyocore",
                ),
                width: 20.r,
                height: 20.r,
              ),
            ),
          ),
        ),
      ),
    ]);
  }

  Widget buildGridItem(BuildContext context, int index, double itemW, double imageW) {
    return Draggable(
      data: images[index],
      onDragCompleted: () {
        Logger.d("on---onDragCompleted");
      },
      onDragEnd: (d) {
        Logger.d("on---onDragEnd--${d}");
      },
      onDragUpdate: (v) {
        Logger.d("on---onDragUpdate--${v}");
      },
      onDragStarted: () {
        if (onDragStarted != null) {
          onDragStarted!();
        }
        Logger.d("on---onDragStarted--");
      },
      onDraggableCanceled: (v, f) {
        if (onDraggableCanceled != null) {
          onDraggableCanceled!();
        }
        Logger.d("on---onDraggableCanceled--${v}---${f}");
      },
      child: GestureDetector(
        onTap: onTapItemHandle == null ? null : () => onTapItemHandle!(index),
        child: buildImageWidget(index: index, itemW: itemW, imageW: imageW),
      ),
      feedback: SizedBox(width: 100.w, height: 100.w, child: buildImageWidget(index: index, itemW: itemW, imageW: imageW)),
    );
  }
}
