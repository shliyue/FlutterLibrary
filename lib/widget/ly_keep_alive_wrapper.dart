import 'package:flutter/rendering.dart';
import 'package:flutter/widgets.dart';

class LYKeepAliveWrapper extends StatefulWidget {
  final Widget child;

  const LYKeepAliveWrapper({Key? key, required this.child})
      : super(key: key);

  @override
  __KeepAliveWrapperState createState() => __KeepAliveWrapperState();
}

class __KeepAliveWrapperState extends State<LYKeepAliveWrapper>
    with AutomaticKeepAliveClientMixin {
  @override
  Widget build(BuildContext context) {
    super.build(context);
    return widget.child;
  }

  @override
  bool get wantKeepAlive => true;
}
