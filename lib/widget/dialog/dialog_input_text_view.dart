import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:zyocore/values/style.dart';
import 'package:zyocore/zyocore.dart';

class DialogInputTextView extends StatefulWidget {
  final String? title;
  final String? hinText;
  final Function(String)? callback;
  final String? desc;
  final Widget? descWidget;
  final String? noEmptyDesc;
  final int? maxLength;
  final List<TextInputFormatter>? inputFormatters;
  final bool? defaultReturn;
  final String? rightTitle;
  final double? height;
  final bool? showCount;
  final int? maxLines;
  final TextInputType? inputType;

  DialogInputTextView(
      {Key? key,
      required this.title,
      required this.hinText,
      required this.callback,
      this.desc,
      this.descWidget,
      this.noEmptyDesc,
      this.maxLength,
      this.inputFormatters,
      this.defaultReturn = true,
      this.rightTitle,
      this.height,
      this.showCount = false,
      this.maxLines = 1,
      this.inputType})
      : super(key: key);

  @override
  State<DialogInputTextView> createState() => _DialogInputTextViewState();
}

class _DialogInputTextViewState extends State<DialogInputTextView> {
  late TextEditingController tec;

  @override
  void initState() {
    // TODO: implement initState
    tec = TextEditingController();
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    return Center(
      child: Container(
        width: 282.w,
        height: widget.height ?? 188.w,
        decoration: BoxDecoration(
          color: Colors.white,
          borderRadius: BorderRadius.all(
            Radius.circular(8.r),
          ),
        ),
        child: Column(
          children: [
            Expanded(
              child: Column(
                mainAxisAlignment: MainAxisAlignment.center,
                children: [
                  Container(
                    margin: EdgeInsets.symmetric(horizontal: 16.w),
                    child: Center(
                      child: Text(
                        widget.title ?? '',
                        style: TextStyle(
                          fontSize: 17.sp,
                          color: dark1A1A1A,
                          fontFamily: AppFontFamily.pingFangMedium,
                        ),
                        maxLines: 3,
                        textAlign: TextAlign.center,
                      ),
                    ),
                  ),
                  if ((widget.desc ?? '').isNotEmpty || widget.descWidget != null)
                    Padding(
                      padding: EdgeInsets.only(top: 8.w),
                      child: widget.descWidget ??
                          Text(
                            widget.desc ?? '',
                            style: TextStyle(
                              fontSize: 12.sp,
                              color: dark333333,
                              fontFamily: AppFontFamily.pingFangRegular,
                            ),
                            maxLines: 3,
                            textAlign: TextAlign.center,
                          ),
                    ),
                  SizedBox(
                    height: 16.w,
                  ),
                  Container(
                    margin: EdgeInsets.symmetric(horizontal: 16.w),
                    padding: EdgeInsets.symmetric(horizontal: 12.w),
                    decoration: BoxDecoration(
                      borderRadius: BorderRadius.circular(
                        8.r,
                      ),
                      color: lightF1F1F1,
                    ),
                    child: TextField(
                      maxLines: widget.maxLines,
                      controller: tec,
                      maxLength: widget.maxLength,
                      keyboardType: widget.inputType ?? TextInputType.text,
                      decoration: InputDecoration(
                        counterText: '',
                        border: InputBorder.none,
                        hintText: widget.hinText,
                        hintStyle: TextStyle(
                          color: dark999999,
                          fontSize: 14.sp,
                          fontFamily: AppFontFamily.pingFangRegular,
                        ),
                      ),
                      inputFormatters: widget.inputFormatters,
                      onChanged: (v) {
                        setState(() {});
                      },
                    ),
                  ),
                  widget.showCount == true
                      ? Container(
                          padding: EdgeInsets.only(right: 16.w, top: 4.w),
                          child: Row(
                            mainAxisAlignment: MainAxisAlignment.end,
                            children: [
                              LYText(
                                color: dark999999,
                                text: tec.text.characters.length.toString(),
                              ),
                              LYText(
                                color: dark999999,
                                text: "/${widget.maxLength}",
                              )
                            ],
                          ),
                        )
                      : Container()
                ],
              ),
            ),
            Container(
              height: 56.w,
              decoration: BoxDecoration(
                color: lightF8F8F8,
                borderRadius: BorderRadius.all(
                  Radius.circular(8.r),
                ),
              ),
              child: Row(
                children: [
                  Expanded(
                    child: GestureDetector(
                      onTap: () => Get.back(),
                      child: Container(
                        decoration: BoxDecoration(
                          color: lightF8F8F8,
                          borderRadius: BorderRadius.only(bottomLeft: Radius.circular(8.r)),
                        ),
                        alignment: Alignment.center,
                        child: LYText(text: '取消', fontSize: 16.sp, color: dark666666),
                      ),
                    ),
                  ),
                  Container(
                    height: 20.w,
                    width: 1,
                    color: lightD2D2D2,
                  ),
                  Expanded(
                    child: GestureDetector(
                      onTap: () {
                        if (tec.text.trim().isEmpty) {
                          ToastUtil.showToast(widget.noEmptyDesc ?? '离职原因不能为空');
                          return;
                        }
                        if (widget.inputType == TextInputType.number && !tec.text.isNumericOnly) {
                          ToastUtil.showToast("只能输入数字");
                          return;
                        }
                        if (widget.defaultReturn!) {
                          Get.back();
                        }
                        widget.callback!(tec.text);
                      },
                      child: Container(
                        decoration: BoxDecoration(
                          color: lightF8F8F8,
                          borderRadius: BorderRadius.only(bottomRight: Radius.circular(8.r)),
                        ),
                        alignment: Alignment.center,
                        child: LYText(text: widget.rightTitle ?? '确认', fontSize: 16.sp, color: dark333333),
                      ),
                    ),
                  ),
                ],
              ),
            )
          ],
        ),
      ),
    );
  }
}
