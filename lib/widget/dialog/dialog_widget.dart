import 'package:flutter/material.dart';
import 'package:flutter_screenutil/flutter_screenutil.dart';
import 'package:zyocore/util/utils.dart';
import 'package:zyocore/values/circular_radius.dart';
import 'package:zyocore/values/color.dart';
import 'package:zyocore/values/style.dart';
import 'package:zyocore/values/text.dart';
import 'package:zyocore/widget/common/flex/column.dart';
import 'package:zyocore/widget/common/flex/row.dart';

import '../../util/logger.dart';
import '../text/ly_text.dart';

class DialogButton {
  final String text;
  final Function? callback;
  final bool autoClose;
  TextStyle? style;

  DialogButton(this.text, {this.callback, this.autoClose = true, this.style});
}

/// 普通的带按钮的dialog弹窗
class DialogWidget extends StatelessWidget {
  final bool? showTitle;
  final String? title;
  final String? content;
  final TextAlign contentTextAlign;
  final Widget? contentWidget;
  final TextStyle? contentStyle;
  final DialogButton? submit;
  final DialogButton? cancel;

  const DialogWidget({
    Key? key,
    this.showTitle,
    this.title,
    this.content,
    this.contentStyle,
    this.contentTextAlign = TextAlign.center,
    this.cancel,
    this.submit,
    this.contentWidget,
  }) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Center(
      child: Container(
        clipBehavior: Clip.hardEdge,
        width: MediaQuery.of(context).orientation == Orientation.portrait ? utils.style.screenWidth * 0.8 : 600.h,
        decoration: BoxDecoration(
          borderRadius: circular8,
          color: lightF8F8F8,
        ),
        child: Container(
          color: lightFFFFFF,
          child: CSC(
            mainAxisSize: MainAxisSize.min,
            children: <Widget>[
              Container(
                alignment: Alignment.center,
                padding: EdgeInsets.symmetric(vertical: 32.w, horizontal: 28.w),
                child: Column(
                  children: <Widget>[
                    showTitle ?? false
                        ? Container(
                            alignment: Alignment.center,
                            margin: EdgeInsets.only(bottom: 11.w),
                            child: LYText.withStyle(
                              title!,
                              style: TextStyle(color: dark333333, fontFamily: AppFontFamily.pingFangMedium, fontSize: 17.sp, decoration: TextDecoration.none),
                              maxLines: 5,
                            ),
                          )
                        : Container(),
                    Container(
                      alignment: Alignment.center,
                      child: contentWidget ??
                          LYText.withStyle(
                            content ?? "",
                            textAlign: contentTextAlign,
                            style: contentStyle ?? text15.dark,
                            maxLines: 5,
                          ),
                    ),
                  ],
                ),
              ),
              _buildConfirm(context),
            ],
          ),
        ),
      ),
    );
  }

  Widget _buildConfirm(BuildContext context) {
    if (cancel != null && submit != null) {
      Widget cancelBtn = _buildButton(
        context,
        cancel!,
        utils.style.screenWidth * 0.3,
        cancel: true,
      );
      Widget submitBtn = _buildButton(context, submit!, utils.style.screenWidth * 0.3);
      return RSC(
        children: [cancelBtn, div, submitBtn],
      );
    } else if (cancel != null) {
      Widget cancelBtn = _buildButton(context, cancel!, utils.style.screenWidth * 0.6, cancel: true);
      return RSS(
        children: [cancelBtn],
      );
    } else if (submit != null) {
      Widget submitBtn = _buildButton(context, submit!, utils.style.screenWidth * 0.6);
      return RSS(
        children: [submitBtn],
      );
    }
    return Container();
  }

  Widget _buildButton(BuildContext context, DialogButton button, double width, {bool cancel = false, Border? border}) {
    button.style ??= cancel
        ? const TextStyle(fontSize: 17, decoration: TextDecoration.none).light.familyMedium
        : const TextStyle(fontSize: 17, decoration: TextDecoration.none).dark.textColor(dark333333);
    return Expanded(
      child: Container(
        height: 56,
        decoration: BoxDecoration(border: border, color: lightF8F8F8),
        child: GestureDetector(
          behavior: HitTestBehavior.opaque,
          onTap: () async {
            try {
              if (button.autoClose) {
                Navigator.of(context).pop(cancel ? false : true);
              }
              if (button.callback != null) {
                await button.callback!();
              }
            } catch (e) {
              Logger.d("==dialog======$e=======");
            }
          },
          child: Center(
            child: LYText.withStyle(button.text, style: button.style),
          ),
        ),
      ),
    );
  }

  Widget get div {
    return Container(
      alignment: Alignment.center,
      height: 56,
      color: lightF8F8F8,
      child: Container(
        decoration: BoxDecoration(
          color: lightE1E1E1,
          borderRadius: BorderRadius.circular(1),
        ),
        height: 20,
        width: 1,
      ),
    );
  }
}
