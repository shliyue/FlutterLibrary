import 'package:flutter/material.dart';
import 'package:get/get.dart';
import 'package:zyocore/util/file_util.dart';
import 'package:zyocore/util/index.dart';
import 'package:zyocore/values/style.dart';
import 'package:zyocore/widget/text/ly_text.dart';
import 'package:zyocore/widget/ly_text_button.dart';

import '../../core/configs/config.dart';
import '../../values/color.dart';

class DownlaodProgressDialog extends StatefulWidget {
  final String fileSrc;

  /// 是否下载原图
  final bool original;

  const DownlaodProgressDialog(
      {Key? key, required this.fileSrc, this.original = false})
      : super(key: key);

  @override
  State<DownlaodProgressDialog> createState() => _DownlaodProgressDialogState();
}

class _DownlaodProgressDialogState extends State<DownlaodProgressDialog> {
  double progress = 0.0;
  int doneCount = 0;

  /// 0 : 未开始 1：下载中 2：完成 3 : 失败
  int status = 0;

  @override
  void initState() {
    super.initState();
    downloadHandle();
  }

  void downloadHandle() async {
    setState(() {
      status = 1;
    });
    try {
      if (FileUtil.isVideo(widget.fileSrc)) {
        await LyDownloadUtil.downloadNetVideo(widget.fileSrc,
            onReceiveProgress: (count, total) {
          progress = count / total;
          setState(() {});
        });
        setState(() {
          status = 2;
        });
      } else {
        await LyDownloadUtil.downloadNetImage(widget.fileSrc,
            original: widget.original, onReceiveProgress: (count, total) {
          progress = count / total;
          setState(() {});
        });
        setState(() {
          status = 2;
        });
      }
    } catch (e) {
      setState(() {
        status = 3;
      });

      Get.back();
    }
  }

  @override
  Widget build(BuildContext context) {
    return Column(
      mainAxisSize: MainAxisSize.min,
      children: [
        Image.asset(
          'assets/images/dialog_download_bg.png',
          height: 94.0,
          package: Config.packageName,
        ),
        if (status == 2) _buildLoadDoneText(),
        if (status != 2) _buildLoadingText(),
        const SizedBox(
          height: 24.0,
        ),
        LYTextButton(
          text: status == 2 ? '好的' : '停止下载',
          fontSize: 15.0,
          color: Colors.white,
          width: 196.0,
          height: 44.0,
          onTap: onTapBtnHandle,
          boxShadow: const [],
        ),
        const SizedBox(
          height: 24.0,
        ),
      ],
    );
  }

  /// 下载中
  Widget _buildLoadingText() {
    return Container(
      padding: const EdgeInsets.only(top: 24.0),
      child: Column(
        mainAxisSize: MainAxisSize.min,
        children: [
          RichText(
            text: TextSpan(
                text: '已下载',
                style: TextStyle(
                  fontSize: 15.0,
                  color: dark191919,
                  fontFamily: AppFontFamily.pingFangRegular,
                ),
                children: [
                  TextSpan(
                    text: doneCount.toString(),
                    style: TextStyle(
                      fontSize: 15.0,
                      color: redFF5B59,
                      fontFamily: AppFontFamily.pingFangRegular,
                    ),
                  ),
                  TextSpan(
                    text: '个，总下载',
                    style: TextStyle(
                      fontSize: 15.0,
                      color: dark191919,
                      fontFamily: AppFontFamily.pingFangRegular,
                    ),
                  ),
                  TextSpan(
                    text: '1',
                    style: TextStyle(
                      fontSize: 15.0,
                      color: redFF5B59,
                      fontFamily: AppFontFamily.pingFangRegular,
                    ),
                  ),
                  TextSpan(
                    text: '个！',
                    style: TextStyle(
                      fontSize: 15.0,
                      color: dark191919,
                      fontFamily: AppFontFamily.pingFangRegular,
                    ),
                  ),
                ]),
          ),
          Container(
            margin: const EdgeInsets.only(left: 33.0, right: 33.0, top: 24.0),
            child: ClipRRect(
              borderRadius: const BorderRadius.all(Radius.circular(15)),
              child: LinearProgressIndicator(
                  value: progress,
                  backgroundColor: lightF1F1F1,
                  valueColor:
                      AlwaysStoppedAnimation<Color>(LibColor.colorMain)),
            ),
            height: 8.0,
            width: 216.0,
          ),
        ],
      ),
    );
  }

  /// 下载完成
  Widget _buildLoadDoneText() {
    return Container(
      padding: const EdgeInsets.only(top: 24.0),
      width: 209.0,
      child: Column(
        mainAxisSize: MainAxisSize.min,
        children: [
          LYText(
            text: '下载完成啦！',
            fontSize: 17.0,
            color: dark191919,
            fontFamily: AppFontFamily.pingFangMedium,
          ),
          const SizedBox(
            height: 12.0,
          ),
          RichText(
            textAlign: TextAlign.center,
            text: TextSpan(
                text: '已成功下载',
                style: TextStyle(
                  fontSize: 15.0,
                  color: dark191919,
                  fontFamily: AppFontFamily.pingFangRegular,
                ),
                children: [
                  TextSpan(
                    text: '1',
                    style: TextStyle(
                      fontSize: 15.0,
                      color: redFF5B59,
                      fontFamily: AppFontFamily.pingFangRegular,
                    ),
                  ),
                  TextSpan(
                    text: '个素材，请在手机 相册查看',
                    style: TextStyle(
                      fontSize: 15.0,
                      color: dark191919,
                      fontFamily: AppFontFamily.pingFangRegular,
                    ),
                  ),
                ]),
          ),
        ],
      ),
    );
  }

  void onTapBtnHandle() {
    if (status == 1) {
      LyDownloadUtil.cancel();
    }

    /// 下载完成
    if (status == 2) {
      Get.back(result: true);
      return;
    }
    Get.back();
  }
}
