import 'dart:io';

import 'package:flutter/material.dart';
import 'package:flutter_screenutil/flutter_screenutil.dart';
import 'package:get/get.dart';
import 'package:open_file/open_file.dart';
import 'package:ota_update/ota_update.dart';
import 'package:path_provider/path_provider.dart';
import 'package:url_launcher/url_launcher.dart';
import 'package:zyocore/values/style.dart';
import 'package:wakelock_plus/wakelock_plus.dart';

import '../../core/configs/global_config.dart';
import '../../entitys/version.dart';
import '../../util/logger.dart';
import '../../util/ly_button_util.dart';
import '../../util/ly_permission.dart';
import '../../values/color.dart';
import '../ly_assets_image.dart';

/// 版本更新
class UpdVersionDialog extends StatefulWidget {
  final VersionEntity model;
  String envType;
  String appType;
  String channelType;

  /// 为了给按钮添加埋点时间才加的
  VoidCallback? onTapHandle;

  UpdVersionDialog(
      {Key? key, required this.model, required this.envType, required this.appType, required this.channelType, this.onTapHandle})
      : super(key: key);

  @override
  _UpdVersionDialogState createState() => _UpdVersionDialogState();
}

class _UpdVersionDialogState extends State<UpdVersionDialog> {
  String progress = "";
  var btnText = "立即升级".obs;
  var otaStatus = 0.obs;

  @override
  void initState() {
    // TODO: implement initState
    super.initState();

    /// 防止进入熄屏状态
    WakelockPlus.enable();
  }

  Widget _buildContent() {
    return Center(
      child: Column(
        mainAxisAlignment: MainAxisAlignment.center,
        children: [
          LYAssetsImage(name: 'upd_version_top.png', width: 282.w),
          Container(
            decoration: BoxDecoration(
                color: Colors.white,
                borderRadius: BorderRadius.only(
                  bottomLeft: Radius.circular(14.r),
                  bottomRight: Radius.circular(14.r),
                )),
            width: 282.w,
            child: Column(
              mainAxisAlignment: MainAxisAlignment.spaceAround,
              children: [
                SizedBox(
                  width: 200.w,
                  child: Row(
                    mainAxisAlignment: MainAxisAlignment.start,
                    children: [
                      Text(
                        '发现新版本',
                        style: TextStyle(
                          fontSize: 20.sp,
                          fontFamily: AppFontFamily.pingFangMedium,
                        ),
                      ),
                      Container(
                        width: 41.w,
                        height: 19.h,
                        decoration: BoxDecoration(borderRadius: BorderRadius.circular(9.r), color: yellowFFDB24),
                        child: Center(
                          child: Text(
                            "NEW",
                            style: TextStyle(fontSize: 11.sp, color: Colors.white),
                          ),
                        ),
                      ),
                    ],
                  ),
                ),
                SizedBox(height: 18.w),
                SizedBox(
                  width: 224.w,
                  child: Row(
                    crossAxisAlignment: CrossAxisAlignment.start,
                    children: [
                      Container(
                        width: 6.w,
                        height: 6.w,
                        margin: EdgeInsets.only(top: 5.w, right: 8.w),
                        decoration: BoxDecoration(borderRadius: BorderRadius.circular(3.r), color: LibColor.colorMain),
                      ),
                      SizedBox(
                        width: 210.w,
                        child: Text(
                          widget.model.desc!,
                          style: TextStyle(fontSize: 12.sp, color: dark333333),
                        ),
                      ),
                    ],
                  ),
                ),
                SizedBox(height: 23.w),
                Row(
                  mainAxisAlignment: MainAxisAlignment.center,
                  children: [..._buildBtn()],
                ),
                SizedBox(height: 20.w),
              ],
            ),
          ),
        ],
      ),
    );
  }

  List<Widget> _buildBtn() {
    List<Widget> rs = [];
    if (!widget.model.forced) {
      rs.add(Obx(() {
        return otaStatus.value != 0
            ? const SizedBox()
            : LYButtonUtil.generalButton(
          height: 44.w,
          width: 100.w,
          bgColor: Colors.transparent,
          border: Border.all(color: LibColor.colorMain, width: 1.w),
          text: "取消",
          radius: 22.r,
          margin: EdgeInsets.only(right: 20.w),
          textStyle: TextStyle(
            fontSize: 16.sp,
            color: LibColor.colorMain,
            fontFamily: AppFontFamily.pingFangMedium,
          ),
          onTap: () {
            WakelockPlus.disable();
            Get.back();
          },
        );
      }));
    }
    rs.add(Obx(() {
      return LYButtonUtil.generalButton(
        height: 44.w,
        width: (widget.model.forced || otaStatus.value != 0) ? 196.w : 125.w,
        bgColor: LibColor.colorMain,
        text: btnText.value,
        radius: 22.r,
        textStyle: TextStyle(
          fontSize: 16.sp,
          color: Colors.white,
          fontFamily: AppFontFamily.pingFangMedium,
        ),
        onTap: _onTapSubmitHandle,
      );
    }));
    return rs;
  }

  @override
  Widget build(BuildContext context) {
    return WillPopScope(onWillPop: _onBackPressed, child: _buildContent());
  }

  Future<bool> _onBackPressed() async {
    return false;
  }

  void updState(OtaStatus status, String? value) {
    switch (status) {
      case OtaStatus.DOWNLOADING: // 下载中
        btnText.value = '下载进度:$value%';
        otaStatus.value = 1;
        break;
      case OtaStatus.INSTALLING: //安装中
        otaStatus.value = 2;
        btnText.value = '点击安装';
        break;
      case OtaStatus.PERMISSION_NOT_GRANTED_ERROR: // 权限错误
        btnText.value = '立即升级';
        otaStatus.value = 0;
        break;
      default: // 其他问题
        btnText.value = '立即升级';
        otaStatus.value = 0;
        break;
    }
    // progress = value == null ? "下载失败,请稍后再试" : '下载进度:$value%';
  }

  //升级
  Future<void> _onTapSubmitHandle() async {
    /// 为了埋点加的
    if (widget.onTapHandle != null) {
      widget.onTapHandle!();
    }
    if (otaStatus.value == 0) {
      _updateVersion();
    } else if (otaStatus.value == 2) {
      WakelockPlus.disable();
      Directory appDocDir = await getApplicationSupportDirectory();
      String appDocPath = appDocDir.path;
      OpenFile.open("$appDocPath/ota_update/baby_${widget.appType}.apk");
    }
  }

  void _onTapCloseHandle() {
    Get.back();
  }

  void _updateVersion() async {
    if (Platform.isIOS) {
      String url = "";
      String appid = widget.appType == "school" ? "id1419988282" : "id1208390127";
      if (widget.envType == "prod") {
        url = 'itms-apps://itunes.apple.com/cn/app/$appid?mt=8';
      } else {
        url = 'itms-beta://beta.itunes.apple.com/v1/app/$appid?mt=8';
      }
      final Uri _url = Uri.parse(url);
      if (await canLaunchUrl(_url)) {
        await launchUrl(_url);
      } else {
        throw 'Could not launch $url';
      }
    } else if (Platform.isAndroid) {
      String url = widget.model.url!.replaceAll("channel", widget.channelType);
      try {
        bool flag = true;
        if (GlobalConfigZyoCore.androidVersion < 33) {
          flag = await LyPermission.storage();
        }
        if (flag) {
          // destinationFilename 是对下载的apk进行重命名
          OtaUpdate().execute(url, destinationFilename: 'baby_${widget.appType}.apk').listen(
                (OtaEvent event) {
              updState(event.status, event.value);
            },
          ).onError((e) {
            Logger.d('更新失败，请稍后再试');
          });
        }
      } catch (e) {
        Logger.d('更新失败，请稍后再试');
      }
    }
  }
}
