import 'package:flutter/material.dart';
import 'package:zyocore/values/style.dart';
import 'package:zyocore/zyocore.dart';

import '../../core/configs/config.dart';

class NaviDateSelector extends StatefulWidget {
  final String year;
  final String month;
  Function? onChange;

  NaviDateSelector({Key? key, required this.year, required this.month, this.onChange}) : super(key: key);

  @override
  State<NaviDateSelector> createState() => _NaviDateSelectorState();
}

class _NaviDateSelectorState extends State<NaviDateSelector> {
  var year = 0.obs;
  var month = 0.obs;

  @override
  void initState() {
    // TODO: implement initState
    super.initState();
    year.value = int.parse(widget.year);
    month.value = int.parse(widget.month);
  }

  void onSelectItem(int val) {
    if (val != 0) {
      int newMonth = month.value + val;
      int newYear = year.value;
      if (month.value == 1 && val < 0) {
        newMonth = 12;
        newYear = year.value - 1;
      } else if (month.value == 12 && val > 0) {
        newMonth = 1;
        newYear = year.value + 1;
      }
      year.value = newYear;
      month.value = newMonth;
    }
    if (widget.onChange != null) {
      widget.onChange!({"year": year.value, "month": month.value});
    }
  }

  void onTextClick() {
    LYPickerUtil.showDatePicker(
        context: context,
        mode: DateMode.YM,
        selected: DateTime(year.value, month.value),
        onConfirm: (y, m, d) {
          year.value = y;
          month.value = m;
          onSelectItem(0);
        });
  }
  @override
  Widget build(BuildContext context) {
    return Container(
      alignment: Alignment.center,
      height: 40.w,
      decoration: BoxDecoration(
        color: Colors.white,
        borderRadius: BorderRadius.circular(20.r),
      ),
      padding: EdgeInsets.symmetric(horizontal: 16.w),
      child: Row(
        mainAxisAlignment: MainAxisAlignment.center,
        crossAxisAlignment: CrossAxisAlignment.center,
        children: [
          InkWell(
            child: Image.asset(
              'assets/images/icon_datetime_left.png',
              width: 16.w,
              height: 16.w,
              fit: BoxFit.fill,
              package: Config.packageName,
            ),
            onTap: () {
              onSelectItem(-1);
            },
          ),
          InkWell(
            child: Container(
              padding: EdgeInsets.symmetric(horizontal: 12.w),
              child: Obx(() {
                return Text(
                  "${year.value}年${month.value}月",
                  style: TextStyle(
                    fontFamily: AppFontFamily.pingFangMedium,
                    color: dark191919,
                    fontSize: 17.sp,
                  ),
                );
              }),
            ),
            onTap: () {
              onTextClick();
            },
          ),
          InkWell(
            child: Image.asset(
              'assets/images/icon_datetime_right.png',
              width: 16.w,
              height: 16.w,
              fit: BoxFit.fill,
              package: Config.packageName,
            ),
            onTap: () {
              onSelectItem(1);
            },
          ),
        ],
      ),
    );
  }
}
