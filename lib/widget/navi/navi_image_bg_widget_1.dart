import 'package:flutter/material.dart';

import '../../core/configs/config.dart';
class NaviImageBgWiget1 extends StatelessWidget {
  final double width;
  final double height;
  final Widget? child;
  const NaviImageBgWiget1({Key? key, required this.width, required this.height,this.child}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Container(
      width: width,
      height: height,
      decoration: BoxDecoration(
        image: DecorationImage(
          fit: BoxFit.cover,
          image: AssetImage('assets/images/navigation_bg.png',
              package: Config.packageName),
        ),
      ),
      child: child,
    );
  }
}
