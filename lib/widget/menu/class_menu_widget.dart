import 'package:flutter/material.dart';
import 'package:flutter_screenutil/flutter_screenutil.dart';
import 'package:get/get_navigation/src/extension_navigation.dart';
import 'package:zyocore/values/style.dart';
import 'package:zyocore/widget/text/ly_text.dart';

import '../../values/color.dart';

class ClassMenuWidget extends StatefulWidget {
  final Function(String?)? backAction;
  final List<String?> list;
  final String currentValue;

  const ClassMenuWidget(
      {Key? key,
      this.backAction,
      required this.list,
      required this.currentValue})
      : super(key: key);

  @override
  State<ClassMenuWidget> createState() => _ClassMenuWidgetState();
}

class _ClassMenuWidgetState extends State<ClassMenuWidget>
    with SingleTickerProviderStateMixin {
  late AnimationController _controller; // 控制器

  late Animation<double> animation; // 动画抽象类
  late CurvedAnimation cure; // 动画运行的速度轨迹 速度的变化

  double cellHeight = 56.0;

  @override
  void initState() {
    // TODO: implement initState
    super.initState();
    _controller = AnimationController(
        vsync: this, duration: const Duration(milliseconds: 2000)); //2s
    cure = CurvedAnimation(parent: _controller, curve: Curves.easeIn);
    // 高度变化
    animation = Tween(begin: 0.0, end: 200.0).animate(cure);

    _controller.addListener(() {
      _controller.forward();
      // 刷新筛选数据
      setState(() {});
    });
  }

  @override
  Widget build(BuildContext context) {
    return Center(
      child: AnimatedBuilder(
          animation: BottomSheet.createAnimationController(navigator!),
          builder: (BuildContext context, Widget? child) {
            return child!;
          },
          child: InkWell(
            onTap: () {
              widget.backAction!(null);
            },
            child: ChildView(
                child: Align(
              alignment: Alignment.topCenter,
              child: Column(
                mainAxisSize: MainAxisSize.min,
                children: [
                  Container(
                    padding: EdgeInsets.symmetric(horizontal: 16.w),
                    width: double.infinity,
                    height: widget.list.length > 4
                        ? 4 * cellHeight
                        : widget.list.length * cellHeight,
                    color: Colors.white,
                    child: ListView.builder(
                      padding: EdgeInsets.zero,
                      itemBuilder: (context, index) {
                        return GestureDetector(
                          onTap: () {
                            if (widget.backAction != null) {
                              widget.backAction!('$index');
                            }
                          },
                          child: SizedBox(
                            height: cellHeight,
                            child: Row(
                              children: [
                                Expanded(
                                    child: LYText(
                                  text: widget.list[index] ?? '',
                                  color:
                                      widget.list[index] == widget.currentValue
                                          ? LibColor.colorMain
                                          : dark333333,
                                  fontSize: 15,
                                  fontFamily: AppFontFamily.pingFangMedium,
                                  overflow: TextOverflow.ellipsis,
                                )),
                                widget.list[index] == widget.currentValue
                                    ? Image.asset(
                                        'assets/images/home_v.png',
                                        width: 20,
                                        height: 20,
                                        fit: BoxFit.fill,
                                        package: "zyocore",
                                      )
                                    : const SizedBox()
                              ],
                            ),
                          ),
                        );
                      },
                      itemCount: widget.list.length,
                      // shrinkWrap: true,
                      // physics: const NeverScrollableScrollPhysics(),
                    ),
                  ),
                ],
              ),
            )),
          )),
    );
  }
}

class ChildView extends StatefulWidget {
  final Widget child;

  const ChildView({Key? key, required this.child}) : super(key: key);

  @override
  State<ChildView> createState() => _ChildViewState();
}

class _ChildViewState extends State<ChildView> {
  @override
  Widget build(BuildContext context) {
    return Container(
      margin: EdgeInsets.only(
          top: ScreenUtil().statusBarHeight + AppBar().preferredSize.height),
      height: double.infinity,
      width: double.infinity,
      color: Colors.grey.withOpacity(0.5),
      child: widget.child,
    );
  }
}
