import 'package:common_utils/common_utils.dart';
import 'package:flutter/material.dart';
import 'package:flutter_screenutil/flutter_screenutil.dart';
import 'package:zyocore/extension/color_extension.dart';
import 'package:zyocore/widget/index.dart';

///加载中
class Loading extends StatelessWidget {
  const Loading({Key? key, this.bgColor, this.color, this.width, this.height, this.strokeWidth, this.tip, this.radius}) : super(key: key);

  final Color? bgColor;
  final Color? color;
  final double? width;
  final double? height;
  final double? strokeWidth;
  final String? tip;
  final double? radius;

  @override
  Widget build(BuildContext context) {
    return Container(
      alignment: Alignment.center,
      width: width,
      height: height,
      decoration: BoxDecoration(
        color: bgColor ?? Colors.white,
        borderRadius: BorderRadius.all(Radius.circular(radius ?? 0)),
      ),
      padding: EdgeInsets.all(2.w),
      child: Column(
        mainAxisAlignment: MainAxisAlignment.center,
        crossAxisAlignment: CrossAxisAlignment.center,
        children: [
          CircularProgressIndicator(color: color ?? LibColor.colorMain, strokeWidth: strokeWidth ?? 4),
          SizedBox(height: TextUtil.isEmpty(tip) ? 0 : 5.w),
          TextUtil.isEmpty(tip) ? const SizedBox() : LYText(text: tip, color: Colors.white),
        ],
      ),
    );
  }
}
