library tooltip_internal;

import 'dart:async';
import 'package:flutter/foundation.dart' show kIsWeb;
import 'package:flutter/material.dart';
import 'package:flutter/rendering.dart';
import 'package:zyocore/values/style.dart';
// ignore: depend_on_referenced_packages
import 'core.dart';
part 'src/tooltip/tooltip.dart';
