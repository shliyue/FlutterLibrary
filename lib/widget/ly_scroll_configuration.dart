import 'package:flutter/material.dart';
import 'package:zyocore/widget/ly_behavior.dart';

class LYScrollConfiguration extends StatelessWidget {
  final Widget child;
  const LYScrollConfiguration({Key? key, required this.child}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return ScrollConfiguration(behavior: LYBehavior(), child: child);
  }
}
