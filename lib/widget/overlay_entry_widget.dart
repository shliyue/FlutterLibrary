import 'package:flutter/material.dart';
import 'package:flutter_screenutil/flutter_screenutil.dart';
import 'package:zyocore/widget/paint_trans_path.dart';

import '../core/configs/config.dart';
import 'ly_image_widget.dart';

class OverlayEntryWidget {
  OverlayEntry getOverEntryWidget(
      {required int tapCount,
      required List<Size> sizes,//锚点位置大小
      required List<Offset> offsets,//锚点偏移量
      required List<double> positions,//图片距离左边宽度
      List<double>? borderRadius,//圆形弧度
      List<double>? lborderRadius,//左边弧度
      List<double>? rborderRadius,//右边弧度
      required List<String> imgUrls,//图片地址
      required List<double> imgWidths,//图片宽度
      required List<double> imgHeights,//图片高度
      List<bool>? bottoms,//图片指向（图片中箭头指向）
      String? packageName,
      required Function(int value) onTap}) {
    var overlayEntry = OverlayEntry(builder: (BuildContext context) {
      String? package = packageName;
      if (packageName == null) {
        package = Config.packageName;
      } else if (packageName == "") {
        package = null;
      }
      return InkWell(
        onTap: () {
          tapCount++;
          onTap(tapCount);
        },
        child: CustomPaint(
          painter: PaintTransPath(
            clipWidth: sizes[tapCount].width,
            clipHeight: sizes[tapCount].height,
            left: offsets[tapCount].dx,
            top: offsets[tapCount].dy,
            borderRadius: borderRadius != null ? borderRadius[tapCount] : null,
            lborderRadius: lborderRadius != null ? lborderRadius[tapCount] : null,
            rborderRadius: rborderRadius != null ? rborderRadius[tapCount] : null,
          ),
          // color: Colors.transparent.withOpacity(0.6),
          child: Stack(
            children: [
              (bottoms != null ? bottoms[tapCount] : false)
                  ? Positioned(
                      bottom: ScreenUtil().screenHeight - offsets[tapCount].dy + 15.w + ScreenUtil().bottomBarHeight,
                      left: positions[tapCount],
                      child: LYImageWidget(
                        isShowDefault: false,
                        fit: BoxFit.contain,
                        src: 'assets/images/guide/${imgUrls[tapCount]}.png',
                        width: imgWidths[tapCount],
                        height: imgHeights[tapCount],
                        package: package,
                      ),
                    )
                  : Positioned(
                      top: (offsets[tapCount].dy + ScreenUtil().statusBarHeight + 16.w),
                      left: positions[tapCount],
                      child: LYImageWidget(
                        isShowDefault: false,
                        fit: BoxFit.contain,
                        src: 'assets/images/guide/${imgUrls[tapCount]}.png',
                        width: imgWidths[tapCount],
                        height: imgHeights[tapCount],
                        package: package,
                      ),
                    ),
            ],
          ),
        ),
      );
    });
    return overlayEntry;
  }
}
