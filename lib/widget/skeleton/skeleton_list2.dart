import 'package:flutter/material.dart';
import 'package:skeletons/skeletons.dart';
import 'package:zyocore/widget/common/flex/column.dart';
import 'package:zyocore/widget/common/flex/row.dart';

/// 带banner的List--成长模块首页
class SkeletonList2 extends StatelessWidget {
  const SkeletonList2({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return SkeletonItem(
      child: SingleChildScrollView(
        child: CSS(
          children: [
            banner,
            menus,
            boxMenu,
            list,
          ],
        ),
      ),
    );
  }

  Widget get div {
    return const SizedBox(height: 26);
  }

  Widget get banner {
    return Container(
      margin: const EdgeInsets.only(left: 16, right: 16, bottom: 16),
      child: const SkeletonLine(
        style: SkeletonLineStyle(
            height: 140, borderRadius: BorderRadius.all(Radius.circular(16))),
      ),
    );
  }

  Widget get menus {
    return Container(
      margin: const EdgeInsets.symmetric(horizontal: 32),
      child: CSS(
        children: [iconList, div, iconList],
      ),
    );
  }

  Widget get iconList {
    return RBC(
      children: List.generate(
          5,
          (index) => const SkeletonAvatar(
                style: SkeletonAvatarStyle(width: 20, height: 20),
              )).toList(),
    );
  }

  Widget get boxMenu {
    return Container(
      alignment: Alignment.topLeft,
      margin: const EdgeInsets.symmetric(horizontal: 52, vertical: 16),
      child: RBC(
        children: List.generate(
            3,
            (index) => CCC(
                  children: const [
                    SkeletonLine(
                      style: SkeletonLineStyle(
                          width: 60,
                          height: 17,
                          borderRadius: BorderRadius.all(Radius.circular(5))),
                    ),
                    SizedBox(height: 5),
                    SkeletonLine(
                      style: SkeletonLineStyle(
                          height: 17,
                          width: 40,
                          borderRadius: BorderRadius.all(Radius.circular(5))),
                    )
                  ],
                )).toList(),
      ),
    );
  }

  Widget get list {
    return Container(
      margin: const EdgeInsets.symmetric(horizontal: 16),
      child: CSS(
        children: List.generate(4, (index) {
          return Container(
            margin: const EdgeInsets.symmetric(vertical: 10),
            child: RSS(
              children: [
                const SkeletonLine(
                  style: SkeletonLineStyle(
                      height: 90,
                      width: 150,
                      borderRadius: BorderRadius.all(Radius.circular(16))),
                ),
                const SizedBox(
                  width: 12,
                ),
                CSS(
                  children: const [
                    SkeletonLine(
                      style: SkeletonLineStyle(
                        height: 20,
                        width: 160,
                      ),
                    ),
                    SizedBox(
                      height: 8,
                    ),
                    SkeletonLine(
                      style: SkeletonLineStyle(
                        height: 15,
                        width: 50,
                      ),
                    )
                  ],
                )
              ],
            ),
          );
        }).toList(),
      ),
    );
  }
}
