import 'package:flutter/material.dart';
import 'package:skeletons/skeletons.dart';
import 'package:zyocore/widget/common/flex/column.dart';
import 'package:zyocore/widget/common/flex/row.dart';

/// 分类页
class SkeletonListType extends StatelessWidget {
  const SkeletonListType({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return SkeletonItem(
      child: SingleChildScrollView(
        child: RSS(
          children: [
            leftList,
            rightList,
          ],
        ),
      ),
    );
  }

  Widget get leftList {
    return SizedBox(
      width: 120,
      child: CSS(
        children: List.generate(6, (index) => leftItem).toList(),
      ),
    );
  }

  Widget get div {
    return const SizedBox(
      height: 10,
    );
  }

  Widget get rightList {
    return Expanded(
      child: Container(
        margin: const EdgeInsets.symmetric(vertical: 5, horizontal: 16),
        child: CSS(
          children: [
            iconList,
            div,
            iconList,
            textList,
            contentList,
          ],
        ),
      ),
    );
  }

  Widget get leftItem {
    return Container(
      margin: const EdgeInsets.symmetric(vertical: 5),
      child: CSS(
        children: const [
          SkeletonLine(
            style: SkeletonLineStyle(
                height: 44,
                borderRadius: BorderRadius.only(
                    topRight: Radius.circular(8),
                    bottomRight: Radius.circular(8))),
          ),
          SizedBox(
            height: 2,
          ),
          SkeletonLine(
            style: SkeletonLineStyle(height: 35),
          ),
          SizedBox(
            height: 2,
          ),
          SkeletonLine(
            style: SkeletonLineStyle(height: 35),
          )
        ],
      ),
    );
  }

  Widget get iconList {
    return RBC(
      children: List.generate(
          3,
          (index) => const SkeletonAvatar(
                style: SkeletonAvatarStyle(width: 70, height: 70),
              )).toList(),
    );
  }

  Widget get textList {
    return CSS(
      children: List.generate(
          5,
          (index) => Container(
                margin: const EdgeInsets.symmetric(vertical: 6),
                child: const SkeletonLine(
                  style: SkeletonLineStyle(height: 40),
                ),
              )).toList(),
    );
  }

  Widget get contentList {
    return CSS(
      children: List.generate(
          4,
          (index) => SkeletonListTile(
                hasLeading: true,
                verticalSpacing: 30,
                hasSubtitle: true,
                titleStyle: const SkeletonLineStyle(),
                subtitleStyle:
                    const SkeletonLineStyle(maxLength: 100, minLength: 30),
                leadingStyle: const SkeletonAvatarStyle(height: 80, width: 80),
              )).toList(),
    );
  }
}
