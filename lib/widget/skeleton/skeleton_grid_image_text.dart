import 'package:flutter/material.dart';
import 'package:flutter_screenutil/flutter_screenutil.dart';
import 'package:skeletons/skeletons.dart';
import 'package:zyocore/widget/common/flex/column.dart';
import 'package:zyocore/widget/common/flex/row.dart';

/// 图片-文本列表---搜索页
class SkeletonGridImageText extends StatelessWidget {
  const SkeletonGridImageText({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return SkeletonListView(
      itemCount: 5,
      itemBuilder: (context, index) {
        return listItem;
      },
    );
  }

  Widget get listItem {
    return Container(
      margin: const EdgeInsets.symmetric(vertical: 15),
      child: itemList,
    );
  }

  Widget get itemList {
    return RBC(
      children: List.generate(
          2,
          (index) => Container(
                child: CSS(
                  children: [
                    SkeletonAvatar(
                      style: SkeletonAvatarStyle(
                          width: (ScreenUtil().screenWidth - 44) / 2,
                          height: 100),
                    ),
                    const SizedBox(height: 10),
                    const SkeletonLine(
                      style: SkeletonLineStyle(
                          height: 20,
                          randomLength: true,
                          minLength: 50,
                          maxLength: 150),
                    )
                  ],
                ),
              )).toList(),
    );
  }
}
