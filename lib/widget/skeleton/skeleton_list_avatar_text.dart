import 'package:flutter/material.dart';
import 'package:skeletons/skeletons.dart';
import 'package:zyocore/widget/common/flex/row.dart';

/// 成长-考试-排行榜
class SkeletonListAvatarText extends StatelessWidget {
  const SkeletonListAvatarText({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return SkeletonListView(
      itemCount: 10,
      itemBuilder: (context, index) {
        return listItem;
      },
    );
  }

  Widget get listItem {
    return Container(
      margin: const EdgeInsets.symmetric(vertical: 15),
      child: RSC(
        children: [
          const SizedBox(width: 16),
          const SkeletonLine(style: SkeletonLineStyle(width: 15)),
          const SizedBox(width: 30),
          const SkeletonAvatar(
            style: SkeletonAvatarStyle(
              width: 44,
              height: 44,
              shape: BoxShape.circle,
            ),
          ),
          const SizedBox(width: 8),
          Expanded(
              child: RBC(
            children: const [
              SkeletonLine(
                  style: SkeletonLineStyle(
                      height: 20,
                      maxLength: 50,
                      minLength: 20,
                      randomLength: true)),
              SkeletonLine(
                  style: SkeletonLineStyle(
                      height: 20,
                      maxLength: 50,
                      minLength: 20,
                      randomLength: true)),
              SkeletonLine(
                  style: SkeletonLineStyle(
                      height: 20,
                      maxLength: 50,
                      minLength: 20,
                      randomLength: true)),
            ],
          ))
        ],
      ),
    );
  }
}
