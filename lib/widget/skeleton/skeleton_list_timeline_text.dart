import 'package:flutter/material.dart';
import 'package:skeletons/skeletons.dart';
import 'package:zyocore/widget/common/flex/column.dart';
import 'package:zyocore/widget/common/flex/row.dart';

/// 学习足迹
class SkeletonListTimeLineText extends StatelessWidget {
  const SkeletonListTimeLineText({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return SkeletonListView(
      itemCount: 5,
      itemBuilder: (context, index) {
        return listItem;
      },
    );
  }

  Widget get listItem {
    return CSS(
      children: [groupTitle, listCell, listCell, listCell],
    );
  }

  Widget get groupTitle {
    return Container(
      margin: const EdgeInsets.symmetric(vertical: 10),
      child: const SkeletonLine(
          style: SkeletonLineStyle(
              height: 30, maxLength: 70, minLength: 40, randomLength: true)),
    );
  }

  Widget get listCell {
    return Container(
      margin: const EdgeInsets.symmetric(vertical: 6),
      child: RSC(
        children: const [
          SkeletonLine(style: SkeletonLineStyle(height: 20, width: 50)),
          SizedBox(width: 14),
          SkeletonAvatar(
            style: SkeletonAvatarStyle(
              width: 7,
              height: 7,
              shape: BoxShape.circle,
            ),
          ),
          SizedBox(width: 20),
          SkeletonLine(
              style: SkeletonLineStyle(
                  height: 20,
                  maxLength: 230,
                  minLength: 50,
                  randomLength: true)),
        ],
      ),
    );
  }
}
