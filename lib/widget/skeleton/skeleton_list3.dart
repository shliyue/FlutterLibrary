import 'package:flutter/material.dart';
import 'package:skeletons/skeletons.dart';
import 'package:zyocore/widget/common/flex/column.dart';

/// 带banner的List-- 动态模块首页
class SkeletonList3 extends StatelessWidget {
  const SkeletonList3({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return SkeletonItem(
      child: SingleChildScrollView(
        child: list,
      ),
    );
  }

  Widget get list {
    return CSS(
      children: List.generate(3, (index) {
        return item;
      }).toList(),
    );
  }

  Widget get item {
    return Container(
      margin: const EdgeInsets.symmetric(vertical: 10),
      child: CSS(
        children: [info, title, content, image, comment],
      ),
    );
  }

  Widget get info {
    return Container(
      margin: const EdgeInsets.symmetric(horizontal: 16),
      child: SkeletonListTile(
        hasLeading: true,
        hasSubtitle: true,
        titleStyle: const SkeletonLineStyle(maxLength: 150, minLength: 50),
        leadingStyle: const SkeletonAvatarStyle(
            height: 40, width: 40, shape: BoxShape.circle),
      ),
    );
  }

  Widget get title {
    return Container(
      margin: const EdgeInsets.all(16),
      child: const SkeletonLine(
        style: SkeletonLineStyle(height: 25),
      ),
    );
  }

  Widget get content {
    return Container(
      margin: const EdgeInsets.symmetric(horizontal: 8),
      child: SkeletonParagraph(
        style: const SkeletonParagraphStyle(
            spacing: 5, lineStyle: SkeletonLineStyle(randomLength: true)),
      ),
    );
  }

  Widget get image {
    return Container(
      margin: const EdgeInsets.symmetric(horizontal: 16),
      child: const SkeletonLine(
        style: SkeletonLineStyle(
          height: 120,
          borderRadius: BorderRadius.all(Radius.circular(16)),
        ),
      ),
    );
  }

  Widget get comment {
    return Container(
      margin: const EdgeInsets.symmetric(vertical: 10, horizontal: 16),
      child: const SkeletonLine(
        style: SkeletonLineStyle(height: 16, randomLength: true),
      ),
    );
  }
}
