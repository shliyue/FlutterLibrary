import 'package:flutter/material.dart';
import 'package:skeletons/skeletons.dart';
import 'package:zyocore/widget/common/flex/column.dart';
import 'package:zyocore/widget/common/flex/row.dart';

/// 带banner的List--工作模块首页
class SkeletonList1 extends StatelessWidget {
  const SkeletonList1({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return SkeletonItem(
      child: SingleChildScrollView(
        child: CSS(
          children: [
            banner,
            menus,
            list,
          ],
        ),
      ),
    );
  }

  Widget get div {
    return const SizedBox(height: 26);
  }

  Widget get banner {
    return Container(
      margin: const EdgeInsets.only(left: 16, right: 16, bottom: 16),
      child: const SkeletonLine(
        style: SkeletonLineStyle(
            height: 140, borderRadius: BorderRadius.all(Radius.circular(16))),
      ),
    );
  }

  Widget get menus {
    return Container(
      margin: const EdgeInsets.symmetric(horizontal: 32),
      child: CSS(
        children: [iconList, div, iconList],
      ),
    );
  }

  Widget get iconList {
    return RBC(
      children: List.generate(
          5,
              (index) =>
          const SkeletonAvatar(
            style: SkeletonAvatarStyle(width: 20, height: 20),
          )).toList(),
    );
  }

  Widget get list {
    return Container(
      margin: const EdgeInsets.all(16),
      child: CSS(
        children: List.generate(
            5,
                (index) =>
                SkeletonParagraph(
                  style: const SkeletonParagraphStyle(),
                )).toList(),
      ),
    );
  }
}
