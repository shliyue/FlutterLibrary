import 'package:flutter/material.dart';
import 'package:skeletons/skeletons.dart';
import 'package:zyocore/widget/common/flex/column.dart';
import 'package:zyocore/widget/common/flex/row.dart';

/// 图片-文本列表---搜索页
class SkeletonListImageText extends StatelessWidget {
  const SkeletonListImageText({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return SkeletonListView(
      itemCount: 8,
      itemBuilder: (context, index) {
        return listItem;
      },
    );
  }

  Widget get listItem {
    return Container(
      margin: const EdgeInsets.symmetric(vertical: 15),
      child: RSS(
        children: [
          const SkeletonAvatar(
            style: SkeletonAvatarStyle(
                width: 152,
                height: 86,
                borderRadius: BorderRadius.all(Radius.circular(16))),
          ),
          const SizedBox(width: 12),
          CSS(
            children: [
              const SkeletonLine(
                  style: SkeletonLineStyle(
                      height: 20,
                      randomLength: true,
                      minLength: 30,
                      maxLength: 160)),
              const SizedBox(height: 5),
              const SkeletonLine(
                  style: SkeletonLineStyle(
                      height: 15,
                      randomLength: true,
                      minLength: 20,
                      maxLength: 70)),
              const SizedBox(height: 15),
              RBC(
                children: const [
                  SizedBox(
                    width: 80,
                  ),
                  SkeletonLine(
                      style: SkeletonLineStyle(
                          height: 28,
                          width: 68,
                          borderRadius: BorderRadius.all(Radius.circular(14))))
                ],
              )
            ],
          )
        ],
      ),
    );
  }
}
