import 'package:flutter/material.dart';
import 'package:zyocore/values/style.dart';
import 'package:zyocore/widget/text/ly_text.dart';

class LYButtonHollow extends StatelessWidget {

  final double width;
  final double height;
  final double radius;
  final double fontSize;
  final String title;
  final Color color;
  final VoidCallback onTap;
  final bool enable;
  final FontWeight? fontWeight;
  final String? fontFamily;

  const LYButtonHollow({
    Key? key,
    required this.title,
    this.width = 343,
    this.height = 44,
    this.radius = 22,
    this.fontSize = 15,
    this.color = const Color(0xFF9F89FF),
    this.enable = true,
    this.fontWeight,
    this.fontFamily,
    required this.onTap,
  }) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return GestureDetector(
      onTap: enable ? onTap : null,
      child: Container(
        width: width,
        height: height,
        decoration: BoxDecoration(
          border: Border.all(width: 1, color: enable ? color : const Color(0xFFBBA9FF)),
          borderRadius: BorderRadius.circular(radius),),
        child: Center(
          child: LYText(
            text: title,
            fontSize: fontSize,
            color: enable ? color : const Color(0xFFBBA9FF),
            fontWeight: fontWeight ?? FontWeight.normal,
            fontFamily: fontFamily ?? AppFontFamily.pingFangMedium,
          ),),
      ),
    );
  }
}
