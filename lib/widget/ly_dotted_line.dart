import 'package:flutter/material.dart';

class LYDottedLine extends StatelessWidget {
  final Axis axis;
  final double dashedWidth;
  final double dashedHeight;
  final int count;
  final Color color;
  const LYDottedLine({Key? key,
    required this.axis,
    this.dashedWidth = 1,
    this.dashedHeight = 1,
    this.count = 0,
    this.color = const Color(0xffff0000),
  }) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return LayoutBuilder(
      builder:(BuildContext context,BoxConstraints constraints){
        var count = (axis == Axis.horizontal) ?  (constraints.maxWidth ~/ (dashedWidth * 2)) : (constraints.maxHeight ~/ (dashedHeight * 2));
        //根据宽度计算个数
        return Flex(
          direction: axis,
          mainAxisAlignment: MainAxisAlignment.spaceBetween,
          children: List.generate(count, (int index){
            return SizedBox(
              width: dashedWidth,
              height: dashedHeight,
              child: DecoratedBox(
                decoration: BoxDecoration(color: color),
              ),
            );
          }),
        );
      },
    );

  }
}
