import 'package:flutter/material.dart';
import 'package:flutter_swiper_null_safety/flutter_swiper_null_safety.dart';
import 'package:zyocore/values/color.dart';
import 'package:zyocore/values/edge_insets.dart';

/// swiper 下方的小点指示器
class LYSwiperPagination extends SwiperPlugin {
  @override
  Widget build(BuildContext context, SwiperPluginConfig config) {
    List<Widget> list = [];

    int itemCount = config.itemCount;
    int activeIndex = config.activeIndex;

    for (int i = 0; i < itemCount; ++i) {
      bool active = i == activeIndex;
      list.add(Container(
        key: Key("pagination_$i"),
        margin: horizontal2 + bottom8,
        child: Container(
          decoration: BoxDecoration(
              color: active
                  ? dark1A1A1A.withOpacity(0.47)
                  : dark1A1A1A.withOpacity(0.25),
              borderRadius: const BorderRadius.all(
                Radius.circular(2),
              )),
          width: 8,
          height: 4,
        ),
      ));
    }

    return Align(
      alignment: Alignment.bottomCenter,
      child: Row(
        mainAxisSize: MainAxisSize.min,
        children: list,
      ),
    );
  }
}
