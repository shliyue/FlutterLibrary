import 'dart:math';

import 'package:flutter/material.dart';

/// 下弧线指示器
class LySmileIndicator extends Decoration {
  const LySmileIndicator({
    this.borderSide = const BorderSide(width: 2.0, color: Colors.white),
    this.insets = EdgeInsets.zero,
    this.width = 0.0,
    this.straight = false,
    this.strokeWidth = 2.0
  });

  final BorderSide borderSide;
  final EdgeInsetsGeometry insets;

  final bool straight;

  ///固定宽度
  final double width;
  final double strokeWidth;

  @override
  Decoration? lerpFrom(Decoration? a, double t) {
    return super.lerpFrom(a, t);
  }

  @override
  Decoration? lerpTo(Decoration? b, double t) {
    return super.lerpTo(b, t);
  }

  @override
  BoxPainter createBoxPainter([VoidCallback? onChanged]) {
    return _UnderlinePainter(this, onChanged);
  }

  Rect _indicatorRectFor(Rect rect, TextDirection textDirection) {
    final Rect indicator = insets.resolve(textDirection).deflateRect(rect);

    double w = (indicator.left + indicator.right) / 2;
    return Rect.fromLTWH(w - width / 2, indicator.bottom - borderSide.width,
        width, borderSide.width);
  }

  @override
  Path getClipPath(Rect rect, TextDirection textDirection) {
    return Path()
      ..addRect(_indicatorRectFor(rect, textDirection));
  }
}

class _UnderlinePainter extends BoxPainter {
  _UnderlinePainter(this.decoration, VoidCallback? onChanged)
      : super(onChanged);

  final LySmileIndicator decoration;

  @override
  void paint(Canvas canvas, Offset offset, ImageConfiguration? configuration) {
    assert(configuration != null);
    assert(configuration!.size != null);
    final Rect rect = offset & configuration!.size!;
    final TextDirection textDirection = configuration.textDirection!;
    final Rect indicator = decoration
        ._indicatorRectFor(rect, textDirection)
        .deflate(decoration.borderSide.width / 3.0);

    final Paint _paint = decoration.borderSide.toPaint()
      ..style = PaintingStyle.stroke
      ..strokeWidth = decoration.strokeWidth
      ..strokeCap = StrokeCap.round
      ..color = decoration.borderSide.color;

    double w = (indicator.left + indicator.right) / 2;
    // canvas.drawRect(
    //     Rect.fromLTWH(w - decoration.width / 2, indicator.bottom- decoration.borderSide.width,
    //         indicator.width, decoration.borderSide.width),
    //     _paint);

    if (decoration.straight) {
      canvas.drawLine(indicator.bottomLeft, indicator.bottomRight, _paint);
    } else {
      canvas.drawArc(
          Rect.fromLTWH(
            w - decoration.width / 2 + _paint.strokeWidth,
            indicator.bottom - decoration.borderSide.width,
            indicator.width,
            decoration.borderSide.width,
          ),
          pi / 8,
          2 * pi * 130 / 360,
          false,
          _paint);
    }


    // final Paint paint = decoration.borderSide.toPaint()
    //   ..strokeCap = StrokeCap.round;
    // canvas.drawLine(indicator.bottomLeft, indicator.bottomRight, paint);
  }
}
