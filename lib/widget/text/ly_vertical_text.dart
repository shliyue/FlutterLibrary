import 'package:flutter/material.dart';
import 'package:flutter_screenutil/flutter_screenutil.dart';
import 'package:zyocore/core/configs/config.dart';
import 'package:zyocore/values/index.dart';

class LYVerticalText extends StatelessWidget {
  final String? text;
  final TextStyle? textStyle;
  final Color? color;
  final double? fontSize;
  final String? fontFamily;
  final double width;
  final FontWeight? fontWeight;
  final TextAlign? textAlign;

  const LYVerticalText({
    Key? key,
    required this.text,
    required this.width,
    this.color,
    this.fontSize,
    this.fontWeight,
    this.fontFamily,
    this.textStyle,
    this.textAlign,
  }) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Center(
      child: SizedBox(
        width: width,
        child: RichText(
          textAlign: textAlign ?? TextAlign.start,
          text: TextSpan(
            text: text,
            style: textStyle ??
                TextStyle(
                  color: color ?? Color(0xff191919),
                  fontSize: fontSize ?? 14.sp,
                  fontFamily: fontFamily ?? AppFontFamily.pingFangRegular,
                  fontWeight: fontWeight ?? FontWeight.w400,
                ),
          ),
        ),
      ),
    );
  }
}
