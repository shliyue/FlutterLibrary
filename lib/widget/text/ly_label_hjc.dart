import 'package:flutter/material.dart';
import 'package:flutter_screenutil/src/size_extension.dart';
import 'package:zyocore/values/color.dart';
import 'package:flutter_screenutil/flutter_screenutil.dart';
import 'package:zyocore/values/style.dart';
import 'package:zyocore/widget/text/ly_text.dart';

class LYLabelHjc extends StatelessWidget {
  final String title;
  final bool hjc; // 是否为必填
  final double? right;

  const LYLabelHjc({Key? key, required this.title, this.hjc = true, this.right}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Stack(children: [
      Container(
        padding: EdgeInsets.only(right: right ?? 12.w),
        child: LYText(
          text: title,
          fontSize: 17.sp,
          fontFamily: AppFontFamily.pingFangMedium,
          color: dark191919,
        ),),
      Visibility(
        visible: hjc,
        child: Positioned(
          right: 0,
          child: LYText(
            text: '*',
            fontSize: 17.sp,
            fontFamily: AppFontFamily.pingFangMedium,
            color: redFA5151,
          ),),
      )
    ],);
  }
}
