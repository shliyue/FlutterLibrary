import 'package:flutter/cupertino.dart';
import 'package:zyocore/core/configs/config.dart';
import 'package:zyocore/values/index.dart';
import 'package:zyocore/widget/index.dart';

/// flutter自带的字体在对齐时展示有问题，需要微调
@Deprecated('方法已废弃请直接使用LYText')
class SuperText extends StatelessWidget {
  const SuperText(
    this.data, {
    Key? key,
    this.style,
    this.strutStyle,
    this.textAlign,
    this.textDirection,
    this.locale,
    this.softWrap,
    this.overflow,
    this.textScaleFactor,
    this.maxLines,
    this.semanticsLabel,
    this.textWidthBasis,
  }) : super(key: key);

  final String? data;
  final TextStyle? style;
  final StrutStyle? strutStyle;
  final TextAlign? textAlign;
  final TextDirection? textDirection;
  final Locale? locale;
  final bool? softWrap;
  final TextOverflow? overflow;
  final double? textScaleFactor;
  final int? maxLines;
  final String? semanticsLabel;
  final TextWidthBasis? textWidthBasis;

  @override
  Widget build(BuildContext context) {
    return LYText.withStyle(data ?? "",
        style: style,
        textAlign: textAlign,
        textDirection: textDirection,
        locale: locale,
        softWrap: softWrap,
        overflow: overflow,
        textScaleFactor: textScaleFactor,
        maxLines: maxLines,
        semanticsLabel: semanticsLabel,
        textWidthBasis: textWidthBasis);
    // return Text(
    //   data ?? "",
    //   style: style?.copyWith(
    //       height: 1,
    //       package: Config.packageName,
    //
    //       /// 为什么这样？用默认属性覆盖，以防止有些自带theme的组件使得文字样式不正确
    //       fontWeight: style!.fontWeight ?? FontWeight.normal,
    //       fontFamily: style!.fontFamily ?? AppStyle.pingFangRegular),
    //   strutStyle: (style?.height ?? 1) == 1
    //       ? null
    //       : StrutStyle(height: 1.21 * style!.height! / 1.5, fontSize: style!.fontSize, leading: 0.2),
    //   textAlign: textAlign,
    //   textDirection: textDirection,
    //   locale: locale,
    //   softWrap: softWrap,
    //   overflow: overflow,
    //   textScaleFactor: textScaleFactor,
    //   maxLines: maxLines,
    //   semanticsLabel: semanticsLabel,
    //   textWidthBasis: textWidthBasis,
    // );
  }
}
