import 'package:flutter/material.dart';
import 'package:flutter_screenutil/flutter_screenutil.dart';
import 'package:zyocore/core/configs/config.dart';
import 'package:zyocore/values/index.dart';

class LYText extends StatelessWidget {
  final String? text;
  final Color? color;
  final double? fontSize;
  final FontWeight? fontWeight;
  final TextOverflow? overflow;
  final TextAlign? textAlign;
  final int? maxLines;
  final bool? softWrap;
  final double? lineHeight;
  final String? fontFamily;
  final TextDirection? textDirection;
  final Locale? locale;
  final double? textScaleFactor;
  final String? semanticsLabel;
  final TextWidthBasis? textWidthBasis;
  final TextLeadingDistribution? leadingDistribution;
  static const Color defautlColor = Color(0xff191919);
  static const FontWeight defautlFontWeight = FontWeight.w400;

  const LYText(
      {Key? key,
      required this.text,
      this.color = defautlColor,
      this.fontSize,
      this.fontWeight = defautlFontWeight, // regular
      this.overflow = TextOverflow.ellipsis,
      this.textAlign = TextAlign.left,
      this.textDirection,
      this.locale,
      this.softWrap,
      this.maxLines,
      this.textScaleFactor,
      this.semanticsLabel,
      this.textWidthBasis,
      this.leadingDistribution,
      this.lineHeight,
      this.fontFamily})
      : super(key: key);

  /// 以传入textStyle方式创建
  LYText.withStyle(
    this.text, {
    Key? key,
    TextStyle? style,
    this.overflow = TextOverflow.ellipsis,
    this.textAlign = TextAlign.left,
    this.textDirection,
    this.locale,
    this.softWrap,
    this.maxLines,
    this.textScaleFactor,
    this.semanticsLabel,
    this.textWidthBasis,
  })  : color = style?.color ?? defautlColor,
        fontSize = style?.fontSize,
        fontWeight = style?.fontWeight ?? defautlFontWeight,
        fontFamily = style?.fontFamily ?? AppFontFamily.pingFangRegular,
        lineHeight = style?.height ?? 1,
        leadingDistribution = style?.leadingDistribution,
        // regular
        super(key: key);

  @override
  Widget build(BuildContext context) {
    return Text(
      text ?? "",
      style: TextStyle(
          package: Config.packageName,
          color: color,
          leadingDistribution: leadingDistribution,
          fontSize: fontSize ?? 12.sp,

          /// 为什么这样？用默认属性覆盖，以防止有些自带theme的组件使得文字样式不正确
          fontWeight: fontWeight ?? defautlFontWeight,
          fontFamily: fontFamily ?? AppFontFamily.pingFangRegular),
      strutStyle:
          (lineHeight ?? 1) == 1 ? null : StrutStyle(height: 1.21 * lineHeight! / 1.5, fontSize: fontSize ?? 12.sp, leading: 0.2),
      textAlign: textAlign,
      textDirection: textDirection,
      locale: locale,
      softWrap: softWrap,
      overflow: overflow,
      textScaleFactor: textScaleFactor,
      maxLines: maxLines,
      semanticsLabel: semanticsLabel,
      textWidthBasis: textWidthBasis,
    );
  }
}
