import 'package:flutter/material.dart';
import 'package:zyocore/widget/index.dart';

@Deprecated('方法已废弃请直接使用LyEmptyWidget')
class EmptyWidget extends StatelessWidget {
  EmptyWidget({
    Key? key,
    this.content,
    this.btnText = "",
    this.onTap,
    this.picUrl,
    this.sanyuHint = "",
    this.btnRadius,
    this.package,
    this.padding = EdgeInsets.zero,
    this.width,
  }) : super(key: key);

  final String btnText;
  final String? content;
  GestureTapCallback? onTap;
  final String? picUrl;
  final String sanyuHint;
  String? package;
  double? btnRadius;
  final EdgeInsets padding;
  double? width;

  @override
  Widget build(BuildContext context) {
    return LyEmptyWidget(
      content: content,
      btnText: btnText,
      picUrl: picUrl,
      sanyuHint: sanyuHint,
      btnRadius: btnRadius,
      onTap: onTap,
      width: width,
      package: package,
    );
  }
}
