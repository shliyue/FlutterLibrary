import 'package:common_utils/common_utils.dart';
import 'package:flutter/material.dart';
import 'package:flutter_screenutil/flutter_screenutil.dart';
import 'package:zyocore/values/style.dart';
import 'package:zyocore/widget/index.dart';

import '../../core/configs/config.dart';
import '../../values/color.dart';

class LyEmptyWidget extends StatelessWidget {
  LyEmptyWidget({
    Key? key,
    this.content,
    this.btnText = "",
    this.onTap,
    this.picUrl,
    this.sanyuHint = "",
    this.btnRadius,
    this.package,
    this.width,
    this.btnWidget,
    this.title,
  }) : super(key: key);

  final String btnText;
  final String? content;
  GestureTapCallback? onTap;
  final String? picUrl;
  final String sanyuHint;
  String? package;
  double? btnRadius;
  double? width;
  Widget? btnWidget;
  String? title;

  @override
  Widget build(BuildContext context) {
    if (package == null) {
      package = Config.packageName;
    } else if (package == "") {
      package = null;
    }
    String tip = content ?? "暂无数据";
    return Center(
      child: Column(
        mainAxisAlignment: MainAxisAlignment.center,
        children: [
          Image.asset(
            picUrl ?? "assets/images/public/not_data_pic.png",
            width: width ?? 121.w,
            fit: BoxFit.fitWidth,
            package: package,
          ),
          SizedBox(height: 16.w),
          TextUtil.isEmpty(title)
              ? const SizedBox()
              : Container(
                  margin: EdgeInsets.only(bottom: 12.w),
                  child: LYText(text: title, fontSize: 17.sp, fontFamily: AppFontFamily.pingFangMedium, fontWeight: FontWeight.w500),
                ),
          TextUtil.isEmpty(tip)
              ? const SizedBox()
              : Container(
                  margin: EdgeInsets.only(bottom: 16.w),
                  width: 225.w,
                  child: Text(
                    tip,
                    style: TextStyle(
                      fontSize: 15.sp,
                      color: dark666666,
                      fontFamily: AppFontFamily.pingFangRegular,
                    ),
                    textAlign: TextAlign.center,
                  ),
                ),
          sanyuHint.isNotEmpty
              ? Container(
                  margin: EdgeInsets.only(bottom: 16.w),
                  width: 225.w,
                  child: Text(
                    sanyuHint,
                    strutStyle: const StrutStyle(leading: 0.5),
                    style: TextStyle(
                      fontSize: 15.sp,
                      color: redE02020,
                      fontFamily: AppFontFamily.pingFangRegular,
                    ),
                    textAlign: TextAlign.center,
                  ),
                )
              : const SizedBox(),
          btnWidget ??
              (btnText.isNotEmpty
                  ? InkWell(
                      child: Container(
                        padding: EdgeInsets.symmetric(vertical: 6.w, horizontal: 16.w),
                        child: Text(
                          btnText,
                          strutStyle: const StrutStyle(leading: 0.5),
                          style: TextStyle(
                            color: lightFFFFFF,
                            fontSize: 14.sp,
                          ),
                        ),
                        decoration: BoxDecoration(
                            color: LibColor.colorMain,
                            borderRadius: BorderRadius.all(
                              Radius.circular(btnRadius ?? 22.w),
                            )),
                      ),
                      onTap: onTap,
                    )
                  : const SizedBox()),
        ],
      ),
    );
  }
}
