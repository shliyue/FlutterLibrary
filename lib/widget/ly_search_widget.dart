import 'package:common_utils/common_utils.dart';
import 'package:flutter/material.dart';
import 'package:zyocore/values/color.dart';
import 'package:flutter_screenutil/flutter_screenutil.dart';
import 'package:get/get.dart';

import '../core/configs/config.dart';
import '../values/style.dart';

class LySearchWidget extends StatelessWidget {
  String? hintText;
  double? borderRadius;
  TextEditingController? tec;
  final Color? cursorColor;
  final Color? bgColor;
  final Color? textBgColor;
  Function(String)? onSearch;
  Function(String)? onChange;

  LySearchWidget(
      {Key? key,
      this.hintText = "搜索关键字",
      this.bgColor = Colors.white,
      this.textBgColor = const Color(0xFFF8F8F8),
      this.borderRadius,
      this.tec,
      this.cursorColor,
      this.onSearch,
      this.onChange})
      : super(key: key);

  var showClean = false.obs;
  String strText = "";

  @override
  Widget build(BuildContext context) {
    tec ??= TextEditingController();
    return Container(
      color: bgColor,
      child: Container(
        alignment: Alignment.center,
        height: 40.w,
        decoration: BoxDecoration(
          color: textBgColor,
          borderRadius: BorderRadius.circular(borderRadius ?? 22.r),
        ),
        margin: EdgeInsets.symmetric(horizontal: 16.w),
        child: Row(
          crossAxisAlignment: CrossAxisAlignment.center,
          children: [
            SizedBox(width: 12.w),
            Image.asset(
              'assets/images/icon_search.png',
              width: 16.w,
              height: 18.w,
              fit: BoxFit.contain,
              package: Config.packageName,
            ),
            SizedBox(width: 8.w),
            Expanded(
              child: TextField(
                keyboardType: TextInputType.text,
                textAlignVertical: TextAlignVertical.center,
                controller: tec,
                textInputAction: TextInputAction.search,
                cursorColor: cursorColor,
                style: TextStyle(fontFamily: AppFontFamily.pingFangRegular, fontSize: 17.sp),
                onChanged: (text) {
                  showClean.value = !TextUtil.isEmpty(text);
                  strText = text;
                  if (onChange != null) {
                    onChange!(strText);
                  }
                },
                onEditingComplete: () {
                  if (onSearch != null) {
                    onSearch!(strText);
                  }
                },
                decoration: InputDecoration(
                  hintText: hintText,
                  hintStyle: TextStyle(
                    fontFamily: AppFontFamily.pingFangRegular,
                    color: dark999999,
                    fontSize: 17.sp,
                  ),
                  focusedBorder: const OutlineInputBorder(borderSide: BorderSide(width: 0, color: Colors.transparent)),
                  disabledBorder: const OutlineInputBorder(borderSide: BorderSide(width: 0, color: Colors.transparent)),
                  enabledBorder: const OutlineInputBorder(borderSide: BorderSide(width: 0, color: Colors.transparent)),
                  border: const OutlineInputBorder(borderSide: BorderSide(width: 0, color: Colors.transparent)),
                  contentPadding: EdgeInsets.zero,
                  // border: InputBorder.none,
                ),
              ),
            ),
            Obx(() {
              return Visibility(
                visible: showClean.value,
                child: InkWell(
                  child: Image.asset(
                    'assets/images/close_grey.png',
                    width: 20.w,
                    height: 20.w,
                    fit: BoxFit.contain,
                    package: Config.packageName,
                  ),
                  onTap: () {
                    tec!.clear();
                    strText = "";
                    showClean.value = false;
                    if (onSearch != null) {
                      onSearch!("");
                    }
                    if (onChange != null) {
                      onChange!("");
                    }
                  },
                ),
              );
            }),
            SizedBox(width: 6.w),
          ],
        ),
      ),
    );
  }
}
