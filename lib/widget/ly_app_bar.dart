import 'package:flutter/material.dart';
import 'package:zyocore/values/style.dart';
import 'package:zyocore/zyocore.dart';

class LYAppBar {
  static AppBar getAppBar({
    String title = '',
    Widget? titleWidget,
    Widget? leading,
    Color? bgColor,
    Color? titleColor,
    double? leadingWidth,
    EnumIconColor? enumIconColor,
    List<Widget>? actions,
    VoidCallback? backTap, //自定义返回事件
    PreferredSizeWidget? bottom,
    Widget? flexibleSpace,
    bool noLeading = false,
    bool canTitleSelect = false,
    double? leadingLeft,
    double? fontSize,
  }) {
    return AppBar(
      backgroundColor: bgColor ?? Colors.white,
      elevation: 0,
      centerTitle: true,
      automaticallyImplyLeading: !noLeading,
      leadingWidth: !noLeading ? leadingWidth : null,
      leading: !noLeading
          ? UnconstrainedBox(
              child: leading ?? LYButtonUtil.backButton(backTap: backTap, enumIconColor: enumIconColor, left: leadingLeft),
            )
          : null,
      actions: actions,
      title: titleWidget ??
          LYText(
            text: title,
            color: titleColor ?? dark191919,
            fontFamily: AppFontFamily.pingFangMedium,
            fontSize: fontSize ?? 17.sp,
          ),
      bottom: bottom,
      flexibleSpace: flexibleSpace,
    );
  }

  /// 一级页面的标题栏
  static AppBar getMainAppBar({required Widget title, List<Widget>? actions}) {
    return getAppBar(titleWidget: title, actions: actions, noLeading: true);
  }

  /// 一级页面的标题栏-透明
  static AppBar getTransparentAppBar({String title = '', bool noLeading = false, List<Widget>? actions}) {
    return getAppBar(title: title, noLeading: noLeading, bgColor: Colors.transparent, actions: actions);
  }

  static AppBar getAppBarSelect(
      {String title = '',
      required List<String> list,
      required Function(String) callback,
      Color? bgColor,
      List<Widget>? actions,
      VoidCallback? backTap, //自定义返回事件
      PreferredSizeWidget? bottom}) {
    Widget titleWidget = AppBarSelectTitle(
      title: title,
      maxWidth: 160.w,
      onTap: () {
        LYPickerUtil.showPickerWithList(Get.context!, list: list, callback: callback);
      },
    );
    return getAppBar(title: title, bgColor: bgColor, actions: actions, bottom: bottom, titleWidget: titleWidget);
  }

  static getAppBarMenu(
      {String title = '',
      required List<String> list,
      required Function(String?) callback,
      Color? bgColor,
      List<Widget>? actions,
      PreferredSizeWidget? bottom}) {
    Widget titleWidget = AppBarSelectTitle(
      title: title,
      onTap: () {
        LYPickerUtil().showSelectorPicker(Get.context!, dataSource: list, currentValue: title, callBack: callback);
      },
    );
    return getAppBar(title: title, bgColor: bgColor, actions: actions, bottom: bottom, titleWidget: titleWidget);
  }
}

class AppBarSelectTitle extends StatelessWidget {
  final String title;
  final double? maxWidth;
  final VoidCallback? onTap;

  const AppBarSelectTitle({Key? key, this.title = '', this.maxWidth, this.onTap}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return GestureDetector(
      child: Row(
        mainAxisAlignment: MainAxisAlignment.center,
        mainAxisSize: MainAxisSize.min,
        children: [
          ConstrainedBox(
            constraints: BoxConstraints(
              maxWidth: maxWidth ?? double.infinity,
            ),
            child: LYText(
              text: title,
              color: dark191919,
              fontFamily: AppFontFamily.pingFangMedium,
              fontSize: 17.sp,
            ),
          ),
          Padding(
            padding: EdgeInsets.only(left: 5.w, top: 5.w),
            child: LYAssetsImage(
              width: 12.w,
              height: 8.w,
              name: 'icon_bar_down.png',
            ),
          ),
        ],
      ),
      onTap: onTap,
    );
  }
}
