import 'package:flutter/material.dart';
import 'package:zyocore/values/index.dart';
import 'package:zyocore/widget/text/ly_text.dart';


class LYTextButton extends StatefulWidget {
  final bool enable;
  final String text;
  final VoidCallback? onTap;
  final double? width;
  final double height;
  final double radius;
  final FontWeight fontWeight;
  final double fontSize;
  final Color textColor;
  final String? fontFamily;


  /// 可用状态下的颜色
  final Color highlightColor;

  /// 不可用状态下的颜色
  final Color color;
  final List<BoxShadow>? boxShadow;

  final EdgeInsetsGeometry? padding;

  const LYTextButton({
    Key? key,
    required this.text,
    this.onTap,
    this.enable = true,
    this.width = 343,
    this.height = 44,
    this.radius = 22,
    this.fontSize = 15,
    this.fontWeight = FontWeight.normal,
    this.fontFamily,
    this.textColor = Colors.white,
    this.highlightColor = const Color(0xFF9F89FF),
    this.color = const Color(0xFFC5B8FF),
    this.padding,
    this.boxShadow = const [
      BoxShadow(
        color: Color(0x66C59AF3),
        offset: Offset(0, 5),
        blurRadius: 18.0,
      )
    ]
  }) : super(key: key);

  @override
  State<LYTextButton> createState() => _LYTextButtonState();
}

class _LYTextButtonState extends State<LYTextButton> {
  bool highlighted = false;

  @override
  Widget build(BuildContext context) {
    if (widget.text.isEmpty) {
      return SizedBox();
    }
    return GestureDetector(
      onTap: widget.enable ? widget.onTap : null,
      onTapDown: (d) {
        setState(() {
          highlighted = true;
        });
      },
      onTapUp: (d) {
        setState(() {
          highlighted = false;
        });
      },
      child: Container(
        width: widget.width,
        height: widget.height,
        alignment: Alignment.center,
        padding: widget.padding,
        decoration: BoxDecoration(
          color:
          widget.enable ? widget.highlightColor : widget.color,
          boxShadow: widget.enable && !highlighted
              ? widget.boxShadow
              : null,
          borderRadius: BorderRadius.circular(widget.radius),
        ),
        child: LYText(
          text: widget.text,
          fontSize: widget.fontSize,
          color: widget.textColor,
          fontWeight: widget.fontWeight,
          fontFamily: widget.fontFamily ?? AppFontFamily.pingFangMedium,
        ),
      ),
    );
  }
}


class LYIconTextButton extends StatefulWidget {
  final bool enable;
  final String text;
  final VoidCallback onTap;
  final double width;
  final double height;
  final double radius;
  final FontWeight fontWeight;
  final String? fontFamily;
  final double fontSize;
  final Color textColor;
  final Widget icon;


  /// 可用状态下的颜色
  final Color highlightColor;

  /// 不可用状态下的颜色
  final Color color;
  final List<BoxShadow>? boxShadow;

  const LYIconTextButton({
    Key? key,
    required this.text,
    required this.icon,
    required this.onTap,
    this.enable = true,
    this.width = 343,
    this.height = 44,
    this.radius = 22,
    this.fontSize = 15,
    this.fontWeight = FontWeight.normal,
    this.fontFamily,
    this.textColor = Colors.white,
    this.highlightColor = const Color(0xFF9F89FF),
    this.color = const Color(0xFFC5B8FF),
    this.boxShadow = const [
      BoxShadow(
        color: Color(0x66C59AF3),
        offset: Offset(0, 5),
        blurRadius: 18.0,
      )
    ]
  }) : super(key: key);

  @override
  State<LYIconTextButton> createState() => _LYIconTextButtonState();
}

class _LYIconTextButtonState extends State<LYIconTextButton> {
  bool highlighted = false;

  @override
  Widget build(BuildContext context) {
    return GestureDetector(
      onTap: widget.enable ? widget.onTap : null,
      onTapDown: (d) {
        setState(() {
          highlighted = true;
        });
      },
      onTapUp: (d) {
        setState(() {
          highlighted = false;
        });
      },
      child: Container(
        width: widget.width,
        height: widget.height,
        decoration: BoxDecoration(
          color:
          widget.enable ? widget.highlightColor : widget.color,
          boxShadow: widget.enable && !highlighted
              ? widget.boxShadow
              : null,
          borderRadius: BorderRadius.circular(widget.radius),
        ),
        child: Center(
            child: Row(
              mainAxisAlignment: MainAxisAlignment.center,
              children: [
                widget.icon,
                SizedBox(width: 12),
                LYText(
                  text: widget.text,
                  fontSize: widget.fontSize,
                  color: widget.textColor,
                  fontWeight: widget.fontWeight,
                  fontFamily: widget.fontFamily ?? AppFontFamily.pingFangMedium,
                ),
              ],)
        ),
      ),
    );
  }
}
