import 'package:cached_network_image/cached_network_image.dart';
import 'package:flutter/material.dart';
import 'package:zyocore/widget/common/image_placeholder_widget.dart';

import '../util/file_util.dart';

@Deprecated('后面版本将弃用,请使用LYImageWidget显示图片')
class LYWebImageWidget extends StatelessWidget {
  final double? width;
  final double? height;
  final String url;
  final double radius;
  final BoxFit? fit;
  final Color? backgroundColor;
  final AssetImage? error;
  final GestureTapCallback? onLongPress;
  final GestureTapCallback? onTap;

  const LYWebImageWidget(
      {Key? key,
      required this.url,
      this.width,
      this.height,
      this.radius = 0,
      this.fit,
      this.backgroundColor,
      this.error,
      this.onLongPress,
      this.onTap})
      : super(key: key);

  @override
  Widget build(BuildContext context) {
    var imageUrl = url;
    if (!url.startsWith("http")) {
      imageUrl = FileUtil.getThumbsImageUrl(url);
    }

    var image = CachedNetworkImage(
        width: width,
        height: height,
        fit: fit,
        imageUrl: url,
        placeholder: (context, url) {
          return const ImagePlaceholderWidget(
            type: 1,
            state: 0,
          );
        },
        errorWidget: (a, b, c) => error == null
            ? const ImagePlaceholderWidget(
                type: 1,
                state: 1,
              )
            : Image(image: error!));

    return GestureDetector(
      onTap: onTap,
      onLongPress: onLongPress,
      child: ClipRRect(
        clipBehavior: Clip.hardEdge,
        borderRadius: BorderRadius.circular(radius),
        child: Container(
          width: width,
          height: height,
          color: backgroundColor,
          child: image,
        ),
      ),
    );
  }
}
