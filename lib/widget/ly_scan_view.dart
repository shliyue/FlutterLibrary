import 'dart:io';
import 'package:flutter/material.dart';
import 'package:scan/scan.dart';
import 'package:zyocore/zyocore.dart';

class LyScanView extends StatefulWidget {
  const LyScanView({Key? key}) : super(key: key);

  @override
  State<LyScanView> createState() => _LyScanViewState();
}

class _LyScanViewState extends State<LyScanView> {
  final ScanController _controller = ScanController();

  @override
  void initState() {
    // TODO: implement initState
    super.initState();
  }

  void getScanResult(String rs) {
    Logger.d("+++++++++扫码结果+++++++++++:${rs}");
    _controller.pause();
    Get.back(result: rs);
  }

  Future<void> onSelectImage() async {
    try {
      File? file = await AssetUtil.onAssetsPhoto(Get.context!, true);
      if (file != null) {
        String? result = await Scan.parse(file.path);
        getScanResult(result!);
      }
    } catch (e) {
      getScanResult("");
    }
  }

  @override
  Widget build(BuildContext context) {
    return Stack(
      children: [
        ScanView(
          controller: _controller,
          scanLineColor: theme9F89FF,
          onCapture: (data) {
            getScanResult(data);
          },
        ),
        Positioned(
          top: 54.w,
          left: 24.w,
          child: GestureDetector(
            onTap: () {
              getScanResult("");
            },
            child: Container(
              width: 24.w,
              height: 24.w,
              decoration: BoxDecoration(
                borderRadius: BorderRadius.all(Radius.circular(24.w)),
                color: Colors.white,
              ),
              child: const Icon(Icons.chevron_left),
            ),
          ),
        ),
        Positioned(
          left: 10.w,
          right: 10.w,
          bottom: 47.w + ScreenUtil().bottomBarHeight,
          child: Column(
            mainAxisAlignment: MainAxisAlignment.center,
            crossAxisAlignment: CrossAxisAlignment.center,
            children: [
              LYText(
                text: "将二维码对准屏幕中间区域",
                color: Colors.white,
                fontSize: 14.sp,
              ),
              SizedBox(height: 80.w),
              LYButtonUtil.imgTextVerticalButton(
                  text: "相册",
                  icon: "icon_album.png",
                  iconWidth: 44.w,
                  iconHeight: 44.w,
                  fontColor: Colors.white,
                  fontSize: 15.sp,
                  imgBottomSpacing: 9.w,
                  onTap: () {
                    onSelectImage();
                  })
            ],
          ),
        ),
      ],
    );
  }
}
