import 'package:flutter/material.dart';
import 'package:flutter_screenutil/flutter_screenutil.dart';
import 'package:zyocore/core/model/select_model.dart';
import 'package:zyocore/values/color.dart';
import 'package:zyocore/widget/icons/ly_radio_icon.dart';

import '../ly_assets_image.dart';
import '../ly_avatar_widget.dart';
import '../text/ly_text.dart';

class LYSelectItem {
  static Widget itemAvatarWidget({
    required SelectModel item,
    Function(SelectModel)? onTap,
    int height = 64,
    bool avatar = true,
    int fontSize = 17,
    Color colorLine = const Color(0xFFE1E1E1),
    String? packageName,
    bool isSearch=false
  }) {
    return GestureDetector(
      child: Container(
        // color: Colors.white,
        height: height.w,
        decoration:isSearch? BoxDecoration(border: Border(bottom: BorderSide(color: colorLine, width: 1.w))):null,
        child: Row(
          crossAxisAlignment: CrossAxisAlignment.center,
          children: [
            LYRadioIcon(
              status: item.checked ? 1 : 0,
              colorType: 1,
              packageName: packageName,
            ),
            if (avatar) SizedBox(width: 16.w),
            if (avatar) LYAvatarWidget(src: item.avatar ?? "", width: 40.w, height: 40.w, radius: 40.r),
            SizedBox(width: 12.w),
            Expanded(
              child: Container(
                height: height.w,
                decoration:!isSearch?BoxDecoration(border: Border(bottom: BorderSide(color: colorLine, width: 1.w))):null,
                child: Column(
                  mainAxisAlignment: MainAxisAlignment.center,
                  crossAxisAlignment: CrossAxisAlignment.start,
                  children: [
                    LYText(
                      text: item.keyName,
                      fontSize: fontSize.sp,
                      color: dark1A1A1A,
                    ),
                    item.reshuffle ? LYText(text: "已异动", color: yellowFFA33C, fontSize: 12.sp) : const SizedBox(),
                  ],
                ),
              ),
            ),
          ],
        ),
      ),
      onTap: () {
        if (onTap != null) {
          onTap(item);
        }
      },
    );
  }

  static Widget itemParentWidget({
    required SelectListModel item,
    Function(SelectListModel)? onSelect,
    Function(SelectListModel)? onChangeOpen,
    int height = 56,
    int fontSize = 17,
    Color colorLine = const Color(0xFFE1E1E1),
    String? packageName,
    bool isSearch=false
  }) {
    return GestureDetector(
      onTap: () {
        if (onChangeOpen != null && !isSearch ) {
          onChangeOpen(item);
        }
      },
      child: Container(
        height: height.w,
        child: Row(
          crossAxisAlignment: CrossAxisAlignment.center,
          children: [
          if(isSearch)  LYText(
              text: item.keyName,
              fontSize: 15.sp,
              color: dark666666,
            ),
            if(!isSearch) GestureDetector(
              child: LYRadioIcon(
                status: item.checked ? 1 : 0,
                colorType: 1,
                packageName: packageName,
              ),
              onTap: () {
                if (onSelect != null) {
                  onSelect(item);
                }
              },
            ),
            if(!isSearch) Expanded(
              child: Container(
                height: height.w,
                padding: EdgeInsets.only(right: 16.w),
                decoration: BoxDecoration(
                  border: Border(bottom: BorderSide(color: colorLine, width: 1.w)),
                ),
                child: Row(
                  mainAxisSize: MainAxisSize.min,
                  children: [
                    SizedBox(
                      width: 12.w,
                    ),
                    ConstrainedBox(
                      constraints: BoxConstraints(
                        maxWidth: 260.w,
                      ),
                      child: LYText(
                        text: item.keyName,
                        fontSize: fontSize.sp,
                        color: dark1A1A1A,
                      ),
                    ),
                    const Spacer(),
                    LYAssetsImage(
                      name: item.isOpen ? "icon_down.png" : 'icon_right.png',
                      width: item.isOpen ? 12.w : 8.w,
                    ),
                  ],
                ),
              ),
            ),
          ],
        ),
      ),
    );
  }
}
