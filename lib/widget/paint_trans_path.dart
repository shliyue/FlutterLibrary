import 'dart:ui';

import 'package:flutter/material.dart';

class PaintTransPath extends CustomPainter {
  final double clipWidth;
  final double clipHeight;
  final double left;
  final double top;
  final double? borderRadius;
  final double? lborderRadius;
  final double? rborderRadius;

  PaintTransPath({required this.clipWidth, required this.clipHeight, this.borderRadius, required this.left, required this.top, this.lborderRadius, this.rborderRadius});

  @override
  void paint(Canvas canvas, Size size) {
    var _lborderRadius=borderRadius??(lborderRadius ?? 0);
    var _rborderRadius=borderRadius??(rborderRadius ?? 0);
    Path path = Path();

    // Rect rect = Rect.fromCircle(center: Offset(clipWidth, clipHeight), radius: 100);
    // RRect rrect = RRect.fromRectAndRadius(rect, Radius.circular(borderRadius));
    // // paint.color = Colors.transparent;
    // canvas.clipRRect(rrect);
    path.moveTo(left, top);
    path.lineTo(clipWidth + left, top);
    path.lineTo(clipWidth + left, top + clipHeight);
    path.lineTo(left, top + clipHeight);
    path.close();
    // canvas.clipPath(path);
    Paint paint = Paint()
      ..color = Colors.transparent
      ..style = PaintingStyle.fill;
    canvas.drawPath(path, paint);
    paint = Paint()
      ..color = Colors.black.withOpacity(0.6)
      ..style = PaintingStyle.fill;
    Path path1 = Path();
    path1.moveTo(0, 0);
    path1.lineTo(size.width, 0);
    path1.lineTo(size.width, top);
    path1.lineTo(left + _lborderRadius, top);
    // path1.lineTo(left+borderRadius/2, top+clipHeight);
    path1.arcToPoint(
        Offset(left + _lborderRadius, top + clipHeight),
        radius: Radius.circular(_lborderRadius),
        clockwise: false
    );
    path1.lineTo(left + clipWidth - _rborderRadius, top + clipHeight);
    // path1.lineTo(left+clipWidth, top);
    path1.arcToPoint(
        Offset(left + clipWidth - _rborderRadius, top),
        radius: Radius.circular(_rborderRadius),
        clockwise: false
    );
    path1.lineTo(size.width, top);
    path1.lineTo(size.width, size.height);
    path1.lineTo(0, size.height);
    path1.close();
    canvas.drawPath(path1, paint);
  }

  @override
  bool shouldRepaint(covariant CustomPainter oldDelegate) {
    // TODO: implement shouldRepaint
    return false;
  }
}
