import 'package:flutter/cupertino.dart';
import 'package:zyocore/util/toast_util.dart';
import 'package:zyocore/util/utils.dart';
import 'package:zyocore/values/circular_radius.dart';
import 'package:zyocore/values/color.dart';
import 'package:zyocore/values/edge_insets.dart';
import 'package:zyocore/values/text.dart';
import 'package:zyocore/widget/common/expanded_pop_widget.dart';
import 'package:zyocore/widget/common/flex/column.dart';
import 'package:zyocore/widget/common/flex/row.dart';

import '../../text/ly_text.dart';

class TimePickerActionSheet extends StatefulWidget {
  final String beginTime;
  final String endTime;

  const TimePickerActionSheet(this.beginTime, this.endTime, {Key? key}) : super(key: key);

  @override
  _TimePickerActionSheetState createState() => _TimePickerActionSheetState();
}

class _TimePickerActionSheetState extends State<TimePickerActionSheet> {

  String? beginTime;
  String? endTime;

  @override
  initState() {
    super.initState();
    beginTime = widget.beginTime;
    endTime = widget.endTime;
  }

  @override
  Widget build(BuildContext context) {
    return SafeArea(
      child: CSS(
        children: <Widget>[
          const ExpandedPopWidget(),
          CSS(
            children: [
              GestureDetector(
                onTap: () {
                  if (utils.date.parseDuration(endTime!).compareTo( utils.date.parseDuration(beginTime!)) <= 0) {
                    ToastUtil.showToast("结束时间必须大于开始时间");
                    return;
                  }
                  Navigator.pop(context, [beginTime, endTime]);
                },
                child: _done
              ),
              _weekday
            ],
          ),
        ],
      ),
    );
  }

  Widget get _done {
    return Container(
      margin: bottom10 + right10,
      child: RES(
        children: [
          LYText.withStyle(
            "确认",
            style: text16.grape,
          )
        ],
      ),
    );
  }

  Widget get _weekday {
    return Container(
      padding: horizontal20,
      height: 220,
      color: dark1A1A1A,
      child: Stack(
        alignment: Alignment.center,
        children: [
          Positioned(
            child: Container(
              height: 38,
              decoration: BoxDecoration(
                color: dark666666,
                borderRadius: circular6
              ),
            )
          ),
          Container(
            padding: horizontal40,
            child: RSC(
              children: [
                Container(
                  width: 75,
                  child: RBC(
                    children: [
                      _renderTime(1),
                      LYText.withStyle(":", style: text20.grape.familyMedium,),
                      _renderTime(2),
                    ],
                  ),
                ),
                Expanded(child: Center(child: LYText.withStyle("至", style: text16.grape.familyMedium,))),
                Container(
                  width: 75,
                  child: RBC(
                    children: [
                      _renderTime(3),
                      LYText.withStyle(":", style: text20.grape.familyMedium,),
                      _renderTime(4),
                    ],
                  ),
                ),
              ],
            ),
          ),
        ],
      ),
    );
  }

  Widget _renderTime(int d) {
    List<String> list = List.generate(d.isEven ? getSelectedString(d - 1) == '24' ? 1 : 60 : 25, (index) => "${index < 10 ? 0 : ''}$index");
    String selectedStr = (d < 3 ? beginTime : endTime)!.split(":").elementAt((d - 1) % 2);
    return Flexible(
      child: CupertinoPicker(
        selectionOverlay : null,
        key: Key("time:picker:$d${d.isEven && getSelectedString(d - 1) == '24' ? "24" : ""}"), /// 控制刷新
        scrollController: FixedExtentScrollController(
          initialItem: list.indexOf(selectedStr),
        ),
        itemExtent: 38.0,
        onSelectedItemChanged: (int index) {
          setState(() {
            if (d < 3) {
              List<String> arr = beginTime!.split(":");
              beginTime = d.isEven ? "${arr[0]}:${list[index]}" :  "${list[index]}:${arr[1]}";
            } else {
              List<String> arr = endTime!.split(":");
              endTime = d.isEven ? "${arr[0]}:${list[index]}" :  "${list[index]}:${arr[1]}";
            }
          });
        },
        children: list.map((t) {
          bool selected = selectedStr == t;
          return Center(child: LYText.withStyle(t, style: selected ? text20.grape : text16.gray,));
        }).toList()
      ),
    );
  }


  String getSelectedString(int d) {
    return (d < 3 ? beginTime : endTime)!.split(":").elementAt((d - 1) % 2);
  }


}
