import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:zyocore/util/app_utils.dart';
import 'package:zyocore/util/utils.dart';
import 'package:zyocore/widget/action_sheets/action_sheet_cell_widget.dart';
import 'package:zyocore/widget/action_sheets/action_sheet_wrapper.dart';
import 'package:zyocore/widget/common/flex/column.dart';

import '../../../util/logger.dart';

/// 拨打电话弹窗
class PhoneCallActionSheet extends StatefulWidget {
  final List<String> data;

  const PhoneCallActionSheet(this.data, {Key? key}) : super(key: key);

  @override
  _PhoneCallActionSheetState createState() => _PhoneCallActionSheetState();
}

class _PhoneCallActionSheetState extends State<PhoneCallActionSheet> {
  @override
  initState() {
    super.initState();
  }

  /// 拨打电话
  callPhone(String phone) {
    try {
      AppUtils.onCallPhone(phone: phone);
    } catch (e) {
      Logger.d('callPhone error=$e');
    }
  }

  @override
  Widget build(BuildContext context) {
    return ActionSheetWrapper(
      close: () {
        Navigator.pop(context);
      },
      child: CSS(
        children:
            widget.data.map((e) => _buildCertificatesItem(context, e)).toList(),
      ),
    );
  }

  Widget _buildCertificatesItem(BuildContext context, String entity) {
    return GestureDetector(
        behavior: HitTestBehavior.opaque,
        onTap: () {
          Navigator.pop(context, entity);
          callPhone(entity);
        },
        child: ActionSheetCellWidget(
          title: '呼叫 ${utils.text.formatSpacePhone(entity)}',
        ));
  }
}
