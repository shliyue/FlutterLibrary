import 'package:flutter/material.dart';
import 'package:zyocore/util/utils.dart';
import 'package:zyocore/values/color.dart';
import 'package:zyocore/values/edge_insets.dart';
import 'package:zyocore/values/text.dart';
import 'package:zyocore/widget/common/expanded_pop_widget.dart';
import 'package:zyocore/widget/common/flex/column.dart';
import 'package:zyocore/widget/common/flex/row.dart';

import '../../text/ly_text.dart';

class WeekdayActionSheet extends StatefulWidget {
  final List<int> selected;

  const WeekdayActionSheet(this.selected, {Key? key}) : super(key: key);

  @override
  _WeekdayActionSheetState createState() => _WeekdayActionSheetState();
}

class _WeekdayActionSheetState extends State<WeekdayActionSheet> {

  List<int>? selected;

  @override
  initState() {
    super.initState();
    selected = widget.selected;
  }

  @override
  Widget build(BuildContext context) {
    return SafeArea(
      child: CSS(
        children: <Widget>[
          ExpandedPopWidget(),
          Container(
            child: CSS(
              children: [
                GestureDetector(
                  onTap: () {
                    selected!.sort((a, b) => a - b);
                    Navigator.pop(context, selected);
                  },
                  child: _done
                ),
                _weekday
              ],
            ),
          ),
        ],
      ),
    );
  }

  Widget get _done {
    return Container(
      margin: bottom10 + right10,
      child: RES(
        children: [
          LYText.withStyle(
            "确认",
            style: text16.grape,
          )
        ],
      ),
    );
  }

  Widget get _weekday {
    return CSS(
      children: [0, 1, 2, 3, 4, 5, 6].map((e) {
        bool isChecked = selected!.indexWhere((element) => e == element) > -1;
        return CSS(
          children: [
            GestureDetector(
              behavior: HitTestBehavior.opaque,
              onTap: () {
                setState(() {
                  if (isChecked) {
                    selected!.remove(e);
                  } else {
                    selected = [...selected!, e];
                  }
                });
              },
              child: Container(
                padding: horizontal20,
                height: 40,
                color: dark1A1A1A,
                child: RBC(
                  children: [
                    LYText.withStyle("星期${utils.date.getWeekdayZh(e)}", style: text14,),
                    isChecked ? const Icon(
                      Icons.close,
                      size: 24,
                      color: Colors.white,
                    ) : Container()
                  ],
                ),
              ),
            ),
            const Divider(height: 1, color: dark1A1A1A,),
          ],
        );
      }).toList(),
    );
  }
}
