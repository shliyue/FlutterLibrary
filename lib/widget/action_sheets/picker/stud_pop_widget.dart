import 'package:flutter/material.dart';
import 'package:zyocore/core/model/select_model.dart';
import 'package:zyocore/values/style.dart';
import 'package:zyocore/widget/common/expanded_pop_widget.dart';
import 'package:zyocore/widget/select_item/ly_select_item.dart';
import 'package:zyocore/zyocore.dart';

import '../../../core/configs/config.dart';
import '../../../values/edge_insets.dart';
import '../../../values/text.dart';
import '../../common/flex/row.dart';

class StudPopWidget extends StatefulWidget {
  final ValueChanged<List<SelectModel>> callback;
  final List<SelectModel>? selects;
  final List<SelectListModel> listData;
  final String? packageName;
  final bool searchVisible;

  const StudPopWidget(
      {Key? key,
      required this.callback,
      this.selects,
      required this.listData,
      this.packageName,
      this.searchVisible = true})
      : super(key: key);

  @override
  _StudPopWidgetState createState() {
    return _StudPopWidgetState();
  }
}

class _StudPopWidgetState extends State<StudPopWidget> {
  int count = 0;
  var selectItem = <SelectModel>[];
  TextEditingController textController = TextEditingController();
  String keyWord = "";
  FocusNode? focusNode;
  bool onFocus = false;
  List<SelectListModel> listSearch = [];

  @override
  void initState() {
    super.initState();
    _refreshClassList();
    focusNode = FocusNode();
    focusNode!.addListener(() {
      if (focusNode!.hasFocus) {
        onFocus = true;
      } else {
        onFocus = false;
      }
      setState(() {});
    });
  }

  void _refreshClassList() {
    for (var element in widget.listData) {
      element.isOpen = false;
      element.checked = false;
      for (var item in element.listChild!) {
        item.checked = false;
      }
    }
    //只有一个班级时候默认展开
    if (widget.listData.isNotEmpty) {
      if (widget.listData.length == 1) {
        widget.listData[0].isOpen = true;
      }
    }
    if (widget.selects != null) {
      if (widget.selects!.isEmpty) {
        return;
      }
      for (var element in widget.listData) {
        for (var item in widget.selects!) {
          var obj = element.listChild!.where((e) => e.key == item.key).toList();
          if (obj.isNotEmpty) {
            ///存在选中
            obj.first.checked = true;
            selectItem.addAll(obj);

            ///默认展开
            element.isOpen = true;
          }
        }

        ///检查是否全选
        var unList = element.listChild!.where((e) => !e.checked).toList();
        element.checked = unList.isEmpty && element.listChild!.isNotEmpty;
      }
      count = selectItem.length;
    }
  }

  void doSearch() {
    listSearch = [];
    if (keyWord.isEmpty) {
      for (var element in widget.listData) {
        element.isHide = false;
        for (var item in element.listChild!) {
          item.isHide = false;
        }

        ///选中展开
        for (var item in selectItem!) {
          var obj = element.listChild!.where((e) => e.key == item.key).toList();
          if (obj.isNotEmpty) {
            ///默认展开
            element.isOpen = true;
          }
        }
      }
    } else {
      for (var element in widget.listData) {
        element.isHide = true;
        for (var item in element.listChild!) {
          item.isHide = true;
        }
        var obj = element.listChild!
            .where((e) => e.keyName.contains(keyWord))
            .toList();
        if (obj.isNotEmpty) {
          for (var e1 in obj) {
            e1.isHide = false;
          }
          element.isOpen = true;
          element.isHide = false;
          listSearch.add(SelectListModel(
              keyName: element.keyName, key: element.key, listChild: obj));
        }
      }
    }
    setState(() {});
  }

  @override
  void dispose() {
    super.dispose();
  }

  @override
  Widget build(BuildContext context) {
    // TODO: implement build
    return SafeArea(
      child: Column(
        children: [
          const ExpandedPopWidget(),
          titleBar(),
          Container(
            child: Column(
              children: [
                if (widget.searchVisible) searchWidget(),
                _buildClass(widget.listData),
              ],
            ),
            color: Colors.white,
          )
        ],
      ),
    );
  }

  Widget titleBar() {
    return Column(
      children: [
        Container(
          color: Colors.white,
          padding: EdgeInsets.symmetric(vertical: 16.w, horizontal: 16.w),
          child: Row(
            children: [
              GestureDetector(
                child: Image.asset(
                  'assets/images/icon_navi_close.png',
                  width: 24.w,
                  height: 24.w,
                  fit: BoxFit.contain,
                  package: Config.packageName,
                ),
                onTap: () {
                  Get.back();
                  // Navigator.pop(Get.context!);
                },
              ),
              const Spacer(),
              LYText(
                text: '幼儿名单',
                fontSize: 17.sp,
                color: dark1A1A1A,
              ),
              const Spacer(),
              GestureDetector(
                child: LYText(
                  text: '确定${count > 0 ? '(' + count.toString() + ')' : ''}',
                  fontSize: 17.sp,
                  color: LibColor.colorMain,
                ),
                onTap: () {
                  if (selectItem.isEmpty) {
                    ToastUtil.showToast('请选择幼儿');
                    return;
                  }
                  widget.callback(selectItem);
                  Get.back();
                },
              ),
            ],
          ),
        ),
      ],
    );
  }

  Widget _buildClass(List<SelectListModel> list) {
    // Logger.d(ScreenUtil().screenHeight);
    return Container(
      color: Colors.white,
      height: ScreenUtil().screenHeight * 0.78,
      child: ListView.builder(
        itemBuilder: (e, index) {
          return _buildItemClass(list[index]);
        },
        itemCount: list.length,
      ),
    );
  }

  Widget _buildItemClass(SelectListModel item) {
    return Container(
      padding: EdgeInsets.only(left: 16.w),
      child: Column(
        children: [
          if (!item.isHide)
            LYSelectItem.itemParentWidget(
                packageName: widget.packageName,
                item: item,
                isSearch: listSearch.isNotEmpty,
                onChangeOpen: (e) {
                  item.isOpen = !item.isOpen;
                  setState(() {});
                },
                onSelect: (e) {
                  item.checked = !item.checked;
                  for (int i = 0; i < item.listChild!.length; i++) {
                    item.listChild![i].checked = item.checked;
                  }
                  selectCount();
                  setState(() {});
                }),
          ListView.builder(
            physics: const NeverScrollableScrollPhysics(),
            shrinkWrap: true,
            itemBuilder: (e, index) {
              return Visibility(
                child: item.isHide
                    ? Container()
                    : Container(
                        margin: listSearch.isNotEmpty
                            ? EdgeInsets.only(left: 0.w)
                            : EdgeInsets.only(left: 31.w),
                        child: item.listChild![index].isHide
                            ? Container()
                            : LYSelectItem.itemAvatarWidget(
                                packageName: widget.packageName,
                                item: item.listChild![index],
                                isSearch: listSearch.isNotEmpty,
                                onTap: (e) {
                                  item.listChild![index].checked =
                                      !item.listChild![index].checked;
                                  selectCount();

                                  ///检查是否全选
                                  var unList = item.listChild!
                                      .where((e) => !e.checked)
                                      .toList();
                                  item.checked = unList.isEmpty;
                                  setState(() {});
                                },
                              ),
                      ),
                visible: item.isOpen,
              );
            },
            itemCount: item.listChild!.length,
          )
        ],
      ),
    );
  }

  void selectCount() {
    selectItem.clear();
    for (var value in widget.listData) {
      var items = value.listChild!.where((element) => element.checked).toList();
      selectItem.addAll(items);
    }
    count = selectItem.length;
    setState(() {});
  }

  Widget searchWidget() {
    return Container(
      color: Colors.white,
      height: 40.w,
      padding: EdgeInsets.symmetric(horizontal: 16.w),
      child: RSC(
        children: [
          Expanded(
            child: textField(),
          ),
          if (onFocus) cancelWidget(),
        ],
      ),
    );
  }

  Widget cancelWidget() {
    return GestureDetector(
      onTap: () {
        focusNode?.unfocus();
        onClear();
      },
      child: Container(
        padding: EdgeInsets.only(left: 12.w),
        child: LYText.withStyle('取消', style: text14.dark),
      ),
    );
  }

  Widget textField() {
    return Container(
      clipBehavior: Clip.hardEdge,
      height: 36.w,
      width: ScreenUtil().screenWidth - 81.w,
      decoration: BoxDecoration(
        color: const Color(0xFFF7F7F7),
        borderRadius: BorderRadius.circular(5.r),
      ),
      child: RSC(
        children: [
          Container(
            height: 36.w,
            padding: left12 + right10,
            child: Image.asset(
              'assets/images/icon_search.png',
              width: 20.w,
              height: 22.5.w,
              fit: BoxFit.contain,
              package: Config.packageName,
            ),
          ),
          SizedBox(
            height: 36.w,
            width: ScreenUtil().screenWidth - 119.w - 24.w,
            child: TextField(
              focusNode: focusNode,
              style: text17.dark,
              keyboardType: TextInputType.text,
              autofocus: false,
              onChanged: (text) {
                keyWord = text.trim();
                Logger.d("keyWord" + text.trim());
                doSearch();
              },
              onEditingComplete: () {
                Logger.d("onEditingComplete");
              },
              onSubmitted: (text) {},
              maxLength: 100,
              controller: textController,
              textInputAction: TextInputAction.search,
              textAlignVertical: TextAlignVertical.center,
              cursorColor: dark666666,
              decoration: InputDecoration(
                counterText: '',
                hintText: '',
                hintStyle: text14,
                isDense: true,
                border: InputBorder.none,
              ),
            ),
          ),
          if (keyWord.isNotEmpty)
            GestureDetector(
              onTap: () => onClear(),
              child: Container(
                height: 36.w,
                // padding: left12 + right10,
                child: Image.asset(
                  'assets/images/close_grey.png',
                  width: 24.w,
                  height: 24.w,
                  fit: BoxFit.contain,
                  package: Config.packageName,
                ),
              ),
            ),
        ],
      ),
    );
  }

  /// 清空
  onClear() {
    keyWord = "";
    textController.clear();
    doSearch();
  }
}
