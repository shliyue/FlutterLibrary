import 'package:flutter/material.dart';
import 'package:flutter_screenutil/flutter_screenutil.dart';
import 'package:zyocore/values/color.dart';
import 'package:zyocore/values/edge_insets.dart';
import 'package:zyocore/values/text.dart';

import '../text/ly_text.dart';

class ActionSheetCellWidget extends StatelessWidget {
  final String? title;
  final Widget? widget;
  final TextStyle? style;

  const ActionSheetCellWidget({Key? key, this.title, this.widget, this.style})
      : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Container(
      padding: horizontal16,
      height: title == "取消" ? 64.w : 56.w,
      decoration: BoxDecoration(
        border: title == "取消" ? Border(
            top: BorderSide(color: lightE1E1E1, width: 8.w)) : Border(
            bottom: BorderSide(color: lightE1E1E1, width: 1.w)),
      ),
      child: Center(
        child: widget ?? LYText.withStyle(
          title,
          style: style ?? text17.dark,
        ),
      ),
    );
  }
}
