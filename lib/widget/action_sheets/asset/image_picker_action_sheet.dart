import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:zyocore/widget/action_sheets/action_sheet_cell_widget.dart';
import 'package:zyocore/widget/action_sheets/action_sheet_wrapper.dart';
import 'package:zyocore/widget/common/flex/column.dart';

/// 图片选择弹窗
class ImagePickerActionSheet extends StatefulWidget {
  final List<String> data;

  const ImagePickerActionSheet(this.data, {Key? key}) : super(key: key);

  @override
  _ImagePickerActionSheetState createState() => _ImagePickerActionSheetState();
}

class _ImagePickerActionSheetState extends State<ImagePickerActionSheet> {
  @override
  initState() {
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    return ActionSheetWrapper(
      close: () {
        Navigator.pop(context);
      },
      child: CSS(
        children:
            widget.data.map((e) => _buildCertificatesItem(context, e)).toList(),
      ),
    );
  }

  Widget _buildCertificatesItem(BuildContext context, String entity) {
    return GestureDetector(
        behavior: HitTestBehavior.opaque,
        onTap: () {
          Navigator.pop(context, entity);
        },
        child: ActionSheetCellWidget(
          title: entity,
        ));
  }
}
