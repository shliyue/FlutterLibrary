import 'package:flutter/material.dart';
import 'package:flutter_screenutil/src/size_extension.dart';
import 'package:zyocore/values/color.dart';
import 'package:zyocore/widget/action_sheets/action_sheet_cell_widget.dart';
import 'package:zyocore/widget/common/expanded_pop_widget.dart';
import 'package:zyocore/widget/common/flex/column.dart';

class ActionSheetWrapper extends StatelessWidget {
  final Function? close;
  final Widget child;
  final bool cancel;
  final Color shapeColor;

  const ActionSheetWrapper(
      {Key? key,
      this.close,
      required this.child,
      this.cancel = true,
      this.shapeColor = lightFFFFFF})
      : super(key: key);

  @override
  Widget build(BuildContext context) {
    return CSS(
      children: <Widget>[
        const ExpandedPopWidget(),
        Container(
          decoration: BoxDecoration(
              color: shapeColor,
              borderRadius: BorderRadius.only(
                topRight: Radius.circular(8.r),
                topLeft: Radius.circular(8.r),
              )),
          child: SafeArea(
            child: CSS(
              children: [
                child,
                cancel
                    ? GestureDetector(
                        behavior: HitTestBehavior.opaque,
                        onTap: close as void Function()?,
                        child: const ActionSheetCellWidget(
                          title: "取消",
                        ))
                    : Container()
              ],
            ),
          ),
        ),
      ],
    );
  }
}
