import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter/scheduler.dart';
import 'package:zyocore/values/edge_insets.dart';
import 'package:zyocore/values/text.dart';
import 'package:zyocore/zyocore.dart';

class TwoWayListView<T> extends StatefulWidget {
  final VoidCallback? insertCallBack;
  final VoidCallback? addCallBack;
  final List<T> topSource;
  final List<T> bottomSource;
  final bool topMore;
  final bool bottomMore;
  final Function(int)? topScrollCallback;
  final Function(int)? bottomScrollCallback;
  final Widget Function(BuildContext context, int index, T entity, bool isTopList) builder;

  const TwoWayListView({
    Key? key,
    required this.topSource,
    required this.bottomSource,
    required this.builder,
    this.insertCallBack,
    this.addCallBack,
    this.topMore = false,
    this.bottomMore = false,
    this.topScrollCallback,
    this.bottomScrollCallback,
  }) : super(key: key);

  @override
  State<TwoWayListView<T>> createState() => _TwoWayListViewState<T>();
}

class _TwoWayListViewState<T> extends State<TwoWayListView<T>> {
  ValueKey centerKey = const ValueKey('center');

  double extentAfter = 0;
  bool enableScroll = false;

  var scroller = ScrollController();

  @override
  Widget build(BuildContext context) {
    return NotificationListener(
        onNotification: (notification) {
          if (notification is ScrollNotification) {
            // Logger.d(
            //     'onNotification 11=${notification.metrics is PageMetrics}');
            if (notification.metrics is PageMetrics) {
              return false;
            }
            // Logger.d(
            //     'onNotification 22=${notification.metrics is FixedScrollMetrics}');
            if (notification.metrics is FixedScrollMetrics) {
              if (notification.metrics.axisDirection == AxisDirection.left ||
                  notification.metrics.axisDirection == AxisDirection.right) {
                return false;
              }
            }
            // Logger.d(
            //     'onNotification callback=${notification.metrics.extentAfter}');
            extentAfter = notification.metrics.extentAfter;
            enableScroll = true;
          }
          return false;
        },
        child: CustomScrollView(
          controller: scroller,
          center: centerKey,
          cacheExtent: 1.0,
          slivers: [
            SliverList(
              delegate: MyChildrenDelegate(
                (BuildContext context, int index) {
                  if (index == widget.topSource.length) {
                    if (widget.topMore) {
                      return TwoWayListViewLoading(
                        loadCallback: () {
                          if (widget.insertCallBack != null) {
                            widget.insertCallBack!();
                          }
                        },
                      );
                    } else {
                      if (widget.topSource.isEmpty) {
                        return Container();
                      } else {
                        return Container(
                          alignment: Alignment.center,
                          padding: vertical12,
                          child: LYText.withStyle(
                            '没有更多数据了',
                            style: text15.dark.medium,
                          ),
                        );
                      }
                    }
                  }
                  return widget.builder(context, index, widget.topSource[index], true);
                  // return Dismissible(
                  //   key: Key('tops-$index'),
                  //   child: widget.builder(
                  //       context, index, widget.topSource[index], true),
                  // );
                },
                callback: (first, last) {
                  if (widget.topScrollCallback != null) {
                    if (widget.topMore && last == widget.topSource.length) {
                      return;
                    }
                    widget.topScrollCallback!(last);
                    // Logger.d('top callback=========================');
                    // Logger.d('top topSource==l=${widget.topSource.length}');
                    // Logger.d('top bottomSource==l=${widget.bottomSource.length}');
                    // Logger.d('top extentAfter==l=$extentAfter}');
                    // Logger.d('top enableScroll==l=$enableScroll}');
                    // Logger.d(
                    //     'top scroller.position.maxScrollExtent==l=${scroller.position.maxScrollExtent}}');
                    Future.delayed(const Duration(milliseconds: 500), () {
                      // Logger.d(
                      //     'top scroller.position.maxScrollExtent==l=${scroller.position.maxScrollExtent}}');
                      // Logger.d(
                      //     'topScreenUtil().screenHeight==l=${ScreenUtil().screenHeight}}');
                      // Logger.d('top topSource==l=${widget.topSource.length}');
                      // Logger.d('top bottomSource==l=${widget.bottomSource.length}');
                      if (extentAfter == 0 && enableScroll == false && widget.bottomSource.isEmpty) {
                        Future.delayed(const Duration(milliseconds: 10), () {
                          // Logger.d(
                          //     'top scroller.position.maxScrollExtent==l=${scroller.position.maxScrollExtent}}');
                          // Logger.d(
                          //     'topScreenUtil().screenHeight==l=${ScreenUtil().screenHeight}}');
                          scroller.jumpTo(-ScreenUtil().screenHeight + kToolbarHeight + ScreenUtil().statusBarHeight);
                          // Future.delayed(const Duration(milliseconds: 400), () {
                          //   Logger.d('top scroller.position.maxScrollExtent==l=${scroller.position.maxScrollExtent}}');
                          //   scroller.jumpTo(scroller.position.maxScrollExtent);
                          // });
                        });
                      }
                    });
                  }
                },
                childCount: widget.topSource.length + 1,
              ),
            ),
            SliverPadding(
              padding: EdgeInsets.zero,
              key: centerKey,
            ),
            SliverList(
              delegate: MyChildrenDelegate(
                (BuildContext context, int index) {
                  if (index == widget.bottomSource.length) {
                    if (widget.bottomMore) {
                      return TwoWayListViewLoading(
                        loadCallback: () {
                          if (widget.insertCallBack != null) {
                            widget.addCallBack!();
                          }
                        },
                      );
                    } else {
                      if (widget.bottomSource.isEmpty) {
                        return Container();
                      } else {
                        return Container(
                          alignment: Alignment.center,
                          padding: vertical12,
                          child: LYText.withStyle(
                            '没有更多数据了',
                            style: text15.dark.medium,
                          ),
                        );
                      }
                    }
                  }
                  return widget.builder(context, index, widget.bottomSource[index], false);
                  // return Dismissible(
                  //   key: Key('bottoms-$index'),
                  //   child: widget.builder(context, index, widget.bottomSource[index], false),
                  // );
                },
                callback: (first, last) {
                  if (widget.bottomScrollCallback != null) {
                    widget.bottomScrollCallback!(first);
                  }
                },
                childCount: widget.bottomSource.length + 1,
              ),
            ),
          ],
        ));
  }
}

class TwoWayListViewLoading extends StatefulWidget {
  final VoidCallback loadCallback;

  const TwoWayListViewLoading({Key? key, required this.loadCallback}) : super(key: key);

  @override
  State<TwoWayListViewLoading> createState() => _TwoWayListViewLoadingState();
}

class _TwoWayListViewLoadingState extends State<TwoWayListViewLoading> {
  @override
  void initState() {
    // TODO: implement initState
    super.initState();
    SchedulerBinding.instance.addPostFrameCallback((timeStamp) {
      widget.loadCallback();
    });
  }

  @override
  Widget build(BuildContext context) {
    return Padding(
      padding: const EdgeInsets.symmetric(vertical: 16),
      child: Center(
          child: Row(
        mainAxisAlignment: MainAxisAlignment.center,
        children: const [
          CupertinoActivityIndicator(
            color: Colors.black,
            radius: 8,
          ),
          SizedBox(
            width: 8,
          ),
          Text('加载中'),
        ],
      )),
    );
  }
}

class _SaltedValueKey extends ValueKey<Key> {
  const _SaltedValueKey(Key key) : super(key);
}

class MyChildrenDelegate extends SliverChildBuilderDelegate {
  MyChildrenDelegate(
    Widget Function(BuildContext, int) builder, {
    required int childCount,
    bool addAutomaticKeepAlive = true,
    bool addRepaintoundries = true,
    this.callback,
  }) : super(builder,
            childCount: childCount, addAutomaticKeepAlives: addAutomaticKeepAlive, addRepaintBoundaries: addRepaintoundries);

  final Function(int, int)? callback;

  Widget _createErrorWidget(dynamic exception, StackTrace stackTrace) {
    final FlutterErrorDetails details = FlutterErrorDetails(
      exception: exception,
      stack: stackTrace,
      library: 'widgets library',
      context: ErrorDescription('building'),
    );
    FlutterError.reportError(details);
    return ErrorWidget.builder(details);
  }

  @override
  Widget? build(BuildContext context, int index) {
    if (index < 0 || (childCount != null && index >= childCount!)) {
      return null;
    }
    Widget? child;
    try {
      child = builder(context, index);
    } catch (exception, stackTrace) {
      child = _createErrorWidget(exception, stackTrace);
    }
    if (child == null) {
      return null;
    }

    final Key? key = child.key != null ? _SaltedValueKey(child.key!) : null;
    if (addSemanticIndexes) {
      final int? semanticIndex = semanticIndexCallback(child, index);
      if (semanticIndex != null) {
        child = IndexedSemantics(
          index: semanticIndex + semanticIndexOffset,
          child: child,
        );
      }
    }
    if (addAutomaticKeepAlives) {
      child = AutomaticKeepAlive(
        child: child,
      );
    }
    return KeyedSubtree(
      child: child,
      key: key,
    );
  }

  @override
  void didFinishLayout(int firstIndex, int lastIndex) {
    // TODO: implement didFinishLayout

    super.didFinishLayout(firstIndex, lastIndex);
    if (callback != null) {
      callback!(firstIndex, lastIndex);
    }
  }

  @override
  double? estimateMaxScrollOffset(int firstIndex, int lastIndex, double leadingScrollOffset, double trailingScrollOffset) {
    // TODO: implement estimateMaxScrollOffset

    return super.estimateMaxScrollOffset(firstIndex, lastIndex, leadingScrollOffset, trailingScrollOffset);
  }
}
