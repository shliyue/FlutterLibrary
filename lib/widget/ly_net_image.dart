import 'package:flutter/material.dart';
import 'package:zyocore/widget/common/image_placeholder_widget.dart';
import 'package:zyocore/zyocore.dart';

@Deprecated('后面版本将弃用,请使用LYImageWidget显示图片')
class LYNetImage extends StatelessWidget {
  final double? width;
  final double? height;
  final double? radius;
  final String src;
  final BoxFit? fit;

  const LYNetImage({
    Key? key,
    this.width,
    this.height,
    required this.src,
    this.radius,
    this.fit = BoxFit.cover,
  }) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Container(
      clipBehavior: radius == null ? Clip.none : Clip.hardEdge,
      decoration: radius == null ? null : BoxDecoration(borderRadius: BorderRadius.circular(radius!)),
      child: CachedNetworkImage(
        width: width,
        height: height,
        imageUrl: src,
        fit: fit,
        placeholder: (context, url) => const ImagePlaceholderWidget(
          type: 1,
          state: 0,
        ),
        errorWidget: (context, url, error) => const ImagePlaceholderWidget(
          type: 1,
          state: 1,
        ),
      ),
    );
  }
}
