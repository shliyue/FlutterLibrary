import 'dart:ui';
import 'package:zyocore/zyocore.dart';

///横屏控制器
class LyBaseLandscapeController extends GetxController {
  @override
  void onInit() {
    // TODO: implement onInit
    super.onInit();
  }

  @override
  void onReady() {
    // TODO: implement onReady
    Logger.d("++++++++++++++++++++++LyBaseLandscapeController onReady");
    ScreenUtil.init(Get.context!, designSize: const Size(812, 375));
    super.onReady();
  }
}
