import 'package:flutter/cupertino.dart';
import 'package:get/get.dart';
import 'ly_base_controller.dart';

abstract class LyBaseView<T extends LyBaseController> extends GetView<T>{
  const LyBaseView({super.key});

  @override
  Widget build(BuildContext context);
}