import 'package:get/get.dart';

class BaseState {
  var loaded = false.obs;
  var error = false.obs;
}
