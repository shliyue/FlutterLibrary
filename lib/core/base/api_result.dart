class ApiResult {
  int? code = 0;
  dynamic content;
  String? message;
  bool success = false;

  ApiResult.success(dynamic content) {
    code = 200;
    content = content;
    success = true;
  }

  ApiResult.error(String message) {
    code = 500;
    message = message;
    success = false;
  }

  ApiResult.fromJson(dynamic json) {
    code = json['code'];
    message = json['message'];
    success = json['success'];
    content = json['content'];
  }

  Map<String, dynamic> toJson() => {
        "code": code,
        "message": message,
        "success": success,
        "content": content,
      };
}

class ApiResultException implements Exception {
  final int? code;
  final String? message;

  ApiResultException(this.code, this.message);

  @override
  String toString() {
    // TODO: implement toString
    // return "$code:$message";
    return "$message";
  }
}
