class SelectModel {
  SelectModel({
    required this.key,
    required this.keyName,
    this.parentKey,
    this.checked = false,
    this.avatar,
    this.reshuffle = false,
    this.isHide=false,
  });

  String key;
  String keyName;
  String? parentKey;
  bool checked;
  String? avatar;
  bool reshuffle; //是否异动
  bool isHide;
}

class SelectListModel {
  SelectListModel({
    required this.key,
    required this.keyName,
    this.checked = false,
    this.avatar,
    this.isOpen = false,
    this.isHide=false,
    this.listChild,
  });

  String key;
  String keyName;
  bool checked;
  String? avatar;
  bool isOpen;
  bool isHide;
  List<SelectModel>? listChild;
}
