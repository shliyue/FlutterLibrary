import 'dart:io';
import 'package:zyocore/core/enum/enum_asset_type.dart';

class FileModel {
  String? key;

  //  1-照片 2-视频 3-音频
  int type;
  File file;
  String? title;
  int width;
  int height;
  int duration;
  int orientation; //旋转角度
  String? md5Hash;
  String? id;

  get assetTye {
    switch (type) {
      case 1:
        return EnumAssetType.photo;
      case 2:
        return EnumAssetType.video;
      case 3:
        return EnumAssetType.audio;
      default:
        return EnumAssetType.none;
    }
  }

  FileModel({
    required this.file,
    required this.type,
    this.title,
    this.key,
    this.width = 0,
    this.height = 0,
    this.duration = 0,
    this.orientation = 0,
    this.md5Hash,
    this.id,
  });

  Map<String, dynamic> toJson() {
    final map = <String, dynamic>{};
    map['file'] = file;
    map['type'] = type;
    map['title'] = title;
    map['key'] = key;
    map['width'] = width;
    map['height'] = height;
    map['orientation'] = orientation;
    map['md5Hash'] = md5Hash;
    map['id'] = id;

    return map;
  }

}
