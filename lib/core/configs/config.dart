class Config {
  static String? packageName = "zyocore";
  static const String imageClipRoute = '/zyocore/image_clip';
  static const String videoFullScreen = '/zyocore/video_full_screen';
}
