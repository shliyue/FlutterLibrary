/// Text Util.
class TextUtil {
  /// isEmpty
  bool isEmpty(String? text) {
    return (text ?? "").isEmpty;
  }

  /// 每隔 x位 加 pattern
  String formatDigitPattern(String? text, {int digit = 4, String pattern = ' '}) {
    text = text?.replaceAllMapped(RegExp('(.{$digit})'), (Match match) {
      return '${match.group(0)}$pattern';
    });
    if (text != null && text.endsWith(pattern)) {
      text = text.substring(0, text.length - 1);
    }
    return text ?? "";
  }

  /// 每隔 x位 加 pattern, 从末尾开始
  String formatDigitPatternEnd(String text, {int digit = 4, String pattern = ' '}) {
    String temp = reverse(text);
    temp = formatDigitPattern(temp, digit: digit, pattern: pattern);
    temp = reverse(temp);
    return temp;
  }

  /// 每隔4位加空格
  String formatSpace4(String text) {
    return formatDigitPattern(text);
  }

  /// 每隔4位加空格
  String formatSpacePhone(String text) {
    return text.substring(0, 3) + ' ' + formatDigitPattern(text.substring(3));
  }

  /// 每隔3三位加逗号
  /// num 数字或数字字符串。int型。
  String formatComma3(Object? num) {
    return formatDigitPatternEnd((num ?? 0).toString(), digit: 3, pattern: ',');
  }

  /// 每隔3三位加逗号
  /// num 数字或数字字符串。double型。
  String formatDoubleComma3(Object? num, {int digit = 3, String pattern = ','}) {
    if (num == null) return '';
    List<String> list = num.toString().split('.');
    String left = formatDigitPatternEnd(list[0], digit: digit, pattern: pattern);
    String right = list[1];
    return '$left.$right';
  }

  /// hideNumber
  String? hideNumber(String? phoneNo, {int start = 3, int end = 7, String replacement = '****'}) {
    return phoneNo?.replaceRange(start, end, replacement);
  }

  /// hideString !!!==>>emoji不安全
  String hideString(String text, {int len = 4, String replacement = '...'}) {
    if ((text).isEmpty) {
      return "";
    } else if (text.length < len) {
      return text;
    } else {
      return "${split(text, '').sublist(0, len).join()}$replacement";
    }
  }

  /// replace
  String? replace(String? text, Pattern from, String replace) {
    return text?.replaceAll(from, replace);
  }

  /// split
  List<String> split(String? text, Pattern pattern, {List<String> defValue = const []}) {
    List<String>? list = text?.split(pattern);
    return list ?? defValue;
  }

  /// reverse
  String reverse(String text) {
    if (isEmpty(text)) return '';
    StringBuffer sb = StringBuffer();
    for (int i = text.length - 1; i >= 0; i--) {
      sb.writeCharCode(text.codeUnitAt(i));
    }
    return sb.toString();
  }

  bool checkPassword(String string) {
    if (string.length < 8) return false;
    RegExp mobile = RegExp(r'^(?![\d]+$)(?![a-zA-Z]+$)(?![^\da-zA-Z]+$).{8,15}$');
    return mobile.hasMatch(string);
  }

  /// 是否为数字型字符串
  bool isNumberStr(String? value) {
    if (value == null) {
      return false;
    }
    return double.tryParse(value) != null;
  }

  /// 将弃用，使用字符串扩展中的判断
  /// 手机号校验
  bool isPhoneNumber(String string) {
    if (string.length != 11) return false;
    RegExp mobile = RegExp(r'^((13[0-9])|(14[0-9])|(15[0-l9])|(16[0-9])|(17[0-9])|(18[0-9])|(19[0-9]))\d{8}$');
    return mobile.hasMatch(string);
  }

  /// 表情符号判断
  bool isEmoji(String s) {
    if (s.isEmpty) return false;
    String emojiExpString =
        "[^\\u0020-\\u007E\\u00A0-\\u00BE\\u2E80-\\uA4CF\\uF900-\\uFAFF\\uFE30-\\uFE4F\\uFF00-\\uFFEF\\u0080-\\u009F\\u2000-\\u201f\r\n]";
    var emojiReg = RegExp(emojiExpString);
    return emojiReg.hasMatch(s);
  }

  /// 从路径中获取参数
  Map<String, dynamic> argsFromPath(String? path) {
    Map<String, dynamic> map = {};
    if ((path ?? '').isNotEmpty) {
      if (path!.contains('&')) {
        List<String> arrays = path.split('&');
        if (arrays.isNotEmpty) {
          for (var element in arrays) {
            var args = element.split('=');
            map[args[0]] = args[1];
          }
        }
      } else {
        var args = path.split('=');
        map[args[0]] = args[1];
      }
    }
    return map;
  }

  String transNum(int num) {
    var numString = '';
    if (num >= 10000) {
      numString = '${(num / 10000).toStringAsFixed(1)}万';
    } else {
      numString = '$num';
    }
    return numString;
  }
}
