import 'dart:ui';

class StyleUtil {

  double get screenWidth => window.physicalSize.width / window.devicePixelRatio;
  double get screenHeight => window.physicalSize.height / window.devicePixelRatio;
  double get bottomHeight => window.padding.bottom / window.devicePixelRatio;
  double get appBarMenuTop => window.padding.top / window.devicePixelRatio + 23;

  double get bannerWidth => screenWidth - 40;
  double get bannerHeight => bannerWidth * 22 / 67;

  double get screenVerticalRatio => window.physicalSize.height / window.physicalSize.width;
  double get windowHeight => screenHeight - bottomHeight;
  double get wRatio => screenWidth / 375;
  double get hRatio => screenHeight / 700;

}
