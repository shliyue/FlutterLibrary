import 'package:intl/intl.dart';
import 'package:zyocore/util/utils.dart';

/// 一些常用格式参照。可以自定义格式，例如：'yyyy/MM/dd HH:mm:ss'，'yyyy/M/d HH:mm:ss'。
/// 格式要求
/// year -> yyyy/yy   month -> MM/M    day -> dd/d
/// hour -> HH/H      minute -> mm/m   second -> ss/s
class DateFormats {
  static String yMdHms = 'yyyy-MM-dd HH:mm:ss';
  static String yMdHm = 'yyyy-MM-dd HH:mm';
  static String yMdHmsS = 'yyyy-MM-dd HH:mm:ss.SSS';
  static String yMd = 'yyyy-MM-dd';
  static String yM = 'yyyy-MM';
  static String md = 'MM-dd';
  static String mdHm = 'MM-dd HH:mm';
  static String hms = 'HH:mm:ss';
  static String hm = 'HH:mm';
  static String ms = 'mm:ss';

  static String yMdHmsZh = 'yyyy年MM月dd日 HH:mm:ss';
  static String yMdHmZh = 'yyyy年MM月dd日 HH:mm';
  static String yMdZh = 'yyyy年M月d日';
  static String yMZh = 'yyyy年MM月';
  static String mdZh = 'M月d日';
  static String mdHmZh = 'MM月dd日 HH:mm';
  static String hmsZh = 'HH时mm分ss秒';
  static String hmZh = 'HH时mm分';
  static String msZh = 'mm分ss秒';

  static String yMdSlash = 'yyyy/MM/dd';
  static String yMdHmSlash = 'yyyy/MM/dd HH:mm';
}

/// Date Util.
class DateUtil {
  /// get DateTime By DateStr.
  DateTime getDateTime(String dateStr) {
    DateTime dateTime = DateTime.tryParse(dateStr) ?? DateTime.now();
    dateTime = dateTime.toUtc().add(const Duration(hours: 8));
    return dateTime;
  }

  /// get DateTime By DateStr.
  DateTime getDateTimeNoUtc(String dateStr) {
    DateTime dateTime = DateTime.tryParse(dateStr) ?? DateTime.now();
    dateTime = dateTime.toUtc(); //.add(const Duration(hours: 8));
    return dateTime;
  }

  /// get DateTime By Milliseconds.
  DateTime getDateTimeByMs(int ms, {bool isUtc = false}) {
    return DateTime.fromMillisecondsSinceEpoch(ms, isUtc: isUtc);
  }

  bool isExpired(DateTime time) {
    time = time.toUtc().add(const Duration(hours: 8));
    DateTime now = DateTime.now().toUtc().add(const Duration(hours: 8));
    return time.difference(now).inSeconds < 0;
  }

  Duration diff(DateTime time) {
    time = time.toUtc().add(const Duration(hours: 8));
    DateTime now = DateTime.now().toUtc().add(const Duration(hours: 8));
    return time.difference(now);
  }

  /// 根据失效时间，展示格式化失效的剩余时间
  String formatByExpiredAt(DateTime expiredAt, {String? format}) {
    int diffTime = diff(expiredAt).inSeconds;
    return formatSecond(diffTime, format: format ?? DateFormats.ms);
  }

  DateTime get now {
    return utcNow.add(const Duration(hours: 8));
  }

  DateTime get utcNow {
    return DateTime.now().toUtc();
  }

  String get utcString {
    return DateTime.now().toUtc().toIso8601String();
  }

  /// get DateMilliseconds By DateStr.
  int getDateMsByTimeStr(String dateStr) {
    DateTime dateTime = getDateTime(dateStr);
    return dateTime.millisecondsSinceEpoch;
  }

  /// get Now Date Milliseconds.
  int getNowDateMs() {
    return DateTime.now().millisecondsSinceEpoch;
  }

  /// get Now Date Str.(yyyy-MM-dd HH:mm:ss)
  String getNowDateStr() {
    return formatDate(DateTime.now().toUtc().add(const Duration(hours: 8)));
  }

  /// format date by milliseconds.
  /// milliseconds 日期毫秒
  String formatDateMs(int ms, {bool isUtc = false, String? format}) {
    return formatDate(getDateTimeByMs(ms, isUtc: isUtc), format: format);
  }

  /// format date by date str.
  /// dateStr 日期字符串
  String formatDateStr(String dateStr, {bool? isUtc, String? format}) {
    return formatDate(getDateTime(dateStr), format: format);
  }

  /// format date by date second.
  /// dateStr 日期字符串
  String formatSecond(int value, {String? format}) {
    Duration duration = Duration(seconds: value);
    DateTime now = DateTime.now();
    DateTime date = DateTime(now.year, now.month, now.day, duration.inHours,
        duration.inMinutes.remainder(60), duration.inSeconds.remainder(60));
    return formatDate(date, format: format ?? DateFormats.hms);
  }

  /// 格式化为分秒【01：30：00 => 90:00】.
  String formatMS(int value) {
    Duration duration = Duration(seconds: value);
    String format = DateFormats.ms;
    format = _comFormat(
        duration.inHours * 60 + duration.inMinutes.remainder(60),
        format,
        'm',
        'mm');
    format = _comFormat(duration.inSeconds.remainder(60), format, 's', 'ss');
    return format;
  }

  /// format date by DateTime.
  /// format 转换格式(已提供常用格式 DateFormats，可以自定义格式：'yyyy/MM/dd HH:mm:ss')
  /// 格式要求
  /// year -> yyyy/yy   month -> MM/M    day -> dd/d
  /// hour -> HH/H      minute -> mm/m   second -> ss/s
  String formatDate(DateTime? dateTime, {String? format}) {
    if (dateTime == null) return '';
    format = format ?? DateFormats.yMdHms;
    if (format.contains('yy')) {
      String year = dateTime.year.toString();
      if (format.contains('yyyy')) {
        format = format.replaceAll('yyyy', year);
      } else {
        format = format.replaceAll(
            'yy', year.substring(year.length - 2, year.length));
      }
    }

    format = _comFormat(dateTime.month, format, 'M', 'MM');
    format = _comFormat(dateTime.day, format, 'd', 'dd');
    format = _comFormat(dateTime.hour, format, 'H', 'HH');
    format = _comFormat(dateTime.minute, format, 'm', 'mm');
    format = _comFormat(dateTime.second, format, 's', 'ss');
    format = _comFormat(dateTime.millisecond, format, 'S', 'SSS');

    return format;
  }

  /// com format.
  String _comFormat(int value, String format, String single, String full) {
    if (format.contains(single)) {
      if (format.contains(full)) {
        format =
            format.replaceAll(full, value < 10 ? '0$value' : value.toString());
      } else {
        format = format.replaceAll(single, value.toString());
      }
    }
    return format;
  }

  /// get WeekDay.
  /// dateTime
  /// isUtc
  /// languageCode zh or en
  /// short
  String? getWeekday(DateTime dateTime,
      {String? languageCode = 'en', bool short = false}) {
    String? weekday;
    switch (dateTime.weekday) {
      case 1:
        weekday = languageCode == 'zh' ? '星期一' : 'Monday';
        break;
      case 2:
        weekday = languageCode == 'zh' ? '星期二' : 'Tuesday';
        break;
      case 3:
        weekday = languageCode == 'zh' ? '星期三' : 'Wednesday';
        break;
      case 4:
        weekday = languageCode == 'zh' ? '星期四' : 'Thursday';
        break;
      case 5:
        weekday = languageCode == 'zh' ? '星期五' : 'Friday';
        break;
      case 6:
        weekday = languageCode == 'zh' ? '星期六' : 'Saturday';
        break;
      case 7:
        weekday = languageCode == 'zh' ? '星期日' : 'Sunday';
        break;
      default:
        break;
    }
    return languageCode == 'zh'
        ? (short ? weekday!.replaceAll('星期', '周') : weekday)
        : weekday!.substring(0, short ? 3 : weekday.length);
  }

  /// get WeekDay By Milliseconds.
  String? getWeekdayByMs(int milliseconds,
      {bool isUtc = false, String? languageCode, bool short = false}) {
    DateTime dateTime = getDateTimeByMs(milliseconds, isUtc: isUtc);
    return getWeekday(dateTime, languageCode: languageCode, short: short);
  }

  /// get day of year.
  /// 在今年的第几天.
  int getDayOfYear(DateTime dateTime) {
    int year = dateTime.year;
    int month = dateTime.month;
    int days = dateTime.day;
    var monthDay = {
      1: 31,
      2: 28,
      3: 31,
      4: 30,
      5: 31,
      6: 30,
      7: 31,
      8: 31,
      9: 30,
      10: 31,
      11: 30,
      12: 31,
    };
    for (int i = 1; i < month; i++) {
      days = days + monthDay[i]!;
    }
    if (isLeapYearByYear(year) && month > 2) {
      days = days + 1;
    }
    return days;
  }

  /// get day of year.
  /// 在今年的第几天.
  int getDayOfYearByMs(int ms, {bool isUtc = false}) {
    return getDayOfYear(DateTime.fromMillisecondsSinceEpoch(ms, isUtc: isUtc));
  }

  /// is today.
  /// 是否是当天.
  bool isToday(int milliseconds, {bool isUtc = false, int? locMs}) {
    if (milliseconds == 0) return false;
    DateTime old =
        DateTime.fromMillisecondsSinceEpoch(milliseconds, isUtc: isUtc);
    DateTime now;
    if (locMs != null) {
      now = getDateTimeByMs(locMs);
    } else {
      now = isUtc ? DateTime.now().toUtc() : DateTime.now().toLocal();
    }
    return old.year == now.year && old.month == now.month && old.day == now.day;
  }

  /// is yesterday by dateTime.
  /// 是否是昨天.
  bool isYesterday(DateTime dateTime, DateTime locDateTime) {
    if (yearIsEqual(dateTime, locDateTime)) {
      int spDay = getDayOfYear(locDateTime) - getDayOfYear(dateTime);
      return spDay == 1;
    } else {
      return ((locDateTime.year - dateTime.year == 1) &&
          dateTime.month == 12 &&
          locDateTime.month == 1 &&
          dateTime.day == 31 &&
          locDateTime.day == 1);
    }
  }

  /// is yesterday by millis.
  /// 是否是昨天.
  bool isYesterdayByMs(int ms, int locMs) {
    return isYesterday(DateTime.fromMillisecondsSinceEpoch(ms),
        DateTime.fromMillisecondsSinceEpoch(locMs));
  }

  /// is Week.
  /// 是否是本周.
  bool isWeek(int ms, {bool isUtc = false, int? locMs}) {
    if (ms <= 0) {
      return false;
    }
    DateTime _old = DateTime.fromMillisecondsSinceEpoch(ms, isUtc: isUtc);
    DateTime _now;
    if (locMs != null) {
      _now = getDateTimeByMs(locMs, isUtc: isUtc);
    } else {
      _now = isUtc ? DateTime.now().toUtc() : DateTime.now().toLocal();
    }

    DateTime old =
        _now.millisecondsSinceEpoch > _old.millisecondsSinceEpoch ? _old : _now;
    DateTime now =
        _now.millisecondsSinceEpoch > _old.millisecondsSinceEpoch ? _now : _old;
    return (now.weekday >= old.weekday) &&
        (now.millisecondsSinceEpoch - old.millisecondsSinceEpoch <=
            7 * 24 * 60 * 60 * 1000);
  }

  /// year is equal.
  /// 是否同年.
  bool yearIsEqual(DateTime dateTime, DateTime locDateTime) {
    return dateTime.year == locDateTime.year;
  }

  /// year is equal.
  /// 是否同年.
  bool yearIsEqualByMs(int ms, int locMs) {
    return yearIsEqual(DateTime.fromMillisecondsSinceEpoch(ms),
        DateTime.fromMillisecondsSinceEpoch(locMs));
  }

  /// Return whether it is leap year.
  /// 是否是闰年
  bool isLeapYear(DateTime dateTime) {
    return isLeapYearByYear(dateTime.year);
  }

  /// Return whether it is leap year.
  /// 是否是闰年
  bool isLeapYearByYear(int year) {
    return year % 4 == 0 && year % 100 != 0 || year % 400 == 0;
  }

  /// 是否是同一天
  bool isSameDay(DateTime startTime, DateTime endTime) {
    return startTime.difference(endTime).inDays.abs() < 1;
  }

  String constellation(DateTime? date) {
    if (date == null) return '';
    const s = '魔羯水瓶双鱼牡羊金牛双子巨蟹狮子处女天秤天蝎射手魔羯';
    const d = [20, 19, 21, 21, 21, 22, 23, 23, 23, 23, 22, 22];
    int start =
        date.month * 2 - (date.day < d.elementAt(date.month - 1) ? 2 : 0);
    return "${s.substring(start, start + 2)}座";
  }

  bool checkAdult(DateTime time) {
    DateTime now = DateTime.now();
    int day = now.day - time.day;
    int month = now.month - time.month;
    int year = now.year - time.year;

    if (year > 18) {
      return true;
    } else if (year < 18) {
      return false;
    }

    if (month > 0) {
      return true;
    } else if (month < 0) {
      return false;
    }
    // 如果月也相等，就比较天
    return day >= 0;
  }

  // String age(birthday) {
  //   String age = '';
  //   DateTime _time;
  //   if (birthday != null) {
  //     if (birthday is int) {
  //       _time = DateTime.fromMillisecondsSinceEpoch(birthday);
  //     } else if (birthday is DateTime) {
  //       _time = birthday;
  //     } else {
  //       _time = DateTime.parse(birthday);
  //     }
  //     DateTime now = DateTime.now();
  //     int inYears = now.year - _time.year;
  //     int m = now.month - _time.month;
  //     int d = now.day - _time.day;
  //     if (m < 0 || (m == 0 && d < 0)) {
  //       inYears--;
  //     }
  //     age = "$inYears";
  //   }
  //   return age;
  // }

  /// 数字转中文
  String? getWeekdayZh(int num) {
    String? weekday;
    switch (num) {
      case 1:
        weekday = '一';
        break;
      case 2:
        weekday = '二';
        break;
      case 3:
        weekday = '三';
        break;
      case 4:
        weekday = '四';
        break;
      case 5:
        weekday = '五';
        break;
      case 6:
        weekday = '六';
        break;
      case 0:
        weekday = '日';
        break;
      default:
        break;
    }
    return weekday;
  }

  Duration parseDuration(String s) {
    int hours = 0;
    int minutes = 0;
    int micros;
    List<String> parts = s.split(':');
    if (parts.length > 2) {
      hours = int.parse(parts[parts.length - 3]);
    }
    if (parts.length > 1) {
      minutes = int.parse(parts[parts.length - 2]);
    }
    micros = (double.parse(parts[parts.length - 1]) * 1000000).round();
    return Duration(hours: hours, minutes: minutes, microseconds: micros);
  }

  List<Map<String, List<Map<String, List<String>>>>> get calendar {
    DateTime end = DateTime(DateTime.now().year - 19);
    DateTime start = DateTime(end.year - 40);
    List<Map<String, List<Map<String, List<String>>>>> data = [];
    for (int y = end.year; y > start.year; y--) {
      List<Map<String, List<String>>> month = [];
      for (int m in List<int>.generate(12, (int m) => m + 1)) {
        List<String> days = [];
        for (int d in List<int>.generate(
            DateTime(m == 12 ? y + 1 : y, m == 12 ? 1 : m + 1)
                .difference(DateTime(y, m))
                .inDays,
            (i) => i + 1)) {
          days.add("$d");
        }
        month.add({"$m": days});
      }
      data.add({"$y": month});
    }
    return data;
  }

  List<Map<String, List<Map<String, List<String>>>>> get calendarFromToday {
    DateTime end = DateTime(DateTime.now().year - 10);
    DateTime start = DateTime(end.year - 40);
    List<Map<String, List<Map<String, List<String>>>>> data = [];
    for (int y = end.year; y > start.year; y--) {
      List<Map<String, List<String>>> month = [];
      for (int m in List<int>.generate(12, (int m) => m + 1)) {
        List<String> days = [];
        for (int d in List<int>.generate(
            DateTime(m == 12 ? y + 1 : y, m == 12 ? 1 : m + 1)
                .difference(DateTime(y, m))
                .inDays,
            (i) => i + 1)) {
          days.add("$d");
        }
        month.add({"$m": days});
      }
      data.add({"$y": month});
    }
    return data;
  }

  /// 仅仅显示时分
  String formatMinuteSimple(int value) {
    var h = '00';
    var m = '00';

    if (value <= 60) {
      m = '$value'.padLeft(2, '0');
    } else {
      h = '${value ~/ 60}'.padLeft(2, '0');
      m = '${value % 60}'.padLeft(2, '0');
    }
    return '$h:$m';
  }

  /// 获取本周开始
  String getWeekFirstDay(DateTime dateTime) {
    int current = dateTime.weekday;
    DateTime firstDay = DateTime.fromMillisecondsSinceEpoch(
        dateTime.millisecondsSinceEpoch -
            (24 * 60 * 60 * 1000 * (current - 1)));
    return utils.date.formatDate(firstDay, format: DateFormats.yMd);
  }

  /// 获取本周结束
  String getWeekLastDay(DateTime dateTime) {
    int current = dateTime.weekday;
    DateTime firstDay = DateTime.fromMillisecondsSinceEpoch(
        dateTime.millisecondsSinceEpoch +
            (24 * 60 * 60 * 1000 * (7 - current)));
    return utils.date.formatDate(firstDay, format: DateFormats.yMd);
  }

  /// 获取本月第一天
  String getMonthFirstDay({String space = '-'}) {
    String year = "${DateTime.now().year}";
    String month = "${DateTime.now().month}".length == 1
        ? "0${DateTime.now().month}"
        : "${DateTime.now().month}";
    return "$year$space$month${space}01";
  }

  /// 获取本月最后一天
  String getMonthLastDay({String space = '-'}) {
    String year = "${DateTime.now().year}";
    String month = "${DateTime.now().month}".length == 1
        ? "0${DateTime.now().month}"
        : "${DateTime.now().month}";
    int d = getDayCounts(DateTime.now().month);
    return "$year$space$month$space$d";
  }

  /// 获取一个月有多少天
  int getDayCounts(int month) {
    int year = DateTime.now().year;
    int end = 0;
    if (month == 1 ||
        month == 3 ||
        month == 5 ||
        month == 7 ||
        month == 8 ||
        month == 10 ||
        month == 12) {
      end = 31;
    } else if (month == 2) {
      if ((year % 4 == 0) && (year % 100 != 0) || (year % 400 == 0)) {
        end = 29;
      } else {
        end = 28;
      }
    } else if (month == 4 || month == 6 || month == 9 || month == 11) {
      end = 30;
    }
    return end;
  }

  /// 获取本年的最后一天
  String getYearFirstDay({String space = '-'}) {
    String year = "${DateTime.now().year}";
    return '$year${space}01${space}01';
  }

  /// 获取本年的最后一天
  String getYearLastDay({String space = '-'}) {
    String year = "${DateTime.now().year}";
    return "$year${space}12${space}31";
  }

  ///  毫秒---> hh:ss
  String secondToTime(int s) {
    if (s > 0) {
      return utils.date.formatSecond(s, format: DateFormats.ms);
    }
    return "00:00";
  }

  String age(String? birthStr, {DateTime? nowdate}) {
    if ((birthStr ?? '').isEmpty) {
      return '0岁';
    }

    DateTime birth = utils.date.getDateTime(
      birthStr!,
    );
    if (birth == DateTime.now()) return '0岁';

    DateTime now = DateTime.now();
    if (nowdate != null) {
      now = nowdate;
    }

    // 规范化 移除 时分秒
    final normalizeFormat = DateFormat(DateFormats.yMd);
    birth = normalizeFormat.parse(normalizeFormat.format(birth));
    now = normalizeFormat.parse(normalizeFormat.format(now));
    if (now.isBefore(birth)) {
      final list = utils.date.bigDateDiffSmallDate(birth, now);
      assert(list.length == 3);
      int trueYear = list[0];
      int trueMonth = list[1];
      int trueDay = list[2];
      final y = trueYear == 0 ? '' : '$trueYear年';
      final m = trueMonth == 0 ? '' : '$trueMonth月';
      final d = trueDay == 0 ? '' : '$trueDay天';
      return '出生前$y$m$d';
    }
    bool isValid(int diffY, int diffM, int diffD) {
      bool valid = true;
      if (diffY < 0) {
        valid = false;
      } else if (diffY > 0) {
        valid = true;
      } else {
        /// 年份相等时
        if (diffM < 0) {
          valid = false;
        } else if (diffM > 0) {
          valid = true;
        } else {
          /// 月份相等时
          if (diffD >= 0) {
            valid = true;
          } else {
            valid = false;
          }
        }
      }
      return valid;
    }

    int diffYear = now.year - birth.year;
    int diffMonth = now.month - birth.month;
    int diffDay = now.day - birth.day;
    if (!isValid(diffYear, diffMonth, diffDay)) {
      return '0岁';
    }

    /// 我们计算的 X 岁
    var list = utils.date.bigDateDiffSmallDate(now, birth);
    assert(list.length == 3);
    int trueYear = list[0];
    int trueMonth = list[1];
    int trueDay = list[2];

    final y = trueYear == 0 ? '' : '$trueYear岁';
    final m = trueMonth == 0 ? '' : '$trueMonth月';
    final d = trueDay == 0 ? '' : '$trueDay天';
    String age = y + m + d;
    return age;
  }

  List<int> bigDateDiffSmallDate(DateTime big, DateTime small) {
    /// 我们计算的 X 岁
    int trueYear = 0;
    int trueMonth = 0;
    int trueDay = 0;

    /// 判断同年月日 当前日期和出生日期的大小
    final thisYearBirthDate = DateTime(big.year, small.month, small.day);

    /// 检查今年过完生日没
    int sameYearDiffHours = big.difference(thisYearBirthDate).inHours;
    if (sameYearDiffHours >= 0) {
      /// 已经过了生日
      trueYear = big.year - small.year;
    } else {
      /// 未过生日
      trueYear = big.year - small.year - 1;
    }

    /// 月份差
    final monthSub = big.month - small.month;
    if (big.day - small.day >= 0) {
      trueDay = big.day - small.day;
      if (monthSub >= 0) {
        trueMonth = monthSub;
      } else {
        trueMonth = 12 + monthSub;
      }
    } else {
      /// 当前月的上一个月的生日日期
      DateTime d1 = DateTime(big.year, big.month - 1, small.day);

      /// 处理特殊情况：当生日当天在上一个月并没有的情况
      /// 取月底最后一天
      if (d1.month > big.month - 1) {
        d1 = DateTime(big.year, big.month, 0);
      }
      trueDay = big.difference(d1).inDays;
      if (monthSub - 1 >= 0) {
        trueMonth = monthSub - 1;
      } else {
        trueMonth = 12 + monthSub - 1;
      }
    }
    return [trueYear, trueMonth, trueDay];
  }

  int getMonthDays(int year, int month) {
    var date = DateTime(year, month + 1, 0);
    return date.day;
  }
}
