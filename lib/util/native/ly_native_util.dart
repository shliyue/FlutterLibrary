import 'my_plugin_platform_interface.dart';

class LyNativeUtil {
  Future<String?> getPlatformVersion() {
    return MyPluginPlatform.instance.getPlatformVersion();
  }

  Future<bool> checkIsPad() {
    return MyPluginPlatform.instance.checkIsPad();
  }
}
