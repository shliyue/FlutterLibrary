import 'dart:io';
import 'dart:math';
import 'package:zyocore/core/enum/enum_asset_type.dart';
import 'package:zyocore/values/event_bus_keys.dart';
import 'package:zyocore/zyocore.dart';

Storage lyStorage = Storage();

/// 七牛云上传底层相关服务
class LyQiNiuUtil {
  static PutController _gPutController = PutController();

  /// 设置压缩目标
  static setPutController(PutController val) {
    _gPutController = val;
  }

  /// 设置压缩目标
  static PutController getPutController() {
    return _gPutController;
  }

  /// 设置压缩目标
  static setCompressedTarget(int val) {
    imageCompressedTarget = val;
  }

  /// 取消当前请求
  static cancel() {
    _gPutController.cancel();
  }

  /// 上传文件，不处理文件、不压缩、不处理进度
  /// 场景：海报上传
  static Future<String> uploadWithFileSimple({required String filePath, required String fileName, required QiniuEntity entity}) async {
    File? file = File(filePath);
    PutResponse flag = await lyStorage.putFile(file, entity.token ?? '', options: PutOptions(controller: _gPutController, key: fileName));
    String fileUrl = "${entity.domain}/${flag.key!}";
    String suffix = await DeviceUtil.getSuffixPhone();
    return fileUrl + suffix;
  }

  /// id:资源文件对应的id，音频文件可以不传，参数用于更新进度
  /// 已使用
  static Future<String?> uploadWithFileModel(
      {required FileModel fileModel,
      required QiniuEntity qiniuEntity,
      String? id,
      bool? listenProgress,
      Function? sendProgressF,
      Function? progressF,
      Function? statusF}) async {
    /// 获取七牛token失败
    if (qiniuEntity.domain!.endsWith("/")) {
      qiniuEntity.domain = qiniuEntity.domain!.substring(0, qiniuEntity.domain!.length - 1);
    }
    if (_gPutController.isCancelled) {
      return null;
    }

    /// 文件名
    /// 上传入参参数是FileModel类型，key已赋值
    String fileName = FileUtil.getUploadFileName(item: fileModel);
    File uploadFile = await FileUtil.getUploadFile(item: fileModel);

    /// 文件发送进度监听
    Function sendProgressListenF =
        buildSendProgressListenF(p: _gPutController, q: qiniuEntity, fileName: fileName, id: id, f: sendProgressF, listenProgress: listenProgress);

    /// 任务进度监听
    Function statusListenF = buildProgressListenF(p: _gPutController, id: id, f: progressF, listenProgress: listenProgress);

    /// 状态监听
    Function progressListenF = buildStatusListenF(p: _gPutController, q: qiniuEntity, fileName: fileName, id: id, f: statusF, listenProgress: listenProgress);

    /// 执行回调
    doListen() {
      statusListenF();
      progressListenF();
      sendProgressListenF();
    }

    PutResponse putResponse =
        await lyStorage.putFile(uploadFile, qiniuEntity.token!, options: PutOptions(controller: _gPutController, key: fileName)).catchError((e) {
      Logger.d('===========error========$e');
      doListen();
      return PutResponse(rawData: {});
    });
    doListen();

    if (putResponse.key == null) {
      return null;
    }
    String fileUrl = "${qiniuEntity.domain}/${putResponse.key!}";
    return fileUrl;
  }

  /// 暂未使用
  /// 备用，图片发生旋转场景下使用
  static Future<File> getUploadFileWithAngle({required FileModel item}) async {
    File uploadFile = item.file;

    if (item.assetTye == EnumAssetType.photo) {
      // 照片
      int maxValue = max(item.width, item.height);
      int compressedW = 0;
      int compressedH = 0;
      bool compressed = false; //是否压缩
      double coefficient = 1;
      if (maxValue > imageCompressedTarget) {
        coefficient = imageCompressedTarget / maxValue;
        compressed = true;
      } else {
        coefficient = 1;
      }
      String suffix = item.file.absolute.path.split(".").last;
      if (!ImageUtil.isSupportCompress(suffix)) {
        coefficient = 1;
      }
      if (item.orientation == 90 || item.orientation == 270) {
        compressedW = (coefficient * item.height).toInt();
        compressedH = (coefficient * item.width).toInt();
        compressed = true;
      } else {
        compressedW = (coefficient * item.width).toInt();
        compressedH = (coefficient * item.height).toInt();
      }
      if (compressed) {
        File? compressedFile;
        try {
          compressedFile = await ImageUtil.compressAndGetFile(uploadFile, title: item.title, minWidth: compressedW, minHeight: compressedH);
        } catch (e) {
          compressedFile = uploadFile;
        }
        if (compressedFile != null) {
          uploadFile = compressedFile;
        }
      }
    }
    return uploadFile;
  }

  static Function buildSendProgressListenF({
    required PutController p,
    required QiniuEntity q,
    required String fileName,
    String? id,
    Function? f,
    bool? listenProgress,
  }) {
    var addSendProgressListener = p.addSendProgressListener((percent) {
      Logger.d('addSendProgressListener----$percent');
      if (f != null) {
        f(percent);
      }
      Logger.d('listenProgress>>>>=$listenProgress');
      if (!(listenProgress ?? false)) return;
      lyBus.emit(
          eventName: '${EventBusKeys.uploadProgress}_$id',
          data: {'fileName': fileName, 'domain': q.domain, 'fullName': '${q.domain}/$fileName', 'percent': percent});
    });
    return addSendProgressListener;
  }

  static Function buildProgressListenF({
    required PutController p,
    String? id,
    Function? f,
    bool? listenProgress,
  }) {
    var addProgressListener = p.addProgressListener((percent) {
      Logger.d('addProgressListener----$percent');
      if (f != null) {
        f(percent);
      }
    });
    return addProgressListener;
  }

  static Function buildStatusListenF(
      {required PutController p, required QiniuEntity q, required String fileName, bool? listenProgress, String? id, Function? f}) {
    var addStatusListener = p.addStatusListener((status) {
      Logger.d('addStatusListener----$status');
      if (f != null) {
        f(status);
      }
      Logger.d('listenProgress>>>>=$listenProgress');
      if (status == StorageStatus.Success) {
        if (!(listenProgress ?? false)) return;
        Logger.d('bus send event>>>>data=$status');
        lyBus.emit(eventName: EventBusKeys.uploadSuccess, data: {'id': id, 'fileName': fileName, 'domain': q.domain, 'fullName': '${q.domain}/$fileName'});
      }
    });
    return addStatusListener;
  }
}
