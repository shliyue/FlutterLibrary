import 'dart:io';
import 'package:flutter_oss_aliyun/flutter_oss_aliyun.dart' as Aliyun;
import 'package:zyocore/zyocore.dart';
import '../values/event_bus_keys.dart';

class LyAliUtil {
  static CancelToken _cancelToken = CancelToken();

  static setPutController(CancelToken val) {
    _cancelToken = val;
  }

  static CancelToken getPutController() {
    return _cancelToken;
  }

  /// 取消当前请求
  static cancel() {
    _cancelToken.cancel();
  }

  static void initClient(AliyunEntity entity) {
    Aliyun.Auth _authGetter() {
      return Aliyun.Auth(
        accessKey: entity.accessKeyId!,
        accessSecret: entity.accessKeySecret!,
        expire: DateUtil.formatDate(DateTime.now().add(Duration(seconds: entity.expire!))),
        secureToken: entity.securityToken!,
      );
    }

    Aliyun.Client.init(ossEndpoint: entity.publicUpload!, bucketName: entity.bucket!, authGetter: _authGetter);
  }

  /// 上传文件，不处理文件、不压缩、不处理进度
  /// 场景：海报上传
  static Future<String> uploadWithFileSimple({
    required String filePath,
    required String fileName,
    required AliyunEntity entity,
    String? folderName,
  }) async {
    File? file = File(filePath);
    String? rs = await uploadFile(fileModel: FileModel(file: file, type: 1, width: 1, height: 1, md5Hash: fileName), folderName: folderName, entity: entity);
    return rs ?? "";
  }

  static Future<String?> uploadFile({
    required FileModel fileModel,
    required AliyunEntity? entity,
    Function? sendProgressF,
    bool? listenBus,
    String? folderName,
    bool? useSuffix,
  }) async {
    String fileUrl = "";
    if (entity != null) {
      if (_cancelToken.isCancelled) {
        return null;
      }
      initClient(entity);

      String fileName = FileUtil.getUploadFileName(item: fileModel, useSuffix: useSuffix);
      File uploadFile = await FileUtil.getUploadFile(item: fileModel);
      String uploadFileName = "";
      if (TextUtil.isEmpty(folderName)) {
        uploadFileName = "/" + fileName;
      } else {
        uploadFileName = "/" + folderName! + "/" + fileName;
      }

      String domain = entity.publicDomain!;
      fileUrl = domain + uploadFileName;
      String fileKey = "${entity.folder}" + uploadFileName;

      var result = await Aliyun.Client()
          .putObjectFile(
        uploadFile.absolute.path,
        fileKey: fileKey,
        cancelToken: _cancelToken,
        option: Aliyun.PutRequestOption(
          onSendProgress: (count, total) {
            var percent = count / (total * 1.0);
            Logger.d("uploadAssets++send++++++: count = $count, and total = $total , percent=$percent");
            if (sendProgressF != null) {
              sendProgressF(percent);
            }
            if (count == total) {
              Logger.d('uploadAssets+++bus send event>>>>data=上传完成');
              if (listenBus ?? false) {
                lyBus.emit(eventName: EventBusKeys.uploadSuccess, data: {'id': fileModel.id, 'fileName': fileName, 'domain': domain, 'fullName': fileUrl});
              }
            }
            if (listenBus ?? false) {
              lyBus.emit(
                  eventName: '${EventBusKeys.uploadProgress}_${fileModel.id}',
                  data: {'fileName': fileName, 'domain': domain, 'fullName': fileUrl, 'percent': percent});
            }
          },
          onReceiveProgress: (count, total) {
            Logger.d("uploadAssets++receive+++++++: count = $count, and total = $total");
          },
          override: false,
        ),
      )
          .catchError((err) {
        if (err is DioException) {
          if (err.response != null && err.response!.statusCode == 409) {
            return Response(data: fileUrl, requestOptions: RequestOptions(), statusCode: 200);
          }
        }
        return Response(data: "", requestOptions: RequestOptions(), statusCode: 201);
      });
      Logger.d(result);
      Logger.d("uploadAssets++++++statusCode---:${result.statusCode}");
      Logger.d("uploadAssets++++++结束---:${result.extra}");
      if (result.statusCode == 200 && result.statusMessage == "OK") {
        Logger.d("fileUrl++++++路径---:$fileUrl");
        return fileUrl;
      } else {
        return "";
      }
    }
    return fileUrl;
  }
}
