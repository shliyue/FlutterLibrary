import 'dart:io';
import 'package:zyocore/zyocore.dart';
import 'package:path/path.dart' as p;

enum DownloadStatus { success, fail, cancel }

/// 下载相关
class LyDownloadUtil {
  static CancelToken? _cancelToken;

  /// 下载网络图到本地相册
  static Future<bool> downloadNetImage(String url,
      {CancelToken? cancelToken,
      ProgressCallback? onReceiveProgress,
      bool original = false,
      Function(DownloadStatus)? progressCallback}) async {
    try {
      Logger.d('download 前 URL=$url');
      Logger.d('download  original=$original');
      bool res = await LyPermission.photos();
      if (!res || !url.contains('http')) {
        if (progressCallback != null) {
          progressCallback(DownloadStatus.fail);
        }
        return false;
      }
      String thumbnail = url;
      if (!original) {
        thumbnail =
            FileUtil.getThumbsImageUrl(url, width: 1280, height: 1280, q: 50);
      } else {
        if (!thumbnail.endsWith('?/q/90')) {
          /// 处理heic图片在Android手机上发生旋转问题
          if (thumbnail.endsWith('HEIC') || thumbnail.endsWith('heic')) {
            thumbnail = '$url?imageMogr2/auto-orient/format/jpg';
          } else {
            thumbnail = '$url?imageMogr2/format/jpg';
          }
        }
      }
      Logger.d('download 后 URL=$thumbnail');
      _cancelToken = cancelToken ?? CancelToken();
      var response = await Dio().get(thumbnail,
          options: Options(responseType: ResponseType.bytes),
          cancelToken: _cancelToken,
          onReceiveProgress: onReceiveProgress);
      // 获取手机存储（getTemporaryDirectory临时存储路径）
      Directory applicationDir = await getTemporaryDirectory();
      // getApplicationDocumentsDirectory();
      // 判断路径是否存在
      bool isDirExist = await Directory(applicationDir.path).exists();
      if (!isDirExist) Directory(applicationDir.path).create();
      // 直接保存，返回的就是保存后的文件
      File saveFile = await File(
              applicationDir.path + "${DateTime.now().toIso8601String()}.jpg")
          .writeAsBytes(response.data);
      var saveFilePath = saveFile.path;
      final result = await ImageGallerySaver.saveFile(saveFilePath);
      if (result["isSuccess"]) {
        if (progressCallback != null) {
          progressCallback(DownloadStatus.success);
          return true;
        }
      } else {
        if (progressCallback != null) {
          progressCallback(DownloadStatus.fail);
          return false;
        }
      }
    } catch (e) {
      Logger.d('saveNetImage e=$e');
      if (progressCallback != null) {
        progressCallback(DownloadStatus.fail);
      }
      return false;
    }
    return false;
  }

  /// 下载网络图到本地相册
  static Future<bool> downloadNetVideo(String url,
      {CancelToken? cancelToken,
      ProgressCallback? onReceiveProgress,
      Function(DownloadStatus)? progressCallback}) async {
    try {
      Logger.d('download 前 URL=$url');
      bool res = await LyPermission.photos();
      if (!res || !url.contains('http')) {
        if (progressCallback != null) {
          progressCallback(DownloadStatus.fail);
        }
        return false;
      }
      _cancelToken = cancelToken ?? CancelToken();
      var appDocDir = await getTemporaryDirectory();
      var savePath = p.join(appDocDir.path,
          "${utils.encrypt.encodeMd5(url)}${p.extension(url).toLowerCase()}");
      await Dio().download(url, savePath,
          cancelToken: _cancelToken, onReceiveProgress: onReceiveProgress);
      final result = await ImageGallerySaver.saveFile(savePath);
      if (result["isSuccess"]) {
        if (progressCallback != null) {
          progressCallback(DownloadStatus.success);
          return true;
        }
      } else {
        if (progressCallback != null) {
          progressCallback(DownloadStatus.fail);
          return false;
        }
      }
    } catch (e) {
      Logger.d('saveNetImage e=$e');
      if (progressCallback != null) {
        progressCallback(DownloadStatus.fail);
      }
      return false;
    }
    return false;
  }

  /// 下载图片、视频---指定本地路径
  /// saveToGallery:是否存到本地相册
  static Future<bool> downloadToLocal(
    String url, {
    CancelToken? cancelToken,
    String? savePath,
    bool saveToGallery = false,
    Function(DownloadStatus)? progressCallback,
    ProgressCallback? onReceiveProgress,
  }) async {
    Logger.d('download 前 URL=$url');
    try {
      // String savePath = p.join(path, fileName + p.extension(url).toLowerCase());
      bool res = await LyPermission.photos();
      if (!res || !url.contains('http')) {
        if (progressCallback != null) {
          progressCallback(DownloadStatus.fail);
        }
        return false;
      }
      if ((savePath ?? '').isEmpty) {
        var appDocDir = await getTemporaryDirectory();
        savePath = p.join(appDocDir.path,
            "${utils.encrypt.encodeMd5(url)}${p.extension(url).toLowerCase()}");
      }
      Logger.d('savePath 前 savePath=$savePath');
      _cancelToken = cancelToken ?? CancelToken();
      var response = await Dio().download(url, savePath,
          cancelToken: _cancelToken, onReceiveProgress: onReceiveProgress);
      Logger.d('saveWithLocalPath resp=$response');
      if (saveToGallery) {
        await ImageGallerySaver.saveFile(
          savePath!,
        );
      }
      if (progressCallback != null) {
        progressCallback(DownloadStatus.success);
        return true;
      }
    } catch (e) {
      Logger.d("saveWithLocalPath=error=$e");
      if (progressCallback != null) {
        progressCallback(DownloadStatus.fail);
        return false;
      }
    }
    return false;
  }

  static cancel() {
    _cancelToken?.cancel();
    _cancelToken = null;
  }
}
