import 'dart:async';

LyEventBus lyBus = LyEventBus();

class LyEvent {
  String eventName;
  dynamic data;

  LyEvent(this.eventName, {this.data});
}

class LyEventBus {
  final StreamController _streamController;

  /// Controller for the event bus stream.
  StreamController get streamController => _streamController;

  LyEventBus({bool sync = false})
      : _streamController = StreamController.broadcast(sync: sync);

  LyEventBus.customController(StreamController controller)
      : _streamController = controller;

  /// 订阅
  Stream<LyEvent> on({required String eventName}) {
    return streamController.stream
        .where((event) => (event is LyEvent) && event.eventName == eventName)
        .cast<LyEvent>();
  }

  /// 发布
  emit({required String eventName, dynamic data}) {
    streamController.add(LyEvent(eventName, data: data));
  }

  destroy() {
    _streamController.close();
  }
}
