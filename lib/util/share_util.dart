import 'dart:async';
import 'dart:io';
import 'dart:math';
import 'dart:typed_data';
import 'package:flutter/services.dart' show rootBundle;
import 'package:uni_links/uni_links.dart';
import 'package:weixin_kit/weixin_kit.dart';
import 'package:zyocore/zyocore.dart';
import 'package:screenshot/src/platform_specific/file_manager/file_manager.dart';

class ShareUtil {
  static const String eventWXAuthName = "event_wx_auth_name";

  static Future<bool?> register({required String appId, required String universalLink}) {
    return WeixinKit.instance.registerApp(
      appId: appId,
      universalLink: universalLink,
    );
  }

  //打开微信客服
  static Future<void> openCustomerServiceChat({
    required String corpId,
    required String url,
  }) async {
    bool? isInstalled = await WeixinKit.instance.isInstalled();
    if (isInstalled != null && !isInstalled) {
      ToastUtil.showToast('未安装微信');
      return;
    }
    WeixinKit.instance.openCustomerServiceChat(corpId: corpId, url: url);
  }

  static Future<bool?> shareWebPage({
    required String title,
    required String url,
    String? description,
    Uint8List? thumbData,
    int scene = WXScene.SESSION,
    String? iconUrl,
  }) async {
    bool? isInstalled = await WeixinKit.instance.isInstalled();
    if (isInstalled != null && !isInstalled) {
      ToastUtil.showToast('未安装微信');
      return false;
    }
    if (thumbData == null) {
      String icon = iconUrl ?? "https://qiniu-sybaby.zyosoft.cn/78945613.png";
      var response = await Dio().get(
        icon,
        options: Options(responseType: ResponseType.bytes),
      );
      thumbData = Uint8List.fromList(response.data);
    }
    return WeixinKit.instance.shareWebpage(scene: scene, webpageUrl: url, title: title, description: description, thumbData: thumbData);
  }

  static Future<bool?> shareMiniProgram({
    required String title,
    required String webpageUrl,
    required String path,
    required String miniId,
    String? description,
    String? icon,
    int type = WXMiniProgram.release,
  }) async {
    bool? isInstalled = await WeixinKit.instance.isInstalled();
    if (isInstalled != null && !isInstalled) {
      ToastUtil.showToast('未安装微信');
      return false;
    }

    icon ??= "https://qiniu-assets.zyosoft.cn/share/office1.jpg";

    var response = await Dio().get(
      icon,
      options: Options(responseType: ResponseType.bytes),
    );
    var iconData = Uint8List.fromList(response.data);

    return WeixinKit.instance.shareMiniProgram(
      scene: WXScene.SESSION,
      path: path,
      webpageUrl: webpageUrl,
      userName: miniId,
      title: title,
      description: description,
      type: type,
      hdImageData: iconData,
    );
  }

  static Future<bool?> shareImage({
    required String title,
    required String imageUrl,
    String? description,
    int scene = WXScene.SESSION,
  }) async {
    bool? isInstalled = await WeixinKit.instance.isInstalled();
    if (isInstalled != null && !isInstalled) {
      ToastUtil.showToast('未安装微信');
      return false;
    }
    ToastUtil.loading("");
    var response = await Dio().get(
      FileUtil.getThumbsImageUrl(imageUrl, width: 1280, height: 1280, q: 50),
      options: Options(responseType: ResponseType.bytes),
    );
    String fileName = 'WxShare-ybt-poster.jpg';
    Directory applicationDir = await getTemporaryDirectory();
    var newPath = applicationDir.path + "/wx-poster";
    bool isDirExist = await Directory(newPath).exists();
    if (!isDirExist) Directory(newPath).create();
    PlatformFileManager fileManager = PlatformFileManager();
    final filePath = await fileManager.saveFile(response.data, newPath, name: fileName);
    if (filePath.isEmpty) {
      ToastUtil.showToast('分享失败');
      return false;
    }
    Uri imgUri = Uri.file(filePath, windows: false);
    ToastUtil.dismiss();
    return WeixinKit.instance.shareImage(
      scene: scene,
      title: title,
      imageUri: imgUri,
      description: description,
    );
  }

  static Future<bool?> shareLocalImage({
    required Uri imageUri,
    String? title,
    String? description,
    int scene = WXScene.SESSION,
  }) async {
    bool? isInstalled = await WeixinKit.instance.isInstalled();
    if (isInstalled != null && !isInstalled) {
      ToastUtil.showToast('未安装微信');
      return false;
    }
    return WeixinKit.instance.shareImage(
      scene: scene,
      imageUri: imageUri,
      title: title,
      description: description,
    );
  }

  static Future<bool?> shareUint8ListImage({
    required Uint8List imageData,
    String? title,
    String? description,
    int scene = WXScene.SESSION,
  }) async {
    bool? isInstalled = await WeixinKit.instance.isInstalled();
    if (isInstalled != null && !isInstalled) {
      ToastUtil.showToast('未安装微信');
      return false;
    }
    return WeixinKit.instance.shareImage(
      scene: scene,
      imageData: imageData,
      title: title,
      description: description,
    );
  }

  static Future<bool?> shareVideo({
    required String title,
    required String videoUrl,
    String? description,
    int scene = WXScene.SESSION,
  }) async {
    bool? isInstalled = await WeixinKit.instance.isInstalled();
    if (isInstalled != null && !isInstalled) {
      ToastUtil.showToast('未安装微信');
      return false;
    }
    ToastUtil.loading("");
    var response = await Dio()
        .get(
      FileUtil.getQiuVideoThumImage(videoUrl),
      options: Options(responseType: ResponseType.bytes),
    )
        .catchError((e) {
      ToastUtil.showToast(e.toString());
    });
    var thumbData = Uint8List.fromList(response.data);

    ToastUtil.dismiss();
    return WeixinKit.instance.shareVideo(
      scene: scene,
      videoUrl: videoUrl,
      title: title,
      thumbData: thumbData,
      description: description,
    );
  }

  static Future<bool?> shareAudio({
    required String title,
    required String url,
    String? description,
    int scene = WXScene.SESSION,
    Uint8List? thumbData,
  }) async {
    bool? isInstalled = await WeixinKit.instance.isInstalled();
    if (isInstalled != null && !isInstalled) {
      ToastUtil.showToast('未安装微信');
      return false;
    }
    if (thumbData == null) {
      String iconUrl = "https://qiniu-assets.zyosoft.cn/icon_audio_bg.png";
      var response = await Dio().get(
        iconUrl,
        options: Options(responseType: ResponseType.bytes),
      );
      thumbData = Uint8List.fromList(response.data);
    }

    return WeixinKit.instance.shareMediaMusic(
      scene: scene,
      musicUrl: url,
      title: title,
      thumbData: thumbData,
      description: description,
    );
  }

  static Future<void> launchMiniProgram({
    required String userName,
    String? path,
    int type = WXMiniProgram.release,
  }) async {
    bool? isInstalled = await WeixinKit.instance.isInstalled();
    if (isInstalled != null && !isInstalled) {
      ToastUtil.showToast('未安装微信');
      return;
    }
    WeixinKit.instance.launchMiniProgram(userName: userName, path: path, type: type);
  }

  /// 微信授权登录
  static Future<void> auth() async {
    bool? isInstalled = await WeixinKit.instance.isInstalled();
    if (isInstalled != null && !isInstalled) {
      ToastUtil.showToast('未安装微信');
      return;
    }

    /// state生成，文档中要求尽量做成唯一的，仅微信接口为了防止csrl攻击
    /// scope按照文档说明，只能使用snsapi_userinfo
    var random = Random().nextInt(1000).toString().padLeft(4, '0');
    var date = DateTime.now();
    var stateStr = '${date.millisecondsSinceEpoch}-$random';
    var stateEncode = utils.encrypt.encodeMd5(stateStr);
    return WeixinKit.instance.auth(
      scope: ['snsapi_userinfo'],
      state: stateEncode,
    );
  }

  static Future<void> onDeeplink(String appId, void Function(dynamic) onData) async {
    try {
      //冷启动参数u
      final uri = await getInitialUri();
      Logger.d("冷启动参数u>>>>>>>>>>>>>cool link ===== $uri");
      onData(uri);
      // if(uri != null){
      //   Map parameters = uri.queryParameters;
      //   if(parameters['messageExt'] != null){
      //     Logger.d("写入数据～～～～～${parameters['messageExt']}");
      //     StorageService.instance.setString('launchAppParams', parameters['messageExt']);
      //   }
      //   var messageExt = convert.jsonDecode(parameters['messageExt']);
      // }
    } on FormatException {
      // Handle exception by warning the user their action did not succeed
      Logger.d("冷启动参数u >>>>>>>>>>>>>cool FormatException");
    }

    //热启动参数
    uriLinkStream.listen((Uri? uri) {
      Logger.d("uriLinkStream热启动参数>>>>>>>>>>>>>hot link ===== $uri");
      onData(uri);
    }, onError: (err) {
      Logger.d("uriLinkStream热启动参数>>>>>>>>>>>>>onError ===== $err");
    });
    //来自微信的
    WeixinKit.instance.reqStream().listen((event) {
      Logger.d("weixin热启动参数>>>>>>>>>>>>>hot link ===== ${event.toJson()}");
      onData(event.toJson());
    });
    //来自微信的
    WeixinKit.instance.respStream().listen((event) {
      Logger.d("weixin回调参数>>>>>>>>>>>>>respStream hot link ===== ${event.toJson()}");
      Map<String, dynamic> map = event.toJson();
      if (map.containsKey("code")) {
        if (TextUtil.isEmpty(map["code"])) {
          ToastUtil.showInfo("获取微信授权失败");
        }
        map["appId"] = appId;

        /// 微信回调
        lyBus.emit(eventName: eventWXAuthName, data: map);
      } else {
        Logger.d("weixin回调参数>>>>>>>>>>>>>onData hot link ===== ${event.toJson()}");
        onData(event.toJson());
      }
    });
  }

  static Future<bool?> openWechat() async {
    bool? isInstalled = await WeixinKit.instance.isInstalled();
    if (isInstalled != null && !isInstalled) {
      ToastUtil.showToast('未安装微信');
      return false;
    }
    return WeixinKit.instance.openWXApp();
  }
}
