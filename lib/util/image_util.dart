import 'dart:io';
import 'dart:typed_data';
import 'package:extended_image/extended_image.dart';
import 'package:flutter/material.dart';
import 'package:flutter_image_compress/flutter_image_compress.dart';
import 'package:image_editor/image_editor.dart';
import 'package:zyocore/entitys/file_entity.dart';
import 'package:zyocore/util/file_util.dart';

import 'logger.dart';

class ImageUtil {
  static List<String> supportCompressFormat = ['jpg', 'jpeg', 'png'];

  ///需要压缩的图片格式
  static List<String> compressFormat = ['dng', 'raw'];
  ///不存在的格式就压缩
  static bool checkExist = false;

  //判断大小是否需要压缩
  static bool isSupportCompress(String? fileUrl) {
    if ((fileUrl ?? '').isEmpty) return false;
    String suffix = fileUrl!.split(".").last.toLowerCase();
    return supportCompressFormat.contains(suffix);
  }

  static String getFileSuffix(String suffix) {
    String rs = "jpg";
    if(checkExist){
      if (!supportCompressFormat.contains(suffix.toLowerCase())) {
        rs = "jpg";
      } else {
        rs = suffix;
      }
    }else {
      if (compressFormat.contains(suffix.toLowerCase())) {
        rs = "jpg";
      } else {
        rs = suffix;
      }
    }
    return rs;
  }

  static CompressFormat? getFileFormat(File file) {
    var suffix = file.absolute.path.split('.').last;
    suffix = getFileSuffix(suffix);
    return getFileFormatBySuffix(suffix);
  }

  static CompressFormat? getFileFormatBySuffix(String suffix) {
    suffix = suffix.toLowerCase();
    if (suffix.endsWith('jpg') || suffix.endsWith('jpeg')) {
      return CompressFormat.jpeg;
    } else if (suffix.endsWith('png')) {
      return CompressFormat.png;
    } else if (suffix.endsWith('heic')) {
      return CompressFormat.heic;
    } else if (suffix.endsWith('webp')) {
      return CompressFormat.webp;
    }
    return null;
  }

  // 1. compress file and get Uint8List
  static Future<Uint8List?> compressFile(File file) async {
    var result = await FlutterImageCompress.compressWithFile(
      file.absolute.path,
      minWidth: 2300,
      minHeight: 1500,
      quality: 94,
      rotate: 90,
    );

    return result;
  }

  /// 2. compress file and get file.
  //已使用
  static Future<File?> compressAndGetFile(
    File file, {
    String? title,
    int minWidth = 1920,
    int minHeight = 1080,
    int rotate = 0,
  }) async {
    Logger.d('path == ${file.absolute.path}');
    Logger.d('title == $title');

    /// 如果file不存在直接返回null
    if (file.existsSync() == false) {
      return null;
    }

    var suffix;
    if ((title ?? '').isEmpty) {
      suffix = file.absolute.path.split('.').last;
    } else {
      suffix = title!.split('.').last;
    }
    suffix = getFileSuffix(suffix);
    int timestamp = DateTime.now().millisecondsSinceEpoch;
    Directory tmpDir = await FileUtil.getUploadCachePath();
    String tmpPath = tmpDir.path + '/upload_temp_$timestamp.$suffix';

    /// 如果为不支持的格式直接返回
    if (isSupportCompress(suffix) == false) {
      var size = file.lengthSync();
      //如果文件大于20M需要压缩
      if (size > 1024 * 1024 * 20) {
        Logger.d('开始压缩.....超过20M:${size}');
        tmpPath = tmpDir.path + '/upload_temp_$timestamp.jpg';
        return lyCompressFile(file.absolute.path, tmpPath, quality: 95, minHeight: minHeight, minWidth: minWidth, rotate: rotate);
      } else {
        return file;
      }
    }
    CompressFormat? format = getFileFormatBySuffix(suffix);
    Logger.d('format == $format');
    if (format == null) {
      return file;
    }
    return lyCompressFile(file.absolute.path, tmpPath, quality: 100, minHeight: minHeight, minWidth: minWidth, format: format, rotate: rotate);
  }

  ///压缩图片
  static Future<File?> lyCompressFile(
    String oriPath,
    String tmpPath, {
    int minWidth = 1920,
    int minHeight = 1080,
    int rotate = 0,
    int quality = 100,
    CompressFormat format = CompressFormat.jpeg,
  }) async {
    /// 清空临时文件
    File temFile = File(tmpPath);
    bool exists = await temFile.exists();
    if (exists) {
      temFile.delete();
    }
    Logger.d('开始压缩.....$oriPath');
    XFile? result = await FlutterImageCompress.compressAndGetFile(
      oriPath,
      tmpPath,
      quality: quality,
      minHeight: minHeight,
      minWidth: minWidth,
      format: format,
      rotate: rotate,
    );
    if (result != null) {
      var rsSize = await result.length();
      Logger.d('完成压缩.....${tmpPath}   size = $rsSize');
      if (rsSize > 1024 * 1024 * 20) {
        return lyCompressFile(oriPath, tmpPath, quality: quality - 5, minHeight: minHeight, minWidth: minWidth, format: format, rotate: rotate);
      }
      return File(result.path);
    } else {
      return null;
    }
  }

  // 3. compress asset and get Uint8List.
  static Future<Uint8List?> compressAsset(String assetName) async {
    var list = await FlutterImageCompress.compressAssetImage(
      assetName,
      minHeight: 1920,
      minWidth: 1080,
      quality: 96,
      rotate: 180,
    );

    return list;
  }

  // 4. compress Uint8List and get another Uint8List.
  static Future<Uint8List?> comporessList(
    Uint8List list, {
    int minHeight = 1920,
    int minWidth = 1080,
    int quality = 96,
    int rotate = 135,
  }) async {
    var result = await FlutterImageCompress.compressWithList(
      list,
      minHeight: minHeight,
      minWidth: minWidth,
      quality: quality,
      rotate: rotate,
    );
    Logger.d(list.length);
    Logger.d(result.length);
    return result;
  }

  static String? getCoverImage(List<FileEntity>? files) {
    if (files == null) {
      return null;
    }
    bool findVideo = false;
    String? firstVideo;
    for (var i = 0; i < files.length; i++) {
      final file = files[i];

      /// 非音频  就是图片
      if (FileUtil.isVideo(file.fileUrl ?? '')) {
        if (!findVideo) {
          firstVideo = file.fileUrl;
        }
        findVideo = true;
      } else {
        /// 找到第一张图片
        return file.fileUrl;
      }
    }
    return FileUtil.getQiuVideoImage(firstVideo ?? '');
  }

  /// 剪裁图片
  static Future<Uint8List?> cropImageDataWithNativeLibrary({required ExtendedImageEditorState state}) async {
    final Rect? cropRect = state.getCropRect();
    final EditActionDetails action = state.editAction!;

    final int rotateAngle = action.rotateAngle.toInt();
    final bool flipHorizontal = action.flipY;
    final bool flipVertical = action.flipX;
    final Uint8List img = state.rawImageData;

    final ImageEditorOption option = ImageEditorOption();

    if (action.needCrop) {
      option.addOption(ClipOption.fromRect(cropRect!));
    }

    if (action.needFlip) {
      option.addOption(FlipOption(horizontal: flipHorizontal, vertical: flipVertical));
    }

    if (action.hasRotateAngle) {
      option.addOption(RotateOption(rotateAngle));
    }

    final Uint8List? result = await ImageEditor.editImage(
      image: img,
      imageEditorOption: option,
    );

    return result;
  }
}
