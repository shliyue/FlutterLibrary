import 'dart:io';
import 'dart:typed_data';

import 'package:flutter/material.dart';
import 'package:photo_manager/photo_manager.dart';
import 'package:zyocore/util/lib/date_util.dart';
import 'package:zyocore/util/utils.dart';
import "package:collection/collection.dart";

import '../widget/assets_picker/entity/ly_asset_entity.dart';
import 'logger.dart';
import 'ly_permission.dart';

typedef AssetTapCallback = void Function(String assetId);

class GalleryFileModel {
  late String id;
  late String path;
  late String createDate;
  int? width;
  int? height;
  String? type; //1 img  2:video 3 audio
  int? typeInt; //1 img  2:video 3 audio
  String? src;
  int? duration;
  String? resourceType;
  bool notSelected;
  Uint8List? thumlistByte;
  Uint8List? orginlistByte;
  String? base64Str;

  GalleryFileModel({required this.id, required this.path, required this.createDate, this.notSelected = true});

  Map toJson() {
    Map map = {};
    map["id"] = id;
    map["path"] = path;
    map["createDate"] = createDate;
    map["width"] = width;
    map["height"] = height;
    map["type"] = type;
    map["typeInt"] = typeInt;
    map["src"] = src;
    map["duration"] = duration;
    map["resourceType"] = resourceType;
    map["thumlistByte"] = thumlistByte;
    map["orginlistByte"] = orginlistByte;
    map["base64Str"] = base64Str;
    map["notSelected"] = notSelected;

    return map;
  }
}

class GalleryModel {
  List<GalleryFileModel>? child;
  List<LyAssetEntity>? childAsset;
  String createDate = '';
  bool notAllSelected = true;

  GalleryModel({this.child, required this.createDate, this.notAllSelected = true, this.childAsset});

  Map toJson() {
    Map map = {};
    map["child"] = child;
    map["childAsset"] = childAsset;
    map["createDate"] = createDate;
    map["notAllSelected"] = notAllSelected;
    return map;
  }
}

class FilePathModel {
  late String id;
  late String path;

  FilePathModel({required this.id, required this.path});

  Map toJson() {
    Map map = {};
    map["id"] = id;
    map["path"] = path;
    return map;
  }
}

class GalleryUtil {
  static Future<List<GalleryModel>> getLocalPhotosData(int page, int pageSize, RequestType type, {bool? needTitle, bool checkIcloud = false}) async {
    // var result = await PhotoManager.requestPermissionExtend();
    var result = false;
    if (type == RequestType.image) {
      result = await LyPermission.photos();
    } else if (type == RequestType.video) {
      result = await LyPermission.videos();
    } else {
      result = await LyPermission.storage();
    }
    if (pageSize < 20) {
      pageSize = 20;
    }
    if (page < 0) {
      page = 0;
    }
    needTitle = needTitle ?? Platform.isIOS;
    List<GalleryModel> assets = <GalleryModel>[];
    if (result) {
      List<AssetPathEntity> list = await PhotoManager.getAssetPathList(
        onlyAll: true,
        type: type,
        filterOption: FilterOptionGroup(
          containsLivePhotos: type == RequestType.image,
          imageOption: FilterOption(
            /// 过滤后，会与系统自带相册不一致
            // sizeConstraint: SizeConstraint(minWidth: 750, minHeight: 750),
            needTitle: needTitle,
          ),
          // videoOption: const FilterOption(
          //   durationConstraint: DurationConstraint(min: Duration(seconds: 5), max: Duration(minutes: 5, seconds: 0, milliseconds: 500)),
          // ),
        ),
      );

      for (var path in list) {
        var images = await path.getAssetListPaged(page: page, size: pageSize);
        Map<String, List<AssetEntity>> rs = groupBy(images, (e) => utils.date.formatDate(e.createDateTime, format: DateFormats.yMd));
        for (String key in rs.keys) {
          List<AssetEntity> value = rs[key]!;
          List<LyAssetEntity> list = <LyAssetEntity>[];
          for (var item in value) {
            bool isAvailable = true;
            if (checkIcloud) {
              isAvailable = await item.isLocallyAvailable();
            }
            list.add(LyAssetEntity.fromAsset(item, isAvailable: isAvailable));
          }
          assets.add(GalleryModel(childAsset: list, createDate: key));
        }
      }
    } else {
      PhotoManager.openSetting();
    }
    assets.sort((a, b) {
      return b.createDate.compareTo(a.createDate);
    });
    return assets;
  }

  static Future<List<FilePathModel>> getLocalPhotosPath(List<String> listId) async {
    List<FilePathModel> rs = [];

    for (var id in listId) {
      final asset = await AssetEntity.fromId(id);
      File? file;
      var title = await asset!.titleAsync;
      // if (title.toLowerCase().endsWith(".heic")) {
      //   file = await asset.file;
      // } else {
      //   file = await asset.originFile;
      // }
      file = await asset.originFile;
      var path = "file://" + file!.path;

      FilePathModel model = FilePathModel(id: id, path: path);
      rs.add(model);
    }
    return rs;
  }

  static Future<List<GalleryModel>> getLocalPhotos(int page, int pageSize) async {
    if (pageSize < 20) {
      pageSize = 20;
    }
    if (page < 0) {
      page = 0;
    }
    List<GalleryModel> assets = [];
    var result = await PhotoManager.requestPermissionExtend();
    if (result.isAuth) {
      List<AssetPathEntity> list = await PhotoManager.getAssetPathList(
        onlyAll: true,
        filterOption: FilterOptionGroup(
            imageOption: const FilterOption(
              /// 过滤后，会与系统自带相册不一致
              sizeConstraint: SizeConstraint(minWidth: 750, minHeight: 750),
            ),
            videoOption: const FilterOption(
              durationConstraint: DurationConstraint(min: Duration(seconds: 10), max: Duration(minutes: 30)),
            )),
      );
      String prefix = "/storage/emulated/0/";
      for (var path in list) {
        var images = await path.getAssetListPaged(page: page, size: pageSize);
        for (var img in images) {
          var imgsrc = "";
          if (Platform.isAndroid) {
            imgsrc = img.relativePath! + img.title!;
            if (!imgsrc.startsWith(prefix)) {
              imgsrc = prefix + imgsrc;
            }
          }
          Uint8List? thumbData = await img.thumbnailData;
          GalleryFileModel item = GalleryFileModel(
            id: img.id,
            path: imgsrc,
            createDate: utils.date.formatDate(img.createDateTime, format: DateFormats.yMd),
          );
          item.width = img.width;
          item.height = img.height;
          item.typeInt = img.typeInt;
          item.duration = img.duration;
          item.thumlistByte = thumbData;

          switch (img.typeInt) {
            case 1:
              item.type = "img";
              break;
            case 2:
              item.type = "video";
              break;
            case 3:
              item.type = "voice";
              break;
          }

          GalleryModel obj = assets.firstWhere((e) => e.createDate == item.createDate, orElse: () => GalleryModel(child: [], createDate: ""));
          if (obj.createDate == "") {
            List<GalleryFileModel> child = [item];
            obj = GalleryModel(child: child, createDate: item.createDate);
            assets.add(obj);
          } else {
            bool rs = assets.where((e) => e.child!.where((m) => m.id == item.id).isNotEmpty).isNotEmpty;
            if (!rs) {
              obj.child!.add(item);
            }
          }
        }
      }
    } else {
      // PhotoManager.openSetting();
    }
    Logger.d('assets======$assets');
    return assets;
  }

  /// 获取排序后的照片列表
  static Future<List<GalleryModel>> getLocalSortedPhotos(int page, int pageSize) async {
    if (pageSize < 20) {
      pageSize = 20;
    }
    if (page < 0) {
      page = 0;
    }
    List<GalleryModel> assets = [];
    List<GalleryFileModel> fileList = [];
    var result = await PhotoManager.requestPermissionExtend();
    if (result.isAuth) {
      List<AssetPathEntity> list = await PhotoManager.getAssetPathList(
        onlyAll: true,
        filterOption: FilterOptionGroup(
            imageOption: const FilterOption(
              /// 过滤后，会与系统自带相册不一致
              sizeConstraint: SizeConstraint(minWidth: 750, minHeight: 750),
            ),
            videoOption: const FilterOption(
              durationConstraint: DurationConstraint(min: Duration(seconds: 10), max: Duration(minutes: 30)),
            )),
      );
      String prefix = "/storage/emulated/0/";
      for (var path in list) {
        var images = await path.getAssetListPaged(page: page, size: pageSize);
        for (var img in images) {
          var imgSrc = "";
          Logger.d('img.relativePath===${img.relativePath}');
          Logger.d('img.title===${img.title}');
          if (Platform.isAndroid) {
            imgSrc = img.relativePath! + img.title!;
            if (!imgSrc.startsWith(prefix)) {
              imgSrc = prefix + imgSrc;
            }
            Logger.d('====imgSrc=after==${imgSrc}');
          } else {
            // var title = await img.titleAsync;
            // if(img.typeInt == 1){
            //   var file = await img.file;
            //   imgsrc = "file://" + file!.path;
            // }
            // imgsrc = String.fromCharCodes(thumbData!);
            // imgsrc = "data:image/png;base64," + base64Encode(thumbData!.toList());
          }
          Uint8List? thumbData = await img.thumbnailData;
          // Uint8List? orginData = await img.originBytes;
          GalleryFileModel item = GalleryFileModel(
            id: img.id,
            path: imgSrc,
            createDate: utils.date.formatDate(img.createDateTime, format: DateFormats.yMd),
          );
          item.width = img.width;
          item.height = img.height;
          item.typeInt = img.typeInt;
          item.duration = img.duration;
          item.thumlistByte = thumbData;
          // item.orginlistByte = orginData;
          // item.base64Str = base64Encode(thumbData!.toList());

          switch (img.typeInt) {
            case 1:
              item.type = "img";
              break;
            case 2:
              item.type = "video";
              break;
            case 3:
              item.type = "voice";
              break;
          }
          fileList.add(item);
        }
      }
    } else {
      // PhotoManager.openSetting();
    }

    /// 分组的map
    Map<String, List<GalleryFileModel>> assetsMap = fileList.groupListsBy<String>((e) => e.createDate);

    /// map转list
    for (var key in assetsMap.keys) {
      assets.add(GalleryModel(child: assetsMap[key] ?? [], createDate: key));
    }
    return assets;
  }
}
