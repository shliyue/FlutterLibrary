import 'dart:io';
import 'package:flutter_image_compress/flutter_image_compress.dart';
import 'package:zyocore/entitys/ly_audio_file.dart';
import 'package:zyocore/entitys/upload_asset_entity.dart';
import 'package:zyocore/util/ly_ali_util.dart';
import 'package:zyocore/util/ly_qiniu_util.dart';
import 'package:zyocore/widget/assets_picker/entity/ly_asset_entity.dart';
import 'package:zyocore/zyocore.dart';

import '../core/enum/enum_asset_type.dart';

/// 上传相关服务的基础调用，其他子模块调用这里的方法，
/// 此服务作用相当于园宝通的业务层，类似的在成长档案kit、招生中的uploadUtil
class LyUploadUtil {
  //#region 上传阿里云
  /// 上传文件到阿里
  ///
  static Future<List<FileEntity>> uploadAliYunEntity({
    required AliyunEntity? entity,
    required List<dynamic> assets,
    QiniuEntity? qiNiuEntity,
    bool? uploadHash,
    bool? listenProgress,
    Function? sendProgressF,
    String? folderName,
    bool useHash = false, //文件名是否使用hash值
  }) async {
    var rs = await _uploadAliYun(
      entity: entity,
      qiNiuEntity: qiNiuEntity,
      assets: assets,
      folderName: folderName,
      uploadHash: uploadHash,
      listenProgress: listenProgress,
      sendProgressF: sendProgressF,
      returnEntity: true,
      useHash: useHash,
    );
    return rs.cast<FileEntity>();
  }

  static Future<List<String>> uploadAliYunString({
    required AliyunEntity? entity,
    required List<dynamic> assets,
    QiniuEntity? qiNiuEntity,
    bool? uploadHash,
    bool? listenProgress,
    Function? sendProgressF,
    String? folderName,
    bool useHash = false, //文件名是否使用hash值
  }) async {
    var rs = await _uploadAliYun(
        entity: entity,
        qiNiuEntity: qiNiuEntity,
        assets: assets,
        folderName: folderName,
        uploadHash: uploadHash,
        listenProgress: listenProgress,
        sendProgressF: sendProgressF,
        useHash: useHash,
        returnEntity: false);
    return rs.cast<String>();
  }

  /// 上传文件，不处理文件、不压缩、不处理进度
  /// 场景：海报上传
  static Future<String> uploadSimpleAli({required String filePath, required String fileName, String? folderName, required AliyunEntity entity}) {
    return LyAliUtil.uploadWithFileSimple(filePath: filePath, fileName: fileName, folderName: folderName, entity: entity);
  }

  /// 上传音频文件
  static Future<FileEntity?> uploadAudioAli(
    AliyunEntity? entity,
    LYAudioFile audioFile, {
    Function? sendProgress,
    Function? progress,
    String? folderName,
    bool? useSuffix,
  }) async {
    if (entity == null || audioFile.file == null) return null;
    // 创建 putController 对象
    if (LyAliUtil.getPutController().isCancelled) {
      LyAliUtil.setPutController(CancelToken());
    }
    var audioModel = FileModel(file: audioFile.file!, type: 3, duration: audioFile.duration!.inSeconds);
    var uploadUrl =
        await LyAliUtil.uploadFile(fileModel: audioModel, folderName: folderName, entity: entity, sendProgressF: sendProgress, useSuffix: useSuffix);
    Logger.d('音频文件上传后URL=$uploadUrl');
    return FileEntity(fileUrl: uploadUrl, duration: audioFile.duration?.inMilliseconds, suffix: 'acc');
  }

  static Future<List> _uploadAliYun({
    required AliyunEntity? entity,
    required List<dynamic> assets,
    QiniuEntity? qiNiuEntity,
    bool? uploadHash,
    bool? listenProgress,
    Function? sendProgressF,
    bool returnEntity = true,
    String? folderName,
    bool useHash = false, //文件名是否使用hash值
  }) async {
    List listRs = [];
    if (entity != null) {
      String? res;
      for (var element in assets) {
        res = null;
        FileModel? model = await createAssetFileModal(element, useHash: useHash);
        if (model == null) {
          continue;
        }
        if (model.assetTye == EnumAssetType.video && qiNiuEntity != null) {
          if (LyQiNiuUtil.getPutController().isCancelled) {
            LyQiNiuUtil.setPutController(PutController());
          }
          Logger.d('id++++=${model.id}');
          res = await LyQiNiuUtil.uploadWithFileModel(
              fileModel: model, id: model.id, qiniuEntity: qiNiuEntity!, listenProgress: listenProgress, sendProgressF: sendProgressF);
        } else {
          if (LyAliUtil.getPutController().isCancelled) {
            LyAliUtil.setPutController(CancelToken());
          }
          res = await LyAliUtil.uploadFile(fileModel: model, folderName: folderName, entity: entity, listenBus: listenProgress, sendProgressF: sendProgressF);
        }
        if (TextUtil.isEmpty(res)) {
          continue;
        } else {
          if (returnEntity) {
            FileEntity? fileEntity = createFileEntity(sourceFile: element, uploadHash: uploadHash, url: res);
            if (fileEntity != null) {
              listRs.add(fileEntity);
            }
          } else {
            listRs.add(res);
          }
        }
      }
    }
    if (returnEntity) {
      listRs.removeWhere((element) => (element.fileUrl ?? '').isEmpty);
    }
    return listRs;
  }

  //#endregion

  //#region 上传七牛
  /// 上传文件，返回List<UploadFileEntity>
  /// listenProgress:是否监听进度（bus发送事件）
  static Future<List<FileEntity>> uploadAssets(
      {required QiniuEntity? qiniuEntity,
      required List<dynamic> assets,
      bool? uploadHash,
      bool? listenProgress,
      Function? sendProgressF,
      Function? progressF,
      Function? statusF}) async {
    /// 获取七牛token失败
    if (qiniuEntity == null) {
      return [];
    }
    List<FileEntity> listFile = [];

    String suffix = await DeviceUtil.getSuffixPhone();

    for (var element in assets) {
      FileModel? model = await createAssetFileModal(element);
      if (model == null) {
        continue;
      }
      if (LyQiNiuUtil.getPutController().isCancelled) {
        LyQiNiuUtil.setPutController(PutController());
      }
      String? res = await LyQiNiuUtil.uploadWithFileModel(
          fileModel: model,
          id: model.id,
          qiniuEntity: qiniuEntity,
          listenProgress: listenProgress,
          sendProgressF: sendProgressF,
          progressF: progressF,
          statusF: statusF);

      if (res == null) {
        continue;
      } else {
        String url = "";
        if (ImageUtil.getFileFormat(model.file) == CompressFormat.heic && model.assetTye == EnumAssetType.photo) {
          url = res + suffix;
        } else {
          url = res;
        }
        FileEntity? fileEntity = createFileEntity(sourceFile: element, uploadHash: uploadHash, url: url);
        if (fileEntity != null) {
          listFile.add(fileEntity);
        }
      }
    }
    listFile.removeWhere((element) => (element.fileUrl ?? '').isEmpty);
    return listFile;
  }

  /// 最终返回List<String>  (成长档案缩略图/图片上传)
  static Future<List<String>> uploadAssetsSimple(
      {required QiniuEntity? qiniuEntity,
      required List<dynamic> assets,
      bool? uploadHash,
      bool? listenProgress,
      Function? sendProgressF,
      Function? progressF,
      Function? statusF}) async {
    /// 获取七牛token失败
    if (qiniuEntity == null) {
      return [];
    }
    String suffix = await DeviceUtil.getSuffixPhone();
    List<String> listFile = [];
    for (var element in assets) {
      FileModel? model = await createAssetFileModal(element);
      if (model == null) {
        continue;
      }
      Logger.d('model++++=${model.toJson()}');

      if (LyQiNiuUtil.getPutController().isCancelled) {
        LyQiNiuUtil.setPutController(PutController());
      }
      String? res = await LyQiNiuUtil.uploadWithFileModel(
          fileModel: model, qiniuEntity: qiniuEntity, listenProgress: listenProgress, sendProgressF: sendProgressF, progressF: progressF, statusF: statusF);

      if (res == null) {
        continue;
      } else {
        String url = "";
        if (ImageUtil.getFileFormat(model.file) == CompressFormat.heic && model.assetTye == EnumAssetType.photo) {
          url = res + suffix;
        } else {
          url = res;
        }
        listFile.add(url);
      }
    }
    return listFile;
  }

  /// 上传音频文件
  static Future<FileEntity?> uploadAudio(QiniuEntity? entity, LYAudioFile audioFile, {Function? sendProgress, Function? progress}) async {
    if (entity == null || audioFile.file == null) return null;
    // 创建 putController 对象
    if (LyQiNiuUtil.getPutController().isCancelled) {
      LyQiNiuUtil.setPutController(PutController());
    }
    var audioModel = FileModel(file: audioFile.file!, type: 3, duration: audioFile.duration!.inSeconds);
    var uploadUrl = await LyQiNiuUtil.uploadWithFileModel(fileModel: audioModel, qiniuEntity: entity, sendProgressF: sendProgress, progressF: progress);
    Logger.d('音频文件上传后URL=$uploadUrl');
    return FileEntity(fileUrl: uploadUrl, duration: audioFile.duration?.inMilliseconds, suffix: 'acc');
  }

  /// 上传文件，不处理文件、不压缩、不处理进度
  /// 场景：海报上传
  static Future<String> uploadFileSimple({required String filePath, required String fileName, required QiniuEntity entity}) {
    return LyQiNiuUtil.uploadWithFileSimple(filePath: filePath, fileName: fileName, entity: entity);
  }

  //#endregion

  /// 根据入参类型创建对应FileEntity
  static FileEntity? createFileEntity({dynamic sourceFile, bool? uploadHash, String? url}) {
    FileEntity? fileEntity;
    if (sourceFile == null || url == null) {
      fileEntity = null;
    }
    if (sourceFile is UploadAssetEntity) {
      var time = Platform.isAndroid ? sourceFile.asset!.modifiedDateTime : sourceFile.asset!.createDateTime;
      fileEntity = FileEntity(
        width: sourceFile.asset!.width,
        height: sourceFile.asset!.height,
        duration: sourceFile.asset!.duration,
        createTime: utils.date.formatDate(time, format: DateFormats.full),
        id: sourceFile.asset!.id,
        hashId: (uploadHash ?? false) ? sourceFile.asset!.id : null,
        fileUrl: url,
        suffix: url!.split('.').last.replaceAll("?/q/90", ""),
      );
    } else if (sourceFile is AssetEntity) {
      var time = Platform.isAndroid ? sourceFile.modifiedDateTime : sourceFile.createDateTime;
      fileEntity = FileEntity(
        width: sourceFile.width,
        height: sourceFile.height,
        duration: sourceFile.duration,
        createTime: utils.date.formatDate(time, format: DateFormats.full),
        id: sourceFile.id,
        hashId: (uploadHash ?? false) ? sourceFile.id : null,
        fileUrl: url,
        suffix: url!.split('.').last.replaceAll("?/q/90", ""),
      );
    } else if (sourceFile is LyAssetEntity) {
      var time = Platform.isAndroid ? sourceFile.modifiedDateTime : sourceFile.createDateTime;
      fileEntity = FileEntity(
        width: sourceFile.width,
        height: sourceFile.height,
        duration: sourceFile.duration,
        createTime: utils.date.formatDate(time, format: DateFormats.full),
        id: sourceFile.id,
        hashId: (uploadHash ?? false) ? sourceFile.id : null,
        fileUrl: url,
        suffix: url!.split('.').last.replaceAll("?/q/90", ""),
      );
    }
    return fileEntity;
  }

  /// 根据入参类型创建对应FileModal
  static Future<FileModel?> createAssetFileModal(sourceFile, {bool useHash = false}) async {
    FileModel? model;
    if (sourceFile == null) {
      model = null;
    }
    if (sourceFile is UploadAssetEntity) {
      File? assetFile = await sourceFile.asset?.file;
      if (assetFile == null) {
        model = null;
      } else {
        String hash = useHash ? (await FileUtil.getFileHash(assetFile.absolute.path)) : "";
        model = FileModel(
          file: assetFile,
          title: sourceFile.asset!.title!,
          type: sourceFile.asset!.typeInt,
          width: sourceFile.asset!.width,
          height: sourceFile.asset!.height,
          duration: sourceFile.asset!.duration,
          md5Hash: hash,
          id: sourceFile.asset!.id,
        );
      }
    } else if (sourceFile is AssetEntity) {
      File? assetFile = await sourceFile.file;
      if (assetFile == null) {
        model = null;
      } else {
        String hash = useHash ? (await FileUtil.getFileHash(assetFile.absolute.path)) : "";
        model = FileModel(
          file: assetFile,
          title: sourceFile.title!,
          type: sourceFile.typeInt,
          width: sourceFile.width,
          height: sourceFile.height,
          duration: sourceFile.duration,
          md5Hash: hash,
          id: sourceFile.id,
        );
      }
    } else if (sourceFile is LyAssetEntity) {
      File? assetFile = await sourceFile.file;
      if (assetFile == null) {
        model = null;
      } else {
        String hash = useHash ? (await FileUtil.getFileHash(assetFile.absolute.path)) : "";
        model = FileModel(
          file: assetFile,
          title: sourceFile.title!,
          type: sourceFile.typeInt,
          width: sourceFile.width,
          height: sourceFile.height,
          duration: sourceFile.duration,
          md5Hash: hash,
          id: sourceFile.id,
        );
      }
    } else if (sourceFile is FileModel) {
      model = sourceFile;
      if (TextUtil.isEmpty(model.md5Hash) && useHash) {
        model.md5Hash = await FileUtil.getFileHash(model.file.absolute.path);
      }
    }
    return model;
  }

  /// 取消当前请求
  static cancel() {
    LyQiNiuUtil.cancel();
    LyAliUtil.cancel();
  }
}
