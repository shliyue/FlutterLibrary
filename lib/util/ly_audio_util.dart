import 'package:audioplayers/audioplayers.dart';
import 'package:common_utils/common_utils.dart';

class LyAudioUtil {
  static final LyAudioUtil instance = LyAudioUtil._internal();
  static AudioPlayer? player;

  factory LyAudioUtil() => instance;

  LyAudioUtil._internal();

  playAudio(String path, {String? package, bool soloPlay = false, bool local = false, Function()? completeCallBack}) {
    if (TextUtil.isEmpty(path)) {
      return;
    }
    Source source;
    if (path.startsWith("https://") || path.startsWith("http://")) {
      source = UrlSource(path);
    } else if (local) {
      source = DeviceFileSource(path);
    } else {
      /// 设置asset前缀
      AudioCache.instance.prefix = (package == null ? "" : "packages/" + package + "/") + "assets/";
      source = AssetSource(path);
    }
    if (soloPlay) {
      dispose();
      AudioPlayer players = AudioPlayer();
      players.play(source);
      players.onPlayerComplete.listen((event) {
        players.dispose();
        if (completeCallBack != null) {
          completeCallBack();
        }
      });
    } else {
      player ??= AudioPlayer();
      player!.play(source);
      player!.onPlayerComplete.listen((event) {
        if (completeCallBack != null) {
          completeCallBack();
        }
      });
    }
  }

  dispose() {
    if (player != null) {
      player!.dispose();
      player = null;
    }
  }
}
