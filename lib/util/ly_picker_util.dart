import 'package:flutter/cupertino.dart';
import 'package:flutter/foundation.dart';
import 'package:flutter/material.dart';
import 'package:zyocore/values/edge_insets.dart';
import 'package:zyocore/values/style.dart';
import 'package:zyocore/values/text.dart';
import 'package:zyocore/widget/common/flex/row.dart';
import 'package:zyocore/widget/flutter_pickers/address_picker/address_entity.dart';
import 'package:zyocore/widget/flutter_pickers/address_picker/route/address_picker_route.dart';
import 'package:zyocore/widget/flutter_pickers/pickers.dart';
import 'package:zyocore/widget/flutter_pickers/style/picker_style.dart';
import 'package:zyocore/widget/flutter_pickers/time_picker/model/pduration.dart';
import 'package:zyocore/widget/flutter_pickers/time_picker/model/suffix.dart';
import 'package:zyocore/widget/menu/class_menu_widget.dart';
import 'package:zyocore/zyocore.dart';

typedef DateTimeCallback = Function(int y, int m, int? d);

Widget _cancelButton = Container(
  margin: EdgeInsets.only(left: 16.w),
  child: LYText.withStyle(
    '取消',
    style: text17.textColor(dark333333),
  ),
);

Widget _commitButton = Container(
  margin: EdgeInsets.only(right: 16.w),
  child: LYText.withStyle(
    '确定',
    style: text17.textColor(LibColor.colorMain).copyWith(
          fontFamily: AppFontFamily.pingFangMedium,
          package: 'zyocore',
        ),
  ),
);

// 头部样式
Decoration headDecoration = const BoxDecoration(
  color: lightF8F8F8,
);

/// item 覆盖样式
Widget itemOverlay = Container(
  decoration: BoxDecoration(color: lightF1F1F1.withOpacity(0.3), borderRadius: BorderRadius.circular(5.r)),
);

/// menu
Widget menuWidget = Container(
  padding: horizontal16,
  color: lightFFFFFF,
  width: utils.style.screenWidth,
  child: RBC(
    children: [
      Expanded(
        child: GestureDetector(
          child: Container(
            decoration: const BoxDecoration(
              border: Border(
                bottom: BorderSide(width: 0.5, color: lightE1E1E1),
              ),
            ),
            child: LYText.withStyle(
              '2022-12-09',
              style: text17.dark,
            ),
          ),
        ),
      ),
      Container(
        width: 47,
        alignment: Alignment.center,
        child: LYText.withStyle(
          '至',
          style: text17.light,
        ),
      ),
      Expanded(
        child: GestureDetector(
          child: Container(
            decoration: const BoxDecoration(
              border: Border(
                bottom: BorderSide(width: 0.5, color: lightE1E1E1),
              ),
            ),
            child: LYText.withStyle(
              '2022-12-09',
              style: text17.dark,
            ),
          ),
        ),
      )
    ],
  ),
);

var pickerStyle = PickerStyle(
  cancelButton: _cancelButton,
  commitButton: _commitButton,
  headDecoration: headDecoration,
  textColor: dark1A1A1A,
  textSize: 17,
  backgroundColor: Colors.white,
  itemOverlay: itemOverlay,
);

var customPickerStyle = PickerStyle(
    cancelButton: _cancelButton,
    commitButton: _commitButton,
    headDecoration: headDecoration,
    textColor: dark1A1A1A,
    textSize: 17,
    backgroundColor: Colors.white,
    itemOverlay: itemOverlay,
    menuHeight: 75,
    menu: menuWidget);

class LYPickerUtil {
  static Widget _buildTopBtn({required Function() callback}) {
    return Container(
      width: ScreenUtil().screenWidth,
      height: 52.w,
      color: lightF9F9F9,
      child: Row(
        mainAxisAlignment: MainAxisAlignment.spaceBetween,
        children: [
          TextButton(
            onPressed: () => Get.back(),
            child: LYText(
              text: '取消',
              fontSize: 17.sp,
              color: dark333333,
            ),
          ),
          TextButton(
            onPressed: () {
              callback();
              Get.back();
            },
            child: LYText(
              text: '确定',
              fontSize: 17.sp,
              color: LibColor.colorMain,
              fontFamily: AppFontFamily.pingFangMedium,
            ),
          ),
        ],
      ),
    );
  }

  static Widget _buildContent({
    required FixedExtentScrollController controller,
    required Widget Function(int index) buildItem,
    required int childCount,
    double? marginBottom,
  }) {
    return Container(
      height: 188.w,
      width: ScreenUtil().screenWidth,
      margin: EdgeInsets.only(bottom: marginBottom ?? 0),
      padding: EdgeInsets.symmetric(horizontal: 16.w),
      child: LYScrollConfiguration(
        child: CupertinoPicker.builder(
          scrollController: controller,
          itemExtent: 41.w,
          onSelectedItemChanged: (index) {},
          itemBuilder: (context, index) {
            return buildItem(index);
          },
          childCount: childCount,
        ),
      ),
    );
  }

  static showPickerWithList(
    BuildContext context, {
    required List<String> list,
    required Function(String) callback,
  }) {
    FixedExtentScrollController controller = FixedExtentScrollController(initialItem: 0);
    return LYSheetUtil.showBaseModalBottomSheet(
      context: context,
      child: UnconstrainedBox(
        child: Container(
          color: Colors.white,
          child: Column(
            children: [
              _buildTopBtn(callback: () {
                callback(list[controller.selectedItem]);
              }),
              _buildContent(
                controller: controller,
                childCount: list.length,
                buildItem: (index) {
                  return Container(
                    width: ScreenUtil().screenWidth,
                    alignment: Alignment.center,
                    child: LYText(
                      text: list[index],
                      fontSize: 17.sp,
                    ),
                  );
                },
              ),
            ],
          ),
        ),
      ),
    );
  }

  /// 学年
  static showYearPicker(
    BuildContext context, {
    required int current,
    required Function(int) callback,
  }) {
    int currentYear = AppUtils().getDefaultGradeYear();
    int startYear = currentYear;
    int currentIndex = current - startYear;
    FixedExtentScrollController controller = FixedExtentScrollController(initialItem: currentIndex);
    return LYSheetUtil.showBaseModalBottomSheet(
      context: context,
      child: UnconstrainedBox(
        child: Container(
          color: Colors.white,
          child: Column(
            children: [
              _buildTopBtn(callback: () {
                callback(startYear + controller.selectedItem);
              }),
              _buildContent(
                controller: controller,
                childCount: 2,
                marginBottom: 34.w,
                buildItem: (index) {
                  return Container(
                    child: Stack(
                      alignment: AlignmentDirectional.center,
                      children: [
                        Container(
                          width: ScreenUtil().screenWidth,
                          alignment: Alignment.center,
                          child: LYText(
                            text: '${startYear + index}学年',
                            fontSize: 17.sp,
                          ),
                        ),
                        Positioned(
                          right: 70.w,
                          child: Visibility(
                            visible: startYear + index == currentYear,
                            child: Container(
                              padding: EdgeInsets.symmetric(horizontal: 6.w, vertical: 2.w),
                              decoration: BoxDecoration(
                                borderRadius: BorderRadius.circular(10.r),
                                color: themeECE7FF,
                              ),
                              child: LYText(
                                text: '当前学年',
                                color: LibColor.colorMain,
                                fontSize: 12.sp,
                              ),
                            ),
                          ),
                        )
                      ],
                    ),
                    alignment: Alignment.center,
                  );
                },
              ),
            ],
          ),
        ),
      ),
    );
  }

  /// 年级
  static showGradePicker(
    BuildContext context, {
    required String currentValue,
    bool showCurrentTip = true,
    Function(String)? callBack,
  }) {
    var dataSource = [
      '托班',
      '小班',
      '中班',
      '大班',
    ];
    int currentIndex = dataSource.indexOf(currentValue);
    FixedExtentScrollController controller = FixedExtentScrollController(initialItem: currentIndex);
    return LYSheetUtil.showBaseModalBottomSheet(
      context: context,
      child: UnconstrainedBox(
        child: Container(
          color: Colors.white,
          child: Column(
            children: [
              _buildTopBtn(callback: () {
                if (callBack != null) {
                  callBack(dataSource[controller.selectedItem]);
                }
              }),
              _buildContent(
                controller: controller,
                childCount: dataSource.length,
                buildItem: (index) {
                  return Container(
                    child: Stack(
                      alignment: AlignmentDirectional.center,
                      children: [
                        Container(
                          width: ScreenUtil().screenWidth,
                          alignment: Alignment.center,
                          child: LYText(
                            text: dataSource[index],
                            fontSize: 17.sp,
                          ),
                        ),
                        if (showCurrentTip)
                          Positioned(
                            right: 70.w,
                            child: Visibility(
                              visible: currentIndex == index,
                              child: Container(
                                padding: EdgeInsets.symmetric(horizontal: 6.w, vertical: 2.w),
                                decoration: BoxDecoration(
                                  borderRadius: BorderRadius.circular(10.r),
                                  color: themeECE7FF,
                                ),
                                child: LYText(
                                  text: '当前年级',
                                  color: LibColor.colorMain,
                                  fontSize: 12.sp,
                                ),
                              ),
                            ),
                          )
                      ],
                    ),
                    alignment: Alignment.center,
                  );
                },
              ),
            ],
          ),
        ),
      ),
    );
  }

  /// 班级
  static showClassNoPicker(
    BuildContext context, {
    required List<String?> dataSource,
    required String currentValue,
    Function(int)? callBack,
  }) {
    int currentIndex = dataSource.indexOf(currentValue);
    FixedExtentScrollController controller = FixedExtentScrollController(initialItem: currentIndex);
    return LYSheetUtil.showBaseModalBottomSheet(
      context: context,
      child: UnconstrainedBox(
        child: Container(
          color: Colors.white,
          child: Column(
            children: [
              _buildTopBtn(callback: () {
                if (callBack != null) {
                  callBack(controller.selectedItem);
                }
              }),
              _buildContent(
                controller: controller,
                childCount: dataSource.length,
                buildItem: (index) {
                  return Container(
                    transform: Matrix4.translationValues(
                        (dataSource[index] == currentValue && (dataSource[index] ?? "").length * 17.sp < 245.w) ? 28.w : 0, 0, 0),
                    child: Row(
                      mainAxisAlignment: MainAxisAlignment.center,
                      // alignment: AlignmentDirectional.center,
                      children: [
                        Container(
                          constraints: BoxConstraints(maxWidth: 245.w),
                          child: LYText(
                            text: dataSource[index] ?? '',
                            fontSize: 17.sp,
                          ),
                        ),
                        Visibility(
                          visible: dataSource[index] == currentValue,
                          child: Container(
                            padding: EdgeInsets.symmetric(horizontal: 6.w, vertical: 2.w),
                            decoration: BoxDecoration(
                              borderRadius: BorderRadius.circular(10.r),
                              color: themeECE7FF,
                            ),
                            child: LYText(
                              text: '当前班级',
                              color: LibColor.colorMain,
                              fontSize: 12.sp,
                            ),
                          ),
                        )
                      ],
                    ),
                    alignment: Alignment.center,
                  );
                },
              ),
            ],
          ),
        ),
      ),
    );
  }

  /// 园所
  static showGrandPicker(
    BuildContext context, {
    required List<String?> dataSource,
    required String currentValue,
    Function(int)? callBack,
  }) {
    return showListPicker(context, dataSource: dataSource, initStr: currentValue, callback: (i, v) {
      if (callBack != null) {
        callBack(i);
      }
    });
  }

  /// 幼儿园类型
  static showTypePicker(BuildContext context, {required Function(int) callback}) {
    var dataSource = ['私立幼儿园', '公立幼儿园'];
    return showListPicker(context, dataSource: dataSource, callback: (i, v) {
      callback(i);
    });
  }

  /// 性别
  static showSexPicker(BuildContext context, {required Function(int) callback, String initSex = '男'}) {
    var dataSource = ['男', '女'];
    return showListPicker(context, dataSource: dataSource, initStr: "男", callback: (i, v) {
      callback(i);
    });
  }

  /// 生日
  static showBirthdayPicker(BuildContext context, {required Function(int) callback}) {
    var dataSource = ['男', '女'];
    return showListPicker(context, dataSource: dataSource, callback: (i, v) {
      callback(i);
    });
  }

  /// 显示选项 返回索引
  static showListPicker(
    BuildContext context, {
    required Function(int, String) callback,
    required List<String?> dataSource,
    String? initStr,
  }) {
    FixedExtentScrollController controller =
        FixedExtentScrollController(initialItem: TextUtil.isEmpty(initStr) ? 0 : dataSource.indexOf(initStr!));
    return LYSheetUtil.showBaseModalBottomSheet(
      context: context,
      child: UnconstrainedBox(
        child: Container(
          color: Colors.white,
          child: Column(
            children: [
              _buildTopBtn(callback: () {
                callback(controller.selectedItem, dataSource[controller.selectedItem]!);
              }),
              _buildContent(
                controller: controller,
                childCount: dataSource.length,
                buildItem: (index) {
                  return Container(
                    width: ScreenUtil().screenWidth,
                    alignment: Alignment.center,
                    child: LYText(
                      text: dataSource[index],
                      fontSize: 17.sp,
                    ),
                  );
                },
              ),
            ],
          ),
        ),
      ),
    );
  }

  /// 省市区三级列表
  static showAddressPicker(AddressEntity p, AddressEntity c,
      {BuildContext? context, List<AddressEntity>? datas, AddressEntity? t, AddressCallback? onConfirm}) async {
    Pickers.showAddressPicker(
      context ?? Get.context!,
      datas!,
      pickerStyle: pickerStyle,
      addAllItem: false,
      initProvince: p,
      initCity: c,
      initTown: t,
      onConfirm: onConfirm,
    );
  }

  /// 年月二级列表
  static showYMDatePicker({
    BuildContext? context,
    DateMode mode = DateMode.YM,
    DateTime? selected,
    PDuration? maxDate,
    PDuration? minDate,
    DateTimeCallback? onConfirm,
    bool? confirmPop,
  }) async {
    return showDatePicker(
      context: context,
      mode: mode,
      selected: selected,
      maxDate: maxDate,
      minDate: minDate,
      onConfirm: onConfirm,
      confirmPop: confirmPop,
    );
  }

  /// 年月日三级列表
  static showDatePicker(
      {BuildContext? context,
      DateMode mode = DateMode.YMD,
      DateTime? selected,
      PDuration? maxDate,
      PDuration? minDate,
      DateTimeCallback? onConfirm,
      bool? confirmPop}) async {
    Suffix suffix = Suffix(
      years: ' 年',
      month: ' 月',
      days: ' 日',
    );
    switch (mode) {
      case DateMode.YMDHMS:
        suffix = Suffix(years: ' 年', month: ' 月', days: ' 日', hours: ' 时', minutes: ' 分', seconds: ' 秒');
        break;
      case DateMode.YMDHM:
        suffix = Suffix(years: ' 年', month: ' 月', days: ' 日', hours: ' 时', minutes: ' 分');
        break;
      case DateMode.YMDH:
        suffix = Suffix(years: ' 年', month: ' 月', days: ' 日', hours: ' 时');
        break;
      case DateMode.YMD:
        suffix = Suffix(years: ' 年', month: ' 月', days: ' 日');
        break;
      case DateMode.YM:
        suffix = Suffix(years: ' 年', month: ' 月');
        break;
      case DateMode.Y:
        suffix = Suffix(years: ' 年');
        break;
      case DateMode.MDHMS:
        // TODO: Handle this case.
        break;
      case DateMode.MDHM:
        // TODO: Handle this case.
        break;
      case DateMode.MDH:
        // TODO: Handle this case.
        break;
      case DateMode.MD:
        // TODO: Handle this case.
        break;
      case DateMode.HMS:
        // TODO: Handle this case.
        break;
      case DateMode.HM:
        // TODO: Handle this case.
        break;
      case DateMode.MS:
        // TODO: Handle this case.
        break;
      case DateMode.S:
        // TODO: Handle this case.
        break;
      case DateMode.M:
        suffix = Suffix(month: ' 月');
        break;
      case DateMode.H:
        // TODO: Handle this case.
        break;
    }
    Pickers.showDatePicker(
      context ?? Get.context!,
      mode: mode,
      maxDate: maxDate,
      minDate: minDate,
      selectDate: selected == null ? null : PDuration.parse(selected),
      suffix: suffix,
      pickerStyle: pickerStyle,
      confirmPop: confirmPop ?? true,
      onConfirm: (p) {
        if (onConfirm != null) {
          onConfirm(p.year ?? 0, p.month ?? 0, p.day ?? 0);
        }
      },
    );
  }

  static OverlayEntry? overlayEntry;
  static List<OverlayEntry?> countList = <OverlayEntry>[];

  showSelectorPicker(BuildContext context,
      {required List<String?> dataSource, required String currentValue, Function(String?)? callBack}) {
    OverlayState? overlayState = Overlay.of(context);
    overlayEntry = OverlayEntry(builder: (context) {
      return ClassMenuWidget(
        backAction: (value) {
          countList.remove(overlayEntry);
          overlayEntry?.remove();
          if (callBack != null) {
            callBack(value);
          }
        },
        list: dataSource,
        currentValue: currentValue,
      );
    });
    if (countList.isEmpty) {
      countList.add(overlayEntry);
      overlayState?.insert(overlayEntry!);
    }
  }

  showTopSheet(BuildContext context, {String? title, String? content, Function(String?)? callBack}) {
    OverlayState? overlayState = Overlay.of(context);
    overlayEntry = OverlayEntry(builder: (context) {
      return InkWell(
        onTap: () {
          countList.remove(overlayEntry);
          overlayEntry?.remove();
        },
        child: Align(
          alignment: Alignment.topCenter,
          child: Container(
            width: double.infinity,
            height: 100.w,
            margin: EdgeInsets.only(top: ScreenUtil().statusBarHeight, left: 16.w, right: 16.w),
            padding: EdgeInsets.symmetric(vertical: 12.w, horizontal: 16.w),
            decoration: BoxDecoration(
              color: Colors.white,
              borderRadius: BorderRadius.circular(20.r),
              boxShadow: [BoxShadow(color: lightE1E1E1.withOpacity(0.74), offset: const Offset(2, 9), blurRadius: 15)],
            ),
            child: Column(
              mainAxisAlignment: MainAxisAlignment.center,
              crossAxisAlignment: CrossAxisAlignment.start,
              children: [
                LYText(text: title ?? "使用说明:", fontSize: 15.sp),
                SizedBox(height: 2.w),
                LYText(text: content ?? "描述", fontSize: 13.sp, maxLines: 2),
              ],
            ),
          ),
        ),
      );
    });
    if (countList.isEmpty) {
      countList.add(overlayEntry);
      overlayState?.insert(overlayEntry!);
    }
  }

  closeOverlayEntry() {
    if (countList.isNotEmpty && overlayEntry != null) {
      countList.remove(overlayEntry);
      overlayEntry?.remove();
    }
  }
}
