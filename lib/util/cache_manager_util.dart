import 'package:flutter_cache_manager/flutter_cache_manager.dart';

class CacheManagerUtil extends CacheManager {
  static const key = 'zyoLibCachedData';

  static final CacheManagerUtil _instance = CacheManagerUtil._();

  factory CacheManagerUtil() {
    return _instance;
  }

  CacheManagerUtil._()
      : super(Config(
          key,
          stalePeriod: const Duration(days: 30),
          maxNrOfCacheObjects: 500, //缓存文件的最大数量。
          repo: JsonCacheInfoRepository(databaseName: key),
          fileService: HttpFileService(),
        ));
}
