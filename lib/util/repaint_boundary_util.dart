import 'dart:io';
import 'dart:typed_data';
import 'package:flutter/material.dart';
import 'package:flutter/rendering.dart';
import 'package:image_gallery_saver/image_gallery_saver.dart';
import 'package:path_provider/path_provider.dart';
import 'package:screenshot/src/platform_specific/file_manager/file_manager.dart';
import 'package:zyocore/util/toast_util.dart';
import 'dart:async';
import 'dart:ui';

import 'ly_permission.dart';

//全局key-截图key
final GlobalKey boundaryKey = GlobalKey();

class RepaintBoundaryUtil {
//生成截图
  /// 截屏图片生成图片流ByteData
  static Future<String> captureImage({GlobalKey? globalKey}) async {
    RenderRepaintBoundary? boundary = (globalKey ?? boundaryKey)
        .currentContext!
        .findRenderObject() as RenderRepaintBoundary?;
    double dpr = window.devicePixelRatio; // 获取当前设备的像素比
    var image = await boundary!.toImage(pixelRatio: dpr);
    // 将image转化成byte
    ByteData? byteData = await image.toByteData(format: ImageByteFormat.png);

    var filePath = "";

    Uint8List pngBytes = byteData!.buffer.asUint8List();
    // 获取手机存储（getTemporaryDirectory临时存储路径）
    Directory applicationDir = await getTemporaryDirectory();
    // getApplicationDocumentsDirectory();
    // 判断路径是否存在
    bool isDirExist = await Directory(applicationDir.path).exists();
    if (!isDirExist) Directory(applicationDir.path).create();
    // 直接保存，返回的就是保存后的文件
    File saveFile = await File(
            applicationDir.path + "${DateTime.now().toIso8601String()}.jpg")
        .writeAsBytes(pngBytes);
    filePath = saveFile.path;

    return filePath;
  }

  /// 截屏图片生成图片流ByteData-临时文件，后缀名为.th
  static Future<String?> savePhotoTh({GlobalKey? globalKey}) async {
    var p = await LyPermission.photos();
    if (!p) return '';
    return _saveFileTh();
  }

  static Future<String?> _saveFileTh({GlobalKey? globalKey}) async {
    RenderRepaintBoundary? boundary = (globalKey ?? boundaryKey)
        .currentContext!
        .findRenderObject() as RenderRepaintBoundary?;
    double dpr = window.devicePixelRatio; // 获取当前设备的像素比
    var image = await boundary!.toImage(pixelRatio: dpr);
    // 将image转化成byte
    ByteData? byteData = await image.toByteData(format: ImageByteFormat.png);
    Uint8List images = byteData!.buffer.asUint8List();

    Directory applicationDir = await getTemporaryDirectory();
    // 判断路径是否存在
    bool isDirExist = await Directory(applicationDir.path).exists();
    if (!isDirExist) Directory(applicationDir.path).create();
    // 直接保存，返回的就是保存后的路径
    PlatformFileManager fileManager = PlatformFileManager();
    String imgName = "${DateTime.now().millisecondsSinceEpoch}.jpg.th";
    return fileManager.saveFile(images, applicationDir.path, name: imgName);
  }

  //保存到相册
  static Future savePhoto({GlobalKey? globalKey}) async {
    var p = await LyPermission.photos();
    if (!p) return;
    _saveFile(globalKey: globalKey);
  }

  static _saveFile({GlobalKey? globalKey}) async {
    RenderRepaintBoundary? boundary = (globalKey ?? boundaryKey)
        .currentContext!
        .findRenderObject() as RenderRepaintBoundary?;
    double dpr = window.devicePixelRatio; // 获取当前设备的像素比
    var image = await boundary!.toImage(pixelRatio: dpr);
    // 将image转化成byte
    ByteData? byteData = await image.toByteData(format: ImageByteFormat.png);
    Uint8List images = byteData!.buffer.asUint8List();
    var result;
    if (Platform.isIOS) {
      result = await ImageGallerySaver.saveImage(images,
          quality: 60, name: "qrcode");
    } else {
      result = await ImageGallerySaver.saveImage(images, quality: 60);
    }
    if (result != null) {
      ToastUtil.showToast("保存成功");
    } else {
      ToastUtil.showToast("保存失败");
    }
  }

  /// 保存临时文件,写入本地路径,仅仅写作为了保存通用文件路径分享使用
  static Future<String> saveFileToTempPath(
      {GlobalKey? globalKey}) async {
    RenderRepaintBoundary? boundary = (globalKey ?? boundaryKey)
        .currentContext!
        .findRenderObject() as RenderRepaintBoundary?;
    double dpr = window.devicePixelRatio; // 获取当前设备的像素比
    var image = await boundary!.toImage(pixelRatio: dpr);
    // 将image转化成byte
    ByteData? byteData = await image.toByteData(format: ImageByteFormat.png);
    Uint8List images = byteData!.buffer.asUint8List();
    // 生成临时文件
    String fileName = "WxShare-ybt-fenxiang-001.jpg";
    Directory applicationDir = await getTemporaryDirectory();
    var newPath = applicationDir.path + "/wx-fenxaing";
    bool isDirExist = await Directory(newPath).exists();
    if (!isDirExist) Directory(newPath).create();
    PlatformFileManager fileManager = PlatformFileManager();
    final filePath =
        await fileManager.saveFile(images, newPath, name: fileName);
    return filePath;
  }
}
