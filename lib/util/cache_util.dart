import 'dart:io';
import 'package:path_provider/path_provider.dart';

import 'logger.dart';

class CacheUtil {
  //获取缓存文件夹
  static Future<Directory> getTempDir() async {
    var tempDir = await getTemporaryDirectory();
    return tempDir;
  }

  // 计算缓存
  static Future<String> cacheSize() async {
    var tempDir = await getTemporaryDirectory();
    double value = await getTotalSizeOfFilesInDir(tempDir);
    return (value / 1048576).toStringAsFixed(1);
  }

  // 清楚缓存
  static Future cacheClean() async {
    try {
      var tempDir = await getTemporaryDirectory();
      await delDir(tempDir);
    } catch (e) {
      Logger.d(e);
    }
  }

  static Future delDir(FileSystemEntity file, {bool isDelDir = true}) async {
    try {
      if (file is Directory) {
        List<FileSystemEntity> children = file.listSync();
        for (FileSystemEntity child in children) {
          await delDir(child);
        }
      }
      if (file is File || isDelDir) {
        await file.delete();
      }
    } catch (e) {
      Logger.d(e);
    }
  }

  static Future<double> getTotalSizeOfFilesInDir(FileSystemEntity file) async {
    try {
      if (file is File) {
        int length = await file.length();
        return double.parse(length.toString());
      }
      if (file is Directory) {
        final List<FileSystemEntity> children = file.listSync();
        double total = 0;
        for (final FileSystemEntity child in children) {
          total += await getTotalSizeOfFilesInDir(child);
        }
        return total;
      }
      return 0;
    } catch (e) {
      return 0;
    }
  }
}
