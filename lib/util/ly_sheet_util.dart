import 'package:flutter/material.dart';
import 'package:zyocore/values/edge_insets.dart';
import 'package:zyocore/values/style.dart';
import 'package:zyocore/widget/common/flex/column.dart';
import 'package:zyocore/zyocore.dart';

import '../core/configs/config.dart';
import '../core/model/select_model.dart';
import '../widget/action_sheets/picker/phone_call_action_sheet.dart';
import '../widget/action_sheets/picker/stud_pop_widget.dart';
import '../widget/action_sheets/picker/time_picker_action_sheet.dart';
import '../widget/action_sheets/picker/weekday_action_sheet.dart';

class LYSheetUtil {
  static Future showBaseModalBottomSheet(
      {BuildContext? context,
      Widget? child,
      bool enableDrag = true,
      bool autoClose = true,
      bool isDismissible = false,
      bool isScrollControlled = true,
      ShapeBorder? shape,
      Color? backgroundColor,
      double? elevation,
      String? routeName}) async {
    var data = await showModalBottomSheet(
        backgroundColor: backgroundColor ?? Colors.transparent,
        context: context ?? Get.context!,
        isScrollControlled: isScrollControlled,
        isDismissible: isDismissible,
        enableDrag: enableDrag,
        elevation: elevation,
        routeSettings: RouteSettings(name: routeName, arguments: autoClose),
        builder: (context) => child!,
        shape: shape);

    return data;
  }

  /// 选择时间
  static Future showTimePicker(String beginTime, String endTime) async {
    return await showBaseModalBottomSheet(child: TimePickerActionSheet(beginTime, endTime), enableDrag: false);
  }

  /// 选择星期几
  static Future showWeekday({List<int> selected = const []}) async {
    return await showBaseModalBottomSheet(child: WeekdayActionSheet(selected));
  }

  /// 选择幼儿
  static Future showStud(
      {List<SelectModel> selects = const [],
      required ValueChanged<List<SelectModel>> callback,
      required List<SelectListModel> listData,
      String? packageName}) async {
    return await showBaseModalBottomSheet(child: StudPopWidget(selects: selects, callback: callback, listData: listData, packageName: packageName));
  }

  /// 招生管理---拨打电话
  static Future showPhoneCall({required List<String> data}) async {
    return await showBaseModalBottomSheet(child: PhoneCallActionSheet(data));
  }

  /// 显示普通的actionSheet
  static Future showDefaultBottomSheet({
    Widget? child,
  }) async {
    return await showBaseModalBottomSheet(child: child, enableDrag: false, isDismissible: true);
  }

  static showListSheet(
    BuildContext context, {
    required List<String> data,
    required Function(int) callback,
  }) {
    showBaseModalBottomSheet(
      context: context,
      isDismissible: true,
      child: Container(
        height: data.length * 56.h + 90.h,
        decoration: BoxDecoration(
          color: Colors.white,
          borderRadius: BorderRadius.only(
            topLeft: Radius.circular(8.r),
            topRight: Radius.circular(8.r),
          ),
        ),
        child: LYScrollConfiguration(
          child: ListView.builder(
            itemBuilder: (_, index) {
              if (index == data.length) {
                return _buildBtnCancel();
              }
              return GestureDetector(
                onTap: () {
                  Get.back();
                  callback(index);
                },
                child: Container(
                  padding: EdgeInsets.symmetric(vertical: 16.h),
                  decoration: BoxDecoration(
                    border: index < data.length - 1
                        ? const Border(
                            bottom: BorderSide(width: 0.5, color: lightE1E1E1),
                          )
                        : null,
                  ),
                  alignment: Alignment.center,
                  child: LYText(
                    text: data[index],
                    fontSize: 17.sp,
                    color: dark191919,
                  ),
                ),
              );
            },
            itemCount: data.length + 1,
          ),
        ),
      ),
    );
  }

  static showSelectFile(
    BuildContext context, {
    bool photo = true,
    String? photoText,
    bool video = true,
    String? videoText,
    Color videoNoteColor = Colors.red,
    int? maxMinute = 5,
    required Function(String) callback,
  }) {
    int count = 1;
    if (photo && video) count = 2;

    showBaseModalBottomSheet(
      context: context,
      isDismissible: true,
      child: Container(
        height: count * 56.w + 90.w,
        decoration: BoxDecoration(
          color: Colors.white,
          borderRadius: BorderRadius.only(
            topLeft: Radius.circular(8.r),
            topRight: Radius.circular(8.r),
          ),
        ),
        child: LYScrollConfiguration(
          child: ListView(
            children: [
              if (photo)
                GestureDetector(
                  onTap: () async {
                    Get.back();
                    var p = await LyPermission.photos();
                    if (p) {
                      callback('photo');
                    }
                  },
                  child: Container(
                    padding: vertical15,
                    decoration: const BoxDecoration(
                      border: Border(
                        bottom: BorderSide(width: 0.5, color: lightE1E1E1),
                      ),
                    ),
                    alignment: Alignment.center,
                    child: LYText(
                      text: photoText ?? '照片',
                      fontSize: 17.sp,
                      color: dark191919,
                    ),
                  ),
                ),
              if (video)
                GestureDetector(
                  onTap: () async {
                    Get.back();
                    var p = await LyPermission.videos();
                    if (p) {
                      callback('video');
                    }
                  },
                  child: Container(
                    padding: vertical15,
                    decoration: const BoxDecoration(
                      border: Border(
                        bottom: BorderSide(width: 0.5, color: lightE1E1E1),
                      ),
                    ),
                    alignment: Alignment.center,
                    child: CSC(
                      children: [
                        LYText(
                          text: videoText ?? "视频",
                          fontSize: 17.sp,
                          color: dark191919,
                        ),
                        LYText(
                          text: '仅支持$maxMinute分钟以内视频',
                          fontSize: 12.sp,
                          color: videoNoteColor,
                        )
                      ],
                    ),
                  ),
                ),
              _buildBtnCancel(),
            ],
          ),
        ),
      ),
    );
  }

  static Future showTextSheet(
    BuildContext context, {
    required String text,
    Color? textColor,
    required VoidCallback callback,
  }) async {
    return showBaseModalBottomSheet(
      context: context,
      isDismissible: true,
      child: Container(
        decoration: const BoxDecoration(
          color: Colors.white,
          borderRadius: BorderRadius.only(
            topLeft: Radius.circular(8.0),
            topRight: Radius.circular(8.0),
          ),
        ),
        child: SafeArea(
          child: Column(
            mainAxisSize: MainAxisSize.min,
            children: [
              _buildBtnItem(text: text, callback: callback, textColor: textColor),
              _buildBtnCancel(),
            ],
          ),
        ),
      ),
    );
  }

  /// 园丁端时光轴操作
  static showSchoolTimelineOption(
    BuildContext context, {
    required String text,
    required String desc,
    required VoidCallback callback,
  }) {
    showBaseModalBottomSheet(
      context: context,
      isDismissible: true,
      child: UnconstrainedBox(
        child: Container(
          width: Get.width,
          decoration: BoxDecoration(
            color: Colors.white,
            borderRadius: BorderRadius.only(
              topLeft: Radius.circular(8.r),
              topRight: Radius.circular(8.r),
            ),
          ),
          child: Column(
            children: [
              Container(
                alignment: Alignment.center,
                padding: EdgeInsets.symmetric(vertical: 16.h),
                decoration: const BoxDecoration(
                  border: Border(
                    bottom: BorderSide(width: .5, color: lightE1E1E1),
                  ),
                ),
                child: LYText(
                  text: desc,
                  fontSize: 15.sp,
                  color: dark666666,
                ),
              ),
              _buildBtnItem(text: text, callback: callback, textColor: redE02020),
              _buildBtnCancel(),
            ],
          ),
        ),
      ),
    );
  }

  static Future showPage(
    BuildContext context, {
    required Widget page,
    String? title,
    bool showAction = true,
    bool showBtnOK = true,
    required VoidCallback callback,
  }) async {
    await showBaseModalBottomSheet(
      context: context,
      isDismissible: true, //允许bottomSheet高度自定义
      child: UnconstrainedBox(
        child: Container(
          width: Get.width,
          height: Get.height * .9,
          decoration: BoxDecoration(
            color: Colors.white,
            borderRadius: BorderRadius.only(
              topLeft: Radius.circular(8.r),
              topRight: Radius.circular(8.r),
            ),
          ),
          child: Column(
            children: [
              Container(
                padding: EdgeInsets.symmetric(horizontal: 16.w, vertical: 16.w),
                child: Row(
                  mainAxisAlignment: MainAxisAlignment.center,
                  children: [
                    GestureDetector(
                      child: Image.asset(
                        'assets/images/icon_navi_close.png',
                        width: 24.w,
                        package: Config.packageName,
                      ),
                      onTap: () => Get.back(),
                    ),
                    Expanded(
                        child: Column(
                      children: [LYText(text: title ?? '', fontSize: 17.sp, color: dark1A1A1A, maxLines: 2, overflow: TextOverflow.ellipsis)],
                    )),
                    showBtnOK
                        ? GestureDetector(
                            onTap: () {
                              callback();
                              Get.back();
                            },
                            child: AnimatedOpacity(
                              opacity: showAction ? 1 : 0,
                              duration: const Duration(milliseconds: 250),
                              child: LYText(
                                text: '确定',
                                fontSize: 17.sp,
                                color: LibColor.colorMain,
                                fontFamily: AppFontFamily.pingFangMedium,
                              ),
                            ),
                          )
                        : const SizedBox(),
                  ],
                ),
              ),
              Expanded(child: page),
            ],
          ),
        ),
      ),
    );
  }

  static Future showCustomPage(
    BuildContext context, {
    required Widget page,
    String? title,
    bool showAction = true,
    double? maxHeight,
    required VoidCallback callback,
  }) async {
    await showBaseModalBottomSheet(
      context: context,
      isDismissible: true,
      child: UnconstrainedBox(
        child: Container(
          width: Get.width,
          height: maxHeight ?? Get.height * .8,
          decoration: BoxDecoration(
            color: Colors.white,
            borderRadius: BorderRadius.only(
              topLeft: Radius.circular(16.r),
              topRight: Radius.circular(16.r),
            ),
          ),
          child: Column(
            children: [
              Container(
                width: 46.w,
                height: 6.w,
                padding: EdgeInsets.symmetric(vertical: 8.w),
                margin: EdgeInsets.symmetric(vertical: 8.w),
                decoration: BoxDecoration(
                  borderRadius: BorderRadius.circular(3.r),
                  color: lightD2D2D2,
                ),
              ),
              Expanded(child: page),
            ],
          ),
        ),
      ),
    );
  }

  /// 通知回复项统计详情
  static Future showNoticeStatisticDetail(
    BuildContext context, {
    required Widget page,
    String? title,
    bool showAction = true,
    required VoidCallback callback,
  }) async {
    await LYSheetUtil.showPage(context, page: page, title: title, showAction: showAction, showBtnOK: false, callback: callback);
  }

  /// 分享弹窗
  /// type : 0-文件  1-动态
  static Future showShareToolBar(
    BuildContext context, {
    int type = 0,
    bool canDel = true,
    bool canPoster = false,
    List<ShareItem>? itemIcons,
    Color? bgColor,
    Color? textColor,
    required Function(String) callback,
  }) async {
    return showBaseModalBottomSheet(
      context: context,
      isDismissible: true,
      child: UnconstrainedBox(
        child: SizedBox(
          width: Get.width,
          child: LYShareBar(
            canDel: canDel,
            callback: callback,
            type: type,
            itemIcons: itemIcons,
            canPoster: canPoster,
            bgColor: bgColor,
            textColor: textColor,
          ),
        ),
      ),
    );
  }

  static Future<String> showBottomSheet({
    List<dynamic>? titleList,
    required BuildContext context,
  }) async {
    String select = '';
    await showBaseModalBottomSheet(
      context: context,
      backgroundColor: Colors.white,
      elevation: 10,
      enableDrag: false,
      shape: RoundedRectangleBorder(borderRadius: BorderRadius.circular(8)),
      child: SafeArea(
        child: Column(
          mainAxisSize: MainAxisSize.min,
          mainAxisAlignment: MainAxisAlignment.end,
          children: [
            SizedBox(
              height: 56.w * (titleList?.length ?? 0),
              child: ListView.builder(
                shrinkWrap: false,
                physics: const NeverScrollableScrollPhysics(),
                itemBuilder: (context, index) {
                  return InkWell(
                      onTap: () {
                        select = titleList![index];
                        Navigator.pop(context);
                      },
                      child: Center(child: LYText(text: titleList?[index] ?? '', fontSize: 17)));
                },
                itemExtent: 56.w,
                itemCount: titleList?.length ?? 2,
              ),
            ),
            Container(
              width: double.infinity,
              height: 8.w,
              color: lightE1E1E1,
            ),
            SizedBox(
              height: 56.w,
              child: InkWell(
                onTap: () {
                  select = '取消';
                  Navigator.pop(context);
                },
                child: Center(child: LYText(text: '取消', fontSize: 17.sp, color: const Color(0XFF000000))),
              ),
            )
          ],
        ),
      ),
    );
    return select;
  }

  static Future showPageSheet(
    BuildContext context, {
    String? title,
    required Widget child,
    double? height,
    bool showClose = false,
    bool showCancel = true,
    bool isDismissible = false,
    MainAxisAlignment? mainAxisAlignment,
    Function? onClose,
  }) async {
    return await showBaseModalBottomSheet(
      context: context,
      isDismissible: isDismissible,
      child: Container(
        height: height ?? Get.height * .5,
        decoration: const BoxDecoration(
          color: Colors.white,
          borderRadius: BorderRadius.only(
            topLeft: Radius.circular(8.0),
            topRight: Radius.circular(8.0),
          ),
        ),
        child: SafeArea(
          child: Column(
            mainAxisSize: MainAxisSize.min,
            children: [
              Container(
                height: 44.w,
                padding: EdgeInsets.only(left: 16.w, right: 14.w),
                child: Row(
                  mainAxisAlignment: mainAxisAlignment ?? MainAxisAlignment.spaceBetween,
                  children: [
                    LYText(text: title, fontSize: 17.sp, fontFamily: AppFontFamily.pingFangMedium),
                    showClose
                        ? GestureDetector(
                            child: Image.asset(
                              'assets/images/icon_navi_close.png',
                              width: 24.w,
                              package: Config.packageName,
                            ),
                            onTap: () {
                              Get.back();
                              if (onClose != null) {
                                onClose();
                              }
                            },
                          )
                        : const SizedBox(),
                  ],
                ),
              ),
              Expanded(child: child),
              showCancel ? _buildBtnCancel() : const SizedBox(),
            ],
          ),
        ),
      ),
    );
  }

  static Widget _buildBtnItem({
    required String text,
    required VoidCallback callback,
    Color? textColor,
  }) {
    return GestureDetector(
      behavior: HitTestBehavior.opaque,
      onTap: () {
        Get.back();
        callback();
      },
      child: Container(
        alignment: Alignment.center,
        padding: const EdgeInsets.symmetric(vertical: 16.0),
        child: LYText(
          text: text,
          fontSize: 17.0,
          color: textColor ?? dark1A1A1A,
        ),
      ),
    );
  }

  static Widget _buildBtnCancel() {
    return GestureDetector(
      onTap: () => Get.back(),
      behavior: HitTestBehavior.opaque,
      child: Container(
        alignment: Alignment.center,
        padding: EdgeInsets.symmetric(vertical: 16.w),
        decoration: const BoxDecoration(
          border: Border(
            top: BorderSide(
              width: 8.0,
              color: lightF1F1F1,
            ),
          ),
        ),
        child: LYText(
          text: '取消',
          fontSize: 17.sp,
          color: dark1A1A1A,
        ),
      ),
    );
  }
}
