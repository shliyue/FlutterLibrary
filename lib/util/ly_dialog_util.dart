import 'dart:io';

import 'package:app_settings/app_settings.dart';
import 'package:common_utils/common_utils.dart';
import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:flutter_screenutil/flutter_screenutil.dart';
import 'package:get/get.dart';
import 'package:zyocore/extension/color_extension.dart';
import 'package:zyocore/values/style.dart';
import 'package:zyocore/widget/index.dart';

import '../core/configs/config.dart';
import '../entitys/version.dart';
import '../widget/dialog/dialog_input_text_view.dart';
import '../widget/dialog/dialog_widget.dart';
import '../widget/dialog/downlaod_progress_dialog.dart';
import '../widget/dialog/upd_version_dialog.dart';
import 'ly_button_util.dart';

class LYDialogUtil {
  /// 确认框
  /// [titleWidget] 标题
  /// [contentWidget] 如果传了 内容使用这个Widget
  /// [content] 在[contentWidget]不传的情况下用这个
  static Future showConfirmation(BuildContext context, {
    Widget? titleWidget,
    Widget? contentWidget,
    String? content,
    required VoidCallback callback,
    Widget? confirmBtn,
    Widget? closeBtn,
    VoidCallback? closeCallBack,
    String? leftBtnFont,
    String? rightBtnFont,
    Color? leftColor,
    Color? rightColor,
    String? routeName,
    String? fontFamily,
    String? contentFamily,
    Color? contentColor,
    bool autoClose = true,
    TextAlign? textAlign,
    EdgeInsetsGeometry? margin,
  }) async {
    return showWidget(
      context: context,
      name: routeName ?? "showConfirmation.dialog",
      Center(
        child: UnconstrainedBox(
          child: Container(
            width: 282.w,
            decoration: const BoxDecoration(
              color: Colors.white,
              borderRadius: BorderRadius.all(Radius.circular(8.0)),
            ),
            child: Column(
              mainAxisAlignment: MainAxisAlignment.center,
              children: [
                Container(
                  margin: margin ?? EdgeInsets.symmetric(horizontal: 16.w, vertical: 32.w),
                  alignment: Alignment.center,
                  child: Column(
                    mainAxisSize: MainAxisSize.min,
                    mainAxisAlignment: MainAxisAlignment.center,
                    children: [
                      if (titleWidget != null) titleWidget,
                      if (contentWidget != null)
                        contentWidget
                      else
                        LYText.withStyle(
                          content,
                          style: TextStyle(
                            fontSize: 17.sp,
                            height: 1.5,
                            color: contentColor ?? dark1A1A1A,
                            fontFamily: contentFamily ?? AppFontFamily.pingFangMedium,
                          ),
                          maxLines: 3,
                          textAlign: textAlign ?? TextAlign.center,
                        ),
                    ],
                  ),
                ),
                Container(
                  height: 56.w,
                  decoration: BoxDecoration(
                    color: lightF8F8F8,
                    borderRadius: BorderRadius.all(Radius.circular(8.r)),
                  ),
                  child: Row(
                    children: [
                      Expanded(
                        child: GestureDetector(
                          onTap: closeCallBack ?? () => Get.back(),
                          child: Container(
                            decoration: const BoxDecoration(
                              color: lightF8F8F8,
                              borderRadius: BorderRadius.only(bottomLeft: Radius.circular(8.0)),
                            ),
                            alignment: Alignment.center,
                            child: closeBtn ??
                                LYText(
                                  fontSize: 16.sp,
                                  text: leftBtnFont ?? '取消',
                                  color: leftColor ?? dark666666,
                                  fontFamily: fontFamily ?? AppFontFamily.pingFangMedium,
                                ),
                          ),
                        ),
                      ),
                      Container(height: 20.w, width: 1, color: lightD2D2D2),
                      Expanded(
                        child: GestureDetector(
                          onTap: () {
                            if (autoClose) {
                              Get.back();
                            }
                            callback();
                          },
                          child: Container(
                            decoration: const BoxDecoration(
                              color: lightF8F8F8,
                              borderRadius: BorderRadius.only(
                                bottomRight: Radius.circular(8.0),
                              ),
                            ),
                            alignment: Alignment.center,
                            child: confirmBtn ??
                                LYText(
                                  fontSize: 16.sp,
                                  text: rightBtnFont ?? '确认',
                                  color: rightColor ?? dark333333,
                                  fontFamily: fontFamily ?? AppFontFamily.pingFangMedium,
                                ),
                          ),
                        ),
                      ),
                    ],
                  ),
                )
              ],
            ),
          ),
        ),
      ),
    );
  }

  /// 带输入框
  static showInputDialog(BuildContext context,
      {required String title,
        required String hinText,
        String? desc,
        Widget? descWidget,
        String? noEmptyDesc,
        int? maxLength,
        List<TextInputFormatter>? inputFormatters,
        bool defaultReturn = true,
        String? rightTitle,
        double? height,
        bool? showCount,
        int? maxLines,
        TextInputType? inputType,
        required Function(String) callback}) {
    TextEditingController tec = TextEditingController();

    showWidget(
      context: context,
      DialogInputTextView(
          title: title,
          hinText: hinText,
          callback: callback,
          defaultReturn: defaultReturn,
          desc: desc,
          noEmptyDesc: noEmptyDesc,
          maxLength: maxLength,
          inputFormatters: inputFormatters,
          rightTitle: rightTitle,
          height: height,
          showCount: showCount,
          inputType: inputType,
          maxLines: maxLines),
    );
  }

  /// 图片下载完成提示
  static Future showDownloadProgress(BuildContext context, {
    required String fileSrc,
    bool isOriginal = false,
  }) async {
    return showWidget(
      Center(
        child: Container(
          decoration: const BoxDecoration(
            color: Colors.white,
            borderRadius: BorderRadius.all(
              Radius.circular(15.0),
            ),
          ),
          clipBehavior: Clip.antiAlias,
          child: DownlaodProgressDialog(fileSrc: fileSrc, original: isOriginal),
        ),
      ),
      context: context,
      name: "showDownloadProgress.dialog",
    );
  }

  /**
   * 顶部带背景的提示框
   * */
  static showTopBgDialog(BuildContext context, {
    String? avatar,
    VoidCallback? callback,
    String? title,
    String? packName,
    String? content,
    String? btnText,
    bool showCloseBtn = false,
    String? bgImgPath,
  }) {
    showWidget(
      Center(
        child: Stack(
          alignment: AlignmentDirectional.center,
          children: [
            SizedBox(
              width: 282.w,
              height: 350.w,
            ),
            Container(
              width: 282.w,
              height: 226.w,
              decoration: BoxDecoration(
                color: Colors.white,
                borderRadius: BorderRadius.all(
                  Radius.circular(8.r),
                ),
              ),
              child: Stack(
                alignment: AlignmentDirectional.topCenter,
                children: [
                  Positioned(
                    top: 0,
                    child: Image.asset(
                      bgImgPath ?? 'assets/images/binding_stud_dialog_bg.png',
                      width: 282.w,
                      package: packName ?? Config.packageName,
                    ),
                  ),
                  Positioned(
                    top: 20.w,
                    child: TextUtil.isEmpty(avatar)
                        ? LYText(
                      text: title,
                      color: lightFFFFFF,
                      fontFamily: AppFontFamily.pingFangMedium,
                      fontSize: 22.sp,
                    )
                        : LYAvatarWidget(
                      src: avatar!,
                      width: 60.w,
                      height: 60.w,
                      radius: 60.r,
                    ),
                  ),
                  Positioned(
                    top: 92.w,
                    child: Container(
                      width: 282.w,
                      padding: EdgeInsets.symmetric(horizontal: 25.w),
                      child: LYText(
                        text: content ?? '您已经关注了宝宝，一起去见证ta的成长吧～',
                        fontSize: 15.sp,
                        textAlign: TextAlign.center,
                        maxLines: 2,
                      ),
                    ),
                  ),
                  Positioned(
                    bottom: 24.w,
                    child: LYButtonUtil.shadowButton(
                      text: btnText ?? '去查看',
                      width: 212.w,
                      height: 44.w,
                      onTap: () {
                        Get.back();
                        if (callback != null) {
                          callback();
                        }
                      },
                    ),
                  ),
                ],
              ),
            ),
            showCloseBtn
                ? Positioned(
              bottom: 0,
              child: GestureDetector(
                onTap: () => Get.back(),
                child: Image.asset(
                  'assets/images/icon_dialog_close.png',
                  width: 38.w,
                  height: 38.w,
                  package: Config.packageName,
                ),
              ),
            )
                : const SizedBox(),
          ],
        ),
      ),
    );
  }

  /// 图片、视频预览
  static showMediaPreview(BuildContext context, {required List files, int index = 0, tapLongPress, bool justPreview = false}) {
    showWidget(
      LyMediaPreview(
        srcs: files,
        index: index,
        onLongPress: tapLongPress,
        justPreview: justPreview,
      ),
    );
  }

  /// 提示modal
  static showInfo(BuildContext context, {
    required String? content,
    TextAlign contentTextAlign = TextAlign.center,
    String? title = "提示",
    barrierDismissible = false,
    String confirmText = "我知道了",
    Function? onCancel,
    showTitle = true,
    bool autoClose = true,
    String? routeName,
    Widget? contentWidget,
  }) {
    showWidget(
      context: context,
      name: routeName ?? "showInfo.dialog",
      DialogWidget(
        showTitle: showTitle,
        title: title,
        content: content,
        contentTextAlign: contentTextAlign,
        contentWidget: contentWidget,
        submit: DialogButton(confirmText, callback: onCancel, autoClose: autoClose),
      ),
    );
  }

  static showLoading({
    Color? bgColor,
    Color? centerColor,
    Color? indicatorColor,
    double? size,
    double strokeWidth = 4,
    MainAxisAlignment mainAxisAlignment = MainAxisAlignment.center,
    CrossAxisAlignment crossAxisAlignment = CrossAxisAlignment.center,
  }) {
    showWidget(
      context: Get.context!,
      name: "showInfo.dialog",
      barrierColor: bgColor ?? Colors.transparent,
      SizedBox(
        width: Get.width,
        child: Column(
          mainAxisAlignment: mainAxisAlignment,
          crossAxisAlignment: crossAxisAlignment,
          children: [
            Container(
              padding: EdgeInsets.all(15.w),
              decoration: BoxDecoration(
                color: centerColor ?? Colors.transparent,
                borderRadius: BorderRadius.all(Radius.circular(8.r)),
              ),
              child: SizedBox(
                width: size ?? 25.w,
                height: size ?? 25.w,
                child: CircularProgressIndicator(color: indicatorColor ?? Colors.white, strokeWidth: strokeWidth),
              ),
            ),
          ],
        ),
      ),
    );
  }

  static close() {
    Get.back();
  }

  static showWidget(Widget child, {
    BuildContext? context,
    Color? barrierColor,
    Duration? transitionDuration,
    String? barrierLabel,
    bool useSafeArea = false,
    bool barrierDismissible = false,
    String? name,
    RouteTransitionsBuilder? transitionBuilder,
  }) async {
    return await showGeneralDialog(
        context: context ?? Get.context!,
        barrierDismissible: barrierDismissible,
        barrierLabel: barrierLabel,
        barrierColor: barrierColor ?? const Color(0x80000000),
        routeSettings: RouteSettings(name: name ?? "showWidget.dialog"),
        transitionDuration: transitionDuration ?? const Duration(milliseconds: 200),
        transitionBuilder: transitionBuilder ??
                (context, anim1, anim2, child) {
              return Transform.scale(scale: anim1.value, child: child);
            },
        pageBuilder: (BuildContext context, Animation<double> animation, Animation<double> secondaryAnimation) {
          return child;
        });
  }

  /// 版本更新提示
  static Future<void> showVersionDialog(BuildContext context,
      VersionEntity model, {
        required String envType,
        required String appType,
        required String channelType,
        VoidCallback? onTapHandle,
      }) async {
    await showWidget(
      context: context,
      UpdVersionDialog(
        model: model,
        envType: envType,
        appType: appType,
        channelType: channelType,
        onTapHandle: onTapHandle,
      ),
    );
  }

  static Future<void> showBlueToothConfirm() async {
    await LYDialogUtil.showConfirmation(
      Get.context!,
      contentWidget: LYText(
        text: "蓝牙未开启，请去设置中打开蓝牙，并允许新连接",
        fontSize: 17.sp,
        softWrap: true,
        maxLines: 2,
        textAlign: TextAlign.center,
        fontWeight: FontWeight.w500,
        color: dark191919,
      ),
      content: "",
      confirmBtn: LYText(
        text: Platform.isIOS ? "我知道了" : "去设置",
        fontSize: 17.sp,
        color: dark191919,
        fontWeight: FontWeight.w500,
      ),
      callback: () {
        if (!Platform.isIOS) {
          openBlueToothSettings();
        }
      },
    );
  }

  /// 打开蓝牙设置
  static Future<void> openBlueToothSettings() async {
    if (Platform.isAndroid) {
      return AppSettings.openBluetoothSettings();
    } else {
      return AppSettings.openAppSettings();
    }
  }
}
