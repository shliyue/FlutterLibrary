import 'package:flutter/foundation.dart';

class Logger {
  static bool get isDebug => kDebugMode;

  static void d(Object? message, {
    bool when = true,
    StackTrace? trace,
  }) {
    assert(() {
      if (when) {
        if (trace == null) {
          if (kDebugMode) {
            print("YBT:${message}");
          }
        } else {
          if (kDebugMode) {
            DebugTrace d = DebugTrace(trace);
            print("文件: ${d.fileName}, 行: ${d.lineNumber}\n $message");
          }
        }
      }
      return true;
    }());
  }
}

class DebugTrace {
  final StackTrace _trace;
  late String fileName;
  late int lineNumber;
  late int columnNumber;

  DebugTrace(this._trace) {
    _parseTrace();
  }

  void _parseTrace() {
    var traceString = _trace.toString().split("\n")[0];
    var indexOfFileName = traceString.indexOf(RegExp(r'[A-Za-z_]+.dart'));
    var fileInfo = traceString.substring(indexOfFileName);
    var listOfInfos = fileInfo.split(":");
    fileName = listOfInfos[0];
    lineNumber = int.parse(listOfInfos[1]);
    var columnStr = listOfInfos[2];
    columnStr = columnStr.replaceFirst(")", "");
    columnNumber = int.parse(columnStr);
  }
}
