import 'dart:io';

import 'package:flutter/material.dart';
import 'package:permission_handler/permission_handler.dart';
import 'package:zyocore/core/configs/global_config.dart';
import 'package:zyocore/zyocore.dart';

import '../values/style.dart';

/// 日历权限被拒绝且点了取消，则后续申请均不再提示用户去打开
// ignore: constant_identifier_names
const String STORAGE_KEY_CALENDAR_REFUSED = 'storage_key_calendar_refused';

class LyPermission {
  static bool isDialogShowing = false;

  /// 录音权限
  static Future<bool> record() async {
    if (GlobalConfigZyoCore.androidVersion >= 33) {
      var p = await handle(Permission.photos);
      var v = await handle(Permission.videos);
      var a = await handle(Permission.audio);
      var m = await microphone();
      Logger.d("[record] => ${p && v}");
      return m && p && v && a;
    } else {
      var s = await storage();
      var m = await microphone();
      return s && m;
    }
  }

  /// 麦克风权限
  static Future<bool> microphone() async {
    return await handle(Permission.microphone);
  }

  /// 存储权限
  static Future<bool> storage({RequestType type = RequestType.all}) async {
    if (GlobalConfigZyoCore.androidVersion >= 33) {
      var p = true;
      var v = true;
      var a = true;
      if (type.containsImage()) {
        p = await handle(Permission.photos);
      }
      if (type.containsVideo()) {
        v = await handle(Permission.videos);
      }
      if (type.containsAudio()) {
        a = await handle(Permission.audio);
      }
      return p && v && a;
    } else {
      if (Platform.isIOS && type.containsImage()) {
        return await handle(Permission.photos);
      }
      return await handle(Permission.storage);
    }
  }

  /// 相册权限/保存图片
  static Future<bool> photoVideos() async {
    Logger.d('GlobalConfig.androidVersion = ${GlobalConfigZyoCore.androidVersion}');
    return await storage(type: RequestType.image | RequestType.video);
  }

  static Future<bool> videos() async {
    Logger.d('GlobalConfig.androidVersion = ${GlobalConfigZyoCore.androidVersion}');
    return await storage(type: RequestType.video);
  }

  static Future<bool> photos() async {
    Logger.d('GlobalConfig.androidVersion = ${GlobalConfigZyoCore.androidVersion}');
    return await storage(type: RequestType.image);
  }

  /// 相机权限
  static Future<bool> cameras() async {
    return await handle(Permission.camera);
  }

  /// 相机权限
  static Future<bool> video() async {
    var m = await handle(Permission.microphone);
    var c = await handle(Permission.camera);
    return m && c;
  }

  /// 日历
  static Future<bool> calendar() async {
    return await handle(Permission.calendar);
  }

  /// 通知
  static Future<bool> notification() async {
    return await handle(Permission.notification);
  }

  /// 电话
  static Future<bool> phone() async {
    return await handle(Permission.phone);
  }

  /// 蓝牙权限
  static Future<bool> blueTooth() async {
    final bAuth = await handle(Permission.bluetooth);
    late bool bAuthConnect;
    late bool bScan;
    bool location = true;
    if (Platform.isIOS) {
      bAuthConnect = true;
      bScan = true;
    } else {
      bAuthConnect = await handle(Permission.bluetoothConnect);
      bScan = await handle(Permission.bluetoothScan);

      /// < android 12 需要开启位置权限
      if (GlobalConfigZyoCore.androidVersion < 31) {
        location = await handle(Permission.location);
      }
    }
    return bAuth && bAuthConnect && bScan && location;
  }

  static Future<bool> handle(Permission permission) async {
    // Logger.d("[request permission] => ${permission.value} : ${await permission.status}");
    /// 部分android机型，Permission.photos 申请失败
    if (Platform.isAndroid && permission == Permission.photos) {
      if (GlobalConfigZyoCore.androidVersion <= 32) {
        return true;
      }
    }
    String lySpKey = permission.value.toString();

    if (!(await permission.isGranted)) {
      /// 如果没有权限
      // Logger.d("[isPermanentlyDenied] => ${await permission.isPermanentlyDenied}"); //这个没用
      // Logger.d("[shouldShowRequestRationale] => ${await permission.shouldShowRequestRationale}");
      if (await permission.isPermanentlyDenied) {
        /// 如果已经永久禁止了,获取受限
        error(permission);
        return false;
      } else {
        String lySpPermV = lySp.getString(key: lySpKey);
        if (!TextUtil.isEmpty(lySpPermV)) {
          error(permission);
          return false;
        }
        if (!Platform.isIOS) {
          showPermissionDesc(permission);
        }

        /// 没禁止开始申请
        var pr = await permission.request();
        lySp.setString(key: lySpKey, value: "1");
        Logger.d("[requested permission pr=]${permission.value} : => $pr");
        if (!Platform.isIOS) {
          LYPickerUtil().closeOverlayEntry();
        }
        switch (pr) {
          case PermissionStatus.granted:

            /// permission.value == 7 为麦克风
            if (permission.value == 7) {
              // ToastUtil.showToast("已获取权限请尝试重新开始！");
            }
            return true;
          case PermissionStatus.denied:
            // ToastUtil.showToast("权限不足！");
            /// 只有拒绝且不再询问时才去app设置打开权限
            // error(permission);
            return false;
          case PermissionStatus.permanentlyDenied:
            if (Platform.isAndroid) {
              error(permission);
            }
            return false;
          case PermissionStatus.limited:

            /// iOS 允许部分权限，允许用户继续操作
            return true;
          default:
            return false;
        }
      }
    }
    return true;
  }

  static showPermissionDesc(Permission permission) {
    String title = '应用权限';
    String message = '应用权限';
    switch (permission.value) {
      case 0:

        /// 日历
        title = "日历";
        message = "用于APP在用户日历上显示对应的任务";
        break;
      case 1:

        /// 相机
        title = "相机";
        message = "用于APP拍摄照片/视频进行发布";
        break;
      case 3:

        /// 相机
        title = "位置";
        message = "用于APP获取用户位置信息";
        break;
      case 7:

        /// 麦克风
        title = "麦克风";
        message = "用于APP录音进行发布";
        break;
      case 8:

        /// 电话
        title = "电话";
        message = "用于APP拨打电话联系用户";
        break;
      case 9:

        /// 相册
        title = "相册";
        if (GlobalConfigZyoCore.androidVersion >= 33) {
          message = "用于APP提供用户选择照片文件";
        } else {
          message = "用于APP提供用户选择照片/视频/音频等文件";
        }
        break;
      case 15:

        /// 存储
        title = "存储";
        message = "用于APP提供读取和存储照片/视频/音频等文件";
        break;
      case 17:

        /// 通知
        title = "通知";
        message = "用于APP通知用户相对应的功能信息";
        break;
      case 21:

        /// 蓝牙
        title = "蓝牙";
        message = "用于APP进行蓝牙搜索、连接设备进行通信";
        break;
      case 28:

        /// 蓝牙搜索
        title = "蓝牙搜索";
        message = "用于APP进行蓝牙搜索功能";
        break;
      case 30:

        /// 蓝牙连接
        title = "蓝牙连接";
        message = "用于APP进行蓝牙连接设备功能";
        break;
      case 32:

        /// video
        title = "视频";
        message = "用于APP选择视频功能";
        break;
      case 33:

        /// audio
        title = "音频";
        message = "用于APP选择音频功能";
        break;
    }

    LYPickerUtil().showTopSheet(
      Get.context!,
      title: "$title权限说明:",
      content: message,
      callBack: (v) {},
    );
  }

  static error(Permission permission) {
    String message = '应用权限';
    switch (permission.value) {
      case 0:

        /// 日历
        message = "日历";
        break;
      case 1:

        /// 相机
        message = "相机";
        break;
      case 3:

        /// 相机
        message = "位置";
        break;

      case 7:

        /// 麦克风
        message = "麦克风";
        break;
      case 8:

        /// 电话
        message = "电话";
        break;

      case 9:

        /// 相册
        message = "照片";
        break;
      case 15:

        /// 存储
        message = "存储";
        break;
      case 17:

        /// 通知
        message = "通知";
        break;
      case 21:

        /// 蓝牙
        message = "蓝牙";
        break;
      case 28:

        /// 蓝牙搜索
        message = "蓝牙搜索、连接设备";
        break;
      case 30:

        /// 蓝牙连接
        message = "蓝牙搜索、连接设备";
        break;
    }
    // ToastUtil.showToast(message);
    showSettingDialog(message);
  }

  static showSettingDialog(String message) {
    if (!LyPermission.isDialogShowing) {
      LyPermission.isDialogShowing = true;
      LYDialogUtil.showConfirmation(
        Get.context!,
        content: '$message权限被拒绝，请去设置中设置权限',
        confirmBtn: LYText(
          fontSize: 17.sp,
          text: "去设置",
          color: LibColor.colorMain,
          fontFamily: AppFontFamily.pingFangMedium,
        ),
        routeName: 'lyPermissionSetting.dialog',
        callback: () async {
          PhotoManager.openSetting();
          LyPermission.isDialogShowing = false;
        },
        closeCallBack: () async {
          LyPermission.isDialogShowing = false;
          Get.back();
          if (message == '日历') {
            lySp.setBool(key: STORAGE_KEY_CALENDAR_REFUSED, value: true);
          }
        },
      );
    }
  }
}
