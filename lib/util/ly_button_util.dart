import 'package:flutter/material.dart';
import 'package:flutter_screenutil/flutter_screenutil.dart';
import 'package:zyocore/core/enum/enum_icon_color.dart';
import 'package:zyocore/values/color.dart';
import 'package:zyocore/values/style.dart';
import 'package:zyocore/widget/button.dart';
import 'package:zyocore/widget/common/button/big_button_widget.dart';
import 'package:zyocore/widget/common/button/gradient_button_widget.dart';
import 'package:zyocore/widget/common/button/small_button_widget.dart';
import 'package:zyocore/widget/index.dart';
import 'package:zyocore/widget/ly_text_button.dart';

class LYButtonUtil {
  static Widget fullScreenButton({
    required String text,
    Widget? iconWidget,
    IconData? iconData,
    int? num,
    double? height,
    Color? bgColor,
    Color? borderColor,
    TextStyle? fontStyle,
    VoidCallback? onTap,
    BorderRadius? radius,
    bool? enable,
    EdgeInsetsGeometry? margin,
    EdgeInsetsGeometry? padding,
    double? opacity,
  }) {
    return BigButtonWidget(
      text: text,
      iconWidget: iconWidget,
      iconData: iconData,
      num: num,
      height: height,
      bgColor: bgColor,
      borderColor: borderColor,
      btnStyle: fontStyle,
      onTap: onTap,
      radius: radius,
      enable: enable ?? true,
      padding: padding,
      margin: margin,
      opacity: opacity,
    );
  }

  static Widget gradientButton({
    required String text,
    bool? enable,
    Color? textColor,
    double? fontSize,
    Gradient? gradient,
    double? radius,
    double? width,
    double? height,
    EdgeInsetsGeometry? margin,
    EdgeInsetsGeometry? padding,
    VoidCallback? onTap,
  }) {
    return GradientButtonWidget(
      text: text,
      enable: enable ?? true,
      textColor: textColor ?? lightFFFFFF,
      fontSize: fontSize ?? 14.sp,
      gradient: gradient ?? gradientSilver,
      radius: radius ?? 5.r,
      width: width,
      height: height,
      onTap: onTap,
      margin: margin,
      padding: padding ?? EdgeInsets.fromLTRB(16.w, 7.w, 16.w, 7.w),
    );
  }

  static Widget generalButton({
    String? text,
    Color? textColor,
    Widget? textWidget,
    Widget? icon,
    int? num,
    Color? bgColor,
    Color? borderColor,
    bool? disable,
    double? height,
    double? width,
    double? fontSize,
    EdgeInsetsGeometry? padding,
    EdgeInsetsGeometry? margin,
    VoidCallback? onTap,
    double? radius,
    String? fontFamily,
    BorderRadiusGeometry? borderRadius,
    TextStyle? textStyle,
    BoxBorder? border,
    Gradient? gradient,
    List<BoxShadow>? boxShadow,
    AlignmentGeometry? alignment,
    Matrix4? matrix,
  }) {
    return SmallButtonWidget(
      text: text,
      textColor: textColor ?? lightFFFFFF,
      textWidget: textWidget,
      icon: icon,
      num: num,
      bgColor: bgColor,
      disable: disable ?? false,
      height: height,
      width: width,
      fontSize: fontSize ?? 14.sp,
      padding: padding,
      margin: margin,
      onTap: onTap,
      radius: radius,
      fontFamily: fontFamily,
      borderColor: borderColor,
      borderRadius: borderRadius,
      textStyle: textStyle,
      border: border,
      gradient: gradient,
      boxShadow: boxShadow,
      alignment: alignment,
      matrix: matrix,
    );
  }

  static Widget textButton({
    String? text,
    String? image,
    VoidCallback? onTap,
    Color color = const Color(0xFF1A1A1A),
    double? fontSize,
    double? width,
    double? height,
    FontWeight? fontWeight,
    double? space,
    bool rtl = false,
    bool enable = true,
    String? package,
    TextStyle? fontStyle,
    Widget? icon,
    double? overAllheight,
  }) {
    return ZUTextButton(
      text: text,
      image: image,
      onPressed: onTap,
      color: color,
      fontSize: fontSize ?? 14.sp,
      width: width ?? 24.w,
      height: height ?? 24.w,
      fontWeight: fontWeight,
      space: space,
      rtl: rtl,
      enable: enable,
      package: package,
      fontStyle: fontStyle,
      icon: icon,
      overAllheight: overAllheight,
    );
  }

  static Widget imgTextVerticalButton({
    required String text,
    required String icon,
    double? height,
    double? width,
    double? iconWidth,
    double? iconHeight,
    String? package,
    String? rightSub,
    double? rightWidth,
    double? rightHeight,
    double? fontSize,
    Color? fontColor,
    VoidCallback? onTap,
    double? imgBottomSpacing,
    String? fontFamily,
    Color? iconColor,
  }) {
    return InkWell(
      onTap: onTap,
      child: Container(
        width: width ?? 75.w,
        height: height ?? 75.w,
        child: Column(
          mainAxisAlignment: MainAxisAlignment.center,
          children: [
            Stack(
              clipBehavior: Clip.none,
              children: [
                LYAssetsImage(
                  width: iconWidth ?? 32.w,
                  height: iconHeight ?? 32.w,
                  package: package,
                  color: iconColor,
                  name: 'assets/images/$icon',
                ),
                if (rightSub != null)
                  Positioned(
                    top: -8.w,
                    left: 16.w,
                    child: LYAssetsImage(
                      name: 'assets/images/$rightSub',
                      package: package,
                      width: rightWidth ?? 28.w,
                      height: rightHeight ?? 15.w,
                    ),
                  ),
              ],
            ),
            Padding(
              padding: EdgeInsets.only(top: imgBottomSpacing ?? 3.w),
              child: LYText(
                text: text ?? '',
                fontSize: fontSize ?? 13.sp,
                color: fontColor ?? dark191919,
                fontFamily: fontFamily ?? AppFontFamily.pingFangRegular,
              ),
            ),
          ],
        ),
      ),
    );
  }

  static Widget iconButton({
    String? icon,
    String? iconUrl,
    double? width,
    double? height,
    VoidCallback? onTap,
    String? package,
    EdgeInsetsGeometry? padding,
    EdgeInsetsGeometry? margin,
    List<BoxShadow>? boxShadow,
  }) {
    return InkWell(
      child: Container(
        width: width ?? 16.w,
        height: height ?? 16.w,
        decoration: BoxDecoration(boxShadow: boxShadow),
        padding: padding,
        margin: margin,
        child: LYImageWidget(
          src: iconUrl ?? 'assets/images/$icon.png',
          height: width ?? 16.w,
          width: height ?? 16.w,
          package: package,
        ),
      ),
      onTap: onTap,
    );
  }

  static Widget shadowButton({
    bool enable = true,
    required String text,
    VoidCallback? onTap,
    double? width,
    double? height,
    double? radius,
    FontWeight fontWeight = FontWeight.normal,
    double? fontSize,
    Color textColor = Colors.white,
    String? fontFamily,
    Color highlightColor = const Color(0xFF9F89FF),
    Color color = const Color(0xFFC5B8FF),
    EdgeInsetsGeometry? padding,
    List<BoxShadow>? boxShadow = const [
      BoxShadow(
        color: Color(0x66C59AF3),
        offset: Offset(0, 5),
        blurRadius: 18,
      )
    ],
  }) {
    return LYTextButton(
      text: text,
      width: width,
      height: height ?? 44.w,
      fontSize: (fontSize ?? 15).sp,
      color: color,
      highlightColor: highlightColor,
      fontWeight: fontWeight,
      textColor: textColor,
      fontFamily: fontFamily,
      radius: radius ?? 22.r,
      onTap: onTap,
      enable: enable,
      boxShadow: boxShadow,
      padding: padding,
    );
  }

  static Widget iconShadowButton({
    bool enable = true,
    required String text,
    required Widget icon,
    required VoidCallback onTap,
    double? width = double.infinity,
    double? height,
    double? radius,
    FontWeight fontWeight = FontWeight.normal,
    double? fontSize,
    Color textColor = Colors.white,
    String? fontFamily,
    Color highlightColor = const Color(0xFF9F89FF),
    Color color = const Color(0xFFC5B8FF),
    List<BoxShadow> boxShadow = const [
      BoxShadow(
        color: Color(0x66C59AF3),
        offset: Offset(0, 5),
        blurRadius: 18.0,
      )
    ],
  }) {
    return LYIconTextButton(
      icon: icon,
      text: text,
      width: width ?? double.infinity,
      height: height ?? 44.w,
      fontSize: fontSize ?? 15.sp,
      color: color,
      highlightColor: highlightColor,
      fontWeight: fontWeight,
      textColor: textColor,
      fontFamily: fontFamily,
      radius: radius ?? 22.r,
      onTap: onTap,
      enable: enable,
      boxShadow: boxShadow,
    );
  }

  static Widget backButton({
    EnumIconColor? enumIconColor = EnumIconColor.black,
    VoidCallback? backTap, //自定义返回事件
    EdgeInsetsGeometry? padding,
    double? left,
  }) {
    return zyoBackButton(
      enumIconColor: enumIconColor,
      backTap: backTap,
      padding: padding,
      left: left ?? 16,
    );
  }

  static Widget customButton(Widget btn, {
    GestureTapCallback? onTap,
  }) {
    return InkWell(
      onTap: onTap,
      child: btn,
    );
  }

  static Widget enableFSButton({
    required String text,
    bool enable = true,
    double? width = 343,
    double? height = 44,
    double? radius = 22,
    Color textColor = Colors.white,
    Color highlightColor = const Color(0xFF9F89FF),
    Color color = const Color(0xFFC5B8FF),
    double fontSize = 15,
    String? fontFamily,
    VoidCallback? onTap,
    FontWeight? fontWeight,
  }) {
    return SizedBox(
      width: width,
      height: height,
      child: fullScreenButton(
        text: text,
        height: height,
        enable: enable,
        bgColor: enable ? Color(0xFF9F89FF) : Color(0xFFC5B8FF),
        radius: BorderRadius.circular(22.w),
        fontStyle: TextStyle(fontFamily: fontFamily ?? AppFontFamily.pingFangMedium, fontSize: fontSize, color: textColor, fontWeight: fontWeight),
        borderColor: enable ? Color(0xFF9F89FF) : Color(0xFFC5B8FF),
        opacity: 1,
        onTap: onTap,
      ),
    );
  }
}
