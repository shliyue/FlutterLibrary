import 'dart:io';
import 'dart:ui' as ui;
import 'dart:math' as math;

import 'package:device_info_plus/device_info_plus.dart';
import 'package:flutter_screenutil/flutter_screenutil.dart';
import 'package:zyocore/util/native/ly_native_util.dart';
import 'package:zyocore/util/utils.dart';

import 'logger.dart';

class Device {
  static double devicePixelRatio = ui.window.devicePixelRatio;
  static ui.Size size = ui.window.physicalSize;
  static double width = size.width;
  static double height = size.height;
  static double screenWidth = width / devicePixelRatio;
  static double screenHeight = height / devicePixelRatio;
  static ui.Size screenSize = ui.Size(screenWidth, screenHeight);
  final bool isTablet, isPhone, isIos, isAndroid, isIphoneX, hasNotch;
  static Device? _device;
  static Function? onMetricsChange;

  ///是否是ipad
  static bool isIpad = false;

  Device({required this.isTablet, required this.isPhone, required this.isIos, required this.isAndroid, required this.isIphoneX, required this.hasNotch});

  factory Device.get() {
    // Logger.d('DeviceType devicePixelRatio=$devicePixelRatio');
    // Logger.d('DeviceType left=${ui.window.viewPadding.left}');
    // Logger.d('DeviceType right=${ui.window.viewPadding.right}');
    // Logger.d('DeviceType top=${ui.window.viewPadding.top}');
    // Logger.d('DeviceType bottom=${ui.window.viewPadding.bottom}');
    if (_device != null) return _device!;

    if (onMetricsChange == null) {
      onMetricsChange = ui.window.onMetricsChanged;
      ui.window.onMetricsChanged = () {
        _device = null;

        size = ui.window.physicalSize;
        width = size.width;
        height = size.height;
        screenWidth = width / devicePixelRatio;
        screenHeight = height / devicePixelRatio;
        screenSize = ui.Size(screenWidth, screenHeight);

        onMetricsChange!();
      };
    }

    bool isTablet = DeviceUtil.isTablet;
    bool isPhone = DeviceUtil.isPhone;
    bool isIos = Platform.isIOS;
    bool isAndroid = Platform.isAndroid;
    bool isIphoneX = false;
    bool hasNotch = false;

    // Recalculate for Android Tablet using device inches

    if (isIos &&
        isPhone &&
        (screenHeight == 812 ||
            screenWidth == 812 ||
            screenHeight == 896 ||
            screenWidth == 896 ||
            // iPhone 12 pro
            screenHeight == 844 ||
            screenWidth == 844 ||
            // Iphone 12 pro max
            screenHeight == 926 ||
            screenWidth == 926)) {
      isIphoneX = true;
      hasNotch = true;
    }

    if (_hasTopOrBottomPadding()) hasNotch = true;

    return _device = Device(isTablet: isTablet, isPhone: isPhone, isAndroid: isAndroid, isIos: isIos, isIphoneX: isIphoneX, hasNotch: hasNotch);
  }

  static double _calWidth() {
    if (width > height) {
      return (width + (ui.window.viewPadding.left + ui.window.viewPadding.right) * width / height);
    }
    return (width + ui.window.viewPadding.left + ui.window.viewPadding.right);
  }

  static double _calHeight() {
    return (height + (ui.window.viewPadding.top + ui.window.viewPadding.bottom));
  }

  static int get _ppi => Platform.isAndroid
      ? 160
      : Platform.isIOS
          ? 150
          : 96;

  static bool _hasTopOrBottomPadding() {
    final padding = ui.window.viewPadding;
    //Logger.d(padding);
    return padding.top > 0 || padding.bottom > 0;
  }
}

class DeviceUtil {
  /// 是否是ipad（通过设备信息来判断）
  static bool isIpad = false;

  /// 是否是tablet（pad 通过宽高方式判断）
  static bool isTablet = false;

  /// 是否是手机
  static bool isPhone = true;

  /// 是否是ios
  static bool isIos = Device.get().isIos;

  /// 是否是Android
  static bool isAndroid = Device.get().isAndroid;

  /// 是否是iphoneX
  static bool isIphoneX = Device.get().isIphoneX;

  /// 是否由缺口（通过pading top or bottom >0 来判断，不是很准确）
  static bool hasNotch = Device.get().hasNotch;

  static init() async{
    await checkIpad();
  }

  static checkIpad() async {
    DeviceInfoPlugin deviceInfo = DeviceInfoPlugin();
    if (Platform.isAndroid == false) {
      IosDeviceInfo info = await deviceInfo.iosInfo;
      /// iPad
      Logger.d('======a=======a=======${info.model}');
      /// ipad13,1
      Logger.d('======a=======a=======${info.utsname.machine.toLowerCase()}');
      /// iPadOS
      Logger.d('======a=======a=======${info.systemName}');

      /// true
      isIpad = info.utsname.machine.toLowerCase().contains("ipad");
      isTablet = isIpad;
      isPhone = !isIpad;
      Logger.d('======a=======a=======$isIpad');
    } else {
      var flag = await LyNativeUtil().checkIsPad();
      if (flag) {
        isTablet = true;
        isPhone = false;
      } else {
        isTablet = false;
        isPhone = true;
      }
    }
  }

  /// 计算屏幕宽高比
  static double calculateAspectRatio() {
    final width = utils.style.screenWidth;
    final height = utils.style.screenHeight;

    return width > height ? width / (height - ScreenUtil().statusBarHeight) : height / width;
  }

  static Future<String> getSuffixPhone() async {
    if (Platform.isIOS) {
      DeviceInfoPlugin deviceInfo = DeviceInfoPlugin();
      IosDeviceInfo iosInfo = await deviceInfo.iosInfo;
      List<String> arrVersion = iosInfo.systemVersion.split(".");
      if (arrVersion.isNotEmpty) {
        if (int.parse(arrVersion[0]) >= 18) {
          return "?/q/90";
        }
      }
    }
    return "";
  }
}
