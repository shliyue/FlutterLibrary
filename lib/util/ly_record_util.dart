import 'dart:async';
import 'dart:io';

import 'package:audio_session/audio_session.dart';
import 'package:flutter_sound/flutter_sound.dart';
import 'package:intl/date_symbol_data_local.dart';
import 'package:flutter_sound_platform_interface/flutter_sound_recorder_platform_interface.dart';
import 'package:intl/intl.dart';

import '../entitys/ly_audio_file.dart';
import '../zyocore.dart';

class LyRecordUtil {
  static final LyRecordUtil instance = LyRecordUtil._internal();

  LyRecordUtil._internal();

  static Function(LYAudioFile)? recordCallBack;
  static Function(Duration)? onRecordProgress;

  // 录音器
  static FlutterSoundRecorder? recorderModule;

  // 订阅录制
  static StreamSubscription? _recorderSubscription;

  // 音频格式
  final Codec _codec = Codec.aacADTS;
  static String recordPath = "";

  // 最大时长3秒钟
  int _maxSeconds = 3;
  Duration curDuration = Duration.zero;

  /// 0 ready  1 recording  2 paused  3 playing
  var recorderState = 0.obs;

  _openTheRecorder() async {
    await recorderModule?.openRecorder();
  }

  /// 初始化
  Future<bool> _initRecorder() async {
    recorderModule = FlutterSoundRecorder();
    await _openTheRecorder();
    recorderModule?.setSubscriptionDuration(const Duration(milliseconds: 100));
    await initializeDateFormatting();

    final session = await AudioSession.instance;
    await session.configure(AudioSessionConfiguration(
      avAudioSessionCategory: AVAudioSessionCategory.playAndRecord,
      avAudioSessionCategoryOptions: AVAudioSessionCategoryOptions.allowBluetooth | AVAudioSessionCategoryOptions.defaultToSpeaker,
      avAudioSessionMode: AVAudioSessionMode.spokenAudio,
      avAudioSessionRouteSharingPolicy: AVAudioSessionRouteSharingPolicy.defaultPolicy,
      avAudioSessionSetActiveOptions: AVAudioSessionSetActiveOptions.none,
      androidAudioAttributes: const AndroidAudioAttributes(
        contentType: AndroidAudioContentType.speech,
        flags: AndroidAudioFlags.none,
        usage: AndroidAudioUsage.voiceCommunication,
      ),
      androidAudioFocusGainType: AndroidAudioFocusGainType.gain,
      androidWillPauseWhenDucked: true,
    ));

    return true;
  }

  /// 开始录制
  /// maxSeconds 最大秒数
  void startRecorder({int maxSeconds = 5, String? filePrefix, Function(Duration)? onProgress, Function(LYAudioFile)? onFinish}) async {
    var status = await LyPermission.microphone();
    if (status) {
      curDuration = Duration.zero;
      _maxSeconds = maxSeconds;
      recordCallBack = onFinish;
      onRecordProgress = onProgress;
      await _initRecorder();
      var tempDir = await getTemporaryDirectory();
      Directory folder = Directory("${tempDir.path}/record");
      if (!folder.existsSync()) {
        folder.createSync();
      }
      recordPath = '${folder.path}/${(filePrefix ?? "")}flutter_sound${ext[_codec.index]}';
      await _deleteTempFile();
      try {
        await recorderModule?.startRecorder(
          toFile: recordPath,
          codec: _codec,
          audioSource: AudioSource.microphone,
        );
        recorderState.value = 1;
        _addRecorderSubscription();
      } on Exception catch (err) {
        recorderState.value = 0;
        _cancelRecorderSubscriptions();
      }
    }
  }

  /// 暂停录制
  void stopRecorder({bool isCallBack = true}) async {
    try {
      await recorderModule?.stopRecorder();
      if (isCallBack) {
        var date = DateTime.fromMillisecondsSinceEpoch(curDuration.inMilliseconds, isUtc: true);
        var txt = DateFormat('mm:ss', 'en_GB').format(date);
        String recorderTxt = txt.substring(0, 5) + '.00';
        File audio = File(recordPath);
        LYAudioFile audioFile = LYAudioFile(path: recordPath, file: audio, duration: curDuration, durationStr: recorderTxt);
        if (recordCallBack != null) {
          recordCallBack!(audioFile);
        }
      }
      _cancelRecorderSubscriptions();
    } on Exception catch (err) {
      recorderState.value = 1;
    }
    recorderState.value = 2;
  }

  /// 删除临时文件
  Future _deleteTempFile() async {
    File audio = File(recordPath);
    if (audio.existsSync()) {
      await audio.delete();
    }
  }

  /// 清空录音文件夹
  Future clearTempFolder() async {
    var tempDir = await getTemporaryDirectory();
    Directory folder = Directory("${tempDir.path}/record");
    if (folder.existsSync()) {
      await folder.delete();
    }
  }

  /// 添加录音订阅
  void _addRecorderSubscription() {
    _recorderSubscription = recorderModule?.onProgress!.listen(
      (e) {
        curDuration = e.duration;
        if (onRecordProgress != null) {
          onRecordProgress!(curDuration);
        }
        // var txt = DateFormat('mm:ss.SS', 'en_GB').format(date);
        // state.recorderTxt.value = txt.substring(0, 8);
        if (curDuration.inMilliseconds >= _maxSeconds * 1000) {
          stopRecorder();
          return;
        }
      },
    );
  }

  /// 取消录制监听
  void _cancelRecorderSubscriptions() {
    if (_recorderSubscription != null) {
      _recorderSubscription!.cancel();
      _recorderSubscription = null;
    }
  }
}
