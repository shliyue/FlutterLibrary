import 'package:navigation_history_observer/navigation_history_observer.dart';

import 'logger.dart';

class RouteUtil {
  /// 按照页面之间排序传入，/页面算最前
  static bool contains(List<String> pageNames) {
    assert(pageNames.isNotEmpty);
    final history = NavigationHistoryObserver().history;
    bool isOk = true;
    for (var arg in pageNames) {
      final index = history.indexWhere((p0) {
        return p0.settings.name == arg;
      });
      if (index < 0) {
        isOk = false;
        break;
      }
    }
    return isOk;
  }

  /// 按照页面之间排序传入，/页面算最前
  static bool equals(List<String> pageNames) {
    assert(pageNames.isNotEmpty);
    final history = NavigationHistoryObserver().history;
    String fullHistory = '';
    for (var item in history) {
      fullHistory += (item.settings.name ?? '');
    }
    String fullPageNames = '';
    for (var item in pageNames) {
      fullPageNames += item;
    }
    bool isOk = true;
    if (fullHistory == fullPageNames) {
      return true;
    }
    return isOk;
  }

  static void logPages() {
    final history = NavigationHistoryObserver().history;
    final ite = history.iterator;
    while (ite.moveNext()) {
      Logger.d('MT-- 路由 --> ' + (ite.current.settings.name ?? ' null '));
    }
  }
}
