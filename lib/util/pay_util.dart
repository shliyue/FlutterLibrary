import 'package:weixin_kit/weixin_kit.dart';
import 'package:zyocore/util/toast_util.dart';

import '../entitys/mch_app_pay_info.dart';
import 'app_utils.dart';

class PayUtil {
  ///微信app支付
  Future<void> wxAppPay(MchAppPayInfo payInfo) async {
    bool? isInstalled = await WeixinKit.instance.isInstalled();
    if (isInstalled != null && !isInstalled) {
      ToastUtil.showToast('未安装微信');
      return;
    }
    return WeixinKit.instance.pay(
      appId: payInfo.appId,
      partnerId: payInfo.partnerId,
      prepayId: payInfo.prepayId,
      package: payInfo.package,
      nonceStr: payInfo.nonceStr,
      timeStamp: payInfo.timeStamp,
      sign: payInfo.sign,
    );
  }

  ///支付宝支付
  Future<void> aliUrlPay(String payUrl) async {
    String path = "alipays://platformapi/startapp?appId=20000067&url=$payUrl";
    AppUtils.onLaunchApp(path).then((rs) {
      if (!rs) {
        ToastUtil.showToast('未安装支付宝');
      }
    });
  }
}
