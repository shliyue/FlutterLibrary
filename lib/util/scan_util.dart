import 'package:flutter/material.dart';
import 'package:majascan/majascan.dart';
import 'package:zyocore/values/color.dart';
import 'package:zyocore/widget/ly_scan_view.dart';

import '../zyocore.dart';
import 'logger.dart';
import 'ly_permission.dart';

const PREFIX_CODE_YBT = 'ybt://courseKey';

enum EnumQRType {
  ///未知
  NONE,

  ///一体机
  YBT_COURSE,

  ///学习机身份卡
  XXJ_IDCARD,

  ///创思机身份卡
  CSJ_IDCARD,
}

class ScanQrResultModel {
  EnumQRType? qrType;
  String? qrContent;
  String? rs;

  ScanQrResultModel({this.qrType, this.rs, this.qrContent});
}

/// 扫描二维码相关处理
class ScanUtil {
  static Future<String?> openScanQr() async {
    bool p = await LyPermission.cameras();
    if (!p) return null;
    var rs = await LYDialogUtil.showWidget(const LyScanView());
    return rs;
  }

  /// 打开扫描页面，只能定制部分UI
  static Future<String?> openScanPage({Color? scanLineColor}) async {
    bool p = await LyPermission.cameras();
    if (!p) return null;
    try {
      String? qrResult = await MajaScan.startScan(
          title: '',
          barColor: Colors.transparent,
          titleColor: lightFFFFFF,
          qRCornerColor: lightFFFFFF,
          qRScannerColor: scanLineColor ?? theme9F89FF,
          flashlightEnable: false,

          /// value 0.0 to 1.0
          scanAreaScale: 0.7);
      return qrResult;
    } catch (e) {
      Logger.d('ScanUtil openScanPage e=$e}');
      return null;
    }
  }

  ///扫描二维码
  static Future<ScanQrResultModel?> onScanQR({List<EnumQRType>? type}) async {
    // https://applinks.zyosoft.cn/qr/wx-coding?s=0&id=1498762630
    ScanQrResultModel rsModel = ScanQrResultModel(rs: "", qrContent: "", qrType: EnumQRType.NONE);
    var rs = (await openScanQr()) ?? "";
    if (TextUtil.isEmpty(rs)) {
      // ToastUtil.showToast('不支持的二维码类型');
      return null;
    } else {
      rsModel.qrContent = rs;
      if (isValidCode(codeStr: rs)) {
        rsModel.qrType = EnumQRType.YBT_COURSE;
        rsModel.rs = rs;
      } else {
        var arr = rs.split("?");
        if (arr.length < 2) {
          ToastUtil.showToast('不支持的二维码类型');
          return null;
        }
        var arrParma = arr[1].split("&");
        //判断扫描的是不是身份卡
        if (arrParma.contains("s=0")) {
          rsModel.qrType = EnumQRType.XXJ_IDCARD;
        } else if (arrParma.contains("s=1")) {
          rsModel.qrType = EnumQRType.CSJ_IDCARD;
        } else {
          rsModel.qrType = EnumQRType.NONE;
        }
        for (String str in arrParma) {
          var arrRs = str.split("=");
          if (arrRs.length == 2) {
            //获取身份卡号
            if (arrRs[0] == "id") {
              rsModel.rs = arrRs[1];
            }
          }
        }
      }
      if (type != null && !type.contains(rsModel.qrType)) {
        ToastUtil.showToast('不支持的二维码类型');
        return null;
      }
    }

    return rsModel;
  }

  /// 是否是有效的园宝通一体机二维码
  static bool isValidCode({String codeStr = ''}) {
    return codeStr.startsWith(PREFIX_CODE_YBT);
  }
}
