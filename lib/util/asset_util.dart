import 'dart:async';
import 'dart:io';
import 'package:flutter/material.dart';
import 'package:zyocore/widget/assets_picker/entity/group_entity.dart';
import '../core/configs/config.dart';
import '../widget/assets_picker/constants/enums.dart';
import '../widget/assets_picker/entity/ly_asset_entity.dart';
import '../widget/assets_picker/ly_assets_picker.dart';
import '../widget/camera_picker/widgets/camera_picker.dart';
import '../zyocore.dart';

class AssetUtil {
  static Future<void> onSelectAssets({
    required BuildContext context,
    bool isOnlyPhoto = false,
    bool isClip = false,
    List<String>? assetActions,
    Function? onCallBack,
    bool compress = false,
    int? maxWidth,
  }) async {
    File? file;
    if (isOnlyPhoto) {
      file = await onAssetsPhoto(context, isOnlyPhoto);
      onCallBackResult(isClip: isClip, isOnlyPhoto: isOnlyPhoto, imageFile: file, onCallBack: onCallBack, compress: compress, maxWidth: maxWidth);
    } else {
      LYSheetUtil.showListSheet(context, data: assetActions ?? ["拍摄", "手机相册"], callback: (value) async {
        if (value == 0) {
          var p = await LyPermission.cameras();
          if (!p) return;
          var p2 = await LyPermission.photos();
          if (!p2) return;
          AssetEntity? assetEntity = await onCameraPick(
            context,
            theme: LyAssetsPicker.themeCameraData(LibColor.colorMain),
            resolutionPreset: ResolutionPreset.medium,
            imageFormatGroup: ImageFormatGroup.jpeg,
          );
          if (assetEntity == null) return null;
          file = await assetEntity.file;
        } else {
          file = await onAssetsPhoto(context, isOnlyPhoto);
        }
        onCallBackResult(isClip: isClip, isOnlyPhoto: isOnlyPhoto, imageFile: file, onCallBack: onCallBack, compress: compress, maxWidth: maxWidth);
      });
    }
  }

  static Future<void> onCallBackResult({
    bool isClip = false,
    File? imageFile,
    bool isOnlyPhoto = false,
    Function? onCallBack,
    bool compress = false,
    int? maxWidth,
  }) async {
    List<String>? uploadImages;
    if (isClip) {
      uploadImages = await clipImage(imageFile: imageFile, isOnlyPhoto: isOnlyPhoto, compress: compress, maxWidth: maxWidth);
      if (uploadImages.isNotEmpty) {
        if (onCallBack != null) {
          onCallBack(uploadImages.first);
        }
      }
    } else {
      if (onCallBack != null) {
        onCallBack(imageFile);
      }
    }
  }

  static Future<File?> onAssetsPhoto(BuildContext context, bool isOnlyPhoto) async {
    // var p = await LyPermission.photos();
    // if (!p) return null;
    List<LyAssetEntity> assets = await onAssetsPick(
      context: context,
      specialItemPosition: isOnlyPhoto ? SpecialItemPosition.none : SpecialItemPosition.prepend,
    );
    if (assets.isEmpty) {
      return null;
    }
    return await assets.first.file;
  }

  /// 剪裁图片
  static Future<List<String>> clipImage({
    File? imageFile,
    bool isOnlyPhoto = false,
    bool compress = false,
    int? maxWidth,
  }) async {
    List<String> urls = [];
    try {
      if (imageFile != null) {
        var uploadedImage = await Get.toNamed(Config.imageClipRoute, arguments: {
          'file': imageFile,
          'upload': false,
          "isOnlyPhoto": isOnlyPhoto,
          'compress': compress,
          "maxWidth": maxWidth,
        });
        if (uploadedImage != null && uploadedImage != "") {
          urls.add(uploadedImage);
          return urls;
        }
      }
    } catch (e) {
    } finally {
      ToastUtil.dismiss();
    }
    return urls;
  }

  /// 选择图片
  static Future<List<LyAssetEntity>> onAssetsPick({
    required BuildContext context,
    RequestType type = RequestType.image,
    int maxAssets = 1,
    SpecialItemPosition specialItemPosition = SpecialItemPosition.prepend,
    CloudCheckApi? cloudCheckApi,
    String? classId,
  }) async {
    try {
      List<LyAssetEntity>? assets = await LyAssetsPicker.pickAssets(
        context,
        maxAssets: maxAssets,
        requestType: type,
        specialItemPosition: specialItemPosition,
        classId: classId,
        cloudCheckApi: cloudCheckApi,
      );
      if (assets == null || assets.isEmpty) return [];
      return assets;
    } catch (e) {}
    return [];
  }

  /// 拍照
  static Future<AssetEntity?> onCameraPick(
    BuildContext context, {
    ThemeData? theme,
    ResolutionPreset resolutionPreset = ResolutionPreset.max,
    ImageFormatGroup imageFormatGroup = ImageFormatGroup.unknown,
  }) async {
    try {
      AssetEntity? assetEntity = await CameraPicker.pickFromCamera(
        context,
        theme: theme,
        resolutionPreset: resolutionPreset,
        imageFormatGroup: imageFormatGroup,
      );
      if (assetEntity == null) return null;
      return assetEntity;
    } catch (e) {}
    return null;
  }

  /// 过滤部分资源的格式
  /// onInvalidSave 获取不支持的格式图片列表
  /// onNotLocallyAvailableSave 获取本地无效格式图片列表 （仅存在远程iCould中，仍未下载到本地）
  static Future<List<AssetEntity>> filterAssetsList(
      {required List<AssetEntity> assets,
      required List<String> allowTypes,
      required Function onInvalidSave,
      required Function onNotLocallyAvailableSave,
      int maxMinute = 5}) async {
    // Logger.d('filterAssetsList====${assets.length}');

    /// 本地已经下载的有效图片、视频
    List<AssetEntity> localValidAssets = <AssetEntity>[];

    /// 格式不支持的图片、视频
    List<String> invalidImageAssets = <String>[];

    /// 本地无效的图片、视频
    List<String> locallyNotAvailableAssets = <String>[];

    var cacheAvailableList = lySp.getList(key: SK_LOCALLY_AVAILABLE_CACHE) ?? [];
    for (var element in assets) {
      /// isLocallyAvailable去检查是否存在于苹果iCould中
      var isLocallyAvailable = await element.isLocallyAvailable();
      // Logger.d('element.title====${element.title}');
      /// 当同时满足在icloud和不符合规格时，优先展示不符合规格
      /// icloud只判断符合规格的图片

      if (element.type == AssetType.video) {
        var videoTooLong = element.duration > ((maxMinute > 0 ? maxMinute : 1) * 60);
        if (!videoTooLong) {
          if (!isLocallyAvailable) {
            /// 从缓存中，剔除系统认为是icloud上未下载，但实际上用户已经点击了手动下载的资源
            if (!cacheAvailableList.contains(element.id)) {
              locallyNotAvailableAssets.add(element.id);
            }
          }
        }

        if (Platform.isAndroid) {
          if ((element.title ?? '').endsWith('.mp4')) {
            localValidAssets.add(element);
          }
        } else {
          localValidAssets.add(element);
        }
      } else if (element.type == AssetType.image) {
        var formatAvailable = allowTypes.contains(assetSuffix(element));
        if (formatAvailable) {
          if (!isLocallyAvailable) {
            /// 从缓存中，剔除系统认为是icloud上未下载，但实际上用户已经点击了手动下载的资源
            if (!cacheAvailableList.contains(element.id)) {
              locallyNotAvailableAssets.add(element.id);
            }
          }
        } else {
          invalidImageAssets.add(element.id);
        }
        localValidAssets.add(element);
      }
    }
    // Logger.d('cacheAvailableList====$cacheAvailableList');

    /// 返回不支持的图片格式的id
    onInvalidSave(invalidImageAssets);

    /// 本地无效的资源
    onNotLocallyAvailableSave(locallyNotAvailableAssets);
    return localValidAssets;
  }

  /// 数组分割
  static List<List<T>> splitList<T>(List<T> list, int len) {
    if (len <= 1) {
      return [list];
    }

    List<List<T>> result = [];
    int index = 1;

    while (true) {
      if (index * len < list.length) {
        List<T> temp = list.skip((index - 1) * len).take(len).toList();
        result.add(temp);
        index++;
        continue;
      }
      List<T> temp = list.skip((index - 1) * len).toList();
      result.add(temp);
      break;
    }
    return result;
  }

  /// 获取AssetEntity后缀
  static String assetSuffix(AssetEntity asset) {
    return (asset.title ?? "").split('.').last.toLowerCase();
  }

  /// 获取AssetEntity后缀,不该为小写
  static String pureAssetSuffix(AssetEntity asset) {
    return (asset.title ?? "").split('.').last;
  }

  /// 资源是否有效（可选）
  static bool isAssetAvailable(AssetEntity asset, List<String> invalidFormats, List<String> notLocallyAvailables) {
    var available = true;

    if (asset.type == AssetType.image) {
      var invalid = invalidFormats.contains(asset.id);
      available = !invalid;
    } else {
      available = asset.duration <= 300;
    }
    var notLocallyAvailable = notLocallyAvailables.contains(asset.id);
    return available && !notLocallyAvailable;
  }

  /// 取出选中项
  static List<LyAssetEntity> transAssetsList(List<GroupEntity> _groupList, List<String> invalidFormats, List<String> notLocallyAvailables) {
    List<LyAssetEntity> assetList = [];
    for (var group in _groupList) {
      if (group.child.isNotEmpty) {
        for (var asset in group.child) {
          if (!asset.notSelected && AssetUtil.isAssetAvailable(asset, invalidFormats, notLocallyAvailables)) {
            assetList.add(asset);
          }
        }
      }
    }
    return assetList;
  }
}
