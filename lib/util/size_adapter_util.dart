// ignore_for_file: public_member_api_docs, sort_constructors_first
import 'package:flutter/material.dart';
import 'package:zyocore/zyocore.dart';

class SizeAdapterUtils {
  static SizeAdapterUtils defaultUtil = SizeAdapterUtils();

  // final Size designSize;
  /// 这里其实是错的，需要参照下面_defaultDesignSizePad进行修改
  static const Size _defaultDesignSize = Size(375.0, 812.0);
  static const Size _defaultDesignSizePad = Size(834.0, 1194.0);

  // SizeAdapterUtils({
  //   this.designSize = _defaultDesignSize,
  // });

  static double autoWidth(BuildContext context) {
    var orientation = MediaQuery
        .of(context)
        .orientation;
    var screenWidth = MediaQuery
        .of(context)
        .size
        .width;
    // Logger.d('SizeAdapterUtils====orientation=$orientation');
    // Logger.d('SizeAdapterUtils====screenWidth=$screenWidth');
    // Logger.d('SizeAdapterUtils====1.w=${1.w}');
    // Logger.d('SizeAdapterUtils====1.w=${1.h}');
    // Logger.d('SizeAdapterUtils====screenWidth / designSize.height=${screenWidth / designSize.height}');
    if (DeviceUtil.isTablet) {
      switch (orientation) {
        case Orientation.portrait:
          return screenWidth / _defaultDesignSizePad.width;
        case Orientation.landscape:
          return screenWidth / _defaultDesignSizePad.height;
      }
    } else {
      switch (orientation) {
        case Orientation.portrait:
          return screenWidth / _defaultDesignSize.width;
        case Orientation.landscape:
          return screenWidth / _defaultDesignSize.height;
      }
    }
  }

  static double autoHeight(BuildContext context) {
    var orientation = MediaQuery
        .of(context)
        .orientation;
    var screenHieght = MediaQuery
        .of(context)
        .size
        .height;
    if (DeviceUtil.isTablet) {
      switch (orientation) {
        case Orientation.portrait:
          return screenHieght / _defaultDesignSizePad.width;
        case Orientation.landscape:
          return screenHieght / _defaultDesignSizePad.height;
      }
    } else {
      switch (orientation) {
        case Orientation.portrait:
          return screenHieght / _defaultDesignSize.height;
        case Orientation.landscape:
          return screenHieght / _defaultDesignSize.width;
      }
    }
  }

  static double adaptiveAutoWidth(BuildContext context) {
    var orientation = MediaQuery
        .of(context)
        .orientation;
    var screenWidth = MediaQuery
        .of(context)
        .size
        .width;

    switch (orientation) {
      case Orientation.portrait:
        return screenWidth / _defaultDesignSize.width;
      case Orientation.landscape:
        return screenWidth / _defaultDesignSize.height;
    }
  }
}

extension LYSizeExtension on num {
  double get lyW {
    final context = Get.context;
    if (context == null) {
      LogUtil.e(
          "lyW - 获取宽度时未传入有效的context，如果遇到这种情况，请用 lyWWithContext 明确传入context替代lyW");
      return toDouble();
    }
    return lyWWithContext(context);
  }

  double get lyH {
    final context = Get.context;
    if (context == null) {
      LogUtil.e(
          "lyH - 获取高度时未传入有效的context，如果遇到这种情况，请用 lyHWithContext 明确传入context替代lyH");
      return toDouble();
    }
    return lyHWithContext(context);
  }

  double get lywA {
    final context = Get.context;
    if (context == null) {
      LogUtil.e(
          "lywA - 获取宽度时未传入有效的context，如果遇到这种情况，请用 lywAWithContext 明确传入context替代lywA");
      return toDouble();
    }
    return lywAWithContext(context);
  }

  double lyWWithContext(BuildContext context) {
    return toDouble() * SizeAdapterUtils.autoWidth(context);
  }

  double lyHWithContext(BuildContext context) {
    return toDouble() * SizeAdapterUtils.autoHeight(context);
  }


  double lywAWithContext(BuildContext context) {
    return toDouble() * SizeAdapterUtils.adaptiveAutoWidth(context);
  }


}
