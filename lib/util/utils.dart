import 'package:zyocore/util/lib/date_util.dart';
import 'package:zyocore/util/lib/encrypt_util.dart';
import 'package:zyocore/util/lib/style_util.dart';
import 'package:zyocore/util/lib/text_util.dart';

/// 工具类对象可以全部配置在这里
class Utils {
  final TextUtil text = TextUtil();
  final StyleUtil style = StyleUtil();
  final DateUtil date = DateUtil();
  final EncryptUtil encrypt = EncryptUtil();
}

final Utils utils = Utils();
