import 'dart:math';

import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';

///圆环状统计显示
class ProgressPaint extends CustomPainter {
  ProgressPaint({
    required this.pointList,
    required this.circularColor,
    required this.strokeWidth,
    this.noData = true,
  });

  final Color circularColor;
  final double strokeWidth;
  List<blockModel> pointList;
  final bool noData;

  @override
  void paint(Canvas canvas, Size size) {
    // TODO: implement paint
    var paintBg = Paint()
      ..color = circularColor
      ..style = PaintingStyle.stroke
      ..strokeWidth = strokeWidth;
    var single = pi / 180;
    canvas.drawCircle(Offset(size.width / 2, size.height / 2), size.width / 2, paintBg);
    if (!noData) {
      for (var i = 0; i < pointList.length; i++) {
        paintBg.color = pointList[i].color!;
        canvas.drawArc(Rect.fromCircle(center: Offset(size.width / 2, size.height / 2), radius: size.width / 2), single * pointList[i].startPoint!, single * pointList[i].endPoint!,
            false, paintBg);
      }
    }
  }

  @override
  bool shouldRepaint(covariant CustomPainter oldDelegate) {
    // TODO: implement shouldRepaint
    return false;
  }
}

class blockModel {
  Color? color;
  double? startPoint;
  double? endPoint;

  blockModel({this.color, this.startPoint, this.endPoint});
}
