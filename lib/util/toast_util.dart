import 'package:flutter/material.dart';
import 'package:flutter_easyloading/flutter_easyloading.dart';

class ToastUtil {
  static Future<void> showToast(String text,
      {Duration? duration,
        EasyLoadingToastPosition? toastPosition,
        EasyLoadingMaskType? maskType,
        bool? dismissOnTap,
        EasyLoadingStyle? easyLoadingStyle,
        Color? textColor,
        Color? indicatorColor,
        Color? backgroundColor,
        double? radius}) {
    if (easyLoadingStyle != null) {
      EasyLoading.instance.loadingStyle = easyLoadingStyle;
      EasyLoading.instance.backgroundColor = backgroundColor ?? Colors.black;
      EasyLoading.instance.indicatorColor = indicatorColor ?? Colors.black;
      EasyLoading.instance.textColor = textColor ?? Colors.white;
      EasyLoading.instance.radius = radius ?? 5;
    }
    return EasyLoading.showToast(
      text,
      duration: duration,
      toastPosition: toastPosition,
      maskType: maskType,
      dismissOnTap: dismissOnTap,
    );
  }

  static Future<void> showError(String text, {
    Duration? duration,
    EasyLoadingToastPosition? toastPosition,
    EasyLoadingMaskType? maskType,
    bool? dismissOnTap,
  }) {
    return EasyLoading.showError(
      text,
      duration: duration,
      maskType: maskType,
      dismissOnTap: dismissOnTap,
    );
  }

  static Future<void> loading(String text, {
    EasyLoadingMaskType? maskType
  }) {
    return EasyLoading.show(
      status: text.isNotEmpty ? text : null,
      maskType: maskType ?? EasyLoadingMaskType.clear,
    );
  }

  static Future<void> dismiss() {
    return EasyLoading.dismiss();
  }

  static Future<void> showSuccess(String text, {
    Duration? duration,
    EasyLoadingMaskType? maskType,
    bool? dismissOnTap,
  }) {
    return EasyLoading.showSuccess(
      text,
      duration: duration,
      maskType: maskType,
      dismissOnTap: dismissOnTap,
    );
  }

  static Future<void> showInfo(String text,
      {Duration? duration, EasyLoadingMaskType? maskType, bool? dismissOnTap, Widget? infoWidget}) {
    if (infoWidget != null) {
      EasyLoading.instance.infoWidget = infoWidget;
    }
    return EasyLoading.showInfo(
      text,
      duration: duration,
      maskType: maskType,
      dismissOnTap: dismissOnTap,
    );
  }

  static Future<void> showProgress(double value, {
    String? status,
    EasyLoadingMaskType? maskType,
  }) {
    return EasyLoading.showProgress(value, status: status, maskType: maskType);
  }

  static TransitionBuilder init({
    TransitionBuilder? builder,
  }) {
    return EasyLoading.init(builder: builder);
  }

  static FlutterEasyLoading({Widget? child}) {
    return FlutterEasyLoading(
      child: child,
    );
  }
}
