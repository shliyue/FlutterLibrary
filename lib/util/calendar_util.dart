import 'package:device_calendar/device_calendar.dart';
import 'package:flutter_native_timezone/flutter_native_timezone.dart';
import 'package:timezone/timezone.dart';
import 'package:zyocore/zyocore.dart';

/// 日历相关
/// **插入日历预约
class CalendarUtil {
  DeviceCalendarPlugin? _deviceCalendarPlugin;

  static final CalendarUtil instance = CalendarUtil._internal();

  factory CalendarUtil() => instance;

  CalendarUtil._internal() {
    _init();
  }

  _init() {
    _deviceCalendarPlugin ??= DeviceCalendarPlugin();
  }

  /// 增加日历提醒事件
  Future<bool> addCalendarEvent(
      {String? aliveStartAt, String? aliveStopAt, String? title}) async {
    try {
      await _checkPermission();
      List<Calendar>? calendars = await _listCalendars();
      if (calendars == null) return false;

      /// 创建事件
      Event? event = await _buildCalendarEvent(
          calendarId: calendars.last.id,
          aliveStartAt: aliveStartAt,
          aliveStopAt: aliveStartAt,
          title: title);

      Logger.d('addCalendarEvent event=======${event!.toJson()}');
      var createEventResult =
          await _deviceCalendarPlugin!.createOrUpdateEvent(event);

      /// 返回的是eventId
      if (createEventResult?.isSuccess == true) {
        return true;
      } else {
        var error = createEventResult?.errors
            .map((err) => '[${err.errorCode}] ${err.errorMessage}')
            .join(' | ');
        Logger.d('addCalendarEvent error=======$error');
        return false;
      }
    } catch (e) {
      LogUtil.e('addCalendarEvent e===$e');
      return false;
    }
  }

  /// 构建日历事件
  Future<Event?> _buildCalendarEvent(
      {String? calendarId,
      String? aliveStartAt,
      String? aliveStopAt,
      String? title}) async {
    try {
      /// 当前时区
      String _timezone = await FlutterNativeTimezone.getLocalTimezone();

      Logger.d(
          'calendar_event _timezone ------------------------- $_timezone');
      // Logger.d('calendar_event liveItem ------------------------- ${liveItem.toJson()}');
      var currentLocation = timeZoneDatabase.locations[_timezone];

      var formatTime = utils.date.formatDateStr(
          aliveStartAt ?? DateTime.now().toString(),
          format: DateFormats.zh_mo_d);
      var finalTitle = "您已成功预约《${title!}》,$formatTime开课，记得准时进入直播间学习哦~";

      var location = currentLocation == null
          ? timeZoneDatabase.locations['Etc/UTC']
          : timeZoneDatabase.locations[_timezone];
      TZDateTime startDate = TZDateTime.from(
          DateTime.parse(aliveStartAt ?? DateTime.now().toString()), location!);

      ///结束时间必须大于开始时间
      TZDateTime endDate = TZDateTime.from(
          DateTime.parse(aliveStopAt ?? DateTime.now().toString()), location);

      ///测试用的结束时间
      // TZDateTime endDate = TZDateTime.from(DateTime.now(), location);
      /// 日历提醒时间，单位为分钟
      List<Reminder> reminders = <Reminder>[];
      reminders.add(Reminder(minutes: 15));

      /// 新创建的日历在最后，
      Event event = Event(calendarId,
          title: finalTitle,
          start: startDate,
          end: endDate,
          reminders: reminders);

      // Logger.d('DeviceCalendarPlugin calendar event: ${event.toJson()}');

      Logger.d('DeviceCalendarPlugin calendar id is: $calendarId');
      return event;
    } catch (e) {
      LogUtil.e('DeviceCalendarPlugin calendar e: $e');
      return null;
    }
  }

  /// 日历日写、读权限检查
  _checkPermission() async {
    var permissionsGranted = await _deviceCalendarPlugin!.hasPermissions();
    Logger.d(
        '_checkPermission isSuccess=======${permissionsGranted.isSuccess}');
    Logger.d('_checkPermission data=======${permissionsGranted.data}');
    if (permissionsGranted.isSuccess &&
        (permissionsGranted.data == null || permissionsGranted.data == false)) {
      permissionsGranted = await _deviceCalendarPlugin!.requestPermissions();
      if (!permissionsGranted.isSuccess ||
          permissionsGranted.data == null ||
          permissionsGranted.data == false) {
        return;
      }
    }
  }

  /// 列出当前系统已有的日历
  Future<List<Calendar>?> _listCalendars() async {
    final calendarsResult = await _deviceCalendarPlugin!.retrieveCalendars();
    // Logger.d('addCalendarEvent calendarsResult.data=======${calendarsResult.data}');
    List<Calendar> _calendars = calendarsResult.data as List<Calendar>;
    // Logger.d('addCalendarEvent _calendars=======$_calendars');
    if (_calendars.isEmpty) return null;
    for (var element in _calendars) {
      // Logger.d('addCalendarEvent calendar=======${element.toJson()}');
    }
    return _calendars;
  }
}
