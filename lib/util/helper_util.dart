class LyHelperUtil {
  /**
   * duration 单位秒
   * */
  static String getStrDuration(int? duration) {
    if (duration == null) {
      return "";
    }
    String m = (duration~/60).toString().padLeft(2, '0');
    String s = (duration % 60).toString().padLeft(2, '0');
    // String m = (duration / 60 % 60).truncate() < 10
    //     ? '0' + (duration / 60 % 60).truncate().toString()
    //     : (duration / 60 % 60).truncate().toString();
    // String s =
    //     (duration % 60).truncate() < 10 ? '0' + (duration % 60).truncate().toString() : (duration % 60).truncate().toString();
    return m + ":" + s;
  }

  String getFormatTime(Duration duration) {
    var h = duration.inHours;
    var m = (duration.inMinutes - h * 60).toString().padLeft(2, "0");
    var s = (duration.inSeconds % 60).toString().padLeft(2, "0");
    return "$h:$m:$s";
  }
}
