import 'package:shared_preferences/shared_preferences.dart';

const String SP_USER_TOKEN = 'user_token';
const String SP_USER_INFO = 'user_info';
const String SP_USER_PHONE = 'user_phone';
const String SP_GUIDE_LICENSE = 'show_guide_license';

/// 用户手动点击icloud图标触发下载后存放的Asset资源文件的id
/// 相册-存储已经从icloud下载到园宝通本地到Asset资源文件的id，
/// 此时该图片在系统相册中，还是需要去继续下载原图
const String SK_LOCALLY_AVAILABLE_CACHE = 'sk_locally_available_cache';
const String STORAGE_KEY_USER_TOKEN = 'user_token';
const String STORAGE_KEY_USER_PHONE = 'user_phone';
const String STORAGE_KEY_USER_INFO = 'user_info';
const String STORAGE_KEY_ACTIVE_PARENT_STUD_ID = 'active_parent_stud_id';
const String STORAGE_KEY_ACTIVE_EMP_SCHOOL_ID = 'active_emp_school_id';
const String STORAGE_KEY_USER_BABY_kINSHIP = 'user_baby_kinship';
const String STORAGE_KEY_ASSET_HASH = 'asset_hash';
const String STORAGE_KEY_APP_ID = 'appId';
const String STORAGE_KEY_WX_APP_ID = 'wxAppId';
const String STORAGE_KEY_WX_LINK = 'wxLink';

/// 本地存储宝宝的亲属权限  {'token':{"studId":false}}

/// 隐私协议弹窗
const String STORAGE_KEY_AGREE_PRIVACY = 'agree_privacy';

class LySp {
  late SharedPreferences instance;

  init() async {
    instance = await SharedPreferences.getInstance();
    return this;
  }

  Future<bool> setString({
    required String key,
    required String value,
  }) {
    return instance.setString(key, value);
  }

  String getString({required String key}) {
    return instance.getString(key) ?? '';
  }

  Future<bool> setBool({
    required String key,
    required bool value,
  }) async {
    return instance.setBool(key, value);
  }

  bool getBool({required String key}) {
    bool? value = instance.getBool(key);
    return value ?? false;
  }

  Future<bool> setInt({
    required String key,
    required int value,
  }) async {
    return instance.setInt(key, value);
  }

  int getInt({required String key}) {
    int? value = instance.getInt(key);
    return value ?? 0;
  }

  Future<bool> setDouble({
    required String key,
    required double value,
  }) async {
    return instance.setDouble(key, value);
  }

  double getDouble({
    required String key,
  }) {
    double? value = instance.getDouble(key);
    return value ?? 0.0;
  }

  Future<bool> setList({
    required String key,
    required List<String> value,
  }) async {
    return instance.setStringList(key, value);
  }

  List<String> getList({
    required String key,
  }) {
    List<String>? value = instance.getStringList(key);
    return value ?? [];
  }

  Future<bool> remove(String key) async {
    return instance.remove(key);
  }

  reset() {
    instance.clear();

    /// 保留隐私协议的选择
    instance.setBool(SP_GUIDE_LICENSE, true);
  }
}

final LySp lySp = LySp();
