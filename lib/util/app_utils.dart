import 'dart:async';
import 'dart:convert';
import 'dart:io';

import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:intl/intl.dart';
import 'package:url_launcher/url_launcher.dart';
import 'package:zyocore/entitys/version_config.dart';
import 'package:zyocore/zyocore.dart';

import '../entitys/version.dart';

typedef ImagePathCallback = Function(String path);

enum EnumFileType { network, system, phone, asset }

class AppUtils {
  static String getMoneyStr(double value, {String format = "#,##.00"}) {
    if (value == 0) {
      return "0";
    }
    final oCcy = NumberFormat(format, "en_US");
    return oCcy.format(value);
  }

  /// 应用文件目录新建文件夹
  static Future<String> createDir(String dirName) async {
    Directory documentsDirectory = await getApplicationDocumentsDirectory();
    String path = '${documentsDirectory.path}${Platform.pathSeparator}$dirName';
    var dir = Directory(path);
    var exist = dir.existsSync();
    if (exist) {
      dir.deleteSync(recursive: true);
    }
    var result = await dir.create();
    return path;
  }

  /// 版本检查
  static Future<bool> checkVersion(
    BuildContext context, {
    String? apiHost,
    required String envType,
    required String appType,
    required String appId,
    String? platform,
    String channel = "other",
    Function(String)? showHandle, // 弹出时的回调   为了埋点所加
    Function(String)? onTapHandle, // 点击升级按钮时的回调 为了埋点所加
  }) async {
    try {
      var pkg = await PackageInfo.fromPlatform();
      int buildNum = int.parse(pkg.buildNumber);
      String versionNumber = pkg.version;
      VersionEntity rs;
      if (!TextUtil.isEmpty(apiHost)) {
        if (TextUtil.isEmpty(platform)) {
          platform = Platform.isAndroid ? "android" : "iphone";
        }
        var query = {
          "appId": appId,
          "platform": platform,
          "env": envType,
          "channel": channel,
          "versionNumber": versionNumber,
        };

        var resp = await Dio()
            .get('$apiHost/api/client/app/latest', queryParameters: query, options: Options(headers: {"app-id": appId}));
        if (resp.statusCode != 200) {
          return false;
        }
        if (resp.data["content"] == null) {
          return false;
        }
        rs = VersionEntity.fromJson(resp.data["content"]);
      } else {
        var fileName = "version_" + appType;
        if (envType != "prod") {
          fileName = fileName + "_" + envType;
        }
        var resp = await Dio().get('https://product.zyosoft.cn/Download/app/babyV2/$fileName.json');
        if (resp.statusCode != 200) {
          return false;
        }
        rs = VersionEntity.fromJson(resp.data);
      }

      int buildVer = rs.buildVersion!;
      String appVer = rs.appVersion ?? '';

      if (buildVer > buildNum && rs.channel!.contains(channel)) {
        if (showHandle != null) {
          showHandle(appVer);
        }
        LYDialogUtil.showVersionDialog(context, rs, envType: envType, appType: appType, channelType: channel, onTapHandle: () {
          if (onTapHandle != null) {
            onTapHandle(appVer);
          }
        });
        return true;
      }
      return false;
    } catch (e) {
      Logger.d("uploadLog=error$e");
    }
    return false;
  }

  static Future<void> onCallPhone({String phone = "400-9968-338"}) async {
    final Uri _uri = Uri.parse("tel:$phone");
    if (await canLaunchUrl(_uri)) {
      await launchUrl(_uri);
    } else {
      throw 'Could not launch $_uri';
    }
  }

  ///唤起第三方app
  static Future<bool> onLaunchApp(String path) async {
    final Uri _uri = Uri.parse(path);
    if (await canLaunchUrl(_uri)) {
      await launchUrl(_uri);
      return true;
    } else {
      return false;
    }
  }

  /// 隐藏键盘
  static hideKeyboard() {
    if (Get.context == null) {
      return;
    }
    FocusScopeNode currentFocus = FocusScope.of(Get.context!);
    if (!currentFocus.hasPrimaryFocus && currentFocus.focusedChild != null) {
      FocusManager.instance.primaryFocus?.unfocus();
    }
  }

  ///是否是网络文件
  EnumFileType getFileType(String? url) {
    if (TextUtil.isEmpty(url)) {
      return EnumFileType.asset;
    } else if (url!.startsWith("http://") || url.startsWith("https://") || !url.contains("/")) {
      return EnumFileType.network;
    } else if (url.contains("assets/images/")) {
      return EnumFileType.system;
    } else {
      return EnumFileType.phone;
    }
  }

  ///打开引导页
  openGuidePage(String type, String userId, Function() callback) {
    if (lySp.instance.containsKey("$type-$userId") && lySp.instance.getString("$type-$userId") == userId) {
    } else {
      lySp.instance.setString("$type-$userId", "");
      WidgetsBinding.instance.addPostFrameCallback((_) {
        Future.delayed(const Duration(milliseconds: 300), () {
          callback();
        });
      });
    }
  }

  ///限制输入长度
  String setContent(int maxLength, TextEditingController con, String oldText) {
    if (maxLength < con.value.text.length) {
      con.value = TextEditingValue(
        text: oldText,
        selection: TextSelection.collapsed(
          offset: oldText.length,
        ),
      );
      return oldText;
    } else {
      return con.value.text;
    }
  }

  ///限制输入长度
  String setContentLine(int maxLine, TextEditingController con, String oldText, double width, double fontWidth) {
    var lines = con.value.text.split("\n");
    if (maxLine < lines.length) {
      con.value = TextEditingValue(
        text: oldText,
        selection: TextSelection.collapsed(
          offset: oldText.length,
        ),
      );
      return oldText;
    } else {
      var lineLength = (width / fontWidth * 2).toInt();
      for (var i = 0; i < lines.length; i++) {
        if (lines[i].length > lineLength) {
          con.value = TextEditingValue(
            text: oldText,
            selection: TextSelection.collapsed(
              offset: oldText.length,
            ),
          );
          return oldText;
        }
      }
      return con.value.text;
    }
  }

  ///获取当前学年
  int getDefaultGradeYear() {
    int year = DateTime.now().year;
    int month = DateTime.now().month;
    if (month >= 1 && month < 9) {
      year = year - 1;
    }
    return year;
  }

  /// 从配置文件中读取版本号
  static Future<VersionConfig?> initVersionConfig() async {
    try {
      var jsonData = await rootBundle.loadString('assets/configs/config.json');
      Logger.d("jsonData=$jsonData");
      if (jsonData.isEmpty) return null;
      var versionJson = json.decode(jsonData);
      if (versionJson != null) {
        Logger.d('versionJson=$versionJson');
        VersionConfig config = VersionConfig.fromJson(versionJson);
        Logger.d('versionJson toJson=${config.toJson()}');
        return config;
      }
    } catch (e) {
      LogUtil.e("getVersionConfig e==${e}");
    }
  }

  /// 轮询执行某个条件，条件成立则轮询结束
  static pollingExcuteFunction({Duration? duration, required bool Function() predicate, required void Function() callBack}) {
    duration ??= const Duration(seconds: 2);
    Timer.periodic(duration, (timer) {
      if (predicate()) {
        timer.cancel();
        callBack();
      }
    });
  }
}
