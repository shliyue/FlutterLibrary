import 'dart:io';
import 'dart:math';
import 'package:flutter/widgets.dart';

import '../core/configs/global_config.dart';
import '../core/enum/enum_asset_type.dart';
import '../entitys/upload_asset_entity.dart';
import '../widget/assets_picker/entity/ly_asset_entity.dart';
import '../zyocore.dart';
import 'package:crypto/crypto.dart';

/// 压缩目标
int imageCompressedTarget = 2000;

class FileUtil {
  static Future<String> getFileHash(String filePath) async {
    final file = File(filePath);
    final bytes = await file.readAsBytes();
    final hash = sha256.convert(bytes);
    return utils.encrypt.encodeMd5(hash.toString());
  }

  ///获取原始路径(不包含任何参数)
  static String getOriUrlPath(String url) {
    if (url.contains('?')) {
      return url.substring(0, url.indexOf("?"));
    }
    return url;
  }

  /// 设置上传的文件名
  /// type 0 图片 1视频
  static String formatImageNameWithFile({required AssetEntity entity}) {
    String filename = '';
    if (entity.type == AssetType.image) {
      filename = formatImageNameWithWH(width: entity.width, height: entity.height);
    } else if (entity.type == AssetType.video) {
      filename = formatImageNameWithDuration(duration: entity.duration);
    }
    return filename;
  }

  /// 设置压缩后图片的文件名
  static String formatImageNameWithWH({required int width, required int height, String? prefix, String suffix = 'jpg'}) {
    if (!TextUtil.isEmpty(prefix) && prefix!.indexOf(".") > 0) {
      return prefix;
    }
    String sPrefix = TextUtil.isEmpty(prefix) ? getFileNamePrefix() : prefix!;
    String filename = sPrefix + '_' + width.toString() + 'X' + height.toString() + '.$suffix';
    return filename;
  }

  /// 格式化Video文件名
  /// duration 单位秒
  static String formatImageNameWithDuration({required int duration, String? prefix}) {
    if (!TextUtil.isEmpty(prefix) && prefix!.indexOf(".") > 0) {
      return prefix;
    }
    String sPrefix = TextUtil.isEmpty(prefix) ? getFileNamePrefix() : prefix!;
    String filename = sPrefix + '_' + duration.toString() + 'X' + duration.toString() + '.mp4';
    return filename;
  }

  /// 上传音频的文件名格式
  /// duration 单位秒
  static String formatAudioName({required int duration, String? prefix, String? suffix}) {
    var audioDuration = Duration(seconds: duration);
    if (!TextUtil.isEmpty(prefix) && prefix!.indexOf(".") > 0) {
      return prefix;
    }
    String sPrefix = TextUtil.isEmpty(prefix) ? getFileNamePrefix() : prefix!;
    String filename = sPrefix + '_' + '${audioDuration.inMilliseconds}' + 'X' + '${audioDuration.inMilliseconds}' + '.' + (suffix ?? "aac");
    return filename;
  }

  /// 根据URL获取视频时长
  static int getVideoDurationWithSrc({required String src}) {
    if (!isVideo(src)) return 0;
    try {
      String durationS = src.split('X').last.split('.').first;
      int? durationI = int.tryParse(durationS);
      if (durationI == null) return 0;
      return durationI;
    } catch (e) {
      return 0;
    }
  }

  /// 获取文件命名
  static String getFileNamePrefix() {
    var random = Random().nextInt(100000).toString().padLeft(4, '0');
    var date = DateTime.now();
    String yyMMddHHmmss = utils.date.formatDate(date, format: "yyMMddHHmmss");
    String s = utils.date.formatDate(date, format: "S");
    s = s.padLeft(4, '0');
    var prefix = yyMMddHHmmss + s + random;
    return prefix;
  }

  /// 获取文件后缀 返回小写
  static String getFileNameSuffix(String? filePath) {
    String suffix = "";
    if (filePath == null || filePath.isEmpty) {
      return suffix;
    }
    String oriPath = getOriUrlPath(filePath);
    int lastIndex = oriPath.lastIndexOf('.') + 1;
    if (lastIndex > 0) {
      suffix = oriPath.substring(lastIndex);
    }
    return suffix.toLowerCase();
  }

  /// 设置上传图片的文件名
  static String formatImageName(String path) {
    String filename = '';
    Image image = Image.file(
      File.fromUri(
        Uri.parse(path),
      ),
    );
    image.image.resolve(const ImageConfiguration()).addListener(
      ImageStreamListener(
        (ImageInfo info, bool _) {
          var width = info.image.width;
          var height = info.image.height;
          filename = formatImageNameWithWH(width: width, height: height);
        },
      ),
    );
    return filename;
  }

  /// 获取缩略图
  static String getThumbsImageUrl(String url, {int width = 600, int height = 600, bool local = false, int q = 90, int mode = 0}) {
    if (url.isEmpty) {
      return "";
    }

    /// 如果已经是缩略图直接返回
    if (url.contains("?x-oss-process=image/")) {
      return url;
    }
    if (url.endsWith('/q/90') && url.contains("?imageView2/$mode/w/")) {
      return url;
    }
    if (isAliYunHost(url)) {
      return url + "?x-oss-process=image/resize,h_" + height.toString() + ",w_" + width.toString() + "|image/quality,Q_$q";
    }
    String newUrl = url + "?imageView2/$mode/w/" + width.toString() + "/h/" + height.toString() + "/q/$q";
    if (url.startsWith("http")) {
      return newUrl;
    }
    if (local) {
      return url;
    }
    return GlobalConfigZyoCore.qiNiuHost + newUrl;
  }

  /// 获取视频第一帧
  static String getQiuVideoImage(String url) {
    return getVideoThumbImage(url, w: 750, h: 750);
  }

  /// 获取视频第一帧缩略图-暂时微信分享用
  static String getQiuVideoThumImage(String url) {
    return getVideoThumbImage(url, w: 100, h: 100);
  }

  static String getVideoThumbImage(String url, {int w = 750, int h = 750, int fps = 0}) {
    if (url.isEmpty) {
      return "";
    }
    if (isAliYunHost(url)) {
      return url + "?x-oss-process=video/snapshot,f_jpg,w_$w,h_$h";
    }
    if (url.startsWith('http')) return url + '?vframe/jpg/offset/$fps/w/$w';
    return GlobalConfigZyoCore.qiNiuHost + url + '?vframe/jpg/offset/$fps/w/$w';
  }

  static bool isAliYunHost(String url) {
    return !TextUtil.isEmpty(GlobalConfigZyoCore.aliYunHost) && url.startsWith(GlobalConfigZyoCore.aliYunHost);
  }

  static bool isM3U8Video(String? url) {
    if (TextUtil.isEmpty(url)) {
      return false;
    }
    return url!.endsWith(".m3u8");
  }

  /// m3u8 --- 待完
  static String m3u8formatVideoSrc(String src) {
    /// 处理旧园宝通数据不支持m3u8格式问题
    if (src.startsWith('http://qiniu.appbaby.net') || src.startsWith('https://qiniu.appbaby.net')) {
      return src;
    }
    String timestamp = '';
    try {
      timestamp = src.split('/').last.split('_').first;
    } catch (e) {
      return src;
    }

    /// 因为上传文件时年份格式是yy，所以需要补年份前的20
    try {
      timestamp = '20' + timestamp.substring(0, 12);
      String yyyy = timestamp.substring(0, 4);
      String MM = timestamp.substring(4, 6);
      String dd = timestamp.substring(6, 8);
      String HH = timestamp.substring(8, 10);
      String mm = timestamp.substring(10, 12);
      String ss = timestamp.substring(12, 14);
      timestamp = "$yyyy-$MM-$dd $HH:$mm:$ss";
      DateTime? createTime = DateUtil.getDateTime(
        timestamp,
      );
      if (createTime == null) return src;
      DateTime now = DateTime.now();
      if (now.difference(createTime).inMinutes > 30) {
        return src.replaceAll('.mp4', '_hd.m3u8');
      }
      return src;
    } catch (e) {
      return src;
    }
  }

  /// 获取音视频文件时常 单位毫秒
  static int getMediaTime(String url) {
    if (url.isEmpty) return 0;
    try {
      String timeStr = url.split('X').last.split('.').first;
      int? time = int.tryParse(timeStr);
      return time ?? 0;
    } catch (e) {
      return 0;
    }
  }

  /// 判断是否是视频
  static bool isVideo(String url) {
    url = getOriUrlPath(url).toUpperCase();
    if (url.endsWith('.MP4') || url.endsWith('.M3U8') || url.endsWith('.HEVC')) return true;
    return false;
  }

  static bool isPhoto(String url) {
    url = getOriUrlPath(url).toUpperCase();
    if (url.endsWith('.JPG') ||
        url.endsWith('.JPEG') ||
        url.endsWith('.PNG') ||
        url.endsWith('.HEIC') ||
        url.endsWith('.HEIF') ||
        url.endsWith('.WEBP') ||
        url.endsWith('.GIF') ||
        url.endsWith('.BMP')) return true;
    return false;
  }

  /// 判断是否是音频
  static bool isAudio(String url) {
    url = getOriUrlPath(url).toUpperCase();
    if (url.endsWith('.AAC') || url.endsWith('.ACC') || url.endsWith('.MP3') || url.endsWith('.FLAC') || url.endsWith('.WAV')) return true;
    return false;
  }

  ///获取上传文件的缓存路径
  static Future<Directory> getUploadCachePath({String? dicName}) async {
    String tmpDirName = dicName ?? "upload";
    Directory tmpDir = await getTemporaryDirectory();
    Directory dirPath = Directory(tmpDir.path + "/" + tmpDirName);
    if (!(await dirPath.exists())) {
      dirPath.create();
    }
    return dirPath;
  }

  static String getUploadFileName({required FileModel item, bool? useSuffix}) {
    if (!TextUtil.isEmpty(item.key)) {
      return item.key!;
    }
    String fileName = '';
    String suffix = item.file.absolute.path.split(".").last;
    switch (item.assetTye) {
      case EnumAssetType.photo:
        // 图片
        suffix = ImageUtil.getFileSuffix(suffix);
        int maxValue = max(item.width, item.height);

        /// 如果为不支持的格式直接返回
        if (ImageUtil.isSupportCompress(suffix)) {
          // 判断是否需要压缩
          if (maxValue > imageCompressedTarget) {
            double coefficient = imageCompressedTarget / maxValue;
            int compressedW = (coefficient * item.width).toInt();
            int compressedH = (coefficient * item.height).toInt();
            fileName = formatImageNameWithWH(width: compressedW, height: compressedH, suffix: suffix, prefix: item.md5Hash);
          } else {
            fileName = formatImageNameWithWH(width: item.width, height: item.height, suffix: suffix, prefix: item.md5Hash);
          }
        } else {
          fileName = formatImageNameWithWH(width: item.width, height: item.height, suffix: suffix, prefix: item.md5Hash);
        }
        break;
      case EnumAssetType.video:
        // 视频
        fileName = formatImageNameWithDuration(duration: item.duration, prefix: item.md5Hash);
        break;
      case EnumAssetType.audio:
        fileName = formatAudioName(duration: item.duration, prefix: item.md5Hash, suffix: (useSuffix ?? false) ? suffix : null);
        break;
      default:
        break;
    }

    return fileName;
  }

  /// 获取资源对应的File
  static Future<File> getUploadFile({required FileModel item}) async {
    File uploadFile = item.file;

    if (item.assetTye == EnumAssetType.photo) {
      // 照片
      int maxValue = max(item.width, item.height);
      // 判断是否需要压缩
      if (maxValue > imageCompressedTarget) {
        double coefficient = imageCompressedTarget / maxValue;
        int compressedW = (coefficient * item.width).toInt();
        int compressedH = (coefficient * item.height).toInt();
        File? compressedFile;
        try {
          compressedFile = await ImageUtil.compressAndGetFile(uploadFile, title: item.title, minWidth: compressedW, minHeight: compressedH);
        } catch (e) {
          compressedFile = uploadFile;
        }
        if (compressedFile != null) {
          uploadFile = compressedFile;
        }
      }
    }
    return uploadFile;
  }

  /// 根据入参类型创建对应asset id
  static Future<String?> createAssetId(sourceFile) async {
    String? id;
    if (sourceFile == null) {
      id = null;
    }
    if (sourceFile is UploadAssetEntity) {
      File? assetFile = await sourceFile.asset?.file;
      id = assetFile == null ? null : sourceFile.asset!.id;
    } else if (sourceFile is AssetEntity) {
      File? assetFile = await sourceFile.file;
      id = assetFile == null ? null : sourceFile.id;
    } else if (sourceFile is LyAssetEntity) {
      File? assetFile = await sourceFile.file;
      id = assetFile == null ? null : sourceFile.id;
    } else if (sourceFile is FileModel) {
      id = null;
    }
    return id;
  }
}
