class VersionConfig {
  int? versionCode;
  String? versionName;
  String? buildTime;
  int? ramVersion;

  VersionConfig({
    this.versionCode,
    this.versionName,
    this.ramVersion,
    this.buildTime,
  });

  VersionConfig.fromJson(dynamic json) {
    versionCode = json['versionCode'];
    versionName = json['versionName'];
    ramVersion = json['ramVersion'];
    buildTime = json['buildTime'];
  }

  Map<String, dynamic> toJson() {
    final map = <String, dynamic>{};
    map['versionCode'] = versionCode;
    map['versionName'] = versionName;
    map['ramVersion'] = ramVersion;
    map['buildTime'] = buildTime;
    return map;
  }
}
