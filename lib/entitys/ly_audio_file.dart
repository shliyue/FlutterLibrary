import 'dart:io';
class LYAudioFile{

  String? path;
  Duration? duration;
  File? file;
  String? durationStr;

  LYAudioFile({
    this.path,
    this.duration,
    this.file,
    this.durationStr
});

  LYAudioFile.fromJson(dynamic json){
    File audio = File(json["path"]);
    path=json["path"];
    duration= Duration(milliseconds: json["duration"]);
    file=audio;
    durationStr=json["durationStr"];
  }
  Map<String, dynamic> toJson() {
    final map = <String, dynamic>{};
    map['path'] = path;
    map['duration'] = duration?.inMilliseconds;
    map['durationStr'] = durationStr;
    return map;
  }

}