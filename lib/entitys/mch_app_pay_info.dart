class MchAppPayInfo {
  late String appId;
  late String partnerId;
  late String prepayId;
  late String package;
  late String nonceStr;
  late String timeStamp;
  late String sign;

  MchAppPayInfo();

  MchAppPayInfo.fromJson(dynamic json) {
    appId = json['appid'];
    partnerId = json['partnerid'];
    prepayId = json['prepayid'];
    package = json['package'];
    nonceStr = json['noncestr'];
    timeStamp = json['timestamp'];
    sign = json['sign'];
  }

  Map<String, dynamic> toJson() {
    final map = <String, dynamic>{};
    map['appId'] = appId;
    map['partnerId'] = partnerId;
    map['prepayId'] = prepayId;
    map['package'] = package;
    map['nonceStr'] = nonceStr;
    map['timeStamp'] = timeStamp;
    map['sign'] = sign;
    return map;
  }
}

class ModelAppPayResult {
  dynamic js;
  late String pay_no;
  late String prepay_no;
  late String state;

  ModelAppPayResult.fromJson(dynamic json) {
    js = json['js'];
    pay_no = json['pay_no'] ?? "";
    prepay_no = json['prepay_no'] ?? "";
    state = json['state'] ?? "";
  }
}
