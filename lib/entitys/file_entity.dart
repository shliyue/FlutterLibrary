class FileEntity {
  String? id;
  String? timelineId;
  String? fileUrl;
  bool? addGrowth;
  String? enumState;
  int? width;
  int? height;
  int? duration;
  String? createTime;
  String? suffix;
  String? studIds;
  String? hashId;
  String? picFrom;

  FileEntity({
    this.id,
    this.timelineId,
    this.fileUrl,
    this.addGrowth,
    this.enumState,
    this.width,
    this.height,
    this.duration,
    this.createTime,
    this.suffix,
    this.studIds,
    this.hashId,
    this.picFrom,
  });

  FileEntity.fromJson(Map<String, dynamic> json) {
    id = json['id'];
    timelineId = json['timelineId'];
    fileUrl = json['fileUrl'];
    addGrowth = json['addGrowth'];
    enumState = json['enumState'];
    width = json['width'];
    height = json['height'];
    duration = json['duration'];
    createTime = json['createTime'];
    suffix = json['suffix'];
    studIds = json['studIds'];
    hashId = json['hashId'];
    hashId = json['picFrom'];
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = <String, dynamic>{};
    data['id'] = id;
    data['timelineId'] = timelineId;
    data['fileUrl'] = fileUrl;
    data['addGrowth'] = addGrowth;
    data['enumState'] = enumState;
    data['width'] = width;
    data['height'] = height;
    data['duration'] = duration;
    data['createTime'] = createTime;
    data['suffix'] = suffix;
    data['studIds'] = studIds;
    data['hashId'] = hashId;
    data['picFrom'] = picFrom;
    return data;
  }

  /// 上传文件传给后端的参数
  Map<String, dynamic> toCreateJson() {
    final Map<String, dynamic> data = <String, dynamic>{};
    data['fileUrl'] = fileUrl;
    data['width'] = width;
    data['height'] = height;
    data['duration'] = duration;
    data['createTime'] = createTime;
    data['suffix'] = suffix;
    data['studIds'] = studIds;
    data['hashId'] = hashId;
    data['picFrom'] = picFrom;
    return data;
  }
}
