import 'dart:io';

class VersionEntity {
  String? appVersion;
  int? buildVersion;
  String? url;
  String? desc;
  String? channel;
  late bool forced;

  VersionEntity({
    this.appVersion,
    this.buildVersion,
    this.url,
    this.desc,
    this.channel,
    this.forced = true,
  });

  VersionEntity.fromJson(dynamic json) {
    if (Platform.isAndroid) {
      appVersion = json['app_version_android'] ?? json['appVersion'];
      buildVersion = json['build_version_android'] ?? json['buildVersion'];
    } else {
      appVersion = json['app_version_ios'] ?? json['appVersion'];
      buildVersion = json['build_version_ios'] ?? json['buildVersion'];
    }
    url = json['url'];
    desc = json['desc'] ?? json['log'] ?? "修复已知问题，提升性能和稳定性，使用更快更顺畅";
    channel = json['channel'] ?? "other";
    forced = json['forced'] ?? true;
  }

  Map<String, dynamic> toJson() {
    final map = <String, dynamic>{};
    map['appVersion'] = appVersion;
    map['buildVersion'] = buildVersion;
    map['channel'] = channel;
    map['forced'] = forced;
    map['url'] = url;
    map['desc'] = desc;
    map['channel'] = channel;
    return map;
  }
}
