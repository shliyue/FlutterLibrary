import 'dart:ffi';

class AliyunEntity {
  String? publicUpload;
  String? publicDomain;
  String? vpcUpload;
  String? vpcDomain;
  String? securityToken;
  String? accessKeyId;
  String? accessKeySecret;
  int? expire;
  String? bucket;
  String? folder;

  AliyunEntity({
    this.publicUpload,
    this.publicDomain,
    this.vpcUpload,
    this.vpcDomain,
    this.securityToken,
    this.accessKeyId,
    this.accessKeySecret,
    this.expire,
    this.bucket,
    this.folder,
  });

  AliyunEntity.fromJson(Map<String, dynamic> json) {
    publicUpload = json['publicUpload'];
    publicDomain = json['publicDomain'];
    vpcUpload = json['vpcUpload'];
    vpcDomain = json['vpcDomain'];
    securityToken = json['securityToken'];
    accessKeyId = json['accessKeyId'];
    accessKeySecret = json['accessKeySecret'];
    expire = json['expire'] ?? 60;
    bucket = json['bucket'];
    folder = json['folder'];
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = <String, dynamic>{};

    data['publicUpload'] = publicUpload;
    data['publicDomain'] = publicDomain;
    data['vpcUpload'] = vpcUpload;
    data['vpcDomain'] = vpcDomain;
    data['securityToken'] = securityToken;
    data['accessKeyId'] = accessKeyId;
    data['accessKeySecret'] = accessKeySecret;
    data['expire'] = expire;
    data['bucket'] = bucket;
    data['folder'] = folder;

    return data;
  }
}
