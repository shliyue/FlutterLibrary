class QiniuEntity{
  String? bucket;
  String? domain;
  String? token;

  QiniuEntity({
    this.bucket,
    this.domain,
    this.token,
});

  QiniuEntity.fromJson(Map<String, dynamic> json) {
    bucket = json['bucket'];
    domain = json['domain'];
    token = json['token'];
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = <String, dynamic>{};
    data['bucket'] = bucket;
    data['domain'] = domain;
    data['token'] = token;
    return data;
  }
}