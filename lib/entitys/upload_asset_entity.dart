import 'package:zyocore/widget/assets_picker/entity/ly_asset_entity.dart';
import 'package:zyocore/zyocore.dart';

class UploadAssetEntity {
  LyAssetEntity? asset;

  // 状态 0等待上传 1上传中 2上传完成 3上传失败
  int? status;
  double? progress;

  /// 续传时需要显示已上传的网络图
  String? uploadUrl;

  String? assetId;

  // 七牛返回的文件哈希值
  String? hash;

  UploadAssetEntity(
      {this.asset,
      this.status,
      this.progress,
      this.uploadUrl,
      this.hash,
      this.assetId});

  static Future<UploadAssetEntity> fromJson(Map<String, dynamic> json) async {
    String assetId = json['assetId'] ?? '';
    LyAssetEntity? assetEntity;
    if (assetId.isNotEmpty) {
      assetEntity =
          LyAssetEntity.fromAsset((await AssetEntity.fromId(assetId))!);
    }
    return UploadAssetEntity(
      asset: assetEntity,
      assetId: assetId,
      status: json['status'],
      progress: json['progress'],
      hash: json['hash'],
      uploadUrl: json['uploadUrl'],
    );
  }

  Map<String, dynamic> toJson() {
    return {
      'assetId': assetId,
      'asset': asset?.toJson(),
      'hash': hash,
      'status': status,
      'progress': progress,
      'uploadUrl': uploadUrl,
    };
  }
}
