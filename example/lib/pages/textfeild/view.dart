import 'package:flutter/material.dart';
import 'package:zyocore/widget/text_field/ly_text_form_field.dart';
import 'package:zyocore/zyocore.dart';
import 'logic.dart';

class TextfeildPage extends GetView<TextfeildLogic> {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: LYAppBar.getAppBar(title: 'Textfeild组件'),
      body: Container(
        color: Colors.white,
        child: Obx(() {
          return controller.state.hasShow.value ? _getContent() : const Loading();
        }),
      ),
    );
  }

  ///内容
  Widget _getContent() {
    return Column(
      crossAxisAlignment: CrossAxisAlignment.start,
      children: [
        const LyTextFieldWidget(
          leading: "姓名",
          placeHolder: "请填写姓名",
          textAlign: TextAlign.right,
        ),
        LYFormTextFiled(
          title: "姓名",
          hintText: "请填写姓名",
          must: true,
          textAlign: TextAlign.right,
          tec: TextEditingController(),
        ),
        const LyTextFieldWidget(
          leading: "备注",
          placeHolder: "请填写备注",
          isTextArea: true,
          maxLength: 200,
        ),
        LyTextFormField(
          hintText: "请填写姓名",
          tec: TextEditingController(),
        ),
      ],
    );
  }
}
