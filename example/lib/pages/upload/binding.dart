import 'package:get/get.dart';

import 'logic.dart';

class UploadBinding extends Bindings {
  @override
  void dependencies() {
    Get.lazyPut(() => UploadLogic());
  }
}
