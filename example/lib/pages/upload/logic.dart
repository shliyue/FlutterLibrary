import 'package:zyocore/entitys/aliyun_entity.dart';
import 'package:zyocore/widget/assets_picker/ly_assets_picker.dart';
import 'package:zyocore/zyocore.dart';

import 'state.dart';

class UploadLogic extends GetxController {
  final UploadState state = UploadState();

  @override
  void onInit() {
    // TODO: implement onInit
    super.onInit();
  }

  static Future<AliyunEntity?> getAliToken(String policy) async {
    var query = {"policy": policy};
    var response = await ZyoCoreHttpClient(dioConfig: HttpConfig(baseUrl: "https://oapi2.zyosoft.cn/push/")).get("api/oss/aliyunToken", queryParameters: query);
    if (response.data != null) {
      AliyunEntity rs = AliyunEntity.fromJson(response.data);
      return rs;
    }
    return null;
  }

  void onBtnClick(int id) {
    switch (id) {
      case 1:
      case 2:
        selectFile(id);
        break;
      case 3: //上传
        uploadAli();
        break;
    }
  }

  onSelectPicker() async {
    LYSheetUtil.showSelectFile(Get.context!, callback: (v) async {
      selectFile(v == "photo" ? 1 : 2);
    });
  }

  Future<void> selectFile(int id) async {
    var listRs = await LyAssetsPicker.pickAssets(Get.context!, requestType: id == 1 ? RequestType.image : RequestType.video);
    if(listRs != null){
      state.list.value = listRs;
    }
  }

  Future<void> uploadAli() async {
    if (state.list.isNotEmpty) {
      AliyunEntity? entity = await getAliToken("ybt-album");
      var rs = await LyUploadUtil.uploadAliYunEntity(
          entity: entity,
          assets: state.list,
          sendProgressF: (v) {
            int p = (v * 100).truncate();
            if (p == 100) {
              ToastUtil.dismiss();
            } else {
              ToastUtil.loading("文件上传中...$p");
            }
          });
      if (rs.isNotEmpty) {
        ToastUtil.showToast("成功上传${rs.length}个文件");
      }
    } else {
      ToastUtil.showToast("请选择需要上传的文件");
    }
  }
}
