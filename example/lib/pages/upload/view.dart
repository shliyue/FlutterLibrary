import 'package:flutter/material.dart';
import 'package:zyocore/zyocore.dart';

import 'logic.dart';

class UploadPage extends GetView<UploadLogic> {
  const UploadPage({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: LYAppBar.getAppBar(title: '上传文件'),
      body: _getContent(),
    );
  }

  ///内容
  Widget _getContent() {
    return Column(
      crossAxisAlignment: CrossAxisAlignment.start,
      children: [
        Wrap(
          spacing: 5,
          runSpacing: 5,
          children: [
            Obx(() {
              return LyUploadImageWidget(
                images: controller.state.list.value,
                addCallBack: () {
                  controller.onSelectPicker();
                },
                delCallBack: (v) {
                  controller.state.list.removeAt(v);
                },
              );
            }),
            _buildBtn("选择图片", 1),
            _buildBtn("选择视频", 2),
          ],
        ),
        _buildBtn("上传阿里云", 3),
      ],
    );
  }

  Widget _buildBtn(String text, int id) {
    return MaterialButton(
      child: Text(text),
      textColor: Colors.white,
      color: Colors.blue,
      onPressed: () {
        controller.onBtnClick(id);
      },
    );
  }
}
