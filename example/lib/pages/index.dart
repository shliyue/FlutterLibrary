import 'package:example/router.dart';
import 'package:flutter/material.dart';
import 'package:zyocore/widget/common/animate/lottie_widget.dart';
import 'package:zyocore/zyocore.dart';
import 'package:url_launcher/url_launcher.dart';

class IndexBinding extends Bindings {
  @override
  void dependencies() {
    Get.lazyPut(() => IndexController());
  }
}

class IndexController extends GetxController {
  @override
  void onInit() {
    // TODO: implement onInit
    super.onInit();
    lySp.init();
  }
}

class IndexPage extends GetView<IndexController> {
  IndexPage({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: LYAppBar.getAppBar(title: '主界面'),
      body: _builWrap(),
    );
  }

  Widget _buildBtn(String name) {
    return MaterialButton(
        child: Text(name),
        textColor: Colors.white,
        color: Colors.blue,
        onPressed: () async {
          LyAudioUtil.instance.playAudio("audios/btn_click.wav", soloPlay: true);
          String router = "";
          switch (name) {
            case "Dialog":
              router = RouterPageConfig.dialogPage;
              break;
            case "button":
              router = RouterPageConfig.buttonPage;
              break;
            case "Action Sheet":
              router = RouterPageConfig.actionSheet;
              break;
            case "Picker":
              router = RouterPageConfig.pickerPage;
              break;
            case "Image":
              router = RouterPageConfig.imagePage;
              break;
            case "TextFeild":
              router = RouterPageConfig.textfeildPage;
              break;
            case "Native":
              router = RouterPageConfig.nativePage;
              break;
            case "Permission":
              router = RouterPageConfig.permissionPage;
              break;
            case "PlayVideo":
              router = RouterPageConfig.videoPage;
              break;
            case "腾讯会议":
              bool flag = await AppUtils.onLaunchApp("wemeet://page/inmeeting?meeting_code=48058116271");
              if (!flag) {
                ToastUtil.showToast('请先安装腾讯会议APP');
              }
              return;
              break;
            case "二维码扫描":
              // bool p = await LyPermission.cameras();
              // if (!p) return;
              // router = RouterPageConfig.scanQrPage;
              ScanUtil.openScanQr();
              return;
              break;
            case "上传文件":
              router = RouterPageConfig.uploadPage;
              break;
          }
          if (router.isNotEmpty) {
            var arg = {};
            Get.toNamed(router, arguments: arg);
          }
        });
  }

  //wemeet://page/inmeeting?meeting_code=48058116271

  Widget _builWrap() {
    return Column(
      crossAxisAlignment: CrossAxisAlignment.start,
      children: [
        LySearchWidget(),
        Wrap(
          spacing: 5,
          runSpacing: 5,
          children: [
            _buildBtn("Dialog"),
            _buildBtn("button"),
            _buildBtn("Action Sheet"),
            _buildBtn("Picker"),
            _buildBtn("Image"),
            _buildBtn("TextFeild"),
            _buildBtn("Native"),
            _buildBtn("Permission"),
            _buildBtn("PlayVideo"),
            _buildBtn("腾讯会议"),
            _buildBtn("二维码扫描"),
            _buildBtn("上传文件"),
            Stack(
              children: [
                LYText(text: "吃多少付款方结束啦"),
                LottieWidget(
                  file: "assets/animations/record.json",
                  width: 100.w,
                  height: 100.w,
                  boxShadow: [BoxShadow(color: Colors.red.withOpacity(0.74), offset: const Offset(2, 5), blurRadius: 15)],
                  radius: 50.w,
                  package: "",
                ),
                LottieWidget(
                  file: "assets/animations/play.json",
                  width: 100.w,
                  height: 50.w,
                  package: "",
                )
              ],
            ),
          ],
        ),
        Row(
          children: [
            Expanded(
              child: Container(
                color: Colors.red,
                child: const LYText(text: "的缴费开始两地分居老水电费水电费水电费士大夫士大夫师端"),
              ),
            ),
            Container(
              width: 200.w,
              height: 50.w,
              color: Colors.blue,
            )
          ],
        )
      ],
    );
  }
}
