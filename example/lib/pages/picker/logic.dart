import 'package:zyocore/widget/flutter_pickers/time_picker/model/pduration.dart';
import 'package:zyocore/zyocore.dart';

import 'state.dart';

class PickerLogic extends GetxController {
  final PickerState state = PickerState();

  @override
  void onInit() {
    // TODO: implement onInit
    super.onInit();
  }

  void onBtnClick(int id) {
    switch (id) {
      case 1:
        LYPickerUtil.showPickerWithList(Get.context!, list: ["111111", "222222"], callback: (v) {
          ToastUtil.showToast(v);
        });
        break;
      case 2:
        LYPickerUtil.showYearPicker(Get.context!, current: 2023, callback: (v) {
          ToastUtil.showToast(v.toString());
        });
        break;
      case 3:
        LYPickerUtil.showTypePicker(Get.context!, callback: (v) {
          ToastUtil.showToast(v.toString());
        });
        break;
      case 4:
        LYPickerUtil.showGradePicker(Get.context!, currentValue: "中班", callBack: (v) {
          ToastUtil.showToast(v.toString());
        });
        break;
      case 5:
        LYPickerUtil.showDatePicker(
          maxDate: PDuration.now(),
          onConfirm: (y, m, d) {
            ToastUtil.showToast("$y $m $d");
          },
        );
        break;
      case 6:
        LYPickerUtil.showDatePicker(
          mode: DateMode.YM,
          onConfirm: (y, m, d) {
            ToastUtil.showToast("$y $m $d");
          },
        );
        break;
      case 7:
        LYPickerUtil().showSelectorPicker(
          Get.context!,
          dataSource: ["111111111", "2222222222", "3333333333"],
          currentValue: "2222222222",
          callBack: (v) {},
        );
        break;
      case 8:
        // LYPickerUtil().showTopSheet(
        //   Get.context!,
        //   title: "存储权限使用说明:",
        //   content: "但是在 APP在调用终端敏感权限时，是弹出了权限说明弹框的，我们要怎么改才能符合规范呢",
        //   callBack: (v) {},
        // );
        LyPermission.photos();
        break;

    }
  }
}
