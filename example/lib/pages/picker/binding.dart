import 'package:get/get.dart';

import 'logic.dart';

class PickerBinding extends Bindings {
  @override
  void dependencies() {
    Get.lazyPut(() => PickerLogic());
  }
}
