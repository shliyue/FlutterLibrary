import 'package:flutter/material.dart';
import 'package:zyocore/zyocore.dart';

import 'logic.dart';

class PickerPage extends GetView<PickerLogic> {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: LYAppBar.getAppBar(title: 'Picker'),
      body: _getContent(),
    );
  }

  ///内容
  Widget _getContent() {
    var enable = false.obs;
    return Column(
      crossAxisAlignment: CrossAxisAlignment.start,
      children: [
        LYText(text: "showListPicker:",fontSize: 15.sp,fontWeight: FontWeight.bold),
        Wrap(
          spacing: 5,
          runSpacing: 5,
          children: [
            _buildBtn("showPickerWithList", 1),
            _buildBtn("showYearPicker", 2),
            _buildBtn("showTypePicker", 3),
            _buildBtn("showGradePicker", 4),
          ],
        ),
        LYText(text: "showDatePicker:",fontSize: 15.sp,fontWeight: FontWeight.bold),
        Wrap(
          spacing: 5,
          runSpacing: 5,
          children: [
            _buildBtn("showDatePicker", 5),
            _buildBtn("showYMDatePicker", 6),
            _buildBtn("showSelectorPicker", 7),
            _buildBtn("showTopSheet", 8),

          ],
        ),
      ],
    );
  }

  Widget _buildBtn(String text, int id) {
    return MaterialButton(
      child: Text(text),
      textColor: Colors.white,
      color: Colors.blue,
      onPressed: () {
        controller.onBtnClick(id);
      },
    );
  }
}
