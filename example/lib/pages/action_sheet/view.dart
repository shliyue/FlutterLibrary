import 'package:flutter/material.dart';
import 'package:zyocore/zyocore.dart';

import 'logic.dart';

class ActionSheetPage extends GetView<ActionSheetLogic> {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: LYAppBar.getAppBar(title: 'ActionSheet'),
      body: _getContent(),
    );
  }

  ///内容
  Widget _getContent() {
    var enable = false.obs;
    return Wrap(
      spacing: 5,
      runSpacing: 5,
      children: [
        _buildBtn("showModalBottomSheetUtils", 1),
        _buildBtn("showTimePicker", 2),
        _buildBtn("showWeekday", 3),
        _buildBtn("showPhoneCall", 4),
        _buildBtn("showBottomSheet", 5),
        _buildBtn("showListSheet", 6),
        _buildBtn("showSelectFile", 7),
        _buildBtn("showTextSheet", 8),
        _buildBtn("showSchoolTimelineOption", 9),
        _buildBtn("showPage", 10),
        _buildBtn("showCustomPage", 11),
        _buildBtn("showNoticeStatisticDetail", 12),
        _buildBtn("showShareToolBar", 13),
        _buildBtn("showBottomSheet", 14),
        _buildBtn("showPageSheet", 15),
      ],
    );
  }

  Widget _buildBtn(String text, int id) {
    return MaterialButton(
      child: Text(text),
      textColor: Colors.white,
      color: Colors.blue,
      onPressed: () {
        switch (id) {
          case 1:
            LYSheetUtil.showBaseModalBottomSheet(isDismissible: true, child: Container(height: 200, color: Colors.red, child: Text("111111111")));
            break;
          case 2:
            LYSheetUtil.showTimePicker("01:01:01", "23:59:59");
            break;
          case 3:
            LYSheetUtil.showWeekday();
            break;
          case 4:
            LYSheetUtil.showPhoneCall(data: ["13524500465", "13524500462"]);
            break;
          case 5:
            LYSheetUtil.showDefaultBottomSheet(child: Container(height: 200, color: Colors.red, child: Text("111111111")));
            break;
          case 6:
            LYSheetUtil.showListSheet(Get.context!, data: ["11111111", "222222"], callback: (v) {});
            break;
          case 7:
            LYSheetUtil.showSelectFile(Get.context!, callback: (v) {});
            break;
          case 8:
            LYSheetUtil.showTextSheet(Get.context!, text: "2132131", callback: () {});
            break;
          case 9:
            LYSheetUtil.showSchoolTimelineOption(Get.context!, text: "11111", desc: "333333", callback: () {});
            break;
          case 10:
            LYSheetUtil.showPage(Get.context!, page: Text("11111"), callback: () {});
            break;
          case 11:
            LYSheetUtil.showCustomPage(Get.context!, page: Text("11111"), callback: () {});
            break;
          case 12:
            LYSheetUtil.showNoticeStatisticDetail(Get.context!, page: Text("11111"), callback: () {});
            break;
          case 13:
            LYSheetUtil.showShareToolBar(Get.context!, itemIcons: [ShareItem(event: "test", title: "测试", iconName: "image1.jpg", package: "")], callback: (v) {
              ToastUtil.showToast(v);
            });
            break;
          case 14:
            LYSheetUtil.showBottomSheet(context: Get.context!, titleList: ["111111", "222222"]);
            break;
          case 15:
            LYSheetUtil.showPageSheet(
              Get.context!,
              title: "选择背景音乐",
              child: const Text("选项"),
              mainAxisAlignment: MainAxisAlignment.center,
            );
            break;
        }
      },
    );
  }
}
