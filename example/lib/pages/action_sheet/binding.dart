import 'package:get/get.dart';

import 'logic.dart';

class ActionSheetBinding extends Bindings {
  @override
  void dependencies() {
    Get.lazyPut(() => ActionSheetLogic());
  }
}
