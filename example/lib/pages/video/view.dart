import 'package:flutter/material.dart';
import 'package:zyocore/zyocore.dart';

import 'logic.dart';

class VideoPage extends GetView<VideoLogic> {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: LYAppBar.getAppBar(title: '视频'),
      body: _getContent(),
    );
  }

  ///内容
  Widget _getContent() {
    return Column(
      children: [
        _buildBtn("播放", 0),
        // Container(
        //   color: Colors.black,
        //   alignment: Alignment.center,
        //   child: AspectRatio(
        //     aspectRatio: controller.state.videoPlayerController!.value.aspectRatio,
        //     child: VideoPlayer(controller.state.videoPlayerController!),
        //   ),
        // ),
        Obx(() {
          return LYVideoPlayer(
            url: controller.state.url.value,
            onInitialize: (v) {
              controller.state.videoPlayerController = v;
            },
          );
        }),
      ],
    );
  }

  Widget _buildBtn(String text, int id) {
    return LyButton(
      margin: EdgeInsets.only(bottom: 10.w),
      height: 44.w,
      bgColor: Colors.blue,
      radius: 50.w,
      btnTitle: text,
      textColor: Colors.white,
      onTap: () {
        controller.onBtnClick(id);
      },
    );
  }
}
