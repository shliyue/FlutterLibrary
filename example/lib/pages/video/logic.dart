import 'package:zyocore/util/cache_manager_util.dart';
import 'package:zyocore/widget/ly_avatar/ly_cache_manager.dart';
import 'package:zyocore/zyocore.dart';
import 'package:audioplayers/audioplayers.dart';

import 'state.dart';

class VideoLogic extends GetxController {
  final VideoState state = VideoState();

  @override
  void onInit() {
    // TODO: implement onInit
    super.onInit();
    // var url = "https://qiniu-assets.zyosoft.cn/qyx/video/py/know-i_hd.m3u8";
    // state.url.value = "https://download.zyosoft.cn/qyx/L6/book1/a1-yuedu.mp4";
    state.url.value = "https://download.zyosoft.cn/qyx/L6/book4/a5-yuedu.mp4";
    initVideoPlayerController();
  }

  @override
  void onClose() {
    Logger.d('ren onClose--------------------');
    state.videoPlayerController?.dispose();
    state.videoPlayerController = null;
    super.onClose();
  }

  initVideoPlayerController() async {

    // var f = await CacheManagerUtil().getFileFromCache(state.url.value);
    // Logger.d("++++++++++++++++++44444++++++++++++++");
    // bool flag = await LyPermission.storage();
    // var file = await CacheManagerUtil().getSingleFile(state.url.value);
    // var dir = await CacheUtil.getTempDir();
    //
    // Logger.d("++++++++++++++${dir.path}++++++++++++++++++");
    // Logger.d("++++++++++++++++++++++++++++++++");
    // Logger.d(file.path);
  }

  Future<void> onBtnClick(int id) async {
    switch (id) {
      case 0:
        if (state.videoPlayerController == null) return;
        LyAudioUtil.instance.playAudio("audios/btn_click.wav", soloPlay: true, completeCallBack: () {
          if (state.videoPlayerController!.value.isPlaying) {
            state.videoPlayerController!.pause();
          } else {
            Logger.d('+++++++++++++videoPlayerController 播放》》》》》》》》》》》');
            state.videoPlayerController!.play();
          }
        });
        break;
    }
  }
}
