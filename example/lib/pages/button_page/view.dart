import 'package:flutter/material.dart';
import 'package:zyocore/core/configs/config.dart';
import 'package:zyocore/values/style.dart';
import 'package:zyocore/zyocore.dart';

import 'logic.dart';

class ButtonPagePage extends GetView<ButtonPageLogic> {
  const ButtonPagePage({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Visibility(
      visible: controller.state.isShow.value,
      child: Scaffold(
        appBar: LYAppBar.getAppBar(title: 'Button'),
        body: Container(
          color: Colors.white,
          child: _getContent(),
        ),
      ),
    );
  }

  ///内容
  Widget _getContent() {
    var enable = false.obs;
    return Wrap(
      spacing: 5,
      runSpacing: 5,
      children: [
        LYButtonUtil.fullScreenButton(
            text: "fullScreenButton",
            iconWidget: const Icon(Icons.home),
            num: 10,
            radius: BorderRadius.all(Radius.circular(40.r)),
            enable: true,
            bgColor: Colors.white,
            fontStyle: TextStyle(color: Colors.black)),
        LYButtonUtil.gradientButton(text: "gradient"),
        LYButtonUtil.generalButton(text: "general1", icon: const Icon(Icons.home), width: 150.w),
        LYButtonUtil.generalButton(
          textWidget: LYText.withStyle("-", style: TextStyle(fontSize: 24.sp, color: null, fontFamily: "")),
          width: 34.w,
          height: 26.w,
          radius: 5.r,
          bgColor: lightF9F9F9,
          borderColor: lightF9F9F9,
          onTap: () {},
        ),
        LYButtonUtil.generalButton(
            textWidget: LYText.withStyle("+", style: TextStyle(fontSize: 18.sp, fontFamily: "")),
            // Text("+", style: TextStyle(fontSize: 18.sp), strutStyle: const StrutStyle(leading: 0.2)),
            width: 34.w,
            height: 26.w,
            radius: 5.r,
            bgColor: lightF9F9F9,
            borderColor: lightF9F9F9,
            onTap: () {}),
        LYButtonUtil.textButton(text: "textButton", image: "icon_right.png", width: 10.w, height: 10.w,package: Config.packageName),
        LYButtonUtil.imgTextVerticalButton(text: "imgTextVerticalButton", icon: "icon_share_SESSION.png", rightSub: "new_tag_icon.png",package: Config.packageName),
        LYButtonUtil.iconButton(icon: "icon_ola_home_address", width: 20.w, height: 20.w),
        LYButtonUtil.shadowButton(text: "shadowButton", fontSize: AppFontSize.fontSize15, color: Color(0xFFF1EEFF), highlightColor: Color(0xFF9F89FF), radius: 28.r, onTap: () {
          LyAudioUtil.instance.playAudio("audios/btn_click.wav", soloPlay: true);
        }),
        LYButtonUtil.iconShadowButton(
            icon: Image.asset('assets/images/icon_share_SESSION.png', width: 32.w, height: 32.w,package: Config.packageName,),
            text: "iconShadowButton",
            fontSize: AppFontSize.fontSize15,
            color: Color(0xFFF1EEFF),
            highlightColor: Color(0xFF9F89FF),
            radius: 28.r,
            onTap: () {}),
        LYButtonUtil.backButton(),
        LYButtonUtil.generalButton(
            text: "g2",
            width: 282.w,
            height: 56.w,
            borderRadius: BorderRadius.only(bottomLeft: Radius.circular(8.r), bottomRight: Radius.circular(8.r)),
            bgColor: lightF8F8F8,
            textStyle: TextStyle(
              color: dark333333,
              fontWeight: AppFontWeight.medium,
              fontSize: AppFontSize.fontSize16.sp,
            ),
            onTap: () {}),
        LYButtonUtil.textButton(
          text: "textButton",
          fontStyle: TextStyle(
            color: Color(0xFF9F89FF),
            fontWeight: AppFontWeight.medium,
            fontSize: AppFontSize.fontSize17.sp,
          ),
          width: 40.w,
          height: 24.w,
        ),
        LYButtonUtil.generalButton(
            text: "g3",
            width: 102.w,
            height: 32.w,
            border: Border.all(color: const Color(0xFF9F89FF), width: 1.w),
            borderRadius: BorderRadius.all(Radius.circular(16.w)),
            bgColor: Colors.transparent,
            textStyle: TextStyle(
              color: const Color(0xFF9F89FF),
              fontWeight: AppFontWeight.normal,
              fontSize: AppFontSize.fontSize14.sp,
            ),
            onTap: () {}),

        LYButtonUtil.generalButton(
            textWidget: Text("g4",
                style: TextStyle(
                  color: lightFFFFFF,
                  fontWeight: AppFontWeight.normal,
                  fontSize: AppFontSize.fontSize12.sp,
                ),
                strutStyle: const StrutStyle(leading: 0.2)),
            alignment: Alignment.centerLeft,
            padding: EdgeInsets.only(left: 18.w),
            width: 103.w,
            height: 32.w,
            borderRadius: BorderRadius.only(
              topLeft: Radius.circular(100.w),
              bottomLeft: Radius.circular(100.w),
            ),
            gradient: LinearGradient(
              begin: Alignment.centerLeft,
              end: Alignment.centerRight,
              colors: [Color(0xFFEFC7FF), Color(0xFF9F89FF)],
            ),
            boxShadow: [BoxShadow(color: lightE1E1E1.withOpacity(0.74), offset: const Offset(2, 9), blurRadius: 15)],
            onTap: () {}),
        LYButtonUtil.generalButton(
            text: "g5",
            width: 48.w,
            height: 22.w,
            bgColor: Color(0xFF9F89FF),
            borderRadius: BorderRadius.circular(11.w),
            textStyle: TextStyle(
              color: lightFFFFFF,
              fontWeight: AppFontWeight.semiBold,
              fontSize: AppFontSize.fontSize12.sp,
            ),
            onTap: () {}),
        LYButtonUtil.generalButton(
          text: "g6",
          width: 42.w,
          height: 27.w,
          bgColor: lightEEEEEE,
          borderRadius: BorderRadius.circular(5.w),
          textStyle: TextStyle(
            color: dark1A1A1A,
            fontWeight: AppFontWeight.normal,
            fontSize: AppFontSize.fontSize12.sp,
          ),
          onTap: () {},
        ),
        LYButtonUtil.generalButton(
          height: 24.w,
          width: 50.w,
          text: "g7",
          bgColor: Colors.transparent,
          border: Border.all(
            width: 1.w,
            color: Color(0xFF9F89FF),
          ),
          borderRadius: BorderRadius.all(Radius.circular(16.w)),
          textStyle: TextStyle(
            color: Color(0xFF9F89FF),
            fontSize: 14.sp,
            fontWeight: FontWeight.w400,
          ),
          onTap: () {},
        ),
        LYButtonUtil.generalButton(
          height: 32.w,
          width: 90.w,
          text: "general8",
          alignment: Alignment.centerLeft,
          icon: Image.asset('assets/images/icon_share_SESSION.png', width: 22.w, height: 22.w,package: Config.packageName,),
          padding: EdgeInsets.only(
            left: 9.w,
          ),
          border: Border.all(
            width: 1.w,
            color: Color(0xFF9F89FF),
          ),
          borderRadius: BorderRadius.only(
            topLeft: Radius.circular(100.w),
            bottomLeft: Radius.circular(100.w),
          ),
          bgColor: lightFFFFFF,
          textStyle: TextStyle(fontSize: AppFontSize.fontSize12.sp, fontWeight: AppFontWeight.normal, color: Color(0xFF9F89FF)),
          boxShadow: [BoxShadow(color: lightE1E1E1.withOpacity(0.93), offset: const Offset(2, 2), blurRadius: 15)],
          onTap: () {},
        ),
        LYButtonUtil.shadowButton(
          text: 'shadowButton',
          width: 212.w,
          height: 44.w,
          onTap: () {},
        ),
        LYButtonUtil.fullScreenButton(
            text: 'fullScreenButton',
            bgColor: Colors.transparent,
            radius: BorderRadius.all(Radius.circular(22.r)),
            fontStyle: TextStyle(color: Color(0xFF9F89FF), fontSize: 15.sp, fontWeight: FontWeight.normal, fontFamily: AppFontFamily.pingFangMedium),
            onTap: () {}),
        LYButtonUtil.generalButton(
            text: "general9",
            width: 132.w,
            height: 44.w,
            bgColor: Color(0xFF9F89FF),
            fontSize: 17.sp,
            fontFamily: AppFontFamily.pingFangMedium,
            borderRadius: BorderRadius.circular(22.w),
            onTap: () {}),
        LYButtonUtil.enableFSButton(
            text: 'enableFSButton',
            enable: enable.value,
            onTap: () {
              enable.value = !enable.value;
            }),
        // LyButtonBorderWidget()
      ],
    );
  }
}
