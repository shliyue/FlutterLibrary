import 'package:get/get.dart';

import 'logic.dart';

class ButtonPageBinding extends Bindings {
  @override
  void dependencies() {
    Get.lazyPut(() => ButtonPageLogic());
  }
}
