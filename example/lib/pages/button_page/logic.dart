import 'package:get/get.dart';
import 'package:zyocore/util/ly_audio_util.dart';

import 'state.dart';

class ButtonPageLogic extends GetxController {
  final ButtonPageState state = ButtonPageState();

  @override
  void onInit() {
    // TODO: implement onInit
    super.onInit();
    playBgm();
  }

  void playBgm(){
    // audios/badge_get.mp3
    LyAudioUtil.instance.playAudio("https://download.zyosoft.cn/static/music/Butterfly.mp3");
    // LyAudioUtil.instance.playAudio(path: "audios/diao.mp3");
  }

  @override
  void onClose() {
    // TODO: implement onClose
    super.onClose();
    LyAudioUtil.instance.dispose();
  }
}
