import 'dart:async';
import 'package:flutter/material.dart';
import 'package:zyocore/values/style.dart';
import 'package:zyocore/values/text.dart';
import 'package:zyocore/zyocore.dart';
import 'state.dart';
import 'package:zyocore/util/lib/date_util.dart' as ty;

class DialogLogic extends GetxController {
  final DialogState state = DialogState();

  late StreamSubscription subscription;

  @override
  void onInit() {
    super.onInit();
  }

  void showConfirmation() {
    LYDialogUtil.showConfirmation(
      Get.context!,
      content: "显示showConfirmation",
      callback: () {
        ToastUtil.showToast("完成");
      },
    );
  }

  void showInputDialog() {
    LYDialogUtil.showInputDialog(Get.context!, title: '添加称谓', hinText: '请输入自定义称谓', callback: (v) {});
  }

  void showDownloadProgress() {
    LYDialogUtil.showDownloadProgress(
      Get.context!,
      fileSrc: "https://qiniu-sybaby.zyosoft.cn/23091314495201700105_1080X1512.jpg",
      // isOriginal: true,
    );
  }

  void showStudAvatarDialog(bool showClose) {
    LYDialogUtil.showTopBgDialog(
      Get.context!,
      avatar: "https://qiniu-sybaby.zyosoft.cn/23091314495201700105_1080X1512.jpg",
      showCloseBtn: showClose,
      callback: () {},
    );
  }

  void showMediaPreview() {
    LYDialogUtil.showMediaPreview(
      Get.context!,
      files: ["https://qiniu-sybaby.zyosoft.cn/23091314495201700105_1080X1512.jpg", "https://qiniu-sybaby.zyosoft.cn/23091314495201700105_1080X1512.jpg"],
      // isOriginal: true,
    );
  }

  void showInfo() {
    LYDialogUtil.showInfo(
      Get.context!,
      content: "基础dialog",
    );
  }

  void showConfirm() {
    // LYDialogUtil.showConfirmation(
    //   Get.context!,
    //   content: '权限被拒绝，请去设置中设置权限',
    //   confirmBtn: LYText(
    //     fontSize: 17.sp,
    //     text: "去设置",
    //     color: LibColor.colorMain,
    //     fontFamily: AppFontFamily.pingFangMedium,
    //   ),
    //   routeName: 'lyPermissionSetting.dialog',
    //   callback: () {
    //     PhotoManager.openSetting();
    //     ToastUtil.showToast("确认");
    //   },
    //   closeCallBack: () {
    //     Get.back();
    //     ToastUtil.showToast("取消");
    //   },
    // );

    LYDialogUtil.showConfirmation(
      Get.context!,
      titleWidget: LYText(text: "确认使用", fontSize: 17.sp, fontFamily: AppFontFamily.pingFangMedium),
      contentWidget: RichText(
        text: TextSpan(children: [
          TextSpan(
            text: '此授权使用后',
            style: text15.textColor(dark333333),
          ),
          TextSpan(
            text: '无法撤回',
            style: text15.red,
          ),
          TextSpan(
            text: '，系统重装后机器码会更改，故机器码只保证在当前机器的当前系统使用，',
            style: text15.textColor(dark333333),
          ),
          TextSpan(
            text: '更换系统后需重新购买授权！',
            style: text15.red,
          ),
        ]),
      ),
      rightBtnFont: '确认使用',
      routeName: 'qr_pwd_use',
      callback: () async {},
    );
  }

  void showLoading() {
    LYDialogUtil.showLoading(bgColor: Colors.blueGrey.withOpacity(0.3));
    Future.delayed(const Duration(milliseconds: 3000), () {
      LYDialogUtil.close();
    });
  }

  void showConfirmTitle() {
    LYDialogUtil.showConfirmation(
      Get.context!,
      titleWidget: LYText(
        text: '核销后无法撤回，请确认是否核销该优惠券？',
        fontSize: 17.sp,
        maxLines: 3,
        fontFamily: AppFontFamily.pingFangMedium,
      ),
      contentWidget: _chargeOffDialogContetn(),
      rightBtnFont: '确定核销',
      rightColor: redE02020,
      leftBtnFont: "暂不核销",
      callback: () async {
        // requestChargeOff(item);
      },
    );
  }

  Widget _chargeOffDialogContetn() {
    return Column(
      crossAxisAlignment: CrossAxisAlignment.start,
      children: const [
        LYText(text: '核销后无法撤回，213313'),
        LYText(text: '核销后无法撤回，545435？'),
      ],
    );
  }

  void showLightDialog() {
    LYDialogUtil.showTopBgDialog(Get.context!, title: "提示", btnText: "我知道了");
  }
}
