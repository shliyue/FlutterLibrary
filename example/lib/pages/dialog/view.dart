import 'package:flutter/material.dart';
import 'package:zyocore/zyocore.dart';
import 'logic.dart';

///订单预览
class DialogPage extends GetView<DialogLogic> {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: LYAppBar.getAppBar(title: 'Dialog'),
      body: Container(
        color: Colors.white,
        child: _getContent(),
      ),
    );
  }

  ///内容
  Widget _getContent() {
    return Wrap(
      spacing: 5,
      runSpacing: 5,
      children: [
        _buildBtn("showConfirmation", 1),
        _buildBtn("showConfirm", 9),
        _buildBtn("showConfirm title", 10),
        _buildBtn("showInputDialog", 2),
        _buildBtn("showDownloadProgress", 3),
        _buildBtn("showTopBgDialog", 4),
        _buildBtn("showTopBgDialog close", 5),
        _buildBtn("showTopBgDialog  title", 6),
        _buildBtn("showMediaPreview", 7),
        _buildBtn("showInfo", 8),
        _buildBtn("showLoading", 11),
      ],
    );
  }

  Widget _buildBtn(String text, int id) {
    return MaterialButton(
      child: Text(text),
      textColor: Colors.white,
      color: Colors.blue,
      onPressed: () {
        switch (id) {
          case 1:
            controller.showConfirmation();
            break;
          case 2:
            controller.showInputDialog();
            break;
          case 3:
            controller.showDownloadProgress();
            break;
          case 4:
            controller.showStudAvatarDialog(false);
            break;
          case 5:
            controller.showStudAvatarDialog(true);
            break;
          case 6:
            controller.showLightDialog();
            break;
          case 7:
            controller.showMediaPreview();
            break;
          case 8:
            controller.showInfo();
            break;
          case 9:
            controller.showConfirm();
            break;
          case 10:
            controller.showConfirmTitle();
            break;
          case 11:
            controller.showLoading();
            break;
        }
      },
    );
  }
}
