import 'package:get/get.dart';

import 'logic.dart';

class DialogBinding extends Bindings {
  @override
  void dependencies() {
    Get.lazyPut(() => DialogLogic());
  }
}
