import 'package:zyocore/util/native/ly_native_util.dart';
import 'package:zyocore/zyocore.dart';

import 'state.dart';

class NativeLogic extends GetxController {
  final NativeState state = NativeState();

  @override
  void onInit() {
    // TODO: implement onInit
    super.onInit();
    getNative();
  }

  Future<void> getNative() async {
    state.versionInfo.value = (await LyNativeUtil().getPlatformVersion()) ?? "";
    state.isPad.value = await LyNativeUtil().checkIsPad();
  }

  void onBtnClick(int id) {}
}
