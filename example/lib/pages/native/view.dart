import 'package:flutter/material.dart';
import 'package:zyocore/zyocore.dart';

import 'logic.dart';

class NativePage extends GetView<NativeLogic> {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: LYAppBar.getAppBar(title: '原生方法'),
      body: _getContent(),
    );
  }

  ///内容
  Widget _getContent() {
    var enable = false.obs;
    return Obx(() {
      return Column(
        crossAxisAlignment: CrossAxisAlignment.start,
        children: [
          LYText(text: "版本信息：${controller.state.versionInfo.value}"),
          LYText(text: "是否pad：${controller.state.isPad.value}"),
        ],
      );
    });
  }

  Widget _buildBtn(String text, int id) {
    return MaterialButton(
      child: Text(text),
      textColor: Colors.white,
      color: Colors.blue,
      onPressed: () {
        controller.onBtnClick(id);
      },
    );
  }
}
