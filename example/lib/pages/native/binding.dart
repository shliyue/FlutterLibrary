import 'package:get/get.dart';

import 'logic.dart';

class NativeBinding extends Bindings {
  @override
  void dependencies() {
    Get.lazyPut(() => NativeLogic());
  }
}
