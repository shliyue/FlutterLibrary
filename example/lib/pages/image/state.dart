import 'dart:io';

import 'package:zyocore/widget/assets_picker/entity/ly_asset_entity.dart';
import 'package:zyocore/zyocore.dart';

class ImageState {
  ImageState() {
    ///Initialize variables
  }

  var imgUrl = "https://qiniu-sybaby.zyosoft.cn/23091314495201700105_1080X1512.jpg".obs;
  var radius = 0.0.obs;
  var imgWidth = 375.w.obs;
  var imgHeight = 300.w.obs;
  var isRadius = false.obs;
  LyAssetEntity? assetEntity;
  var compressImg = "".obs;
  Directory? directory;
}
