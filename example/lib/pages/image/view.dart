import 'package:flutter/material.dart';
import 'package:zyocore/widget/radio/LYRadioWidget.dart';
import 'package:zyocore/zyocore.dart';
import 'logic.dart';

///图片组件
class ImagePage extends GetView<ImageLogic> {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: LYAppBar.getAppBar(title: 'Image组件'),
      body: SingleChildScrollView(
        child: Container(
          color: Colors.white,
          child: _getContent(),
        ),
      ),
    );
  }

  ///内容
  Widget _getContent() {
    return Column(
      crossAxisAlignment: CrossAxisAlignment.start,
      children: [
        const LYText(text: "LYImageWidget："),
        Obx(() {
          return LYImageWidget(
            bgColor: Colors.grey,
            src: controller.state.imgUrl.value,
            fit: controller.state.isRadius.value ? BoxFit.cover : BoxFit.contain,
            width: controller.state.imgWidth.value,
            height: controller.state.imgHeight.value,
            radius: controller.state.radius.value,
            assetEntity: controller.state.assetEntity,
            isOriginal: true,
          );
        }),
        const LYText(text: "压缩图片："),
        Obx(() {
          return LYImageWidget(
            bgColor: Colors.grey,
            src: controller.state.compressImg.value,
            fit: BoxFit.contain,
            width: controller.state.imgWidth.value,
            height: controller.state.imgHeight.value,
          );
        }),
        SizedBox(height: 20.w),
        LYRadioWidget(
            defaultSelect: controller.state.isRadius.value,
            callBack: (v) {
              controller.state.isRadius.value = v;
              controller.btnClick(2);
            }),
        Wrap(
          spacing: 5,
          runSpacing: 5,
          children: [
            _buildBtn("网络图片", 0),
            _buildBtn("本地图片", 1),
            _buildBtn("选择图片", 3),
            _buildBtn("图片压缩", 4),
            // _buildBtn("上传阿里云", 5),
          ],
        ),
        LYImageWidget(width: 40.w, radius: 20.w, height: 40.w, src: controller.state.imgUrl.value),
        SizedBox(height: 20.w),
        const LYText(text: "LYAssetsImage："),
        Wrap(
          spacing: 5,
          runSpacing: 5,
          children: [
            Image(image: zuAssetImage("icon_share_SESSION1.png")),
            LYAssetsImage(
              width: 12.w,
              height: 8.w,
              name: 'icon_bar_down.png',
            ),
            LYAssetsImage(
              name: "icon_share_SESSION1.png",
              width: 100.w,
              height: 100.w,
              package: "",
            ),
            LYAssetsImage(
              name: "icon_right1.png",
              width: 50.w,
              color: Colors.red,
              package: "",
            ),
            LYAssetsImage(
              name: "image1.jpg",
              height: 50.w,
              width: 50.w,
              radius: 50.w,
              fit: BoxFit.cover,
              package: "",
              onTap: () {
                ToastUtil.showToast("图片点击");
              },
            ),
          ],
        ),
      ],
    );
  }

  Widget _buildBtn(String text, int id) {
    return MaterialButton(
      child: Text(text),
      textColor: Colors.white,
      color: Colors.blue,
      onPressed: () {
        controller.btnClick(id);
      },
    );
  }
}
