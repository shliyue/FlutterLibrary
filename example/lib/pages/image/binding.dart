import 'package:get/get.dart';

import 'logic.dart';

class ImageBinding extends Bindings {
  @override
  void dependencies() {
    Get.lazyPut(() => ImageLogic());
  }
}
