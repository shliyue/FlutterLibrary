import 'dart:async';
import 'dart:io';
import 'dart:typed_data';
import 'package:zyocore/entitys/aliyun_entity.dart';
import 'package:zyocore/widget/assets_picker/entity/ly_asset_entity.dart';
import 'package:zyocore/widget/assets_picker/ly_assets_picker.dart';
import 'package:zyocore/zyocore.dart';
import 'state.dart';
import 'package:image/image.dart' as img;
import 'package:flutter_image_compress/flutter_image_compress.dart';

class ImageLogic extends GetxController {
  final ImageState state = ImageState();

  late StreamSubscription subscription;

  @override
  void onInit() {
    super.onInit();
    state.compressImg.value = "/data/user/0/com.example.example/cache/compress/20240409141446292.jpg";
  }

  @override
  void dispose() {
    // TODO: implement dispose
    super.dispose();
    if (state.directory != null) {
      if (state.directory!.existsSync()) {
        state.directory!.deleteSync(recursive: true);
      }
    }
  }

  void initImage() {
    state.radius.value = 0;
    state.imgWidth.value = 375.w;
    state.imgHeight.value = 300.w;
  }

  Future<void> btnClick(int id) async {
    switch (id) {
      case 0:
        // state.imgUrl.value = "https://qiniu-sybaby.zyosoft.cn/23091314495201700105_1080X1512.jpg";
        // state.imgUrl.value = "https://qiniu-sybaby.zyosoft.cn/23042010515909290337_2000X1500.dng";
        state.imgUrl.value = "https://ali-sybaby.zyosoft.cn/album/24092413093303050787_5712X4284.HEIC?x-oss-process=image/resize,w_100";
        break;
      case 1:
        state.imgUrl.value = "assets/images/image1.jpg";
        break;
      case 2:
        if (state.isRadius.value) {
          state.imgWidth.value = 100.w;
          state.imgHeight.value = 100.w;
          state.radius.value = 50.w;
        } else {
          initImage();
        }
        break;
      case 3:
        var listRs = await LyAssetsPicker.pickAssets(Get.context!, appendAllowTypes: ["dng"]);

        if (listRs != null && listRs.isNotEmpty) {
          state.assetEntity = listRs[0];
          state.imgUrl.value = "";
        }
        break;
      case 4:
        if (state.assetEntity != null) {
          final inputFile = await state.assetEntity!.file;
          var orifile = await inputFile!.stat();
          print("++++++++++++原图图片信息+++++++:${orifile.size},${orifile.type}");
          state.directory = await FileUtil.getUploadCachePath(dicName: "compress");
          String oupFilePath = state.directory!.path + "/${DateUtil.formatDate(DateTime.now(), format: "yyyyMMddHHmmssS")}.jpg";
          print("++++++++++++压缩图片路径+++++++:$oupFilePath");

          await flutterImageCompress(inputFile, oupFilePath);
          state.compressImg.value = oupFilePath;
        } else {
          ToastUtil.showToast("请先选择本地图片");
        }
        break;
    }
  }

  Future<void> flutterImageCompress(File oriFile, String outFilePath) async {
    // 压缩图片到JPEG格式
    final compressedFile = await FlutterImageCompress.compressAndGetFile(
      oriFile.path,
      outFilePath,
      format: CompressFormat.jpeg,
      quality: 90,
    );
    if (compressedFile != null) {
      var file = await compressedFile.length();
      print("++++++++++++压缩图片信息+++++++:$file");
    }
  }

  Future<void> imageCompress(File oriFile, String oupFilePath) async {
    Uint8List uint8list = await oriFile.readAsBytes();
    final outputFile = File(oupFilePath);
    final image = img.decodeImage(uint8list);
    await outputFile.writeAsBytes(img.encodeJpg(image!, quality: 90, chroma: img.JpegChroma.yuv420)); // 转换为JPG格式
    var file = await outputFile.stat();
    print("++++++++++++压缩图片信息+++++++:${file.size},${file.type}");
  }
}
