import 'package:get/get.dart';

import 'logic.dart';

class PermissionsBinding extends Bindings {
  @override
  void dependencies() {
    Get.lazyPut(() => PermissionsLogic());
  }
}
