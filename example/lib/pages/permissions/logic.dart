import 'package:zyocore/zyocore.dart';

import 'state.dart';

class PermissionsLogic extends GetxController {
  final PermissionsState state = PermissionsState();

  @override
  void onInit() {
    // TODO: implement onInit
    super.onInit();
  }

  Future<void> onBtnClick(int id) async {
    RequestType type = RequestType.all;
    switch (id) {
      case 0:
        type = RequestType.all;
        break;
      case 1:
        type = RequestType.image;
        break;
      case 2:
        type = RequestType.video;
        break;
      case 3:
        type = RequestType.audio;
        break;
      case 4:
        type = RequestType.video | RequestType.audio;
        break;
      case 5:
        type = RequestType.image | RequestType.video;
        break;
    }
    var flag = await LyPermission.storage(type: type);
    ToastUtil.showToast(flag ? "权限通过" : "权限拒绝");
  }
}
