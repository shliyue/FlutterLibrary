import 'package:flutter/material.dart';
import 'package:zyocore/zyocore.dart';

import 'logic.dart';

class PermissionsPage extends GetView<PermissionsLogic> {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: LYAppBar.getAppBar(title: '权限'),
      body: _getContent(),
    );
  }

  ///内容
  Widget _getContent() {
    return Wrap(
      children: [
        _buildBtn("获取存储权限", 0),
        _buildBtn("获取相册权限", 1),
        _buildBtn("获取视频权限", 2),
        _buildBtn("获取音频权限", 3),
        _buildBtn("获取音视频权限", 4),
        _buildBtn("获取相册视屏权限", 5),
      ],
    );
  }

  Widget _buildBtn(String text, int id) {
    return LyButton(
      margin: EdgeInsets.only(bottom: 10.w),
      height: 44.w,
      bgColor: Colors.blue,
      radius: 50.w,
      btnTitle: text,
      textColor: Colors.white,
      onTap: () {
        controller.onBtnClick(id);
      },
    );
  }
}
