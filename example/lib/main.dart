import 'dart:async';
import 'dart:io';
import 'package:example/pages/index.dart';
import 'package:example/router.dart';
import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:zyocore/core/configs/global_config.dart';
import 'package:zyocore/values/style.dart';
import 'package:zyocore/zyocore.dart';

Future<void> main() async {
  WidgetsFlutterBinding.ensureInitialized();
  lySp.init();
  initAppVersion();
  await DeviceUtil.init();
  runApp(const MyApp());
}

initAppVersion() async {
  if (Platform.isAndroid) {
    var androidInfo = await DeviceInfoPlugin().androidInfo;
    Logger.d('android version =========== ${androidInfo.version.sdkInt}');
    GlobalConfigZyoCore.androidVersion = androidInfo.version.sdkInt;
  }
}

class MyApp extends StatelessWidget {
  const MyApp({Key? key}) : super(key: key);

  // This widget is the root of your application.
  @override
  Widget build(BuildContext context) {
    return RefreshConfiguration(
      headerBuilder: () => const WaterDropMaterialHeader(),
      // 配置默认头部指示器,假如你每个页面的头部指示器都一样的话,你需要设置这个
      footerBuilder: () => const ClassicFooter(),
      // 配置默认底部指示器
      headerTriggerDistance: 80.0,
      // 头部触发刷新的越界距离
      springDescription: const SpringDescription(stiffness: 170, damping: 16, mass: 1.9),
      // 自定义回弹动画,三个属性值意义请查询flutter api
      maxOverScrollExtent: 100,
      //头部最大可以拖动的范围,如果发生冲出视图范围区域,请设置这个属性
      maxUnderScrollExtent: 0,
      // 底部最大可以拖动的范围
      enableScrollWhenRefreshCompleted: true,
      //这个属性不兼容PageView和TabBarView,如果你特别需要TabBarView左右滑动,你需要把它设置为true
      enableLoadingWhenFailed: true,
      //在加载失败的状态下,用户仍然可以通过手势上拉来触发加载更多
      hideFooterWhenNotFull: false,
      // Viewport不满一屏时,禁用上拉加载更多功能
      enableBallisticLoad: true,
      // 可以通过惯性滑动触发加载更多
      child: ScreenUtilInit(
        designSize: const Size(375, 812),
        builder: (context, child) {
          return GetMaterialApp(
            //右上角debug显示
            debugShowCheckedModeBanner: false,
            getPages: RouterPageConfig.pages,
            initialBinding: IndexBinding(),
            defaultTransition: Transition.cupertino,
            home: MaterialApp(
              title: 'Flutter Demo',
              theme: ThemeData(
                fontFamily: AppFontFamily.pingFangRegular,
                primaryColor: const Color(0xFFF1F1F1),
                highlightColor: const Color(0xFFF1F1F1),
              ),
              home: child,
            ),
            builder: ToastUtil.init(builder: (BuildContext context, Widget? child) {
              return GestureDetector(
                onTap: () {
                  ///点击空白位置移除焦点
                  SystemChannels.textInput.invokeMethod('TextInput.hide');
                },
                // child: child ?? Container(),
                child: MediaQuery(
                  data: MediaQuery.of(context).copyWith(textScaleFactor: 1.0),
                  child: child ?? Container(),
                ),
              );
            }),
          );
        },
        child: IndexPage(),
      ),
    );
  }
}

class MyHomePage extends StatefulWidget {
  MyHomePage({Key? key, required this.title}) : super(key: key);

  final String title;

  @override
  State<MyHomePage> createState() => _MyHomePageState();
}

class _MyHomePageState extends State<MyHomePage> {
  int _counter = 0;

  AudioBarWaveController controller = AudioBarWaveController();

  TabController? tabController;

  @override
  void initState() {
    // TODO: implement initState
    super.initState();

    // controller.values.clear();
    // for (var i = 0; i < 50; i++) {
    //   controller.values.add(Random().nextDouble() * 100);
    // }
  }

  void _incrementCounter() {
    setState(() {
      _counter++;

      Timer.periodic(const Duration(milliseconds: 100), (timer) {
        //callback function
        // controller.addAndStep(Random().nextDouble() * 100);
        controller.index++;
      });
    });
  }

  @override
  Widget build(BuildContext context) {
    return ScreenUtilInit(
      designSize: const Size(375, 812),
      builder: (BuildContext context, Widget? child) {
        return GetMaterialApp(
          //右上角debug显示
          getPages: RouterPageConfig.pages,
          debugShowCheckedModeBanner: false,
          initialBinding: IndexBinding(),
          defaultTransition: Transition.cupertino,
          home: MaterialApp(
            title: 'Flutter Demo',
            theme: ThemeData(
              fontFamily: AppFontFamily.pingFangRegular,
              primaryColor: const Color(0xFFF1F1F1),
              highlightColor: const Color(0xFFF1F1F1),
            ),
            home: child,
          ),
          builder: ToastUtil.init(builder: (BuildContext context, Widget? child) {
            return GestureDetector(
              onTap: () {},
              child: MediaQuery(
                data: MediaQuery.of(context).copyWith(textScaleFactor: 1.0),
                child: child ?? Container(),
              ),
            );
          }),
        );
      },
      child: IndexPage(),
    );
  }

  Widget _buildW() {
    return Column(
      mainAxisAlignment: MainAxisAlignment.center,
      children: <Widget>[
        const Text(
          'You have pushed the button this many times:',
        ),
        Text(
          '$_counter',
          style: Theme.of(context).textTheme.headline4,
        ),
        SizedBox(
          height: 100,
          child: AudioBarWave(
            controller: controller,
          ),
        )
      ],
    );
  }
}
