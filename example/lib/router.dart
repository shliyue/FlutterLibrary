import 'package:example/pages/action_sheet/binding.dart';
import 'package:example/pages/action_sheet/view.dart';
import 'package:example/pages/button_page/binding.dart';
import 'package:example/pages/button_page/view.dart';
import 'package:example/pages/dialog/binding.dart';
import 'package:example/pages/dialog/view.dart';
import 'package:example/pages/image/binding.dart';
import 'package:example/pages/image/view.dart';
import 'package:example/pages/index.dart';
import 'package:example/pages/native/binding.dart';
import 'package:example/pages/native/view.dart';
import 'package:example/pages/permissions/binding.dart';
import 'package:example/pages/permissions/view.dart';
import 'package:example/pages/picker/binding.dart';
import 'package:example/pages/picker/view.dart';
import 'package:example/pages/textfeild/binding.dart';
import 'package:example/pages/textfeild/view.dart';
import 'package:example/pages/upload/binding.dart';
import 'package:example/pages/upload/view.dart';
import 'package:example/pages/video/binding.dart';
import 'package:example/pages/video/view.dart';
import 'package:zyocore/zyocore.dart';

class RouterPageConfig {
  static const String moduleName = 'flutterLibrary';
  static const String indexPage = '/$moduleName/index';
  static const String dialogPage = '/$moduleName/dialog';
  static const String buttonPage = '/$moduleName/button_page';
  static const String actionSheet = '/$moduleName/action_sheet';
  static const String pickerPage = '/$moduleName/picker';
  static const String imagePage = '/$moduleName/image';
  static const String textfeildPage = '/$moduleName/textfeild';
  static const String nativePage = '/$moduleName/native_page';
  static const String permissionPage = '/$moduleName/permission_page';
  static const String videoPage = '/$moduleName/video_page';
  static const String uploadPage = '/$moduleName/upload_page';

  static final List<GetPage> pages = [
    GetPage(
      name: indexPage,
      title: '主界面',
      page: () => IndexPage(),
      binding: IndexBinding(),
    ),
    GetPage(
      name: dialogPage,
      title: '编辑器',
      page: () => DialogPage(),
      binding: DialogBinding(),
    ),
    GetPage(
      name: buttonPage,
      title: '按钮',
      page: () => ButtonPagePage(),
      binding: ButtonPageBinding(),
    ),
    GetPage(
      name: actionSheet,
      title: 'ActionSheet',
      page: () => ActionSheetPage(),
      binding: ActionSheetBinding(),
    ),
    GetPage(
      name: pickerPage,
      title: 'Picker',
      page: () => PickerPage(),
      binding: PickerBinding(),
    ),
    GetPage(
      name: imagePage,
      title: 'Image',
      page: () => ImagePage(),
      binding: ImageBinding(),
    ),
    GetPage(
      name: textfeildPage,
      title: 'Textfeild',
      page: () => TextfeildPage(),
      binding: TextfeildBinding(),
    ),
    GetPage(
      name: nativePage,
      title: '调用原生方法',
      page: () => NativePage(),
      binding: NativeBinding(),
    ),
    GetPage(
      name: permissionPage,
      title: '权限',
      page: () => PermissionsPage(),
      binding: PermissionsBinding(),
    ),
    GetPage(
      name: videoPage,
      title: '视频',
      page: () => VideoPage(),
      binding: VideoBinding(),
    ),
    GetPage(
      name: uploadPage,
      title: '上传文件',
      page: () => UploadPage(),
      binding: UploadBinding(),
    ),
  ];
}
