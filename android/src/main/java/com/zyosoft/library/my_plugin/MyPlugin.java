package com.zyosoft.library.my_plugin;

import android.content.Context;
import android.util.DisplayMetrics;
import android.util.Log;
import android.view.Display;
import android.view.WindowManager;

import androidx.annotation.NonNull;

import io.flutter.embedding.engine.plugins.FlutterPlugin;
import io.flutter.plugin.common.MethodCall;
import io.flutter.plugin.common.MethodChannel;
import io.flutter.plugin.common.MethodChannel.MethodCallHandler;
import io.flutter.plugin.common.MethodChannel.Result;

/** MyPlugin */
public class MyPlugin implements FlutterPlugin, MethodCallHandler {
  private final static String TAG = "MyPluginLibrary";
  private MethodChannel channel;
  private Context _context;

  @Override
  public void onAttachedToEngine(@NonNull FlutterPluginBinding flutterPluginBinding) {
    _context = flutterPluginBinding.getApplicationContext();
    channel = new MethodChannel(flutterPluginBinding.getBinaryMessenger(), "com.zyosoft.my_plugin.method");
    channel.setMethodCallHandler(this);
  }

  @Override
  public void onMethodCall(@NonNull MethodCall call, @NonNull Result result) {
    if (call.method.equals("getPlatformVersion")) {
      result.success("Android " + android.os.Build.VERSION.RELEASE);
    }else if(call.method.equals("checkIsPad")){
      boolean rs = isPad(_context);
      result.success(rs);
    } else {
      result.notImplemented();
    }
  }

  @Override
  public void onDetachedFromEngine(@NonNull FlutterPluginBinding binding) {
    channel.setMethodCallHandler(null);
  }

  public static boolean isPad(Context context) {
    boolean result = false;
    WindowManager wm = (WindowManager) context.getSystemService(Context.WINDOW_SERVICE);
    Display display = wm.getDefaultDisplay();
    DisplayMetrics dm = new DisplayMetrics();
    display.getMetrics(dm);
    Log.d(TAG, "widthPixels:" + dm.widthPixels);
    Log.d(TAG, "heightPixels:" + dm.heightPixels);
    Log.d(TAG, "xdpi:" + dm.xdpi);
    Log.d(TAG, "ydpi:" + dm.ydpi);
    double x = Math.pow(dm.widthPixels / dm.xdpi, 2);
    double y = Math.pow(dm.heightPixels / dm.ydpi, 2);
    // 屏幕尺寸
    double screenInches = Math.sqrt(x + y);
    // 大于7尺寸则为Pad
    if (screenInches >= 7.0) {
      result = true;
    }
    Log.d(TAG, "screenInches:" + screenInches);
    Log.d(TAG, "isPad:" + result);
    return result;
  }

//  public static boolean isPad() {
//    boolean result = false;
//    String mDeviceType = SysProp.get("ro.build.characteristics", "default");
//    if (mDeviceType != null && mDeviceType.equalsIgnoreCase("tablet")) {
//      result = true;
//    }
//    Log.d(TAG, "isPad:" + result);
//    return result;
//  }

//  public static boolean isTablet(Context context) {
//    return (context.getResources().getConfiguration().screenLayout & Configuration.SCREENLAYOUT_SIZE_MASK) >= Configuration.SCREENLAYOUT_SIZE_LARGE;
//  }
}
